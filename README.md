# Ganga Tattoo Panel

## Setup Passport
1. Require the package using composer:
    ```
    composer require laravel/passport
    ```
1. Launch migration:
    ```
    php artisan migrate
    ```
1. Install:
    ```
    php artisan passport:install
    ```
1. More steps on: [https://laravel.com/docs/5.6/passport#installation]()
1. Deploying Passport
    ```
    php artisan passport:keys
    ```

## Setup Laravel-AdminLTE
1. Require the package using composer:
    ```
    composer require jeroennoten/laravel-adminlte
    ```
1. Publish the public assets:
    ```
    php artisan vendor:publish --provider="JeroenNoten\LaravelAdminLte\ServiceProvider" --tag=assets
    ```
1. Replaces the authentication views with AdminLTE style views:
    ```
    php artisan make:adminlte
    ```
1. Publish the configuration file:
   ```
   php artisan vendor:publish --provider="JeroenNoten\LaravelAdminLte\ServiceProvider" --tag=config
   ```
1. Publish translations:
    ```
    php artisan vendor:publish --provider="JeroenNoten\LaravelAdminLte\ServiceProvider" --tag=translations
    ```
1. Customize views:
    ```
    php artisan vendor:publish --provider="JeroenNoten\LaravelAdminLte\ServiceProvider" --tag=views
    ```
1. More info on: [https://github.com/jeroennoten/Laravel-AdminLTE]()

## Setup Laravel Debug Bar
1. Require the package using composer:
    ```
    composer require barryvdh/laravel-debugbar --dev
    ```
1. To use the facade to log messages, add this to your facades in app.php:
    ```
    'Debugbar' => Barryvdh\Debugbar\Facade::class,
    ```
1. Copy the package config to your local config with the publish command:
   ```
   php artisan vendor:publish --provider="Barryvdh\Debugbar\ServiceProvider"
   ```
1. More info on: [https://github.com/barryvdh/laravel-debugbar]()

## Laravel Permissions
1. Require the package using composer:
    ```
    composer require spatie/laravel-permission
    ```
1. Publish the config file:
    ```
    php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider" --tag="config"
    ```
1. Publish and launch the migration:
    ```
    php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider" --tag="migrations"
    php artisan migrate
    ```
1. Add middleware inside your app/Http/Kernel.php file:
    ```
    protected $routeMiddleware = [
        // ...
        'role' => \Spatie\Permission\Middlewares\RoleMiddleware::class,
        'permission' => \Spatie\Permission\Middlewares\PermissionMiddleware::class,
    ];
    ```
1. Learn how to use on [https://github.com/spatie/laravel-permission#usage]()
1. To get UI check out [https://scotch.io/tutorials/user-authorization-in-laravel-54-with-spatie-laravel-permission]()

## Install useful Laravel collection macros
1. You can pull in the package via composer:
    ```
    composer require spatie/laravel-collection-macros
    ```
1. More info on: [https://github.com/spatie/laravel-collection-macros]()

## Laravel html generation
1. Install the package via composer:
    ```
    composer require spatie/laravel-html 
    ```
1. Register an alias for the facade.
   ```
   // config/app.php
   'aliases' => [
       // ...
       'Html' => Spatie\Html\Facades\Html::class,
   ];
   ```
1. Full documentarion on: [https://docs.spatie.be/laravel-html/v2/introduction]()
1. More info on: [https://github.com/spatie/laravel-html]()

# Runs artisan command in web application:
1. Install the package via composer:
    ```
    composer require recca0120/terminal
    ```
1. Publish config and resource files:
    ```
    php artisan vendor:publish --provider="Recca0120\Terminal\TerminalServiceProvider"
    ```
1. More info on: [https://github.com/recca0120/laravel-terminal]()

## Associate files with Eloquent models
1. You can install this package via composer using this command:
    ```
    composer require spatie/laravel-medialibrary:^7.0.0
    ```
1. Publish the migration and migrate:
    ```
    php artisan vendor:publish --provider="Spatie\MediaLibrary\MediaLibraryServiceProvider" --tag="migrations"
    php artisan migrate
    ``` 
1. Publish the config-file with:
    ```
    php artisan vendor:publish --provider="Spatie\MediaLibrary\MediaLibraryServiceProvider" --tag="config"
    ```
1. Learn hoy to use on: [https://docs.spatie.be/laravel-medialibrary/v7/introduction]()
1. More info on: [https://github.com/spatie/laravel-medialibrary]()

## Add tags and taggable behaviour to a Laravel app
1. You can install the package via composer:
    ```
    composer require spatie/laravel-tags
    ```
1. Publish the migration and migrate:
    ```
    php artisan vendor:publish --provider="Spatie\Tags\TagsServiceProvider" --tag="migrations"
    php artisan migrate
    ```
1. Publish config file:
    ```
    php artisan vendor:publish --provider="Spatie\Tags\TagsServiceProvider" --tag="config"
    ```
1. Learn how to use on [https://docs.spatie.be/laravel-tags/v2/introduction]()
1. More info on: [https://github.com/spatie/laravel-tags]()

## Laravel Comment
1. Install the package using composer:
    ```
    composer require actuallymab/laravel-comment
    ```
1. Publish & Migrate comments table:
    ```
    php artisan vendor:publish --provider="Actuallymab\LaravelComment\LaravelCommentServiceProvider"
    php artisan migrate
    ```
1. More info on: [https://github.com/actuallymab/laravel-comment]()

## Sortable behaviour for Eloquent models
1. This package can be installed through Composer:
    ```
    composer require spatie/eloquent-sortable
    ```
## jQuery DataTables API for Laravel 4|5
1. Install the package using composer:
    ```
    composer require yajra/laravel-datatables-oracle:"~8.0"
    ```
1. Register facade on your config/app.php file:
    ```
    'aliases' => [
        ///...,
        'DataTables' => Yajra\DataTables\Facades\DataTables::class,
    ]
    ```
1. Publish configuration:
    ```
    php artisan vendor:publish --provider=Yajra\DataTables\DataTablesServiceProvider
    ```
    
## Easy Flash Messages for Your Laravel App
1. Begin by pulling in the package through Composer:
    ```
    composer require laracasts/flash
    ```
1. If you need to modify the flash message partials, you can run:
    ```
    php artisan vendor:publish --provider="Laracasts\Flash\FlashServiceProvider"
    ```
1. More info on: [https://github.com/laracasts/flash]()

## Intervention Image
1. To install the most recent version, run the following command:
    ```
    composer require intervention/image
    ```
1. Add the facade of this package to the app $aliases array:
    ```
    'Image' => Intervention\Image\Facades\Image::class
    ```
1. Publish configuration:
    ```
    php artisan vendor:publish --provider="Intervention\Image\ImageServiceProviderLaravel5"
    ```
1. More info on: [https://github.com/Intervention/image]()

## Seed database
1. To seed database run command:
    ```
    php artisan db:seed
    ```

## Add Libraries
1. https://github.com/spatie/eloquent-sortable
1. https://github.com/spatie/laravel-query-builder
1. https://github.com/spatie/geocoder
1. https://github.com/spatie/laravel-db-snapshots
1. https://github.com/spatie/laravel-cookie-consent
