@extends('adminlte::page')

@section('title', 'Características')

@section('content_header')
    <h1>
        Características
        <small>Listado de características</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Características</li>
    </ol>
@stop

@section('content')
    <div class="nav nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="list-inline-item active">
                <a><i class="fa fa-tags"></i>
                    Características</a>
            </li>
        </ul>
        <!-- /.box-header -->
        <div class="box-body table-fix-height">
            <div class="box-body table-fix-height">
                {{ html()->form('GET', route('features.index'))->class('form-horizontal')->open() }}
                <div class="row">
                    <div class="col-sm-2" data-toggle="tooltip" data-placement="top" title="Filtrar por características">
                        {{ html()->select('feature_id', $features, request('feature_id'))->class('form-control select2') }}
                    </div>
                    <div class="col-sm-2">
                        <div class="btn-group btn-block">
                            {{ html()->submit('<i class="fa fa-fw fa-filter"></i> Filtrar')->class('btn btn-primary') }}
                            {{ html()->reset('<i class="fa fa-fw fa-trash"></i>')->class('btn btn-primary') }}
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <a href="{!! route('features.new') !!}" class="btn btn-xs btn-success pull-right">Añadir Características</a>
                    </div>
                </div>
                    {{ html()->form()->close() }}
                </div>
                <table class="table table-bordered table-hover table-striped" id="features-table" style="width:100%">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Características</th>
                        <th>Acción</th>
                    </tr>
                    </thead>
                </table>
        </div>
        <!-- /.box-body -->
    </div>
@stop

@push('scripts')
    <script>

        $("#feature_id").select2({});

        var drawCallbackHandler = function(api) {
            removeBehavior();
        };

        var initDataTableComplete = function(api) {
            //removeBehavior();
        };


        $(document).ready(function() {

            $('#features-table').DataTable({
                dom                 : 'Bfrtip',
                buttons             : [
                    'copy', 'excel', 'pdf', 'print'
                ],
                processing          : true,
                serverSide          : true,
                fixedHeader         : true,
                scrollX             : true,
                scrollCollapse      : true,
                // rowReorder          : true,
                stateSave           : true,
                stateDuration       : -1,
                language            : {
                    url     : 'https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json',
                    buttons : {
                        'copy'  : 'Copiar',
                        'Excel' : 'Excel',
                        'PDF'   : 'PDF',
                        'print' : 'Imprimir'
                    }
                },
                order               : [[ 0, "asc" ]],
                fixedColumns        : {
                    leftColumns: 1,
                    rightColumns: 1
                },
                columnDefs          : [
                    { targets: 0, width: 'calc(50% - 20)' },
                    { targets: 1, width: 'calc(50% - 20)' },
                    { targets: 2, width: '40px', className: 'dt-center' }
                ],
                columns             : [
                    { data : 'name', name : 'name'  },
                    { data : 'feature', name : 'feature'},
                    { data : 'action', name : 'action', orderable : false }
                ],
                ajax                : {
                    url : '{!! route('features.datatable') !!}',
                    data : function (d) {

                        d.feature_id     = '{{ request('feature_id', null) }}';
                    }
                },
                drawCallback        : function() {
                    drawCallbackHandler(this.api());
                },
                initComplete        : function() {
                    initDataTableComplete(this.api());
                }

            });
        });
    </script>
@endpush