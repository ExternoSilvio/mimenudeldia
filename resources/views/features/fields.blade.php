<div class="row">
    <div class="col-xs-12 col-sm-4">
        <div class="form-group">
            {{ html()->label('Nombre', 'name')->class('col-sm-4 control-label') }}
            {{ html()->div()->class('col-sm-8')->open() }}
            {{ html()->text('name')->class('form-control')->autofocus()->required()->attribute('aria-describedby', 'helpName') }}
            {{ html()->span()->id('helpName')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-4">
        <div class="form-group">
            {{ html()->label('Característica (opcional)', 'feature_id')->class('col-sm-4 control-label') }}
            {{ html()->div()->class('col-sm-8')->open() }}
            {{ html()->select('feature_id', $features)->class('form-control')->required()->attribute('aria-describedby', 'helpFeature') }}
            {{ html()->span()->id('helpFeature')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>
