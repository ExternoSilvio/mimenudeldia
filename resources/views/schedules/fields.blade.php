<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {{ html()->label('Dia', 'day')->class('col-sm-1 control-label') }}
            {{ html()->div()->class('col-sm-3')->open() }}
            {{ html()->select('day',$weekdays)->class('form-control')->attribute('aria-describedby', 'helpDay') }}
            {{ html()->span()->id('helpDay')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {{ html()->label('Empieza', 'start_at')->class('col-sm-1 control-label') }}
            {{ html()->div()->class('col-sm-3')->open() }}
            {{ html()->input('time','start_at')->class('form-control')->attribute('aria-describedby', 'helpStart_at') }}
            {{ html()->span()->id('helpStart_at')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {{ html()->label('Termina', 'finish_at')->class('col-sm-1 control-label') }}
            {{ html()->div()->class('col-sm-3')->open() }}
            {{ html()->input('time','finish_at')->class('form-control')->attribute('aria-describedby', 'helpFinish_at') }}
            {{ html()->span()->id('helpFinish_at')->class('help-block') }}
            {{ html()->div()->close() }}
            {{ html()->hidden('hidden_id',$entity_id) }}
        </div>
    </div>
</div>


