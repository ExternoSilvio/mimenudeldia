{{---{{ html()->modelForm($object, 'POST', route($route.'.createSchedule' ))->class('form-horizontal')->acceptsFiles()->open() }}---}}
<div class="box-body">
    <div class="row">
        <div class="col-sm-6">
            <h4>Crear horario</h4>
            @include($folder)
        </div>
        <div class="col-sm-6">
            <h4>Horarios</h4>
                <div class="list-group">
                    @foreach ($schedules as $schedule)
                        <a href="#" class="list-group-item">
                                <input type="checkbox" name="schedules[]" value="{{ $schedule->id }}" style="position:absolute;right: 3px"><small style="position:absolute;right: 20px;color:red;">Borrar</small>
                            <div class="row">
                                <div class="col-sm-4">
                                    {{ $weekdays[$schedule->day] }}
                                </div>
                                <div class="col-sm-4">
                                    Desde:&nbsp;{{ $schedule->start_at }}
                                </div>
                                <div class="col-sm-4">
                                    Hasta:&nbsp;{{$schedule->finish_at}}
                                </div>
                            </div>
                        </a>
                    @endforeach
                </div>
        </div>
    </div>
    {{---<div class="box-footer">
        @if(isset($companyId))
            {{ html()->a(route('companies.'.$route.'.index',$companyId), '<i class="fa fa-fw fa-chevron-left"></i> Volver')->class('btn btn-primary') }}
        @else
            {{ html()->a(route($route.'.index'), '<i class="fa fa-fw fa-chevron-left"></i> Volver')->class('btn btn-primary') }}
        @endif
        {{ html()->div()->class('btn-group pull-right')->open() }}
        {{ html()->submit('<i class="fa fa-fw fa-check"></i> Guardar')->class('btn btn-success') }}
        {{ html()->div()->close() }}
    </div>--}}
</div>
{{---{{ html()->form()->close() }}---}}