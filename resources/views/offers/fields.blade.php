    <div class="row">
    <div class="col-sm-7">
        <div class="form-group">
            {{ html()->label('Empieza', 'start_at')->class('col-sm-3 control-label') }}
            {{ html()->div()->class('col-sm-8')->open() }}
            {{ html()->input('datetime-local','start_at')->class('form-control')->attribute('aria-describedby', 'helpStart_at') }}
            {{ html()->span()->id('helpStart_at')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-7">
        <div class="form-group">
            {{ html()->label('Termina', 'finish_at')->class('col-sm-3 control-label') }}
            {{ html()->div()->class('col-sm-8')->open() }}
            {{ html()->input('datetime-local','finish_at')->class('form-control')->attribute('aria-describedby', 'helpFinish_at') }}
            {{ html()->span()->id('helpFinish_at')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-7">
        <div class="form-group">
            {{ html()->label('Tipo', 'type')->class('col-sm-3 control-label') }}
            {{ html()->div()->class('col-sm-8')->open() }}
            {{ html()->select('type',['price' => 'precio','discount' => 'descuento'])->class('form-control')->attribute('aria-describedby', 'helpType') }}
            {{ html()->span()->id('helpTyoe')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-7">
        <div class="form-group">
            {{ html()->label('Precio/descuento', 'type_value')->class('col-sm-3 control-label') }}
            {{ html()->div()->class('col-sm-8')->open() }}
            {{ html()->input('number','type_value')->class('form-control')->attribute('aria-describedby', 'helpType_value') }}
            {{ html()->span()->id('helpType_value')->class('help-block') }}
            {{ html()->div()->close() }}
            {{html()->hidden("hidden_id",$entity_id)}}
        </div>
    </div>
</div>

