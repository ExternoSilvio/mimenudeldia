{{--@if(strpos($route,"companies"))
    {{ html()->modelForm($object, 'POST', route($route.'.createOffer',$companyId))->class('form-horizontal')->acceptsFiles()->open() }}
@else
    {{ html()->modelForm($object, 'POST', route($route.'.createOffer'))->class('form-horizontal')->acceptsFiles()->open() }}
@endif--}}
<div class="box-body">
    <div class="row">
        <div class="col-sm-6">
            <h4>Crear oferta</h4>
            @include($folder)
        </div>
        <div class="col-sm-6">
            <h4>Ofertas</h4>
                <div class="list-group">
                    @foreach ($offers as $offer)
                        <a href="#" class="list-group-item @if($carbon->now() > $offer->finish_at) disabled @endif" >
                            <input type="checkbox" name="offers[]" value="{{ $offer->id }}" style="position:absolute;right: 3px" ><small style="position:absolute;right: 20px;color:red;">Borrar</small>
                                &nbsp;Desde:&nbsp;{{ $offer->start_at }}
                                Hasta:&nbsp;{{$offer->finish_at}}
                                Tipo:&nbsp{{$offer->price !== null ? "price" : "discount"}}
                        </a>
                    @endforeach
                </div>
        </div>
    </div>
    {{---<div class="box-footer">
        @if(isset($companyId))
            {{ html()->a(route('companies.'.$route.'.index',$companyId), '<i class="fa fa-fw fa-chevron-left"></i> Volver')->class('btn btn-primary') }}
        @else
            {{ html()->a(route($route.'.index'), '<i class="fa fa-fw fa-chevron-left"></i> Volver')->class('btn btn-primary') }}
        @endif
        {{ html()->div()->class('btn-group pull-right')->open() }}
        {{ html()->submit('<i class="fa fa-fw fa-check"></i> Guardar')->class('btn btn-success') }}
        {{ html()->div()->close() }}
    </div>--}}
</div>
{{--{{ html()->form()->close() }}--}}