@extends('adminlte::page')

@section('title', 'Sedes - Editar')

@push('css')

@endpush

@section('content_header')
<h1>
    Sedes
    <small>Editar Sede</small>
</h1>
<ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li><a href="{{route('subsidiaries.index')}}"><i class="fa fa-map-signs"></i> Sedes</a></li>
    <li class="active">Editar</li>
</ol>
@stop

@section('content')
<div class="nav nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="list-inline-item active">
            <a><i class="fa fa-map-signs"></i>
                Sede</a>
        </li>
    </ul>
    <!-- /.box-header -->
    {{ html()->modelForm($subsidiary, 'PUT', route('subsidiaries.update',$subsidiary->id ))->class('form-horizontal')->acceptsFiles()->open() }}
    <div class="box-body">
        <div>
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#fields" aria-controls="fields" role="tab" data-toggle="tab"><i class="fa fa-edit"></i> Datos básicos</a></li>
                <li role="presentation"><a href="#schedules" aria-controls="schedules" role="tab" data-toggle="tab"><i class="fa fa-clock-o"></i> Horarios</a></li>
                <li role="presentation"><a href="#opinions" aria-controls="opinions" role="tab" data-toggle="tab"><i class="fa fa-comments"></i> Comentarios</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="fields">
                    <br>
                    @include('subsidiaries.fields')
                </div>
                <div role="tabpanel" class="tab-pane" id="schedules">
                    <br>
                    <!--schedules form-->
                    @include('schedules.edit')
                </div>
                <div role="tabpanel" class="tab-pane" id="opinions">
                    <br>
                    <!--opinions form-->
                    @include('opinions.edit')
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer">

        {{ html()->div()->class('btn-group pull-right')->open() }}

        {{ html()->a(route('subsidiaries.delete', $subsidiary->id), '<i class="fa fa-fw fa-trash"></i> Eliminar')->class('btn btn-danger')->data('remove', '¿Está seguro de que quiere borrar esta localidad?') }}

        {{ html()->submit('<i class="fa fa-fw fa-check"></i> Guardar')->class('btn btn-success') }}
        {{ html()->div()->close() }}
    </div>
    {{ html()->form()->close() }}
</div>

@stop

@push('scripts')

@endpush