<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {{ html()->label('Dirección', 'address')->class('col-sm-2 control-label') }}
            {{ html()->div()->class('col-sm-6')->open() }}
            {{ html()->text('address')->class('form-control')->autofocus()->required()->attribute('aria-describedby', 'helpAddress') }}
            {{ html()->span()->id('helpAddress')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {{ html()->label('Coordenadas', 'coordinates')->class('col-sm-2 control-label') }}
            {{ html()->div()->class('col-sm-6')->open() }}
            {{ html()->text('coordinates')->class('form-control')->required()->attribute('size', 5)->attribute('aria-describedby', 'helpCoordinates') }}
            {{ html()->span()->id('helpCoordinates')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {{ html()->label('Teléfono', 'phone')->class('col-sm-2 control-label') }}
            {{ html()->div()->class('col-sm-6')->open() }}
            {{ html()->text('phone')->class('form-control')->required()->attribute('size', 5)->attribute('aria-describedby', 'helpPhone') }}
            {{ html()->span()->id('helpPhone')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {{ html()->label('Email', 'email')->class('col-sm-2 control-label') }}
            {{ html()->div()->class('col-sm-6')->open() }}
            {{ html()->text('email')->class('form-control')->required()->attribute('size', 5)->attribute('aria-describedby', 'helpEmail') }}
            {{ html()->span()->id('helpEmail')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {{ html()->label('Localización (obligatorio)', 'location_id')->class('col-sm-2 control-label') }}
            {{ html()->div()->class('col-sm-6')->open() }}
            {{ html()->select('location_id', $locations)->class('form-control')->required()->attribute('aria-describedby', 'helpLocation') }}
            {{ html()->span()->id('helpLocation')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {{ html()->label('Compañia (obligatorio)', 'company_id')->class('col-sm-2 control-label') }}
            {{ html()->div()->class('col-sm-6')->open() }}
            {{ html()->select('company_id', $companies)->class('form-control')->required()->attribute('aria-describedby', 'helpCompany') }}
            {{ html()->span()->id('helpCompany')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>
