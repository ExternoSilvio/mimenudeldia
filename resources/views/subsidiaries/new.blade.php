@extends('adminlte::page')

@section('title', 'Sedes - Nuevo')

@push('css')

@endpush

@section('content_header')
    <h1>
        Sedes
        <small>Nueva Sede</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="/subsidiaries"><i class="fa fa-subsidiaries"></i> Sede</a></li>
        <li class="active">Nuevo</li>
    </ol>
@stop

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="nav nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="list-inline-item active">
                <a><i class="fa fa-map-signs"></i>
                    Sede</a>
            </li>
        </ul>
        <!-- /.box-header -->
        {{ html()->modelForm($subsidiary, 'POST', route('subsidiaries.create'))->class('form-horizontal')->open() }}
        <div class="box-body">
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#fields" aria-controls="fields" role="tab" data-toggle="tab"><i class="fa fa-edit"></i> Datos básicos</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="fields">
                        <br>
                        @include('subsidiaries.fields')
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            {{ html()->a(route('subsidiaries.index'), '<i class="fa fa-fw fa-chevron-left"></i> Volver')->class('btn btn-primary') }}
            {{ html()->div()->class('btn-group pull-right')->open() }}
            {{ html()->submit('Crear')->class('btn btn-success') }}
            {{ html()->div()->close() }}
        </div>
    {{ html()->form()->close() }}
    <!-- /.box-body -->
    </div>
@stop

@push('scripts')

@endpush