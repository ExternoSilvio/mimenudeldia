@extends('adminlte::page')

@section('title', 'Compañías - Nuevo')

@push('css')

@endpush

@section('content_header')
    <h1>
        Compañías
        <small>Nueva Compañías</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="/companies"><i class="fa fa-categories"></i> Compañia</a></li>
        <li class="active">Nuevo</li>
    </ol>
@stop

@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <i class="fa fa-companies"></i>
            <h3 class="box-title">Compañías</h3>
        </div>
        <!-- /.box-header -->
        {{ html()->modelForm($company, 'POST', route('companies.create'))->class('form-horizontal')->acceptsFiles()->open() }}
        <div class="box-body">
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#fields" aria-controls="fields" role="tab" data-toggle="tab"><i class="fa fa-edit"></i> Datos básicos</a></li>
                    <li role="presentation"><a href="#features" aria-controls="features" role="tab" data-toggle="tab"><i class="fa fa-fw fa-tags "></i>Características</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="fields">
                        <br>
                        @include('companies.fields')
                    </div>
                    <div role="tabpanel" class="tab-pane" id="features">
                        <div class="list-group">
                            <br>
                            <div class="row">
                                @foreach ($features as $feature)
                                    <div class="col-sm-3">
                                        <a href="javascript:void(0)" class="list-group-item">
                                            <input type="checkbox" name="features[]" value="{{ $feature->id }}"> &nbsp;{{ $feature->name }}
                                        </a>
                                    </div>
                                    @if ($loop->iteration % 4 == 0)
                            </div>
                            <br>
                            <div class="row">
                                @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="box-footer">
            {{ html()->a(route('companies.index'), '<i class="fa fa-fw fa-chevron-left"></i> Volver')->class('btn btn-primary') }}
            {{ html()->div()->class('btn-group pull-right')->open() }}
            {{ html()->submit('Crear')->class('btn btn-success') }}
            {{ html()->div()->close() }}
        </div>
    {{ html()->form()->close() }}
    <!-- /.box-body -->
    </div>
@stop

@push('scripts')

@endpush