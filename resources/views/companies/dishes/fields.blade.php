<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {{ html()->label('Nombre', 'name')->class('col-sm-1 control-label') }}
            {{ html()->div()->class('col-sm-3')->open() }}
            {{ html()->text('name')->class('form-control')->autofocus()->required()->attribute('aria-describedby', 'helpName') }}
            {{ html()->span()->id('helpName')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {{ html()->label('Precio', 'price')->class('col-sm-1 control-label') }}
            {{ html()->div()->class('col-sm-3')->open() }}
            {{ html()->text('price')->class('form-control')->autofocus()->required()->attribute('aria-describedby', 'helpPrice') }}
            {{ html()->span()->id('helpPrice')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {{ html()->label('categoría (opcional)', 'category_id')->class('col-sm-1 control-label') }}
            {{ html()->div()->class('col-sm-3')->open() }}
            {{ html()->select('category_id',$categories)->class('form-control')->attribute('aria-describedby', 'helpCategory_id') }}
            {{ html()->span()->id('helpCategory_id')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {{ html()->label('Foto (opcional)', 'photo')->class('col-sm-1 control-label') }}
            {{ html()->div()->class('col-sm-3')->open() }}
            {{ html()->file('photo')->class('form-control')->attribute('aria-describedby', 'helpPhoto') }}
            {{ html()->span()->id('helpPhoto')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>
@if($photo != null)
    <div class="row">
        <div class="col-sm-12">
            <img src="{{asset('storage/'.str_after($photo->url,'public'))}}" class="img-thumbnail">
        </div>
    </div>
@endif