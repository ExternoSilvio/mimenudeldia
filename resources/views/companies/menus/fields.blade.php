<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {{ html()->label('Nombre', 'name')->class('col-sm-1 control-label') }}
            {{ html()->div()->class('col-sm-3')->open() }}
            {{ html()->text('name')->class('form-control')->autofocus()->required()->attribute('aria-describedby', 'helpName') }}
            {{ html()->span()->id('helpName')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {{ html()->label('Precio', 'price')->class('col-sm-1 control-label') }}
            {{ html()->div()->class('col-sm-3')->open() }}
            {{ html()->text('price')->class('form-control')->required()->attribute('aria-describedby', 'helpPrice') }}
            {{ html()->span()->id('helpPrice')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {{ html()->label('Observaciones', 'observations')->class('col-sm-1 control-label') }}
            {{ html()->div()->class('col-sm-3')->open() }}
            {{ html()->text('observations')->class('form-control')->attribute('aria-describedby', 'helpObservations') }}
            {{ html()->span()->id('helpObservations')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>

</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {{ html()->label('Foto (opcional)', 'photo')->class('col-sm-1 control-label') }}
            {{ html()->div()->class('col-sm-3')->open() }}
            {{ html()->file('photo')->class('form-control')->attribute('aria-describedby', 'helpPhoto') }}
            {{ html()->span()->id('helpPhoto')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>
@if($photo != null)
    <div class="row">
        <div class="col-sm-12">
            <img src="{{asset('storage/'.str_after($photo->url,'public'))}}" width="400px" class="img-thumbnail">
        </div>
    </div>
@endif