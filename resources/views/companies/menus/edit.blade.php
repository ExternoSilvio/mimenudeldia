@extends('adminlte::page')

@section('title', 'Menus - Editar')

@push('css')

@endpush

@section('content_header')
<h1>
    Menus
    <small>Editar Menus</small>
</h1>
<ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li><a href="{{route('companies.menus.index',$menu->company()->get([0 =>'id'])[0] )}}"><i class="fa fa-cutlery"></i> Menus</a></li>
    <li class="active">Editar</li>
</ol>
@stop

@section('content')
    {{ html()->modelForm($menu, 'PUT', route('companies.menus.update', [$companyId, $menu->id]))->class('form-horizontal')->acceptsFiles()->open() }}
    <div class="nav nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="list-inline-item">
                <a href="{{ route('companies.edit',$menu->company()->get([0 =>'id'])[0] )}}"><i class="fa fa-building"></i>
                    Compañia</a>
            </li>
            <li class="list-inline-item">
                <a href="{{route('companies.subsidiaries.index',$menu->company()->get([0 =>'id'])[0] )}}"><i class="fa fa-map-signs"></i>
                    Sede</a>
            </li>
            <li class="list-inline-item active">
                <a href="{{ route('companies.menus.index',$menu->company()->get([0 =>'id'])[0] )}}"><i class="fa fa-cutlery"></i>
                    Menús</a>
            </li>
            <li class="list-inline-item">
                <a href="{{ route('companies.dishes.index',$menu->company()->get([0 =>'id'])[0] )}}"><i class="fa fa-circle-o"></i>
                    Platos</a>
            </li>
        </ul>
    <!-- /.box-header -->
        <div class="box-body">
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#fields" aria-controls="fields" role="tab" data-toggle="tab"><i class="fa fa-edit"></i> Datos básicos</a></li>
                    <li role="presentation"><a href="#dishes" aria-controls="dishes" role="tab" data-toggle="tab"><i class="fa fa-cutlery"></i>Platos</a></li>
                    <li role="presentation"><a href="#offers" aria-controls="offers" role="tab" data-toggle="tab"><i class="fa fa-area-chart"></i>Ofertas</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="fields">
                        <br>
                        @include('companies.menus.fields')
                    </div>
                    <div role="tabpanel" class="tab-pane" id="dishes">
                        <div class="list-group">
                            <br>
                            <div class="row">
                                @foreach ($dishes as $dish)
                                    <div class="col-sm-3">
                                        <a href="javascript:void(0)" class="list-group-item @if ($current_dishes->contains('dish_id', $dish->id)) list-group-item-info @endif">
                                            <input type="checkbox" name="dishes[]" value="{{ $dish->id }}"  @if ($current_dishes->contains('dish_id', $dish->id)) checked="checked" @endif> &nbsp;{{ $dish->name }}
                                        </a>
                                    </div>
                                    @if ($loop->iteration % 4 == 0)
                            </div>
                            <br>
                            <div class="row">
                                @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="offers">
                        <br>
                        <!--edit-offers-block-->
                        @include('offers.edit')
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">

            {{ html()->a(route('companies.menus.index',$companyId), '<i class="fa fa-fw fa-chevron-left"></i> Volver')->class('btn btn-primary') }}
            {{ html()->div()->class('btn-group pull-right')->open() }}

            {{ html()->a(route('companies.menus.delete', [$companyId, $menu->id]), '<i class="fa fa-fw fa-trash"></i> Eliminar')->class('btn btn-danger')->data('remove', '¿Está seguro de que quiere borrar este menu?') }}

            {{ html()->submit('<i class="fa fa-fw fa-check"></i> Guardar')->class('btn btn-success') }}
            {{ html()->div()->close() }}
        </div>
    </div>
    {{ html()->form()->close() }}
@stop

@push('scripts')

@endpush