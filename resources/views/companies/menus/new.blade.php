@extends('adminlte::page')

@section('title', 'Menus - Nuevo')

@push('css')

@endpush

@section('content_header')
    <h1>
        Menus
        <small>Nuevo Menu</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="{{route('companies.menus.index',$companyId)}}"><i class="fa fa-menus"></i> Menus</a></li>
        <li class="active">Nuevo</li>
    </ol>
@stop

@section('content')
    {{ html()->modelForm($menu, 'POST', route('companies.menus.create',$companyId))->class('form-horizontal')->acceptsFiles()->open() }}
    <div class="nav nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="list-inline-item">
                <a href="{{ route('companies.edit',$companyId) }}"><i class="fa fa-building"></i>
                    Compañia</a>
            </li>
            <li class="list-inline-item">
                <a  href="{{ route('companies.subsidiaries.index',$companyId)  }}"><i class="fa fa-map-signs"></i>
                    Sede</a>
            </li>
            <li class="list-inline-item active">
                <a  href="{{ route('companies.menus.index',$companyId) }}"><i class="fa fa-fw fa-cutlery "></i>
                    Menus</a>
            </li>
            <li class="list-inline-item">
                <a  href="{{ route('companies.dishes.index',$companyId) }}"><i class="fa fa-fw fa-circle-thin "></i>
                    Platos</a>
            </li>
        </ul>
        <!-- /.box-header -->
        <div class="box-body">
            <div>
            <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#fields" aria-controls="fields" role="tab" data-toggle="tab"><i class="fa fa-edit"></i> Datos básicos</a></li>
                    <li role="presentation"><a href="#dishes" aria-controls="dishes" role="tab" data-toggle="tab"><i class="fa fa-fw fa-circle-thin "></i>Platos</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="fields">
                        <br>
                        @include('companies.menus.fields')
                    </div>
                    <div role="tabpanel" class="tab-pane" id="dishes">
                        <div class="list-group">
                            <br>
                            <div class="row">
                                @foreach ($dishes as $dish)
                                    <div class="col-sm-3">
                                        <a href="javascript:void(0)" class="list-group-item">
                                            <input type="checkbox" name="dishes[]" value="{{ $dish->id }}"> &nbsp;{{ $dish->name }}
                                        </a>
                                    </div>
                                    @if ($loop->iteration % 4 == 0)
                            </div>
                            <br>
                            <div class="row">
                                @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            {{ html()->a(route('companies.menus.index',$companyId), '<i class="fa fa-fw fa-chevron-left"></i> Volver')->class('btn btn-primary') }}
            {{ html()->div()->class('btn-group pull-right')->open() }}
            {{ html()->submit('Crear')->class('btn btn-success') }}
            {{ html()->div()->close() }}
        </div>
    <!-- /.box-body -->
    </div>
    {{ html()->form()->close() }}
@stop

@push('scripts')

@endpush