<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {{ html()->label('Nombre', 'name')->class('col-sm-1 control-label') }}
            {{ html()->div()->class('col-sm-3')->open() }}
            {{ html()->text('name')->class('form-control')->autofocus()->required()->attribute('aria-describedby', 'helpName') }}
            {{ html()->span()->id('helpName')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {{ html()->label('Identificación', 'identification')->class('col-sm-1 control-label') }}
            {{ html()->div()->class('col-sm-3')->open() }}
            {{ html()->text('identification')->class('form-control')->required()->attribute('aria-describedby', 'helpIdentification') }}
            {{ html()->span()->id('helpIdenification')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {{ html()->label('Web (opcional)', 'web')->class('col-sm-1 control-label') }}
            {{ html()->div()->class('col-sm-3')->open() }}
            {{ html()->text('web')->class('form-control')->attribute('aria-describedby', 'helpWeb') }}
            {{ html()->span()->id('helpWeb')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {{ html()->label('Información (opcional)', 'information')->class('col-sm-1 control-label') }}
            {{ html()->div()->class('col-sm-3')->open() }}
            {{ html()->text('information')->class('form-control')->attribute('aria-describedby', 'helpInformation') }}
            {{ html()->span()->id('helpInformation')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {{ html()->label('Logo (opcional)', 'logo')->class('col-sm-1 control-label') }}
            {{ html()->div()->class('col-sm-3')->open() }}
            {{ html()->file('logo')->class('form-control')->attribute('aria-describedby', 'helpLogo') }}
            {{ html()->span()->id('helpLogo')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>
@if($logo != null)
    <div class="row">
        <div class="col-sm-12">
            <img src="{{asset('storage/'.str_after($logo->url,'public'))}}" class="img-thumbnail">
        </div>
    </div>
@endif
<input type="hidden" name="primary_first">