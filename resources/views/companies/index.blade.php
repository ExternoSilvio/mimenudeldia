@extends('adminlte::page')

@section('title', 'Compañías')

@section('content_header')
    <h1>
        Compañías
        <small>Listado de Compañías</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Compañías</li>
    </ol>
@stop

@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <i class="fa fa-companies"></i>
            <h3 class="box-title">Compañías</h3>
            <a href="{!! route('companies.new') !!}" class="btn btn-xs btn-success pull-right">Añadir Compañía</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-fix-height">
            <div class="box-body table-fix-height">
                {{ html()->form('GET', route('companies.index'))->class('form-horizontal')->open() }}
                <div class="row">
                    <div class="col-sm-2" data-toggle="tooltip" data-placement="top" title="Filtrar por características">
                        {{ html()->select('features', $features, request('features'))->multiple()->class('form-control select2') }}
                    </div>
                    <div class="col-sm-2">
                        <div class="btn-group btn-block">
                            {{ html()->submit('<i class="fa fa-fw fa-filter"></i> Filtrar')->class('btn btn-primary') }}
                            {{ html()->reset('<i class="fa fa-fw fa-trash"></i>')->class('btn btn-primary') }}
                        </div>
                    </div>
                </div>
                {{ html()->form()->close() }}
            </div>
            <table class="table table-bordered table-hover table-striped" id="company-table" style="width:100%">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Identificacíon</th>
                    <th>Web</th>
                    <th>Información</th>
                    <th>Score</th>
                    <th>Logo</th>
                    <th>Acción</th>
                </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@stop

@push('scripts')
    <script>

        $("#features").select2({
            placeholder: 'Todas las características'
        });

        var drawCallbackHandler = function(api) {
            removeBehavior();
        };

        var initDataTableComplete = function(api) {
            //removeBehavior();
        };


        $(document).ready(function() {


            $('#company-table').DataTable({
                dom                 : 'Bfrtip',
                buttons             : [
                    'copy', 'excel', 'pdf', 'print'
                ],
                processing          : true,
                serverSide          : true,
                fixedHeader         : true,
                scrollX             : true,
                scrollCollapse      : true,
                // rowReorder          : true,
                stateSave           : true,
                stateDuration       : -1,
                language            : {
                    url     : 'https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json',
                    buttons : {
                        'copy'  : 'Copiar',
                        'Excel' : 'Excel',
                        'PDF'   : 'PDF',
                        'print' : 'Imprimir'
                    }
                },
                order               : [[ 0, "asc" ]],
                // fixedColumns        : {
                //     leftColumns: 1,
                //     rightColumns: 1
                // },
                columnDefs          : [
                    { targets: 0, width: 'calc( 20% -10 )' },
                    { targets: 1, width: 'calc( 20% -10 )' },
                    { targets: 2, width: 'calc( 20% -10 )' },
                    { targets: 3, width: 'calc( 20%)' },
                    { targets: 4, width: 'calc( 20% -10)' },
                    { targets: 5, width: 'calc( 20%)' },
                    { targets: 6, width: '40px', className: 'dt-center' }
                ],
                columns             : [
                    { data : 'name', name : 'name'  },
                    { data : 'identification', name : 'identification'},
                    { data : 'web', name : 'web'},
                    { data : 'information', name : 'information'},
                    { data : 'score', name : 'score'},
                    { data : 'logo_id', name : 'logo_id'},
                    { data : 'action', name : 'action', orderable : false }
                ],
                ajax                : {
                    url : '{!! route('companies.datatable') !!}',
                    data : function (d) {

                        d.features  = '{{ implode('|', request('features', [])) }}';

                    }
                },
                drawCallback        : function() {
                    drawCallbackHandler(this.api());
                },
                initComplete        : function() {
                    initDataTableComplete(this.api());
                }

            });
        });
    </script>
    @endpush