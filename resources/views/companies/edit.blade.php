@extends('adminlte::page')

@section('title', 'Compañias - Editar')

@push('css')

@endpush

@section('content_header')
    <h1>
        Compañías
        <small>Editar Compañías</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="/companies"><i class="fa fa-building"></i> Compañías</a></li>
        <li class="active">Editar</li>
    </ol>
    @stop

    @section('content')
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    {{ html()->modelForm($company, 'PUT', route('companies.update', $company->id))->class('form-horizontal')->acceptsFiles()->open() }}
    <div class="nav nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="list-inline-item active">
                <a><i class="fa fa-building"></i>
                    Compañia</a>
            </li>
            <li class="list-inline-item">
                <a href="{{route('companies.subsidiaries.index',$company->id)}}"><i class="fa fa-map-signs"></i>
                    Sedes</a>
            </li>
            <li class="list-inline-item">
                <a href="{{route('companies.menus.index',$company->id )}}"><i class="fa fa-cutlery"></i>
                    Menus</a>
            </li>
            <li class="list-inline-item">
                <a href="{{ route('companies.dishes.index',$company->id )}}"><i class="fa fa-circle-o"></i>
                    Platos</a>
            </li>
        </ul>

        <!-- /.box-header -->

        <div class="box-body">
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#fields" aria-controls="fields" role="tab" data-toggle="tab"><i class="fa fa-edit"></i> Datos básicos</a></li>
                    <li role="presentation"><a href="#features" aria-controls="features" role="tab" data-toggle="tab"><i class="fa fa-fw fa-tags "></i>Características</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="fields">
                        <br>
                        @include('companies.fields')
                    </div>
                    <div role="tabpanel" class="tab-pane" id="features">
                        <div class="list-group">
                            <br>
                            <div class="row">
                                @foreach ($features as $feature)
                                    <div class="col-sm-3">
                                        <a href="javascript:void(0)" class="list-group-item @if ($current_features->contains('feature_id', $feature->id)) list-group-item-info @endif">
                                            <input type="checkbox" name="features[]" value="{{ $feature->id }}"  @if ($current_features->contains('feature_id', $feature->id)) checked="checked" @endif> &nbsp;{{ $feature->name }}
                                        </a>
                                    </div>
                                    @if ($loop->iteration % 4 == 0)
                            </div>
                                <br>
                            <div class="row">
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">

            {{ html()->div()->class('btn-group pull-right')->open() }}

            {{ html()->a(route('companies.delete', $company->id), '<i class="fa fa-fw fa-trash"></i> Eliminar')->class('btn btn-danger')->data('remove', '¿Está seguro de que quiere borrar esta compañía?') }}

            {{ html()->submit('<i class="fa fa-fw fa-check"></i> Guardar')->class('btn btn-success') }}
            {{ html()->div()->close() }}
        </div>

    <!-- /.box-body -->
    </div>
    {{ html()->form()->close() }}

@stop

@push('scripts')

@endpush