@extends('adminlte::page')

@section('title', 'Sedes')

@section('content_header')
    <h1>
        Sedes
        <small>Listado de sedes</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Sedes</li>
    </ol>
@stop

@section('content')
    <div class="nav nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="list-inline-item">
                    <a href="{{route('companies.edit',$companyId )}}"><i class="fa fa-building"></i>
                        Compañia</a>
                </li>
                <li class="list-inline-item active">
                    <a><i class="fa fa-map-signs"></i>
                        Sedes</a>
                </li>
                <li class="list-inline-item">
                    <a href="{{route('companies.menus.index',$companyId)}}"><i class="fa fa-cutlery"></i>
                        Menús</a>
                </li>
                <li class="list-inline-item">
                    <a href="{{ route('companies.dishes.index',$companyId )}}"><i class="fa fa-circle-o"></i>
                        Platos</a>
                </li>
            </ul>

        <!-- /.box-header -->
        <div class="box-body table-fix-height">
            <div class="box-body table-fix-height">
                {{ html()->form('GET', route('companies.subsidiaries.index',$companyId))->class('form-horizontal')->open() }}
                <div class="row">
                    <div class="col-sm-3" data-toggle="tooltip" data-placement="top" title="Filtrar por Localidad">
                        {{ html()->select('location_id', $locations, request('location_id'))->class('form-control select2') }}
                    </div>
                    <div class="col-sm-3">
                        <div class="btn-group btn-block">
                            {{ html()->submit('<i class="fa fa-fw fa-filter"></i> Filtrar')->class('btn btn-primary') }}
                            {{ html()->reset('<i class="fa fa-fw fa-trash"></i>')->class('btn btn-primary') }}
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <a href="{!! route('companies.subsidiaries.new',$companyId) !!}" class="btn btn-xs btn-success pull-right">Añadir Sede</a>
                    </div>
                </div>
                {{ html()->form()->close() }}
            </div>
            <table class="table table-bordered table-hover table-striped" id="subsidiaries-table" style="width:100%">
                <thead>
                <tr>
                    <th>Compañia</th>
                    <th>Localización</th>
                    <th>Dirección</th>
                    <th>Coordenadas</th>
                    <th>Teléfono</th>
                    <th>email</th>
                    <th>Acción</th>
                </tr>
                </thead>
            </table>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
@stop

@push('scripts')
    <script>
        $("#location_id").select2({

        });
        var drawCallbackHandler = function(api) {
            removeBehavior();
        };

        var initDataTableComplete = function(api) {
            //removeBehavior();
        };

        $(document).ready(function() {

            $('#subsidiaries-table').DataTable({
                dom                 : 'Bfrtip',
                buttons             : [
                    'copy', 'excel', 'pdf', 'print'
                ],
                processing          : true,
                serverSide          : true,
                fixedHeader         : true,
                scrollX             : true,
                scrollCollapse      : true,
                // rowReorder          : true,
                stateSave           : true,
                stateDuration       : -1,
                language            : {
                    url     : 'https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json',
                    buttons : {
                        'copy'  : 'Copiar',
                        'Excel' : 'Excel',
                        'PDF'   : 'PDF',
                        'print' : 'Imprimir'
                    }
                },
                order               : [[ 0, "asc" ]],
                fixedColumns        : {
                    leftColumns: 1,
                    rightColumns: 1
                },
                columnDefs          : [
                    { targets: 0, width: 'calc(20% -40px)' },
                    { targets: 1, width: '15%' },
                    { targets: 2, width: '15%' },
                    { targets: 3, width: '15%' },
                    { targets: 4, width: '15%' },
                    { targets: 5, width: '20%' },
                    { targets: 6, width: '40px', className: 'dt-center' }
                ],
                columns             : [
                    { data : 'company', name : 'company' },
                    { data : 'location', name : 'location' },
                    { data : 'address', name : 'address'  },
                    { data : 'coordinates', name : 'coordinates'},
                    { data : 'phone', name : 'phone' },
                    { data : 'email', name : 'email' },
                    { data : 'action', name : 'action', orderable : false }
                ],
                ajax                : {
                    url : '{!! route('subsidiaries.datatable') !!}',
                    data : function (d) {

                        d.company_id     =  '{{$companyId}}';
                        d.location_id = '{{ request('location_id', null) }}';
                    }
                },
                drawCallback        : function() {
                    drawCallbackHandler(this.api());
                },
                initComplete        : function() {
                    initDataTableComplete(this.api());
                }
            });
        });
    </script>
@endpush