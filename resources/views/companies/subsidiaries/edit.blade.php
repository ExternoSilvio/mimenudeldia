@extends('adminlte::page')

@section('title', 'Sedes - Editar')

@push('css')

@endpush

@section('content_header')
<h1>
    Sedes
    <small>Editar Sede</small>
</h1>
<ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li><a href="{{route('companies.subsidiaries.index',['company_id' => $subsidiary->company()->get([0 => 'id'])[0]])}}"><i class="fa fa-map-signs"></i> Sedes</a></li>
    <li class="active">Editar</li>
</ol>
@stop

@section('content')
<div class="nav nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="list-inline-item">
            <a href="{{ route('companies.edit',['company_id' => $subsidiary->company()->get([0 => 'id'])[0]]) }}"><i class="fa fa-building"></i>
                Compañia</a>
        </li>
        <li class="list-inline-item active">
            <a  href="{{ route('companies.subsidiaries.edit',['company_id' => $subsidiary->company()->get([0 => 'id'])[0],$subsidiary->id ] )}}"><i class="fa fa-map-signs"></i>
                Sede</a>
        </li>
        <li class="list-inline-item">
            <a  href="{{ route('companies.menus.index',['company_id' => $subsidiary->company()->get([0 => 'id'])[0]]) }}"><i class="fa fa-cutlery"></i>
                Menus</a>
        </li>
        <li class="list-inline-item">
            <a  href="{{ route('companies.dishes.index',['company_id' => $subsidiary->company()->get([0 => 'id'])[0]]) }}"><i class="fa fa-circle-o"></i>
                Platos</a>
        </li>
    </ul>
    <!-- /.box-header -->
    {{ html()->modelForm($subsidiary, 'PUT', route('companies.subsidiaries.update', [$subsidiary->company()->get()[0],$subsidiary->id]))->class('form-horizontal')->acceptsFiles()->open() }}
    <div class="box-body">
        <div>
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#fields" aria-controls="fields" role="tab" data-toggle="tab"><i class="fa fa-edit"></i> Datos básicos</a></li>
                <li role="presentation"><a href="#schedules" aria-controls="schedules" role="tab" data-toggle="tab"><i class="fa fa-clock-o"></i> Horarios</a></li>
                <li role="presentation"><a href="#opinions" aria-controls="opinions" role="tab" data-toggle="tab"><i class="fa fa-comments"></i> Comentarios</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="fields">
                    <br>
                    @include('companies.subsidiaries.fields')
                </div>
                <div role="tabpanel" class="tab-pane" id="schedules">
                    <br>
                    <!--schedules form-->
                    @include('schedules.edit')
                </div>
                <div role="tabpanel" class="tab-pane" id="opinions">
                    <br>
                    <!--opinions form-->
                    @include('companies.subsidiaries.opinions.edit')
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer">

        {{ html()->a(route('companies.subsidiaries.index',$subsidiary->company()->get([0 => 'id'])[0]), '<i class="fa fa-fw fa-chevron-left"></i> Volver')->class('btn btn-primary') }}

        {{ html()->div()->class('btn-group pull-right')->open() }}

        {{ html()->a(route('companies.subsidiaries.delete', [$subsidiary->company()->get([0 => 'id'])[0],$subsidiary->id]), '<i class="fa fa-fw fa-trash"></i> Eliminar')->class('btn btn-danger')->data('remove', '¿Está seguro de que quiere borrar esta localidad?') }}

        {{ html()->submit('<i class="fa fa-fw fa-check"></i> Guardar')->class('btn btn-success') }}
        {{ html()->div()->close() }}
    </div>
    {{ html()->form()->close() }}
    <!-- /.box-body -->
</div>
@stop

@push('scripts')

@endpush