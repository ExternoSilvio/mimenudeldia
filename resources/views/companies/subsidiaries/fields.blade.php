<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {{ html()->label('Dirección', 'address')->class('col-sm-1 control-label') }}
            {{ html()->div()->class('col-sm-3')->open() }}
            {{ html()->text('address')->class('form-control')->autofocus()->required()->attribute('aria-describedby', 'helpAddress') }}
            {{ html()->span()->id('helpAddress')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {{ html()->label('Coordenadas', 'coordinates')->class('col-sm-1 control-label') }}
            {{ html()->div()->class('col-sm-3')->open() }}
            {{ html()->text('coordinates')->class('form-control')->required()->attribute('size', 5)->attribute('aria-describedby', 'helpCoordinates') }}
            {{ html()->span()->id('helpCoordinates')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {{ html()->label('Teléfono', 'phone')->class('col-sm-1 control-label') }}
            {{ html()->div()->class('col-sm-3')->open() }}
            {{ html()->text('phone')->class('form-control')->required()->attribute('size', 5)->attribute('aria-describedby', 'helpPhone') }}
            {{ html()->span()->id('helpPhone')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {{ html()->label('Email', 'email')->class('col-sm-1 control-label') }}
            {{ html()->div()->class('col-sm-3')->open() }}
            {{ html()->text('email')->class('form-control')->required()->attribute('size', 5)->attribute('aria-describedby', 'helpEmail') }}
            {{ html()->span()->id('helpEmail')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {{ html()->label('Localización (obligatorio)', 'location_id')->class('col-sm-1 control-label') }}
            {{ html()->div()->class('col-sm-3')->open() }}
            {{ html()->select('location_id', $locations)->class('form-control')->required()->attribute('aria-describedby', 'helpLocation') }}
            {{ html()->span()->id('helpLocation')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>
@if(isset($subsidiary))
@if($subsidiary->primary === 0 || empty($subsidiary->primary))
<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label class="col-sm-1 control-label" for="primary">Sede primaria</label>
                <div class="col-sm-3 checkbox">
                    <input type="checkbox" name="primary" aria-describedby="helpPrimary">
                    <span id="helpPrimary" class="help-block"></span>
                </div>
        </div>
    </div>
</div>

@else
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <div class="col-sm-8">
                    <h4><i class="fa fa-star" style="color: blue"></i>&nbsp;Sede primaria</h4>
                </div>
            </div>
        </div>
    </div>
@endif
@endif


