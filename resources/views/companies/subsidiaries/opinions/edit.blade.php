<div class="box-body">
    <div class="row">
        <div class="col-sm-4">
            <h4>Opiniones</h4>
               {{-- <ul class="list-group">
                    @foreach ($subsidiary->opinions()->get() as $opinion)
                        <li href="#" class="list-group-item  @if($opinion->status) alert alert-info @else alert alert-danger @endif">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>
                                        <label>{{ $opinion->user()->get(['name'])[0]->name }}</label>
                                    </label>
                                </div>
                                    <div class="col-sm-6 text-center">
                                        @if($opinion->status)
                                            <div>
                                                <label>
                                                    <a href= "{{route($route.'.denyOpinions',['subsidiaryId' => $subsidiary->id, 'opinionId' => $opinion->id])}}">
                                                        <i class="fa fa-thumbs-o-down"></i></a>
                                                </label>
                                            </div>
                                        @else
                                            <div>
                                                <label>
                                                    <a href= "{{route($route.'.acceptOpinions',['subsidiaryId' => $subsidiary->id, 'opinionId' => $opinion->id])}}" >
                                                        <i class="fa fa-thumbs-o-up"></i></a>
                                                </label>
                                            </div>
                                        @endif
                                    </div>
                                <div class="col-sm-3 text-right">
                                    <label>
                                        <a href= "{{route($route.'.deleteOpinions',['subsidiaryId' => $subsidiary->id, 'opinionId' => $opinion->id])}}"
                                           class="btn btn-xs btn-danger" data-remove="¿Estás seguro de que quiere eliminar este comentario?">
                                            <i class="fa fa-trash"></i></a>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    {{$opinion->comment}}
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>--}}
            @foreach ($subsidiary->opinions()->get() as $opinion)
                <div class="panel @if($opinion->status) panel-primary @else panel-danger @endif">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-sm-8">
                                <h3 class="panel-title">{{ $opinion->user()->get(['name'])[0]->name }}
                                </h3>
                            </div>
                            <div class="col-sm-4">
                                @if($opinion->status)
                                    <a href= "{{route('companies.'.$route.'.denyOpinions',['companyId' => $subsidiary->company()->get()->first()->id,'subsidiaryId' => $subsidiary->id, 'opinionId' => $opinion->id])}}" class="btn btn-xs btn-danger pull-right">
                                        denegar <i class="fa fa-times"></i></a>
                                @else
                                    <div>
                                        <a href= "{{route('companies.'.$route.'.acceptOpinions',['companyId' => $subsidiary->company()->get()->first()->id,'subsidiaryId' => $subsidiary->id, 'opinionId' => $opinion->id])}}" class="btn btn-xs btn-success pull-right">
                                            aprobar <i class="fa fa-check"></i></a>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div> <div class="panel-body">
                        {{$opinion->comment}}
                        <div class="col-sm-12">
                            <a href= "{{route('companies.'.$route.'.deleteOpinions',['companyId' => $subsidiary->company()->get()->first()->id,'subsidiaryId' => $subsidiary->id, 'opinionId' => $opinion->id])}}"
                               class="btn btn-xs btn-danger pull-right" data-remove="¿Estás seguro de que quiere eliminar este comentario?">
                                <i class="fa fa-trash"></i></a>
                        </div>
                    </div>
                </div>
            @endforeach
            {{--<div class="box-footer">
                @if(isset($companyId))
                    {{ html()->a(route('companies.'.$route.'.index',$companyId), '<i class="fa fa-fw fa-chevron-left"></i> Volver')->class('btn btn-primary') }}
                @else
                    {{ html()->a(route($route.'.index'), '<i class="fa fa-fw fa-chevron-left"></i> Volver')->class('btn btn-primary') }}
                @endif
                </div>--}}
        </div>
    </div>
</div>
