@extends('adminlte::page')

@section('title', 'Menus - Nuevo')

@push('css')

@endpush

@section('content_header')
    <h1>
        Menus
        <small>Nuevo Menu</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="/menus"><i class="fa fa-menus"></i> Menu</a></li>
        <li class="active">Nuevo</li>
    </ol>
@stop

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="nav nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="list-inline-item active">
                <a><i class="fa fa-map-cutlery"></i>
                    Menus</a>
            </li>
        </ul>
        <!-- /.box-header -->
        {{ html()->modelForm($menu, 'POST', route('menus.create'))->class('form-horizontal')->acceptsFiles()->open() }}
        <div class="box-body">
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#fields" aria-controls="fields" role="tab" data-toggle="tab"><i class="fa fa-edit"></i> Datos básicos</a></li>
                    <li role="presentation"><a href="#dishes" aria-controls="dishes" role="tab" data-toggle="tab"><i class="fa fa-fw fa-circle-thin"></i>Platos</a></li>
                    <li role="presentation"><a href="#features" aria-controls="features" role="tab" data-toggle="tab"><i class="fa fa-fw fa-tags"></i>Características</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="fields">
                        <br>
                        @include('companies.menus.fields')
                    </div>
                    <div role="tabpanel" class="tab-pane" id="dishes">
                        <div class="list-group">
                            <br>
                            <div class="row">
                                @foreach ($dishes as $dish)
                                    <div class="col-sm-3">
                                        <a href="javascript:void(0)" class="list-group-item">
                                            <input type="checkbox" name="dishes[]" value="{{ $dish->id }}"> &nbsp;{{ $dish->name }}
                                        </a>
                                    </div>
                                    @if ($loop->iteration % 4 == 0)
                            </div>
                            <br>
                            <div class="row">
                                @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="features">
                        <div class="list-group">
                            <br>
                            <div class="row">
                                @foreach ($features as $feature)
                                    <div class="col-sm-3">
                                        <a href="javascript:void(0)" class="list-group-item">
                                            <input type="checkbox" name="dishes[]" value="{{ $feature->id }}"> &nbsp;{{ $feature->name }}
                                        </a>
                                    </div>
                                    @if ($loop->iteration % 4 == 0)
                            </div>
                            <br>
                            <div class="row">
                                @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            {{ html()->a(route('menus.index'), '<i class="fa fa-fw fa-chevron-left"></i> Volver')->class('btn btn-primary') }}
            {{ html()->div()->class('btn-group pull-right')->open() }}
            {{ html()->submit('Crear')->class('btn btn-success') }}
            {{ html()->div()->close() }}
        </div>
    {{ html()->form()->close() }}
    <!-- /.box-body -->
    </div>
@stop

@push('scripts')

@endpush