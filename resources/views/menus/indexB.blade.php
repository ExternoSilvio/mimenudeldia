@extends('adminlte::page')

@section('title', 'Menus')

@section('content_header')
    <h1>
        Menus
        <small>Listado de menus</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Menus</li>
    </ol>
@stop

@section('content')
    <div class="nav nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="list-inline-item active">
                <a><i class="fa fa-cutlery"></i>
                    Menús</a>
            </li>
        </ul>
        <!-- /.box-header -->
        <div class="box-body table-fix-height">
            <div class="box-body table-fix-height">
                {{ html()->form('GET', route('menus.index'))->class('form-horizontal')->open() }}
                <div class="row">
                    <div class="col-sm-3" data-toggle="tooltip" data-placement="top" title="Filtrar por Compañías">
                        {{ html()->select('company', $companies, request('company'))->class('form-control select2') }}
                    </div>
                    <div class="col-sm-3" data-toggle="tooltip" data-placement="top" title="Filtrar por características">
                        {{ html()->select('features', $features, request('features'))->multiple()->class('form-control select2') }}
                    </div>
                    <div class="col-sm-3">
                        <div class="btn-group btn-block">
                            {{ html()->submit('<i class="fa fa-fw fa-filter"></i> Filtrar')->class('btn btn-primary') }}
                            {{ html()->reset('<i class="fa fa-fw fa-trash"></i>')->class('btn btn-primary') }}
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <a href="{!! route('menus.new') !!}" class="btn btn-xs btn-success pull-right">Añadir Menus</a>
                    </div>
                </div>
                    {{ html()->form()->close() }}
                </div>
                <table class="table table-bordered table-hover table-striped" id="menu-table" style="width:100%">
                    <thead>
                    <tr>
                        <th>Compañía</th>
                        <th>Nombre</th>
                        <th>Precio</th>
                        <th>Foto</th>
                        <th>Puntuación</th>
                        <th>Observaciones</th>
                        <th>Acción</th>
                    </tr>
                    </thead>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        @stop

        @push('scripts')
            <script>

                $("#company").select2({});

                $("#features").select2({
                  placeholder: 'Todas las características'
                })

                var drawCallbackHandler = function(api) {
                    removeBehavior();
                };

                var initDataTableComplete = function(api) {
                    //removeBehavior();
                };


                $(document).ready(function() {


                    $('#menu-table').DataTable({
                        dom                 : 'Bfrtip',
                        buttons             : [
                            'copy', 'excel', 'pdf', 'print'
                        ],
                        processing          : true,
                        serverSide          : true,
                        fixedHeader         : true,
                        scrollX             : true,
                        scrollCollapse      : true,
                        // rowReorder          : true,
                        stateSave           : true,
                        stateDuration       : -1,
                        language            : {
                            url     : 'https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json',
                            buttons : {
                                'copy'  : 'Copiar',
                                'Excel' : 'Excel',
                                'PDF'   : 'PDF',
                                'print' : 'Imprimir'
                            }
                        },
                        order               : [[ 0, "asc" ]],
                        /*fixedColumns        : {
                            leftColumns: 1,
                            rightColumns: 1
                        },*/
                        columnDefs          : [
                            { targets: 0, width: 'calc( 16.04% -20 )' },
                            { targets: 1, width: 'calc( 16.04% -10 )' },
                            { targets: 2, width: 'calc( 16.04% -10 )' },
                            { targets: 3, width: 'calc( 5%)'},
                            { targets: 4, width: 'calc( 16.04%)'},
                            { targets: 5, width: 'calc( 16.04%)'},
                            { targets: 6, width: '40px', height: 'auto', className: 'dt-center' }
                        ],
                        columns             : [
                            { data : 'company_id', name : 'company_id'},
                            { data : 'name', name : 'name'  },
                            { data : 'price', name : 'price'},
                            { data : 'photo_id', name : 'photo_id'},
                            { data : 'score' , name : 'score' },
                            { data : 'observations' , name : 'observations' },
                            { data : 'action', name : 'action', orderable : false }
                        ],
                        ajax                : {
                            url : '{!! route('menus.datatable') !!}',
                            data : function (d) {

                                d.company_global   = '{{request('company')}}';
                                d.features  = '{{ implode('|', request('features', [])) }}';

                            }
                        },
                        drawCallback        : function() {
                            drawCallbackHandler(this.api());
                        },
                        initComplete        : function() {
                            initDataTableComplete(this.api());
                        }

                    });
                });
            </script>
    @endpush
