<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Meta tags -->
    <meta name="description" content="MiMenúDelDía, la web y app que te ayudarán a elegir el menú de hoy que más te guste " />
    <meta name="keywords" content="menu, menú, diario, restaurante, comida, app, asiatica, asiática, china, chino, italianto, vegetariano, cerca, desayuno" />
    <meta name="author" content="DigitalApp">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">

    <!-- Fonts  -->
    <link href="http://fonts.googleapis.com/css?family=Lato:400,900,300,700" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,400italic,700italic" rel="stylesheet">

    <!-- Icons -->
    <link href="{{asset('fonts/fontello.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">


</head>
<body>
@yield('main')
<div class="row" style="color: #a3b1bf; font-family: 'Source Sans Pro', sans-serif;">
    <div class="col-sm-11" style="background-color: #304051;font-size:1em;padding: 2em">
        <h3 style="color: white;font-size: 1.5em">MiMenúDelDía</h3>
        <ul class="contacts" style="list-style: none; padding-left: 0px">
            <li><i class="icon-location contact"></i> C\Avenida Campo de Golf, Nº 5, 3º E</li>
            <li><i class="icon-globe-1 contact"></i> Altorreal, Murcia, España</li>
            <li><i class="icon-mobile contact"></i> +34 616 716 777</li>
            <li><a href="mailto:info@mimenudeldia.es"><i class="icon-mail-1 contact"></i> info@mimenudeldia.es</a></li>
        </ul><!-- /.contacts -->
    </div><!-- /.col -->
</div><!-- /.row -->
<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
</body>