<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Demystifying Email Design</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
</html>
<body>
<table style="width:100%;max-width: 500px">
    <tr>
        <td style="padding: 10px;border-bottom: 1px solid #A3B1BF">
            <a href="http://mimenudeldia.es"><img src="{{asset('images/logo.png')}}" style="width: 200px"></a>
        </td>
    </tr>
    <tr>
        <td>
            <table style="padding: 40px 20px 80px 20px;width: 100%">
                <tr>
                    <td>Hola <strong>nombre</strong>,</td>
                </tr>
                <tr>
                    <td style="padding:5px 10px">
                        Se ha realizado una nueva reserva <br>con los siguiente datos:
                    </td>
                </tr>
                <tr>
                    <td style="padding:5px 10px">Nombre: {{$name}}</td>
                </tr>
                <tr>
                    <td style="padding:5px 10px">Email: {{$email}}</td>
                </tr>
                <tr>
                    <td style="padding:5px 10px">Teléfono: {{$phone}}</td>
                </tr>
                <tr>
                    <td style="padding:5px 10px">Nº comensales: {{$diners}} </td>
                </tr>
                <tr>
                    <td style="padding:5px 10px">Cuerpo: {{$body}} </td>
                </tr>
                <tr>
                    <td style="padding:5px 10px">Fecha: {{$date}} a las  {{$time}}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr style="background-color: #2f4052;color: #A3B1BF">
        <td>
            <table style="padding: 30px 20px;width: 100%">
                <tr>
                    <td><img src="{{asset('images/logo-producto-de.png')}}" style="width: 100px"></td>
                </tr>
                <tr>
                    <td>En digitalApp nos ponemos a su </td>
                </tr>
                <tr>
                    <td>disposición para resolver cualquier duda o </td>
                </tr>
                <tr>
                    <td  style="padding-bottom: 10px;">consulta que tenga.</td>
                </tr>
                <tr>
                    <td>Avenida del Golf, Nº5, 3º E</td>
                </tr>
                <tr>
                    <td>Altorreal, Murcia, España</td>
                </tr>
                <tr>
                    <td>+34 616 716 777</td>
                </tr>
                <tr>
                    <td><a href="info@mimenudeldia.es" style="color: #A3B1BF">info@mimenudeldia.es</a></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>