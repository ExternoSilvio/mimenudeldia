@extends('adminlte::page')

@section('title', 'Inicio')

@section('content_header')
    <h1>
        Inicio
        <small>Panel de control</small>
    </h1>
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-dashboard"></i> Inicio</li>
    </ol>
@stop

@section('content')
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{ $opinions }}</h3>
                    <p>{{ $opinions === 1 ? 'Comentario' : 'Comentarios' }}</p>
                </div>
                <div class="icon">
                    <i class="fa fa-comments"></i>
                </div>
                <a class="small-box-footer">
                    Más detalles <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>{{ $features }}</h3>
                    <p>{{ $features === 1 ? 'Característica' : 'Características' }}</p>
                </div>
                <div class="icon">
                    <i class="fa fa-tags"></i>
                </div>
                <a href="{{ route('features.index') }}" class="small-box-footer">
                    Más detalles <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>{{ $users }}</h3>
                    <p>{{ $users === 1 ? 'Usuario' : 'Usuarios' }}</p>
                </div>
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
                <a href="{{route('users.index')}}" class="small-box-footer">
                    Más detalles <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>{{ $menus }}</h3>
                    <p>{{ $menus === 1 ? 'Menú' : 'Menus' }}</p>
                </div>
                <div class="icon">
                    <i class="fa fa-cutlery"></i>
                </div>
                <a href="/menus" class="small-box-footer">
                    Más detalles <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>{{ $dishes }}</h3>
                    <p>{{ $dishes === 1 ? 'Plato' : 'Platos' }}</p>
                </div>
                <div class="icon">
                    <i class="fa fa-circle-o"></i>
                </div>
                <a href="/dishes" class="small-box-footer">
                    Más detalles <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
    </div>
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-primary">
                <div class="inner">
                    <h3>&nbsp;</h3>
                    <p>Roles y permisos</p>
                </div>
                <div class="icon">
                    <i class="fa fa-unlock"></i>
                </div>
                <a class="small-box-footer">
                    Más detalles <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
    </div>
@stop