@extends('mimenudeldiaweb/page')

@section('main')
    <!-- ============================================================= SECTION – HERO ============================================================= -->

    <section id="hero" class="light-bg img-bg img-bg-softer" style="background-image: url({{asset('images/art/image-background06.jpg')}});">
        <div class="container inner">
            <div class="row">

                <div class="col-md-12">
                    <header style="text-align:right">
                        <h1 style="color:#444">El Nuevo Marketing</h1>
                        <p class="light-color" style="font-weight:bold">¿Existe una vaya publicitaria dinámica que llegue a todos nuestros posibles usuarios?</p>
                    </header>
                </div><!-- /.col -->


            </div><!-- ./row -->
        </div><!-- /.container -->
    </section>

    <!-- ============================================================= SECTION – HERO : END ============================================================= -->


    <!-- ============================================================= SECTION – WHO WE ARE ============================================================= -->

    <section id="who-we-are">
        <div class="container inner-top inner-bottom-sm">

            <div class="row">
                <div class="col-md-8 col-sm-9 center-block text-center">
                    <header>
                        <h1>Los tiempos han cambiado.</h1>
                        <p>Vivimos en una era tecnológica donde el smartphone es el rey. Y como rey de reyes, nuestra dependencia a él va más allá de los antiguos sms y las llamadas a móviles de antaño. Los smartphone se han integrado como parte de nuestra vida, ayudándonos en nuestro día a día en casi todas nuestras actividades, inclusive en nuestra toma de decisiones. </br>Por ello que MiMenúDelDía le aportará... </p>
                    </header>
                </div><!-- /.col -->
            </div><!-- /.row -->

            <div class="row inner-top-sm">

                <div class="col-md-1">
                    <div class="icon pull-right">
                        <i class="icon-megaphone icn"></i>
                    </div><!-- /.icon -->
                </div><!-- /.col -->

                <div class="col-md-3 inner-bottom-xs">
                    <h2>Mayor alcance</h2>
                    <p class="text-small">Mantenerse anunciado en una simple vaya publicitaria es costoso, además de estar localmente limitada. Ponemos a su alcance un nuevo canal con acceso a miles de usuarios por una pequeña cuota mensual sin permanencia.</p>
                </div><!-- /.col -->

                <div class="col-md-1">
                    <div class="icon pull-right">
                        <i class="icon-eye-1 icn"></i>
                    </div><!-- /.icon -->
                </div><!-- /.col -->

                <div class="col-md-3 inner-bottom-xs">
                    <h2>Nuevo escaparate</h2>
                    <p class="text-small">Un escaparate digital sin limitaciones, con cientos de miles de usuarios potenciales ávidos por consumir las nuevas tecnologías. Estar hoy en dicho mundo, puede marcar las diferencias de mañana.</p>
                </div><!-- /.col -->

                <div class="col-md-1">
                    <div class="icon pull-right">
                        <i class="icon-cog icn"></i>
                    </div><!-- /.icon -->
                </div><!-- /.col -->

                <div class="col-md-3 inner-bottom-xs">
                    <h2>Control total</h2>
                    <p class="text-small">Desde un sencillo panel web, podrá controlar toda la información publicada de su empresa, así como las imágenes de sus productos, noticias y ofertas. Estas serán mostradasde de forma atractiva a los usuarios de MiMenúDelDía. </p>
                </div><!-- /.col -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </section>

    <!-- ============================================================= SECTION – WHO WE ARE : END ============================================================= -->


    <!-- ============================================================= SECTION – LATEST WORKS ============================================================= -->

    <section id="latest-works" class="light-bg">
        <div class="container inner">

            <div class="row">
                <div class="col-md-8 col-sm-9 center-block text-center">
                    <header>
                        <h1>Últimos negocios registrados</h1>
                        <p>Observa una serie de ejemplos de partners dados de alta recientemente.</p>
                    </header>
                </div><!-- /.col -->
            </div><!-- /.row -->

            <div class="row">
            @foreach($companies->take('4') as $company)
                <div class="col-sm-6 inner-top-sm">
                    <figure>

                        <div class="icon-overlay icn-link">
                            <a href="{{route('website.negocio',$company->id)}}"><img src="@if(isset($company->logo()->get()[0])) {{$company->logo()->get()[0]->url}} @endif" alt=""></a>
                        </div><!-- /.icon-overlay -->

                        <figcaption class="bordered no-top-border">
                            <div class="info">
                                <h3><a href="{{route('website.negocio',$company->id)}}">{{$company->name}}</a></h3>
                                <p>Identity</p>
                            </div><!-- /.info -->
                        </figcaption>

                    </figure>
                </div><!-- /.col -->
                @endforeach
            </div><!-- /.row -->

            <div class="row inner-top-sm">
                <div id="owl-latest-works" class="owl-carousel owl-item-gap">
                    @foreach($companies as $company)
                    <div class="item">
                        <a href="{{route('website.negocio',$company->id)}}">
                            <figure>
                                <figcaption class="text-overlay">
                                    <div class="info">
                                        <h4>{{$company->name}})</h4>
                                        <p>Interactive</p>
                                    </div><!-- /.info -->
                                </figcaption>
                                <img src="{{asset('images/art/work03.jpg')}}" alt="">
                            </figure>
                        </a>
                    </div><!-- /.item -->
                        @endforeach
                </div><!-- /.owl-carousel -->
            </div><!-- /.row -->

        </div><!-- /.container -->
    </section>

    <!-- ============================================================= SECTION – LATEST WORKS : END ============================================================= -->


    <!-- ============================================================= SECTION – GET IN TOUCH ============================================================= -->
    <section id="get-in-touch">
        <div class="container inner">
            <div class="row">
                <div class="col-md-8 col-sm-9 center-block text-center">
                    <header>
                        <h1>¿Quieres formar parte de la familia?</h1>
                        <p>Ponte en contacto con nosotros y solicítanos información sin compromiso.</p>
                    </header>
                    <a href="{{route('website.contacto')}}" class="btn btn-large">Contactar</a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>

    <!-- ============================================================= SECTION – GET IN TOUCH : END ============================================================= -->

    <!-- ============================================================= SECTION – GET STARTED ============================================================= -->

    <section id="get-started" class="light-bg">
        <div class="container inner-sm">
            <div class="row">
                <div class="col-sm-11 center-block text-center">
                    <span><a href="http://www.digitalapp.es"><img src="{{asset('/images/logo-producto-de.png')}}" alt="DigitalApp" /></a></span>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>

    <!-- ============================================================= SECTION – GET STARTED : END ============================================================= -->

@stop