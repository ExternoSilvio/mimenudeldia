@extends('mimenudeldiaweb/page')

@section('main')
    <section id="hero" class="light-bg img-bg img-bg-softer" style="background-image: url({{asset('images/art/image-background07.jpg')}});">
        <div class="container inner">
            <div class="row">
                <div class="col-md-12">
                    <header>
                        <h1 style="color:#222">El escaparate perfecto</h1>
                        <p class="navy" style="font-weight:bold">Simplifícales la vida a tus clientes, te lo agradecerán!</p>
                    </header>
                </div><!-- /.col -->

            </div><!-- ./row -->
        </div><!-- /.container -->
    </section>

    <!-- ============================================================= SECTION – HERO : END ============================================================= -->


    <!-- ============================================================= SECTION – FEATURES ============================================================= -->

    <section id="features">
        <div class="container inner-top">

            <div class="row">
                <div class="col-md-8 col-sm-9 center-block text-center">
                    <header>
                        <h1>Fácil. Rápida. Atractiva.</h1>
                        <p>Cada día, cientos de usuarios deciden dónde comer o desayunar gracias a MiMenúDelDía, reservando sus mesas, y todo ello, sin la necesidad de perder el tiempo de acercarse a los locales. Esto, y además de que es una herramienta ágil y sencilla de utilizar, convierten a MiMenúDelDía en una plataforma casi indispensable para locales y negocios.  </p>
                    </header>
                </div><!-- /.col -->
            </div><!-- /.row -->

            <div class="row inner-top-sm">

                <div class="col-md-3 inner-bottom-sm">
                    <h2>Sitio Web</h2>
                    <p class="text-small">Podrás publicitar tu local en MiMenúDelDía poniéndolo al alcance de miles de usuarios, con una atractiva web responsive, visible desde todos los dispositivos y navegadores.</p>
                    <a href="{{route('website.index')}}" class="txt-btn">Véalo usted mismo</a>

                    </br> </br> </br>
                    <h2>Panel Web</h2>
                    <p class="text-small">Podrás publicitar tu local en MiMenúDelDía poniéndolo al alcance de miles de usuarios, con una atractiva web responsive, visible desde todos los dispositivos y navegadores.</p>
                </div><!-- /.col -->

                <div class="col-md-3 inner-bottom-sm">
                    <h2>App Móvil</h2>
                    <p class="text-small">Hoy en día, los clientes demandan apps. La usabilidad y las sensaciones que ofrecen las aplicaciones nativas están muy encima de las web, y MiMenúDelDía se aprovecha de ello. </p>
                    <a href="{{route('website.appmovil')}}" class="txt-btn">Véalo usted mismo</a>

                    </br> </br> </br>
                    <h2>Control Total</h2>
                    <p class="text-small">Vaya faena!, ¿se ha quedado sin un plato del menú?. No pasa nada, modifique su menú de hoy rápidamente desde su propio móvil mediante su acceso individual al panel web.</p>

                </div><!-- /.col -->

                <div class="col-md-6 inner-left-xs">
                    <figure><img src="{{asset('images/art/service01.jpg')}}" alt=""></figure>
                </div><!-- /.col -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </section>

    <!-- ============================================================= SECTION – FEATURES : END ============================================================= -->


    <!-- ============================================================= SECTION – SPECIALS ============================================================= -->

    <section id="specials" class="light-bg">
        <div class="container inner-top">

            <div class="row">
                <div class="col-md-8 col-sm-9 center-block text-center">
                    <header>
                        <h1>Licencias</h1>
                        <p>Comience a trabajar con nosotros y dote a su negocio del escaparate que realmente necesita.</p>
                    </header>
                </div><!-- /.col -->
            </div><!-- /.row -->

            <div class="row inner-top-sm">
                <div class="col-xs-12">
                    <div class="tabs tabs-2-big-top tab-container">

                        <ul class="etabs text-center">

                            <li class="tab">
                                <a href="#tab-1">
                                    <h3>Módulo Básico</h3>
                                    <p class="text-small">Con el módulo básico podrás dar de alta hasta 4 tipos diferentes de menús, toda la información de contacto necesaria, ubicación, teléfonos, redes sociales y la gestión de reservas.</p>
                                </a><!-- /#tab-1 -->
                            </li><!-- /.tabs -->

                            <li class="tab">
                                <a href="#tab-2">
                                    <h3>Módulo Avanzado</h3>
                                    <p class="text-small">El módulo avanzado implica, además del módulo básico, el poder dar de alta tantos menús diferentes como quieras, añadir una galería de fotos y publicitar la carta completa de tu negocio.</p>
                                </a><!-- /#tab-2 -->
                            </li><!-- /.tabs -->

                        </ul><!-- /.etabs -->

                        <div class="panel-container screen-container">

                            <div class="tab-content" id="tab-1">
                                <div class="row">
                                    <div class="col-md-12">
                                        <figure><img src="{{asset('images/art/photograph01-lg.jpg')}}" alt=""></figure>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div><!-- /.tab-content -->

                            <div class="tab-content" id="tab-2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <figure><img src="{{asset('images/art/work14-lg.jpg')}}" alt=""></figure>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div><!-- /.tab-content -->

                        </div><!-- /.panel-container -->

                    </div><!-- /.tabs -->
                </div><!-- /.col -->
            </div><!-- /.row -->

        </div><!-- /.container -->
    </section>

    <!-- ============================================================= SECTION – SPECIALS : END ============================================================= -->





    <!-- ============================================================= SECTION – LATEST WORKS ============================================================= -->

    <section id="latest-works">
        <div class="container inner">

            <div class="row">
                <div class="col-md-8 col-sm-9 center-block text-center">
                    <header>
                        <h1>Últimos negocios registrados</h1>
                        <p>Observa una serie de ejemplos de locales y negocios dados de alta recientemente.</p>
                    </header>
                </div><!-- /.col -->
            </div><!-- /.row -->

            <div class="row">

                @foreach($subsidiaries as $subsidiary)
                <div class="col-sm-6 inner-top-sm">
                    <figure>

                        <div class="icon-overlay icn-link">
                            <a href="{{route('website.restaurante',$subsidiary->id)}}"><img src="{{asset('images/art/work02.jpg')}}" alt=""></a>
                        </div><!-- /.icon-overlay -->

                        <figcaption class="bordered no-top-border">
                            <div class="info">
                                <h3><a href="{{route('website.restaurante',$subsidiary->id)}}">{{$subsidiary->company()->get()[0]->name}}</a></h3>
                                <p>({{$subsidiary->location()->get()[0]->name}},&nbsp{{$subsidiary->location()->get()[0]->province()->get()[0]->name}})</p>
                            </div><!-- /.info -->
                        </figcaption>

                    </figure>
                </div><!-- /.col -->
                @endforeach
            </div><!-- /.row -->

            <div class="row inner-top-sm">
                <div id="owl-latest-works" class="owl-carousel owl-item-gap">

                    @foreach($subsidiaries as $subsidiary)
                    <div class="item">
                        <a href="{{route('website.restaurante',$subsidiary->id)}}">
                            <figure>
                                <figcaption class="text-overlay">
                                    <div class="info">
                                        <h4>{{$subsidiary->company()->get()[0]->name}}</h4>
                                        <p>({{$subsidiary->location()->get()[0]->name}}, &nbsp {{$subsidiary->location()->get()[0]->province()->get()[0]->name}})</p>
                                    </div><!-- /.info -->
                                </figcaption>
                                <img src="{{asset('images/art/work03.jpg')}}" alt="">
                            </figure>
                        </a>
                    </div><!-- /.item -->
                    @endforeach
                </div><!-- /.owl-carousel -->
            </div><!-- /.row -->

        </div><!-- /.container -->
    </section>

    <!-- ============================================================= SECTION – LATEST WORKS : END ============================================================= -->

    <!-- ============================================================= SECTION – GET IN TOUCH ============================================================= -->

    <section id="get-in-touch" class="light-bg">
        <div class="container inner">
            <div class="row">
                <div class="col-md-8 col-sm-9 center-block text-center">
                    <header>
                        <h1>¿Quieres formar parte de la familia?</h1>
                        <p>Ponte en contacto con nosotros y solicítanos información sin compromiso.</p>
                    </header>
                    <a href="{{route('website.contacto')}}" class="btn btn-large">Contactar</a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>

    <!-- ============================================================= SECTION – GET IN TOUCH : END ============================================================= -->


    <!-- ============================================================= SECTION – GET STARTED ============================================================= -->

    <section id="get-started" >
        <div class="container inner-sm">
            <div class="row">
                <div class="col-sm-11 center-block text-center">
                    <span><a href="http://www.digitalapp.es"><img src="{{asset('images/logo-producto-de.png')}}" alt="DigitalApp" /></a></span>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>

    <!-- ============================================================= SECTION – GET STARTED : END ============================================================= -->
@stop