{{ html()->form('post',route('website.reservation'))->class('forms')->attribute('id','contactform')->open() }}
<div class="row">
    <div class="col-sm-12">
        {{html()->text('name')->class('form-control')->placeholder('* Nombre')}}
    </div><!-- /.col -->
</div>
<div class="row">
    <div class="col-sm-6">
        {{html()->email('email')->class('form-control')->placeholder('* Email')}}
    </div><!-- /.col -->

    <div class="col-sm-6">
        {{html()->text('phone')->class('form-control')->placeholder('* Teléfono')}}
    </div><!-- /.col -->
</div><!-- /.row -->

<div class="row">

    <div class="col-sm-5">
        {{html()->text('diners')->class('form-control')->placeholder('* Nº de comensales')}}
    </div><!-- /.col -->
    <div class="col-sm-4">
        {{html()->date('date')->class('form-control')->placeholder('* Hora de reserva')}}
    </div><!-- /.col -->
    <div class="col-sm-3">
        {{html()->time('time')->class('form-control')->placeholder('* Hora de reserva')}}
    </div><!-- /.col -->
</div><!-- /.row -->

<div class="row">
    <div class="col-sm-12">
        {{html()->textarea('body')->class('form-control')->placeholder('¿Necesita indicarnos algo sobre su reserva? Hágalo aquí... ')}}
    </div><!-- /.col -->
</div><!-- /.row -->
{{html()->submit('Reservar')->class('btn btn-default btn-submit   ')}}
{{ html()->form()->close() }}