{{html()->form('post',route('website.contactmail'))->attribute('id','contactform')->open()}}
<div class="row">
    <div class="col-sm-6">
        {{html()->text('name')->class('form-control')->placeholder("* Nombre")}}
    </div><!-- /.col -->
</div><!-- /.row -->

<div class="row">
    <div class="col-sm-6">
        {{html()->email('email')->class('form-control')->placeholder("* Email")}}
    </div><!-- /.col -->
</div><!-- /.row -->

<div class="row">
    <div class="col-sm-6">
        {{html()->text('subject')->class('form-control')->placeholder("* Asunto")}}
    </div><!-- /.col -->
</div><!-- /.row -->

<div class="row">
    <div class="col-sm-12">
        {{html()->textarea('body')->class('form-control')->placeholder("* Introduzca su mensaje ... ")}}
    </div><!-- /.col -->
</div><!-- /.row -->

<button type="submit" name='submit' class="btn btn-default btn-submit">Enviar mensaje</button>
{{html()->form()->close()}}
