@extends('mimenudeldiaweb/page')
@section('main')

    <section id="main-slider-header">


        <div id="owl-main" class="owl-carousel height-md owl-inner-nav owl-ui-lg">

            <div class="item img-bg img-bg-soft light-bg" style="background-image: url({{asset('images/art/slider01.jpg')}});">
                <div class="container">
                    <div class="caption vertical-top text-center">

                        <h1 class="fadeInDown-1 dark-bg light-color"><span>¿No sabes dónde comer hoy?</span></h1>
                        <p class="fadeInDown-2 light-color navy-bg" style="font-weight:bold">Te ayudamos a encontrar el menú que más te guste.</p>
                        <div class="fadeInDown-3">
                            <a href="#carousels" class="btn btn-large">Buscar</a>
                        </div><!-- /.fadeIn -->
                    </div><!-- /.caption -->
                </div><!-- /.container -->
            </div><!-- /.item -->

            <div class="item img-bg img-bg-soft light-bg" style="background-image: url({{asset('images/art/slider02.jpg')}});">
                <div class="container">
                    <div class="caption vertical-center text-left">

                        <h1 class="fadeIn-1 light-bg dark-color"><span>Menús del día</span></h1>
                        <p class="fadeInLeft-2 light-color navy-bg" style="font-weight:bold">¿Quieres saber qué se cuece hoy en las cocinas de tus restaurantes más cercanos?</p>
                        <div class="fadeInLeft-3">
                            <a href="#carousels" class="btn btn-large">Buscar</a>
                        </div><!-- /.fadeIn -->
                    </div><!-- /.caption -->
                </div><!-- /.container -->
            </div><!-- /.item -->

            <div class="item img-bg img-bg-soft light-bg" style="background-image: url({{asset('images/art/slider03.jpg')}});">
                <div class="container">
                    <div class="caption vertical-center text-right">

                        <h1 class="fadeInDown-1 dark-bg light-color"><span>Menús de desayuno</span></h1>
                        <p class="fadeInRight-2 light-color navy-bg" style="font-weight:bold">Porque el desayuno es la comida más importante del día, carga bien las pilas.</p>
                        <div class="fadeInRight-3">
                            <a href="#carousels" class="btn btn-large">Buscar</a>
                        </div><!-- /.fadeIn -->

                    </div><!-- /.caption -->
                </div><!-- /.container -->
            </div><!-- /.item -->

            <div class="item img-bg img-bg-soft light-bg" style="background-image: url({{asset('images/art/slider04.jpg')}});">
                <div class="container">
                    <div class="caption vertical-top text-left">

                        <h1 class="fadeIn-1 light-bg dark-color"><span>Menús especiales</span></h1>
                        <p class="fadeIn-2 light-color navy-bg" style="font-weight:bold">Para esas cenas de navidad, o para la cena de la empresa o para el bautizo de mi peque o para...</p>
                        <div class="fadeIn-3">
                            <a href="#carousels" class="btn btn-large">Buscar</a>
                        </div><!-- /.fadeIn -->

                    </div><!-- /.caption -->
                </div><!-- /.container -->
            </div><!-- /.item -->

            <div class="item img-bg img-bg-soft light-bg" style="background-image: url({{asset('images/art/slider05.jpg')}});">
                <div class="container">
                    <div class="caption vertical-bottom text-right">

                        <h1 class="fadeInUp-1 dark-bg light-color"><span>Ofertas y Negocios</span></h1>
                        <p class="fadeInUp-2 light-color navy-bg" style="font-weight:bold">Aprovéchate de las ofertas en menús y en negocios registrándote y usando nuestra App.</p>
                        <div class="fadeInUp-3">
                            <a href="{{route('website.negocios')}}" class="btn btn-large">Ver negocios</a>
                        </div><!-- /.fadeIn -->

                    </div><!-- /.caption -->
                </div><!-- /.container -->
            </div><!-- /.item -->

            <div class="item img-bg img-bg-soft light-bg" style="background-image: url('{{asset('images/art/slider06.jpg')}}');">
                <div class="container">
                    <div class="caption vertical-bottom text-center">

                        <h1 class="fadeInUp-1 dark-bg light-color"><span>Reserva desde tu móvil</span></h1>
                        <p class="fadeInUp-2 light-color navy-bg" style="font-weight:bold">Utiliza nuestra app y benefíciate de las mejores ofertas.</p>
                        <div class="fadeInUp-3">
                            <a href="app.html" class="btn btn-large">App móvil</a>
                        </div><!-- /.fadeIn -->

                    </div><!-- /.caption -->
                </div><!-- /.container -->
            </div><!-- /.item -->

        </div><!-- /.owl-carousel -->

    </section>





    <!-- ============================================================= SECTION – MAIN SLIDER HEADER : END ============================================================= -->


    <section id="carousels" >
        <div class="container">

            <div class="row">
                <div class="col-md-8 col-sm-9 center-block text-center">
                    <header>
                        <h1 style="padding-top:20px">Seleccione zona</h1>
                        <p>Utiliza los filtros para seleccionar la zona dónde desea buscar los menús del día</p>
                    </header>
                </div><!-- /.col -->
            </div><!-- /.row -->

            <div class="row">

                <div style="padding-top:40px">
                <form method="get" action="{{route('website.index')}}" enctype="text/plain" id="menus_filter" class="col-12">
                    <div class="col-md-4 col-sm-6">
                        {{ html()->select('province',$provinces,request('province'))->class('form-control')}}
                    </div>

                    <div class="col-md-4 col-sm-6">
                        <select id="location" name="location" class="form-control">
                            @if(count($locations))
                            @foreach($locations as $key => $value)
                                <option value="{{$key}}" @if(request('location') == $key) selected @endif>{{$value}}</option>
                            @endforeach
                            @endif
                        </select>
                    </div><!-- /.col -->
                        <div class="row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-4">
                                <input type="text" name="zip" id="zip">
                                <input type="hidden" name="lat" id="lat">
                                <input type="hidden" name="lng" id="lng">
                            </div>
                        </div>
                    <div class="col-sm-6">
                        <button type="submit"  name="submit" class="btn btn-default btn-submit" style="margin-top:0px">Filtrar Restaurantes</button>
                    </div><!-- /.col -->
                    {{ csrf_field() }}
                </form>
                    <div id="response"></div>
                </div>
            </div>
        </div>
    </section>


    <!-- ============================================================= SECTION – PORTFOLIO ============================================================= -->

    <section id="portfolio" >

        <div class="container inner-bottom">

            <div class="row">
                <div class="col-xs-12 inner-top" style="padding-top:30px">
                    <div class="tabs tabs-services tabs-circle-top tab-container">

                        <ul class="text-center filter">
                            <li class="tab"><a href="#tab-1" data-filter="*"><div><i class="icon-lamp"></i></div>Todo</a></li>
                            @foreach($features_companies as $feature)
                            <li class="tab"><a href="#tab-1" data-filter=".company_feature-{{$feature->id}}"><div><i class="icon-lamp"></i></div>{{$feature->name}}</a></li>
                            @endforeach
                        </ul><!-- /.etabs -->
                        <div class="panel-container">
                            <div class="tab-content" id="tab-1">
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-12 portfolio">

                    <div class="loading hide text-center">
                        <i class="fa fa-spinner fa-spin fa-fw"></i>
                        Cargando...
                    </div>

                    <ul class="filter text-center no-line">

                    </ul><!-- /.filter -->

                    <ul class="items col-4 gap">

                    </ul><!-- /.items -->
                    <div style="padding-top:30px">
                            @include('mimenudeldiaweb/paginator')
                        <!-- /.pagination -->
                    </div>

                </div><!-- /.col -->


            </div><!-- /.row -->



        </div><!-- /.container -->
    </section>

    <!-- ============================================================= SECTION – PORTFOLIO : END ============================================================= -->





    <!-- ============================================================= SECTION – BUY TEMPLATE ============================================================= -->

    <section id="filters" class="light-bg">
        <div class="container inner-sm">
            <div class="row">
                <div class="col-md-8 col-sm-9 center-block text-center">
                    <header>
                        <h1>Negocios destacados</h1>
                        <p>A continuación se muestran una serie de negocios de tu zona que pueden serte de interés.</p>
                    </header>
                </div><!-- /.col -->
            </div><!-- /.row -->
            <div class="row">
                <div class="col-sm-12 inner-top-md">
                    <div id="owl-companies-loader" class="text-center">Cargando...</div>
                    <div id="owl-companies" class="hide"></div><!-- /.owl-carousel -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>
@stop