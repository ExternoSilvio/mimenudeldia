@extends('mimenudeldiaweb/page')

@section('main')
    <section id="hero" class=" img-bg img-bg-softer" style="background-image: url({{asset('images/art/image-background02.jpg')}});">
        <div class="container inner ">
            <div class="row">
                <div class="caption vertical-top text-center">
                    <header>
                        <h1 class="fadeInUp-1 dark-bg light-color">{{$subsidiary->company()->first()->name}}</h1>
                        <p class="fadeInUp-2 light-color navy-bg" style="font-weight:bold">{{$subsidiary->company()->first()->information}}</p>

                    </header>


                </div><!-- /.col -->

            </div><!-- ./row -->
        </div><!-- /.container -->
    </section>

    <!-- ============================================================= SECTION – HERO : END ============================================================= -->

    <!-- ============================================================= SECTION – CIRCLE TABS ============================================================= -->

    <section id="circle-tabs">
        <div class="container inner">

            <div class="row">
                <div class="col-xs-12 inner-top-short">
                    <div class="tabs tabs-services tabs-circle-top tab-container">

                        <ul class="etabs text-center">
                            <li class="tab"><a href="#tab-1"><div><i class="icon-lamp"></i></div>Menús</a></li>
                            <li class="tab"><a href="#tab-2"><div><i class="icon-megaphone-1"></i></div>Ofertas</a></li>
                            <li class="tab"><a href="#tab-3"><div><i class="icon-info"></i></div>Empresa</a></li>
                            <li class="tab"><a href="#tab-4"><div><i class="icon-chat-empty"></i></div>Contacto</a></li>
                            <li class="tab"><a href="#tab-5"><div><i class="icon-paste"></i></div>Carta</a></li>
                            <li class="tab"><a href="#tab-6"><div><i class="icon-picture"></i></div>Galería</a></li>
                        </ul><!-- /.etabs -->

                        <div class="panel-container">

                            <div class="tab-content" id="tab-1">



                                <div class="row">
                                    <div class="col-md-8 col-sm-10 center-block text-center">
                                        <header>
                                            <p>Disponemos de una amplia gama de platos combinados y siendo especialistas en carnes a la brasa.</p>
                                            <div class="fadeInDown-3">
                                                <a href="#reserva" class="btn btn-large">Reservar</a>
                                                <a href="{{route('website.setFavorite',$subsidiary->company()->get()[0]->id)}}" data-create="Añadido a favoritos" class="btn btn-large purple-bg">Marcar como Favorito</a>
                                            </div><!-- /.fadeIn -->



                                        </header>

                                        <ul class="filter text-center no-line">
                                            <li><a href="#" data-filter="*" class="active">Todas</a></li>
                                            <li><a href="#" data-filter=".identity">Comidas</a></li>
                                            <li><a href="#" data-filter=".identity">Desayunos</a></li>
                                            <li><a href="#" data-filter=".favouriteType">Especiales</a></li>
                                            <li><a href="#" data-filter=".favouriteType">Negocios</a></li>
                                        </ul><!-- /.filter -->
                                    </div><!-- /.col -->
                                </div><!-- /.row -->

                                <div class="row inner-top-sm text-center"  style="padding-top:15px">

                                    <div class="col-sm-6 inner-bottom-sm inner-left inner-right">
                                        <figure class="member">

                                            <div class="icon-overlay icn-link">
                                                <a href="#reserva"><img src="{{asset('images/art/dish01.jpg')}}" class="img-circle"></a>
                                            </div><!-- /.icon-overlay -->

                                            <figcaption>

                                                <h2>
                                                    Menú del día
                                                </h2>
                                                <span>Ensalada</span>
                                                <h4> Primeros </h4>
                                                <span>Espagueti a la boloñesa</span></br>
                                                <span>Risotto al roquefort</span></br>
                                                <span>Pastel de berenjenas</span></br></br>
                                                <h3> Segundos </h3>
                                                <span>Entrecot a la brasa</span></br>
                                                <span>Arroz con conejo</span></br>
                                                <span>Salmón a la miel con cebolla confitada</span></br></br>

                                                <span>Incluído café o postre (flan de huevo, fruta variada, tarta de la abuela, tiramisú)</span></br></br>

                                                <h2>10 €</h2>

                                                <div class="fadeInDown-3">
                                                    <a href="#reserva" class="btn btn-large">Reservar</a>
                                                </div><!-- /.fadeIn -->

                                                <ul class="social">
                                                    <li><a href="#reserva"><i class="icon-mail-alt"></i></a></li>
                                                    <li><a href="#"><i class="icon-s-facebook"></i></a></li>
                                                    <li><a href="#"><i class="icon-s-gplus"></i></a></li>
                                                    <li><a href="#"><i class="icon-s-twitter"></i></a></li>
                                                </ul><!-- /.social -->

                                            </figcaption>

                                        </figure>
                                    </div><!-- /.col -->

                                    <div class="col-sm-6 inner-bottom-sm inner-left inner-right">
                                        <figure class="member">

                                            <div class="icon-overlay icn-link">
                                                <a href="#reserva"><img src="{{asset('images/art/dish02.jpg')}}" class="img-circle"></a>
                                            </div><!-- /.icon-overlay -->

                                            <figcaption>

                                                <h2>
                                                    Menú Vegetariano / Vegano
                                                </h2>

                                                <blockquote>2 platos a elegir entre una amplia variedad de ensaladas, platos con tofu, verduras, cuscus, arroces, etc.</blockquote>

                                                <span>Incluído café o postre (flan de huevo, fruta variada, tarta de la abuela, tiramisú)</span></br></br>

                                                <h2>9 €</h2>

                                                <div class="fadeInDown-3">
                                                    <a href="#reserva" class="btn btn-large">Reservar</a>
                                                </div><!-- /.fadeIn -->



                                                <ul class="social">
                                                    <li><a href="#reserva"><i class="icon-mail-alt"></i></a></li>
                                                    <li><a href="#"><i class="icon-s-facebook"></i></a></li>
                                                    <li><a href="#"><i class="icon-s-gplus"></i></a></li>
                                                    <li><a href="#"><i class="icon-s-twitter"></i></a></li>
                                                </ul><!-- /.social -->

                                            </figcaption>

                                        </figure>
                                    </div><!-- /.col -->

                                </div><!-- /.row -->
                            </div><!-- /.tab-content -->


                            <div class="tab-content" id="tab-2">
                                <div class="row">
                                    <div class="col-md-8 col-sm-9 center-block text-center">
                                        <a href="{{route('website.setFavorite',$subsidiary->company()->get()[0]->id)}}" data-create="Añadido a favoritos" class="btn btn-large purple-bg">Marcar como Favorito</a>
                                        @foreach($subsidiary->company()->get()[0]->dishes()->get() as $dish)
                                            @foreach($dish->offers()->get() as $offer)
                                            <h2>Ahorra 1€ en tu menú</h2>
                                            <ul class="item-details">
                                                <li class="date">{{$offer->finish_at}}</li>
                                            </ul><!-- /.item-details -->
                                            <p class="text-normal">{{$subsidiary->company()->get()[0]->name}} quiere agradecer la confianza depositada de sus clientes y a través de MiMenúDelDía lanza una oferta de <a href="#" title="" data-rel="tooltip" data-placement="top" rel="tooltip" data-original-title="1 euro de descuento en el menú que elija">1 euro</a> para los días jueves y viernes.</p>
                                            @endforeach
                                        @endforeach
                                        </br>
                                        <h2>Segunda bebida al 50%</h2>
                                        <ul class="item-details">
                                            <li class="date">Hasta 23 de Diciembre de 2015</li>
                                        </ul><!-- /.item-details -->
                                        <p class="text-normal">A través de MiMenúDelDía podrá beneficiarse de un descuento del <a href="#" title="" data-rel="tooltip" data-placement="top" rel="tooltip" data-original-title="válido sólo ne comidas mediante menús y mediante platos en carta">50%</a> en la segunda bebida que se tome.</p>


                                    </div>
                                    <div class="col-sm-8 center-block text-center inner-top-xs">
                                        <h3>Condiciones</h3>
                                        <ul class="item-details">
                                            <li class="categories">Necesita estar registrado</li>
                                            <li class="client">Debe mostrar su usuario en la app: </li>
                                        </ul><!-- /.item-details -->

                                        <figure><img src="assets/images/art/registrado.jpg" style="max-width:400px"></figure>
                                    </div>

                                </div><!-- /.row -->
                            </div><!-- /.tab-content -->

                            <div class="tab-content" id="tab-3">
                                <div class="row">

                                    <div class="col-md-5 col-md-offset-1 col-sm-6 inner-right-xs">

                                        <figure><img src="assets/images/art/restaurante.jpg" alt=""></figure>
                                    </div><!-- /.col -->

                                    <div class="col-md-5 col-sm-6 inner-top-xs inner-left-xs">
                                        <h3>El restaurante</h3>
                                        <a href="{{route('website.setFavorite',$subsidiary->company()->get()[0]->id)}}" data-create="Añadido a favoritos" class="btn btn-large purple-bg">Marcar como Favorito</a>
                                        <p>Ha pasado mucho tiempo desde que en 2011 iniciamos nuestra andadura gastronómica, y durante todo este tiempo, cientos de clientes pueden dar cuenta de la riqueza y frescura de nuestros platos.<br></br>

                                            Ganadores por 2 ocasiones en la ruta de la tapa, Restaurante Domingo es una buena elección para disfrutar de los sabores de nuestra tierra.</br></br>

                                            Le invitamos a probar nuestro asado de cordero, o nuestro exquisito arroz y conejo, no dude en visitarnos!.</br></br>

                                            Si desea obtener más información sobre nuestros productos, no deje de pasar por la sección de Carta.
                                        </p>
                                    </div><!-- /.col -->

                                </div><!-- /.row -->


                                <div class="row inner-top-xs">

                                    <div class="col-md-8 col-sm-9 center-block text-center">
                                     @if(isset($restaurante) && $restaurante !== null)
                                        <h3>Características</h3>
                                        <ul class="widget-tag">
                                            @foreach($features as $feature)
                                                @if($restaurante_features->contains('feature_id',$feature->id))
                                                <li><a>{{$feature->name}}</a></li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </div><!-- /.col -->
                                    @endif
                                    <div class="col-sm-6 inner-left-xs inner-bottom-xs">
                                    </div><!-- /.col -->

                                </div><!-- /.row -->


                            </div>

                            <div class="tab-content" id="tab-4">
                                <div class="row">
                                    <div class="col-md-8 col-sm-9 center-block text-center">
                                        <a href="{{route('website.setFavorite',$subsidiary->company()->get()[0]->id)}}" data-create="Añadido a favoritos" class="btn btn-large purple-bg">Marcar como Favorito</a>
                                        <h3>Contacto</h3>
                                        <p>Envíanos un email con lo que necesites o ponte en contacto con nosotros al teléfono que a continuación indicamos.</p>
                                        <ul class="contacts">
                                            <li><i class="icon-location contact"></i> {{$subsidiary->address}}</li>
                                            <li><i class="icon-globe-1 contact"></i> {{$subsidiary->location()->get()[0]->name}}, {{$subsidiary->location()->get()[0]->province()->get()[0]->name}}</li>
                                            <li><i class="icon-mobile contact"></i> {{$subsidiary->phone}}</li>
                                            <li><a href="#"><i class="icon-mail-1 contact"></i> {{$subsidiary->email}}</a></li>
                                        </ul><!-- /.contacts -->
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div><!-- /.tab-content -->


                            <div class="tab-content" id="tab-5">

                                <!-- --> <div class="row">
                                    <div class="col-md-5 col-sm-6 col-xs-8 center-block text-center">
                                        <figure><img src="assets/images/art/product03.jpg" alt=""
                                                     style="max-width:200px"></figure>
                                    </div>
                                </div><!-- /.row -->

                                <div class="row">
                                    <div class="col-sm-8 center-block text-center inner-top-xs">
                                        <h3>Carta de productos</h3>
                                        <a href="#"><p>Descargar carta en pdf</p></a>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->

                                <div class="row inner-top-sm text-center" style="padding-top:20px">

                                    <div class="col-sm-6 inner-bottom-sm inner-left inner-right">
                                        <figure class="member">



                                            <figcaption style="text-align:left">

                                                <h3>
                                                    Ensaladas
                                                </h3>

                                                <span class="alignleft"> Ensalada murciana</span>
                                                <span class="alignright">8,00 €</span>
                                                </br>
                                                <span class="alignleft"> Ensalada Caprese</span>
                                                <span class="alignright">6,50 €</span>
                                                </br>
                                                <span style="color:#888;font-size:12px; padding-left:10px">Rodajas de tomate, orégano, queso caprese y jamón york</span>
                                                </br></br>

                                                <h3> Carnes </h3>
                                                <span>Entrecot a la brasa</span></br>
                                                <span>Arroz con conejo</span></br></br>

                                                <h3> Pescados </h3>
                                                <span>Lubina a la espalda</span></br>
                                                <span>Salmón a la miel con cebolla confitada</span></br></br>

                                                <h3> Pastas </h3>
                                                <span>Risotto al roquefort</span></br>
                                                <span>Espagueti a la boloñesa</span></br>
                                                <span>Pastel de berenjenas</span></br></br>



                                            </figcaption>

                                        </figure>
                                    </div><!-- /.col -->

                                    <div class="col-sm-6 inner-bottom-sm inner-left inner-right">
                                        <figure class="member">



                                            <figcaption style="text-align:left;font-family:Arial, Helvetica, sans-serif">

                                                <h3>
                                                    Bebidas
                                                </h3>

                                                <span class="alignleft"> Caña</span>
                                                <span class="alignright">2,00 €</span>
                                                </br>

                                                <span class="alignleft"> Jarra</span>
                                                <span class="alignright">5,00 €</span>
                                                </br>

                                                <span class="alignleft"> Refresco</span>
                                                <span class="alignright">2,00 €</span>
                                                </br></br>

                                                <h3> Vinos </h3>
                                                <span class="alignleft">Altos de Luzón</span>
                                                <span class="alignright">15,50 €</span>
                                                </br>
                                                <span style="color:#888;font-size:12px; padding-left:10px">Jumilla</span>
                                                </br>

                                                <span class="alignleft">Protos</span>
                                                <span class="alignright">16,50 €</span>
                                                </br>
                                                <span style="color:#888;font-size:12px; padding-left:10px">Ribera del Duero</span>
                                                </br></br>

                                                <h3> Postres </h3>
                                                <span class="alignleft"> Tiramisú</span>
                                                <span class="alignright">4,50 €</span>
                                                </br>

                                                <span class="alignleft"> Piezas de fruta</span>
                                                <span class="alignright">4,50 €</span>
                                                </br>

                                                <span class="alignleft"> Tarta de la abuela</span>
                                                <span class="alignright">4,50 €</span>
                                                </br></br>

                                            </figcaption>

                                        </figure>
                                    </div><!-- /.col -->

                                </div><!-- /.row -->


                            </div><!-- /.tab-content -->

                            <div class="tab-content" id="tab-6">

                                <div class="row">
                                    <div class="col-sm-8 center-block text-center">

                                        <h2>Mira lo que hacemos...</h2>
                                        <p class="single-line">Ven y disfruta con nosotros de nuestros mejores platos.</p>

                                        <div id="owl-work-samples" class="owl-carousel">

                                            <div class="item">
                                                <a href="negocio.html">
                                                    <figure>
                                                        <figcaption class="text-overlay">
                                                            <div class="info">
                                                                <h4>Astor & Yancy</h4>
                                                                <p>Identity</p>
                                                            </div><!-- /.info -->
                                                        </figcaption>
                                                        <img src="assets/images/art/work09.jpg" alt="">
                                                    </figure>
                                                </a>
                                            </div><!-- /.item -->

                                            <div class="item">
                                                <a href="negocio.html">
                                                    <figure>
                                                        <figcaption class="text-overlay">
                                                            <div class="info">
                                                                <h4>Signwall</h4>
                                                                <p>Identity</p>
                                                            </div><!-- /.info -->
                                                        </figcaption>
                                                        <img src="assets/images/art/work16.jpg" alt="">
                                                    </figure>
                                                </a>
                                            </div><!-- /.item -->

                                            <div class="item">
                                                <a href="negocio.html">
                                                    <figure>
                                                        <figcaption class="text-overlay">
                                                            <div class="info">
                                                                <h4>Tri Fold Brochure</h4>
                                                                <p>Print</p>
                                                            </div><!-- /.info -->
                                                        </figcaption>
                                                        <img src="assets/images/art/work10.jpg" alt="">
                                                    </figure>
                                                </a>
                                            </div><!-- /.item -->

                                            <div class="item">
                                                <a href="negocio.html">
                                                    <figure>
                                                        <figcaption class="text-overlay">
                                                            <div class="info">
                                                                <h4>Vintage Bicycles</h4>
                                                                <p>Interactive</p>
                                                            </div><!-- /.info -->
                                                        </figcaption>
                                                        <img src="assets/images/art/work03.jpg" alt="">
                                                    </figure>
                                                </a>
                                            </div><!-- /.item -->

                                            <div class="item">
                                                <a href="negocio.html">
                                                    <figure>
                                                        <figcaption class="text-overlay">
                                                            <div class="info">
                                                                <h4>Simpli Nota</h4>
                                                                <p>Identity</p>
                                                            </div><!-- /.info -->
                                                        </figcaption>
                                                        <img src="assets/images/art/work04.jpg" alt="">
                                                    </figure>
                                                </a>
                                            </div><!-- /.item -->

                                            <div class="item">
                                                <a href="negocio.html">
                                                    <figure>
                                                        <figcaption class="text-overlay">
                                                            <div class="info">
                                                                <h4>Vinyl Records</h4>
                                                                <p>Identity</p>
                                                            </div><!-- /.info -->
                                                        </figcaption>
                                                        <img src="{{asset('images/art/work07.jpg')}}" alt="">
                                                    </figure>
                                                </a>
                                            </div><!-- /.item -->

                                            <div class="item">
                                                <a href="negocio.html">
                                                    <figure>
                                                        <figcaption class="text-overlay">
                                                            <div class="info">
                                                                <h4>Embroidered</h4>
                                                                <p>Identity</p>
                                                            </div><!-- /.info -->
                                                        </figcaption>
                                                        <img src="{{asset('images/art/work05.jpg')}}" alt="">
                                                    </figure>
                                                </a>
                                            </div><!-- /.item -->

                                            <div class="item">
                                                <a href="negocio.html">
                                                    <figure>
                                                        <figcaption class="text-overlay">
                                                            <div class="info">
                                                                <h4>El Corcho</h4>
                                                                <p>Identity</p>
                                                            </div><!-- /.info -->
                                                        </figcaption>
                                                        <img src="{{asset('images/art/work14.jpg')}}" alt="">
                                                    </figure>
                                                </a>
                                            </div><!-- /.item -->

                                        </div><!-- /.owl-carousel -->

                                    </div><!-- /.col -->



                                </div><!-- /.row -->

                            </div><!-- /.tab-content -->

                        </div><!-- /.panel-container -->

                    </div><!-- /.tabs -->
                </div><!-- /.col -->
            </div><!-- /.row -->



        </div><!-- /.container -->
    </section>

    <!-- ============================================================= SECTION – CIRCLE TABS : END ============================================================= -->



    <!-- ============================================================= SECTION – CONTACT FORM ============================================================= -->

    <section  id="reserva" class="light-bg">
        <div class="container inner">

            <div class="row">
                <div class="col-md-8 col-sm-9 center-block text-center">
                    <header>
                        <h1>Reserva una mesa</h1>
                        <p>¿Quieres disfrutar de nuestros menús?</p>
                    </header>
                </div><!-- /.col -->
            </div><!-- /.row -->

            <div class="row">
                <div class="col-sm-12">
                    <div class="row">

                        <div class="col-sm-6 outer-top-md inner-right-sm">

                            <h2>Formulario de reserva</h2>

                            @include('mimenudeldiaweb.forms.reserva')

                            <div id="response"></div>

                        </div><!-- ./col -->

                        <div class="col-sm-6 outer-top-md inner-left-sm border-left">

                            <h2>Contacto</h2>
                            <p>Déjanos tus comentarios o cualquier cosa que necesites decirnos.</p>

                            <h3>Restaurante Domingo</h3>
                            <a href="{{route('website.setFavorite',$subsidiary->company()->get()[0]->id)}}" data-create="Añadido a favoritos" class="btn btn-large purple-bg">Marcar como Favorito</a>
                            <ul class="contacts">
                                <li><i class="icon-location contact"></i> {{$subsidiary->address}}</li>
                                <li><i class="icon-globe-1 contact"></i> {{$subsidiary->location()->get()[0]->name}}, {{$subsidiary->location()->get()[0]->province()->get()[0]->name}}</li>
                                <li><i class="icon-mobile contact"></i> {{$subsidiary->phone}}</li>
                                <li><a href="#"><i class="icon-mail-1 contact"></i> {{$subsidiary->email}}</a></li>
                                <li>
                                    <i class="icon-clock contact"></i>
                                    Horario:
                                    <ul>
                                @foreach($subsidiary->schedules()->get() as $schedule)
                                <li><i class="icon-dot   contact"></i>{{$weekdays[$schedule->day]}}&nbsp;{{$schedule->start_at}} a  {{$schedule->finish_at}}</li>
                                @endforeach
                                    </ul>
                                </li>
                            </ul><!-- /.contacts -->
                            <div class="social-network">
                                <h3>Social</h3>
                                <ul class="social">
                                    <li><a href="#"><i class="icon-s-facebook"></i></a></li>
                                    <li><a href="#"><i class="icon-s-gplus"></i></a></li>
                                    <li><a href="#"><i class="icon-s-twitter"></i></a></li>
                                    <li><a href="#"><i class="icon-s-pinterest"></i></a></li>
                                    <li><a href="#"><i class="icon-s-behance"></i></a></li>
                                    <li><a href="#"><i class="icon-s-dribbble"></i></a></li>
                                </ul><!-- /.social -->
                            </div><!-- /.social-network -->

                        </div><!-- /.col -->

                    </div><!-- /.row -->
                </div><!-- /.col -->
            </div><!-- /.row -->

        </div><!-- /.container -->
    </section>

    <!-- ============================================================= SECTION – CONTACT FORM : END ============================================================= -->


    <!-- ============================================================= SECTION – MAP ============================================================= -->

    <section id="map" class="height-sm"></section>

    <!-- ============================================================= SECTION – MAP : END ============================================================= -->


    <!-- ============================================================= SECTION – BUY TEMPLATE ============================================================= -->

    <section id="filters" class="light-bg">
        <div class="container inner-sm">
            <div class="row">
                <div class="col-md-8 col-sm-9 center-block text-center">
                    <header>
                        <h1>Negocios destacados</h1>
                        <p>A continuación se muestran una serie de negocios de tu zona que pueden serte de interés.</p>
                    </header>
                </div><!-- /.col -->
            </div><!-- /.row -->

            <div class="row">

                <div class="col-sm-12 inner-top-md">
                    <div id="owl-videos" class="owl-carousel owl-outer-nav owl-ui-md owl-item-gap">

                        @foreach($subsidiary->location()->get() as $location)
                            @foreach($location->subsidiaries()->get() as $subsidiary)
                        <div class="item">
                            <figure>

                                <div class="icon-overlay icn-link">
                                    <a href="negocio.html"><img src="
                                    @if($img = $subsidiary->company()->get()[0]->logo()->get())
                                            @if(isset($img[0])) {{$img[0]->url}} @endif
                                    @endif" alt=""></a>
                                </div><!-- /.icon-overlay -->

                                <figcaption class="bordered no-top-border">
                                    <div class="info">
                                        <h4><a href="negocio.html">{{$subsidiary->company()->get()[0]->name}},{{$subsidiary->address}}</a></h4>
                                        <p>Interactive</p>
                                    </div><!-- /.info -->
                                </figcaption>
                            </figure>
                        </div><!-- /.item -->
                            @endforeach
                            @endforeach
                    </div><!-- /.owl-carousel -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>
@stop
