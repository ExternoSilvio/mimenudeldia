@extends('mimenudeldiaweb/master')

@section('title')
    mi menú del día
@endsection
@section('header')
    <header>
        <div class="navbar">

            <div class="navbar-header">
                <div class="container">

                    <ul class="info pull-left">
                        <li><a href="#"><i class="icon-mail-1 contact"></i> info@mimenudeldia.es</a></li>
                        <li><i class="icon-mobile contact"></i> +34 616 716 777</li>
                    </ul><!-- /.info -->

                    <ul class="info pull-right">
                        <li><a href="#" class="toggle-menu menu-right push-body jPushMenuBtn"><i class="icon-user-md contact"></i>Iniciar sesión</a></li>

                    </ul>
                    <ul class="social pull-right">
                        <li><a href="#"><i class="icon-s-facebook"></i></a></li>
                        <li><a href="#"><i class="icon-s-gplus"></i></a></li>
                        <li><a href="#"><i class="icon-s-twitter"></i></a></li>
                        <li><a href="#"><i class="icon-s-pinterest"></i></a></li>
                        <li><a href="#"><i class="icon-s-behance"></i></a></li>
                        <li><a href="#"><i class="icon-s-dribbble"></i></a></li>
                    </ul><!-- /.social -->

                    <!-- ============================================================= LOGO MOBILE ============================================================= -->

                    <a class="navbar-brand-mobile" href="{{route('website.index')}}"><img src="{{asset('images/logo.png')}}" class="logo" alt=""></a>

                    <!-- ============================================================= LOGO MOBILE : END ============================================================= -->

                    <a class="btn responsive-menu pull-right" data-toggle="collapse" data-target=".navbar-collapse"><i class='icon-menu-1'></i></a>

                </div><!-- /.container -->
            </div><!-- /.navbar-header -->

            <div class="yamm">
                <div class="navbar-collapse collapse">
                    <div class="container">

                        <!-- ============================================================= LOGO ============================================================= -->

                        <a class="navbar-brand" href="{{route('website.index')}}"><img src="{{asset('images/logo.png')}}" class="logo" alt=""></a>

                        <!-- ============================================================= LOGO : END ============================================================= -->


                        <!-- ============================================================= MAIN NAVIGATION ============================================================= -->

                        <ul class="nav navbar-nav">

                            <li class="dropdown-submenu">
                                <a href="{{route('website.index')}}" @if(url()->current() === url('/').'/inde') style="color:white;background-color:rgba(63,141,191,1)" @endif>Menús</a>
                            </li><!-- /.dropdown -->

                            <li class="dropdown-submenu">
                                <a href="{{route('website.restaurantes')}}" @if(url()->current() === url('/').'/restaurantes') style="color:white;background-color:rgba(63,141,191,1)@endif">Restaurantes</a>
                            </li><!-- /.dropdown -->

                            <li class="dropdown-submenu">
                                <a href="{{route('website.negocios')}}" @if(url()->current() === url('/').'/negocios') style="color:white;background-color:rgba(63,141,191,1)" @endif>Partners</a>
                            </li><!-- /.dropdown -->

                            <li class="dropdown-submenu">
                                <a href="{{route('website.appmovil')}}" @if(url()->current() === url('/').'/appMovil') style="color:white;background-color:rgba(63,141,191,1)" @endif>App móvil</a>
                            </li><!-- /.dropdown -->

                            <li class="dropdown-submenu">
                                <a href="{{route('website.contacto')}}" @if(url()->current() === url('/').'/contacto') style="color:white;background-color:rgba(63,141,191,1)" @endif>Contacto</a>
                            </li><!-- /.dropdown -->

                            <li class="dropdown-submenu">
                                <a class="toggle-menu menu-right push-body jPushMenuBtn"><i class="icon-menu-1"></i></a>
                            </li>

                            <li class="dropdown pull-right searchbox">
                                <a href="#" class="dropdown-toggle js-activated"><i class="icon-search"></i></a>

                                <div class="dropdown-menu">
                                    <form id="search" class="navbar-form search" role="search">
                                        <input type="search" class="form-control" placeholder="Type to search">
                                        <button type="submit" class="btn btn-default btn-submit icon-right-open"></button>
                                    </form>
                                </div><!-- /.dropdown-menu -->
                            </li><!-- /.searchbox -->

                        </ul><!-- /.nav -->

                        <!-- ============================================================= MAIN NAVIGATION : END ============================================================= -->

                    </div><!-- /.container -->
                </div><!-- /.navbar-collapse -->
            </div><!-- /.yamm -->
        </div><!-- /.navbar -->
    </header>
@stop
@section('footer')
    <section class="side-menu cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right">
        <a class="menu-close toggle-menu menu-right push-body"><i class="icon icon-cancel-1"></i></a>

        <h5 style="color:white">Usuarios</h5>
            <h5 style="color:white">Inicia Sesión</h5>
            <div class="sign-in">
                <input class="input-sm form-full" type="email" aria-required="true" id="email" name="email" placeholder="Email" value="">
                <input class="input-sm form-full" type="password" aria-required="true" id="password" name="password" placeholder="Password" value="">
                <input type="submit" class="btn btn-md btn-color-b form-full" value="Entrar">
                <a>¿Eres nuevo?</a>
            </div>

            <h4 style="color:white">Restaurantes y Partners</h4>
                <h5 style="color:white">Inicia Sesión</h5>
                <div class="sign-in">
                    <input class="input-sm form-full" type="email" aria-required="true" id="email2" name="email" placeholder="Email" value="">
                    <input class="input-sm form-full" type="password" aria-required="true" id="password2" name="password" placeholder="Password" value="">
                    <input type="submit" class="btn btn-md btn-color-b form-full" value="Entrar">
                    <a>¿Eres nuevo?</a>
                </div>
    </section>
    <footer class="dark-bg">
        <div class="container inner">
            <div class="row">

                <div class="col-md-3 col-sm-6 inner">
                    <h4>Quiénes somos</h4>
                    <a href="index.html"><img class="logo img-intext" src="{{asset('images/logo-white.png')}}" alt=""
                                              style="max-height:70px"></a>
                    <p>DigitalApp es una empresa de desarrollo de apps móviles para negocios con el objetivo de beneficiar al máximo a nuestros clientes de las últimas tecnologías y, sobre todo, del auge en el uso de los dispositivos móviles.</p>
                    <a href="http://www.digitalapp.es" class="txt-btn">Más acerca de nosostros</a>
                </div><!-- /.col -->

                <div class="col-md-3 col-sm-6 inner">
                    <h4>Últimos Registrados</h4>

                    <div class="row thumbs gap-xs">

                        @foreach(\App\Http\Controllers\WebSite\RestauranteController::findLast(4) as $lastRegister)

                        <div class="col-xs-6 thumb">
                            <figure class="icon-overlay icn-link">
                                <a href="{{route('website.restaurante',$lastRegister->id)}}"><img src="@if(isset($lastRegister->company()->get()[0]->logo()->get()[0])){{$lastRegister->company()->get()[0]->logo()->get()[0]->url}} @endif" alt=""></a>
                            </figure>
                        </div><!-- /.thumb -->
                        @endforeach
                    </div><!-- /.row -->
                </div><!-- /.col -->


                <div class="col-md-3 col-sm-6 inner">
                    <h4>Contacto</h4>
                    <p>En DigitalApp nos ponemos a su disposición para resolver cualquier duda o consulta que tenga.</p>
                    <ul class="contacts">
                        <li><i class="icon-location contact"></i> Avenida del Golf, Nº5, 3º E</li>
                        <li><i class="icon-globe-1 contact"></i> Altorreal, Murcia, España</li>
                        <li><i class="icon-mobile contact"></i> +34 616 716 777</li>
                        <li><a href="#"><i class="icon-mail-1 contact"></i> info@mimenudeldia.es</a></li>
                    </ul><!-- /.contacts -->
                </div><!-- /.col -->

                <div class="col-md-3 col-sm-6 inner">
                    <h4>Subscríbete</h4>
                    <p>Déjanos tu email para recibir noticias e info sobre MiMenúDelDía.</p>
                    <form id="newsletter" class="form-inline newsletter" role="form">
                        <label class="sr-only" for="exampleInputEmail">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail" placeholder="Indique su email">
                        <button type="submit" class="btn btn-default btn-submit">Subscríbete</button>
                    </form>
                </div><!-- /.col -->

            </div><!-- /.row -->
        </div><!-- .container -->

        <div class="footer-bottom">
            <div class="container inner">
                <p class="pull-left">© 2014 DigitalApp. All rights reserved.</p>
                <ul class="footer-menu pull-right">
                    <li><a href="{{route('website.index')}}">Menús</a></li>
                    <li><a href="{{route('website.restaurantes')}}">Restaurantes</a></li>
                    <li><a href="{{route('website.negocios')}}">Partners</a></li>
                    <li><a href="{{route('website.appmovil')}}">App móvil</a></li>
                    <li><a href="{{route('website.contacto')}}">Contacto</a></li>
                </ul><!-- .footer-menu -->
            </div><!-- .container -->
        </div><!-- .footer-bottom -->
    </footer>
@stop

