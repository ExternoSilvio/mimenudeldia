@extends('mimenudeldiaweb/page')

@section('main')
    <!-- ============================================================= SECTION – CIRCLE TABS ============================================================= -->

    <!-- ============================================================= SECTION – HERO ============================================================= -->

    <section id="hero" class=" img-bg img-bg-softer" style="background-image: url(assets/images/art/slider07.jpg);">
        <div class="container inner ">
            <div class="row">
                <div class="caption vertical-top text-center">
                    <header>
                        <h1 class="fadeInUp-1 dark-bg light-color">{{$subsidiary->company()->get()->first()->name}}</h1>
                        <p class="fadeInUp-2 light-color navy-bg" style="font-weight:bold">{{$subsidiary->company()->get()->first()->information}}</p>

                    </header>


                </div><!-- /.col -->

            </div><!-- ./row -->
        </div><!-- /.container -->
    </section>

    <!-- ============================================================= SECTION – HERO : END ============================================================= -->

    <section id="circle-tabs">
        <div class="container inner">



            <div class="row">
                <div class="col-xs-12 inner-top">
                    <div class="tabs tabs-services tabs-circle-top tab-container">

                        <ul class="etabs text-center">
                            <li class="tab"><a href="#tab-1"><div><i class="icon-info"></i></div>Empresa</a></li>
                            <li class="tab"><a href="#tab-2"><div><i class="icon-eye-1"></i></div>Productos</a></li>
                            <li class="tab"><a href="#tab-3"><div><i class="icon-network"></i></div>Enlaces</a></li>

                        </ul><!-- /.etabs -->

                        <div class="panel-container">

                            <div class="tab-content" id="tab-1">
                                <div class="row">

                                    <div class="col-md-5 col-md-offset-1 col-sm-6 inner-right-xs">
                                        <figure><img src="assets/images/art/alimentacion.jpg" alt=""></figure>
                                    </div><!-- /.col -->

                                    <div class="col-md-5 col-sm-6 inner-top-xs inner-left-xs">
                                        <h3>La empresa</h3>
                                        <p>Más de 20 años de experiencia nos avalan y nos colocan a la cabeza de las empresas distribuidoras alimenticias de España.<br></br>

                                            Además de alimentos, ofrecemos bebidas de nuestra propia elaboración como horchatas de chufa natural, granizados de múltiples sabores, leche merengada, batidos y concentrados para preparar granizados.</br></br>

                                            Contamos con servicio técnico para nuestros clientes así como con una red distributiva a lo largo de todo el territorio nacional gracias a nuestra flota de vehículos. Somos una de las empresas líderes del sector gracias a la confianza de nuestros clientes.</br></br>

                                            Si desea obtener un prespuesto o más información sobre nuestros productos, no dude en contactarnos por este medio gratuitamente.
                                        </p>
                                    </div><!-- /.col -->

                                </div><!-- /.row -->



                                <div class="row inner-top-xs">

                                    <div class="col-sm-6 inner-right-xs inner-bottom-xs">
                                        <h2>¿Quiénes somos?</h2>
                                        <p>Magnis modipsae que lib voloratati andigen daepeditem quiate ut reporemni aut labor. Laceaque quiae sitiorem rest non restibusaes es tumquam core posae volor remped modis volor. Doloreiur qui commolu ptatemp dolupta oreprerum tibusam. Eumenis et consent accullignis dentibea autem inisita posae volor conecus resto explabo.</p>
                                    </div><!-- /.col -->

                                    <div class="col-sm-6 inner-left-xs inner-bottom-xs">
                                        <h2>¿Qué hacemos?</h2>
                                        <p>Laceaque quiae sitiorem rest non restibusaes es tumquam core posaeremni volor remped modis volor. Doloreiur qui commolu dolor oreprerum tibusam. Eumenis et consent accullignis dentibea autem inisita posae volor conecus resto explabo. Magnis modipsae que lib voloratati andigen daepeditem quiate ut reporemni aut labor.</p>
                                    </div><!-- /.col -->

                                </div><!-- /.row -->


                            </div><!-- /.tab-content -->

                            <div class="tab-content" id="tab-2">
                                <div class="row">
                                    <div class="col-sm-8 center-block text-center">

                                        <h2>Nuestras ofertas</h2>
                                        <p class="single-line">Observe nuestros productos y ofertas, y no dude en ponerse en contacto con nosotros para más información.</p>

                                        <div id="owl-work" class="owl-carousel owl-inner-nav owl-ui-md">
                                            @foreach($subsidiary->company()->get()[0]->dishes()->get() as $dish)
                                            <div class="item">
                                                <figure class="member">
                                                    <div class="member-image">
                                                        <div class="text-overlay">
                                                            <div class="info">
                                                                <h4><a href="#">@if( count($dish->category()->get()) ) {{$dish->category()->get()[0]->name}} @else No tiene categoría @endif</a></h4>
                                                                <p>@if(count($dish->offers()->get())) Esta de oferta @endif</p>
                                                                <p>Info 2</p>
                                                                <p>Info 3</p>
                                                            </div><!-- /.info -->
                                                        </div><!-- /.text-overlay -->
                                                        <img src="@if(count($dish->photo()->get())) {{asset('storage/'.str_after($dish->photo()->get()[0]->url,'public'))}} @else  {{asset('images/art/work14.jpg') }} @endif" >
                                                    </div><!-- /.member-image -->

                                                    <figcaption class="bordered no-top-border">
                                                        <div class="info">
                                                            <h4><a href="#">{{$dish->name}}</a></h4>
                                                            <p>Comentarios sobre la imagen que se está mostrando; este comentario puede tener varias líneas de altura.</p>
                                                        </div><!-- /.info -->
                                                    </figcaption>
                                                </figure>
                                            </div><!-- /.item -->
                                            @endforeach
                                        </div><!-- /.owl-carousel -->
                                    </div>
                                </div><!-- /.row -->
                            </div><!-- /.tab-content -->

                            <div class="tab-content" id="tab-3">
                                <div class="row">
                                    <div class="col-md-8 col-sm-9 center-block text-center">
                                        <h3>Links y redes sociales</h3>
                                        <p>Utilice los siguientes links para ponerse en contacto con GMS Alimentación.</p>
                                        <ul class="contacts">
                                            <li><a href="#"><i class="icon-globe-1 contact"></i> {{$subsidiary->company()->get()[0]->web}}</a></li>
                                            <li><a href="#"><i class="icon-twitter contact"></i> twitter</a></li>
                                            <li><a href="#"><i class="icon-facebook contact"></i> facebook</a></li>
                                            <li><a href="mailto:info@grupogmsalimentacion.com"><i class="icon-mail-1 contact"></i> {{$subsidiary->email}}</a></li>

                                            <li><a href="#"><i class="icon-basket-1 contact"></i> Catálogo de productos</a></li>
                                            <li><i class="icon-mobile contact"></i> +34 555 555 777</li>

                                        </ul><!-- /.contacts -->
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div><!-- /.tab-content -->



                        </div><!-- /.panel-container -->

                    </div><!-- /.tabs -->
                </div><!-- /.col -->
            </div><!-- /.row -->

        </div><!-- /.container -->
    </section>

    <!-- ============================================================= SECTION – CIRCLE TABS : END ============================================================= -->

    <!-- ============================================================= SECTION – CONTACT FORM ============================================================= -->

    <section id="contact-form" class="light-bg">
        <div class="container inner">

            <div class="row">
                <div class="col-md-8 col-sm-9 center-block text-center">
                    <header>
                        <h1>Contacto</h1>
                        <p>Ponte en contacto con nosotros y te resolveremos cualquier duda o consulta.</p>
                    </header>
                </div><!-- /.col -->
            </div><!-- /.row -->

            <div class="row">
                <div class="col-sm-12">
                    <div class="row">

                        <div class="col-sm-6 outer-top-md inner-right-sm">

                            <h2>Déjanos tu mensaje</h2>
                                @include('mimenudeldiaweb.forms.contacto')
                            <div id="response"></div>

                        </div><!-- ./col -->

                        <div class="col-sm-6 outer-top-md inner-left-sm border-left">

                            <h2>Contacto</h2>

                            <h3>{{$subsidiary->company()->get()[0]->name}}</h3>
                            <ul class="contacts">
                                <li><i class="icon-location contact"></i> {{$subsidiary->address}}</li>
                                <li><i class="icon-globe-1 contact"></i> {{$subsidiary->location()->get()[0]->name}}</li>
                                <li><i class="icon-mobile contact"></i>{{$subsidiary->phone}}</li>
                                <li><a href="mailto:info@grupogmsalimentacion.com"><i class="icon-mail-1 contact"></i> {{$subsidiary->email}}</a></li>
                            </ul><!-- /.contacts -->

                            <div class="social-network">
                                <h3>Social</h3>
                                <ul class="social">
                                    <li><a href="#"><i class="icon-s-facebook"></i></a></li>
                                    <li><a href="#"><i class="icon-s-gplus"></i></a></li>
                                    <li><a href="#"><i class="icon-s-twitter"></i></a></li>
                                    <li><a href="#"><i class="icon-s-pinterest"></i></a></li>
                                    <li><a href="#"><i class="icon-s-behance"></i></a></li>
                                    <li><a href="#"><i class="icon-s-dribbble"></i></a></li>
                                </ul><!-- /.social -->
                            </div><!-- /.social-network -->

                        </div><!-- /.col -->

                    </div><!-- /.row -->
                </div><!-- /.col -->
            </div><!-- /.row -->

        </div><!-- /.container -->
    </section>

    <!-- ============================================================= SECTION – CONTACT FORM : END ============================================================= -->


    <!-- ============================================================= SECTION – MAP ============================================================= -->

    <section id="map" class="height-sm"></section>

    <!-- ============================================================= SECTION – MAP : END ============================================================= -->
@stop