@if(!($paginate >= $size))
<ul class="pagination">
    @if(request('page') !== null)
     @if(request('page') != 1)
         <li>
             <a href="{{url()->current()}}?location={{request('location')}}&province={{request('province')}}&page={{request('page') -1 }}#carousels">Previus</a>
         </li>
     @endif
    @endif
        @for($i = 1 ; $i <=  ceil($size / $paginate); $i++)
            <li class=" @if(request('page') == $i) active @elseif(!request('page') && $i ==1 ) active }}@endif">
                <a href="{{url()->current()}}?location={{request('location')}}&province={{request('province')}}&page={{$i}}#carousels">{{$i}}</a>
            </li>
        @endfor
         @if(!(request('page') >= ceil($size / $paginate)))
             <li>
                 <a href="{{url()->current()}}?location={{request('location')}}&province={{request('province')}}&page={{request('page') + 1 }}#carousels">Next</a>
             </li>
         @endif
    </ul>
@endif
