@extends('mimenudeldiaweb/page')

@section('main')
    <!-- ============================================================= SECTION – HERO ============================================================= -->

    <section id="hero" class="light-bg img-bg img-bg-softer" style="background-image: url({{asset('images/art/image-background04.jpg')}});">
        <div class="container inner">
            <div class="row">

                <div class="caption vertical-center text-right">
                    <h1 class="fadeInDown-1 text-warning"><span>Contacta con nosotros</span></h1>
                    <p class="fadeInRight-2 light-color navy-bg" style="font-weight:bold">¿Quieres saber más? ¡Estaremos encantados de escucharte!</p>
                </div><!-- /.caption -->

            </div><!-- ./row -->
        </div><!-- /.container -->

        <section id="contacto">
        </section>
    </section>

    <!-- ============================================================= SECTION – HERO : END ============================================================= -->

    <div class="container inner">
        <div class="row">

            <div class="col-md-8 inner-right-md">

                <!-- ============================================================= SECTION – CONTACT FORM ============================================================= -->

                <section id="contact-form">

                    <h2>Déjanos tu mensaje</h2>
                    @include('mimenudeldiaweb.forms.contacto')
                    <div id="response"></div>

                </section>



                <!-- ============================================================= SECTION – CONTACT FORM : END ============================================================= -->


                <!-- ============================================================= SECTION – CONTACTS ============================================================= -->

                <section id="contacts" class="inner-top-md inner-bottom-sm">

                    <h2>Localización</h2>
                    <p>¿Quieres conocer que hay detrás de las cámaras? <br>Visita nuestras instalaciones.</p>

                    <div class="row">
                        <div class="col-sm-6">
                            <h3>MiMenúDelDía</h3>
                            <ul class="contacts">
                                <li><i class="icon-location contact"></i> C\Avenida Campo de Golf, Nº 5, 3º E</li>
                                <li><i class="icon-globe-1 contact"></i> Altorreal, Murcia, España</li>
                                <li><i class="icon-mobile contact"></i> +34 616 716 777</li>
                                <li><a href="mailto:info@mimenudeldia.es"><i class="icon-mail-1 contact"></i> info@mimenudeldia.es</a></li>
                            </ul><!-- /.contacts -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->

                </section>

                <!-- ============================================================= SECTION – CONTACTS : END ============================================================= -->

            </div><!-- ./col -->

            <div class="col-md-4">

                <!-- ============================================================= SECTION – CONTACT NAMES ============================================================= -->

                <section id="contact-names" class="light-bg inner-xs inner-left-xs inner-right-xs">

                    <h3 class="team-headline sidelines text-center"><span>Equipo</span></h3>

                    <div class="row thumbs gap-md text-center">

                        <div class="col-sm-6 thumb">
                            <figure class="member">

                                <div class="member-image">

                                    <div class="text-overlay">
                                        <div class="info">
                                            <ul class="social">
                                                <li><a href="#"><i class="icon-s-facebook"></i></a></li>
                                                <li><a href="#"><i class="icon-s-gplus"></i></a></li>
                                                <li><a href="#"><i class="icon-s-twitter"></i></a></li>
                                            </ul><!-- /.social -->
                                        </div><!-- /.info -->
                                    </div><!-- /.text-overlay -->

                                    <img src="{{asset('images/art/human06.jpg')}}">

                                </div><!-- /.member-image -->

                                <figcaption class="member-details bordered no-top-border">
                                    <h3>
                                        Leandro Franco
                                        <span>Administrator Account</span>
                                    </h3>
                                </figcaption>

                            </figure>
                        </div><!-- /.col -->

                        <div class="col-sm-6 thumb">
                            <figure class="member">

                                <div class="member-image">

                                    <div class="text-overlay">
                                        <div class="info">
                                            <ul class="social">
                                                <li><a href="#"><i class="icon-s-facebook"></i></a></li>
                                                <li><a href="#"><i class="icon-s-gplus"></i></a></li>
                                                <li><a href="#"><i class="icon-s-twitter"></i></a></li>
                                            </ul><!-- /.social -->
                                        </div><!-- /.info -->
                                    </div><!-- /.text-overlay -->

                                    <img src="{{asset('images/art/fio.jpg')}}">

                                </div><!-- /.member-image -->

                                <figcaption class="member-details bordered no-top-border">
                                    <h3>
                                        Fiorella Mendoza
                                        <span>Sales </br>Manager</span>
                                    </h3>
                                </figcaption>

                            </figure>
                        </div><!-- /.col -->

                    </div><!-- /.row -->

                    <div class="row thumbs gap-md text-center">

                        <div class="col-sm-6 thumb">
                            <figure class="member">

                                <div class="member-image">

                                    <div class="text-overlay">
                                        <div class="info">
                                            <ul class="social">
                                                <li><a href="#"><i class="icon-s-facebook"></i></a></li>
                                                <li><a href="#"><i class="icon-s-gplus"></i></a></li>
                                                <li><a href="#"><i class="icon-s-twitter"></i></a></li>
                                            </ul><!-- /.social -->
                                        </div><!-- /.info -->
                                    </div><!-- /.text-overlay -->

                                    <img src="{{asset('images/art/jero.jpg')}}">

                                </div><!-- /.member-image -->

                                <figcaption class="member-details bordered no-top-border">
                                    <h3>
                                        Jerónimo Cánovas
                                        <span>Product </br> Manager</span>
                                    </h3>
                                </figcaption>

                            </figure>
                        </div><!-- /.col -->

                        <div class="col-sm-6 thumb">
                            <figure class="member">

                                <div class="member-image">

                                    <div class="text-overlay">
                                        <div class="info">
                                            <ul class="social">
                                                <li><a href="#"><i class="icon-s-facebook"></i></a></li>
                                                <li><a href="#"><i class="icon-s-gplus"></i></a></li>
                                                <li><a href="#"><i class="icon-s-twitter"></i></a></li>
                                            </ul><!-- /.social -->
                                        </div><!-- /.info -->
                                    </div><!-- /.text-overlay -->

                                    <img src="{{asset('images/art/laura.jpg')}}">

                                </div><!-- /.member-image -->

                                <figcaption class="member-details bordered no-top-border">
                                    <h3>
                                        Laura </br>Cascales
                                        <span>Community Manager</span>
                                    </h3>
                                </figcaption>

                            </figure>
                        </div><!-- /.col -->

                    </div><!-- /.row -->

                    <div class="row thumbs gap-md text-center">

                        <div class="col-sm-6 thumb last-bottom">
                            <figure class="member">

                                <div class="member-image">

                                    <div class="text-overlay">
                                        <div class="info">
                                            <ul class="social">
                                                <li><a href="#"><i class="icon-s-facebook"></i></a></li>
                                                <li><a href="#"><i class="icon-s-gplus"></i></a></li>
                                                <li><a href="#"><i class="icon-s-twitter"></i></a></li>
                                            </ul><!-- /.social -->
                                        </div><!-- /.info -->
                                    </div><!-- /.text-overlay -->

                                    <img src="{{asset('images/art/gines.jpg')}}">

                                </div><!-- /.member-image -->

                                <figcaption class="member-details bordered no-top-border">
                                    <h3>
                                        Ginés Sáez
                                        <span>Mobile Team Leader</span>
                                    </h3>
                                </figcaption>

                            </figure>
                        </div><!-- /.col -->

                        <div class="col-sm-6 thumb last-bottom">
                            <figure class="member">

                                <div class="member-image">

                                    <div class="text-overlay">
                                        <div class="info">
                                            <ul class="social">
                                                <li><a href="#"><i class="icon-s-facebook"></i></a></li>
                                                <li><a href="#"><i class="icon-s-gplus"></i></a></li>
                                                <li><a href="#"><i class="icon-s-twitter"></i></a></li>
                                            </ul><!-- /.social -->
                                        </div><!-- /.info -->
                                    </div><!-- /.text-overlay -->

                                    <img src="{{asset('images/art/human01.jpg')}}">

                                </div><!-- /.member-image -->

                                <figcaption class="member-details bordered no-top-border">
                                    <h3>
                                        Jesús López
                                        <span>Web Team </br>Leader</span>
                                    </h3>
                                </figcaption>

                            </figure>
                        </div><!-- /.col -->

                    </div><!-- /.row -->

                </section>

                <!-- ============================================================= SECTION – CONTACT NAMES : END ============================================================= -->

            </div><!-- /.col -->

        </div><!-- /.row -->
    </div><!-- /.container -->


    <!-- ============================================================= SECTION – GET STARTED ============================================================= -->

    <section id="get-started" class="light-bg">
        <div class="container inner-sm">
            <div class="row">
                <div class="col-sm-11 center-block text-center">
                    <span><a href="http://www.digitalapp.es"><img src="{{asset('images/logo-producto-de.png')}}" alt="DigitalApp" /></a></span>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>

    <!-- ============================================================= SECTION – GET STARTED : END ============================================================= -->

@stop