@extends('mimenudeldiaweb/page')

@section('main')
    <section id="hero" class="light-bg img-bg img-bg-softer" style="background-image: url({{asset('images/art/image-background05.jpg')}});">
        <div class="container inner">
            <div class="row">
                <div class="caption vertical-center text-left">
                    <h1 class="fadeInDown-1 text-info"><span>Elige desde tu móvil</span></h1>
                    <p class="fadeInRight-2 light-color navy-bg" style="font-weight:bold">Consulta menús, consigue ofertas y reserva mesa fácilmente</p>
                </div><!-- /.caption -->

            </div><!-- ./row -->
        </div><!-- /.container -->
    </section>

    <!-- ============================================================= SECTION – HERO : END ============================================================= -->


    <!-- ============================================================= SECTION – PRODUCT ============================================================= -->

    <section id="product">
        <div class="container inner">

            <div class="row">

                <div class="col-sm-6 inner-right-xs text-right">
                    <figure><img src="{{asset('images/art/product01.jpg')}}" alt="" style="max-width:400px"></figure>
                </div><!-- /.col -->

                <div class="col-sm-6 inner-top-xs inner-left-xs">
                    <h2>Multiplataforma</h2>
                    <p>App nativa programada para Android y para iOS, aprovechando las ventajas que aportan las apps nativas sobre las soluciones web, dotándola de una mayor usabilidad y de una mejor experiencia de usuario, requisitos indispensables hoy día. </p>
                    <a href="#descargar" class="txt-btn">Descargar</a>
                </div><!-- /.col -->

            </div><!-- /.row -->

            <div class="row inner-top-md">

                <div class="col-sm-6 col-sm-push-6 inner-left-xs">
                    <figure><img src="{{asset('images/art/registrado.jpg')}}" alt="" style="max-width:400px"></figure>
                </div><!-- /.col -->

                <div class="col-sm-6 col-sm-pull-6 inner-top-xs inner-right-xs">
                    <h2>Sencilla y atractiva</h2>
                    <p>"Al grano, pero con desparpajo". Así podríamos definir la app de MiMenúDelDía. Útil, fácil de manejar y a la vez, rápida, permitiendo simplificar el día a día de sus clientes y ahorrar en tiempo perdido, que es el verdadero objetivo de MiMenúDelDía.</p>
                    <a href="#descargar" class="txt-btn">Descargar</a>
                </div><!-- /.col -->

            </div><!-- /.row -->

            <div class="row inner-top-md">

                <div class="col-sm-6 inner-right-xs text-right">
                    <figure><img src="{{asset('images/art/service01.jpg')}}" alt=""></figure>
                </div><!-- /.col -->

                <div class="col-sm-6 inner-top-xs inner-left-xs">
                    <h2>Control total</h2>
                    <p>Desde su dispositivo móvil podrá controlar el contenido publicado de su negocio fácilmente, pudiendo cambiar a su gusto las imágenes y textos para elegir aquellos que mejor le identifican.</p>
                </div><!-- /.col -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </section>

    <!-- ============================================================= SECTION – PRODUCT : END ============================================================= -->


    <!-- ============================================================= SECTION – VISIT OUR STORE ============================================================= -->

    <section id="visit-our-store" class="img-bg img-bg-soft tint-bg" style="background-image: url(images/art/pattern-background01.jpg);">
        <div class="container inner">

            <div class="row">
                <div class="col-md-8 col-sm-9">
                    <header>
                        <h1>El camino hacia el éxito</h1>
                        <p>Quieres saber más de cómo funciona MiMenúDelDía, no dudes en ponerte en contacto con nosotros. Estaremos gustosos de responderte a lo que necesites.</p>
                    </header>
                    <a href="{{route('website.contacto')}}" class="btn btn-large">Contacto</a>

                    <div id="descargar" />

                </div><!-- /.col -->
            </div><!-- /.row -->


        </div><!-- /.container -->
    </section>

    <!-- ============================================================= SECTION – VISIT OUR STORE : END ============================================================= -->







    <!-- ============================================================= SECTION – GET STARTED ============================================================= -->

    <section id="get-started" class="light-bg">
        <div class="container inner-sm">
            <div class="row">
                <div class="col-sm-11 center-block text-center">

                    <!-- DOWNLOAD BUTTONS -->
                    <p class="download-buttons wow fadeIn" data-wow-duration="1s">
                        <!-- APP STORE DOWNLOAD -->
                        <a href="#" class="btn btn-black btn-app-download btn-ios navy-bg">
                            <img src="{{asset('images/art/apple.png')}}">
                            <strong>Descargar para IOS</strong> <span>Apple Store</span>
                        </a>
                        <!-- PLAY STORE DOWNLOAD -->
                        <a href="#" class="btn btn-black btn-app-download btn-android navy-bg" >
                            <img src="{{asset('images/art/android.png')}}">
                            <strong>Descargar para Android</strong> <span>Google Play</span>
                        </a>
                        <!-- WINDOWS PHONE STORE DOWNLOAD -->
                        <!--<a href="#please-edit-this-link" class="btn btn-app-download btn-windows-phone">
                            <i class="fa fa-windows"></i>
                            <strong>Download App</strong> <span>from Windows Store</span>
                        </a>-->
                    </p>

                    <span><a href="http://www.digitalapp.es"><img src="{{asset('images/logo-producto-de.png')}}" alt="DigitalApp" /></a></span>


                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>

    <!-- ============================================================= SECTION – GET STARTED : END ============================================================= -->
@stop