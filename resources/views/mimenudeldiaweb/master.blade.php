<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', 'titulo')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Meta tags -->
    <meta name="description" content="MiMenúDelDía, la web y app que te ayudarán a elegir el menú de hoy que más te guste " />
    <meta name="keywords" content="menu, menú, diario, restaurante, comida, app, asiatica, asiática, china, chino, italianto, vegetariano, cerca, desayuno" />
    <meta name="author" content="DigitalApp">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">

    <!-- Customizable CSS -->
    <link href="{{asset('css/main.css')}}" rel="stylesheet" data-skrollr-stylesheet>
    <link href="{{asset('css/blue.css')}}" rel="stylesheet" title="Color">
    <link href="{{asset('css/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{asset('css/owl.transitions.css')}}" rel="stylesheet">
    <link href="{{asset('css/animate.min.css')}}" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{asset('css/plugin/jPushMenu.css')}}" />

    <!-- Fonts  -->
    <link href="http://fonts.googleapis.com/css?family=Lato:400,900,300,700" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,400italic,700italic" rel="stylesheet">

    <!-- Icons/Glyphs -->
{{--    <link href="{{asset('fonts/fontello.css')}}" rel="stylesheet">--}}

    <!-- favicon -->
    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}">

    <!-- HTML5 elements and media queries Support for IE8 : HTML5 shim and Respond.js -->
    <!--[if lt IE 9]>
    <script src="{{asset('js/html5shiv.js')}}"></script>
    <script src="{{asset('js/respond.min.js')}}"></script>
    <![endif]-->

</head>
<body>
@yield('header')
<main>
    @yield('main')
</main>
@yield('footer')
<!-- JavaScripts placed at the end of the document so the pages load faster -->
<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/jquery.easing.1.3.min.js')}}"></script>
<script src="{{asset('js/jquery.form.js')}}"></script>
<script src="{{asset('js/jquery.validate.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/bootstrap-hover-dropdown.min.js')}}"></script>
<script src="{{asset('js/skrollr.min.js')}}"></script>
<script src="{{asset('js/skrollr.stylesheets.min.js')}}"></script>
<script src="{{asset('js/waypoints.min.js')}}"></script>
<script src="{{asset('js/waypoints-sticky.min.js')}}"></script>
<script src="{{asset('js/jquery.isotope.min.js')}}"></script>
<script src="{{asset('js/jquery.easytabs.min.js')}}"></script>
<script src="{{asset('js/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/google.maps.api.v3.js')}}"></script>
<script src="{{asset('js/viewport-units-buggyfill.js')}}"></script>
<script src="{{asset('js/scripts.js')}}"></script>
<script src="{{asset('js/utilsWeb.js')}}"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBKztfWEMsiTI38JrEll24JsBivvnf039w"></script>


<!-- OnScroll CSS3 Animations for Modern Browsers and IE10+ -->
<!--[if !IE]> -->
<script src="{{asset('js/onscroll.js')}}"></script>
<!-- <![endif]-->


<!-- For demo purposes – can be removed on production -->
<link href="{{asset('css/green.css')}}" rel="alternate stylesheet" title="Green color">
<link href="{{asset('css/blue.css')}}" rel="alternate stylesheet" title="Blue color">
<link href="{{asset('css/red.css')}}" rel="alternate stylesheet" title="Red color">
<link href="{{asset('css/pink.css')}}" rel="alternate stylesheet" title="Pink color">
<link href="{{asset('css/purple.css')}}" rel="alternate stylesheet" title="Purple color">
<link href="{{asset('css/orange.css')}}" rel="alternate stylesheet" title="Orange color">
<link href="{{asset('css/navy.css')}}" rel="alternate stylesheet" title="Navy color">
<link href="{{asset('css/gray.css')}}" rel="alternate stylesheet" title="Gray color">

<script src="{{asset('js/switchstylesheet/switchstylesheet.js')}}"></script>


<script src="{{asset('js/plugin/jPushMenu.js')}}"></script>


<script>
    $(document).ready(function(){
        $(".changecolor").switchstylesheet( { seperator:"color"} );

        $('.toggle-menu').jPushMenu();

    });
</script>
<!-- For demo purposes – can be removed on production : End -->
</body>
</html>
