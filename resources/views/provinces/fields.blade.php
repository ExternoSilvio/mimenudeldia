<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {{ html()->label('Nombre', 'name')->class('col-sm-2 control-label') }}
            {{ html()->div()->class('col-sm-8')->open() }}
            {{ html()->text('name')->class('form-control')->autofocus()->required()->attribute('aria-describedby', 'helpName') }}
            {{ html()->span()->id('helpName')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>
