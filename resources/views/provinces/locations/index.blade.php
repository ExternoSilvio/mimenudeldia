@extends('adminlte::page')

@section('title', 'Localidades')

@section('content_header')
    <h1>
        Localidades
        <small>Listado de localidades</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Localidades</li>
    </ol>
@stop

@section('content')
    <div class="nav nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="list-inline-item">
                <a href="{{route('provinces.edit',$provinceId)}}"><i class="fa fa-map"></i>
                    Provincia</a>
            </li>
            <li class="list-inline-item active">
                <a href="{{route('provinces.locations.index',$provinceId)}}"><i class="fa fa-map-signs"></i>
                    Localidades</a>
            </li>
        </ul>

        <!-- /.box-header -->
        <div class="box-body table-fix-height">
            <div class="row">
                <div class="col-sm-12">
                    <a href="{!! route('provinces.locations.new',$provinceId) !!}" class="btn btn-xs btn-success pull-right">Añadir localidad</a>
                </div>
            </div>
            <div class="box-body table-fix-height">
            <table class="table table-bordered table-hover table-striped" id="locations-table" style="width:100%">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>zip</th>
                    <th>Provincia</th>
                    <th>Acción</th>
                </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@stop

@push('scripts')
    <script>
        var drawCallbackHandler = function(api) {
            removeBehavior();
        };

        var initDataTableComplete = function(api) {
            //removeBehavior();
        };

        $(document).ready(function() {

            $('#locations-table').DataTable({
                dom                 : 'Bfrtip',
                buttons             : [
                    'copy', 'excel', 'pdf', 'print'
                ],
                processing          : true,
                serverSide          : true,
                fixedHeader         : true,
                scrollX             : true,
                scrollCollapse      : true,
                // rowReorder          : true,
                stateSave           : true,
                stateDuration       : -1,
                language            : {
                    url     : 'https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json',
                    buttons : {
                        'copy'  : 'Copiar',
                        'Excel' : 'Excel',
                        'PDF'   : 'PDF',
                        'print' : 'Imprimir'
                    }
                },
                order               : [[ 0, "asc" ]],
                fixedColumns        : {
                    leftColumns: 1,
                    rightColumns: 1
                },
                columnDefs          : [
                    { targets: 0, width: 'calc(40%)' },
                    { targets: 1, width: '20%'},
                    { targets: 2, width: 'calc(40% - 40px)'},
                    { targets: 3, width: '40px', className: 'dt-center' }
                ],
                columns             : [
                    { data : 'name', name : 'name'  },
                    { data : 'zip', name : 'zip'},
                    { data : 'province', name : 'province' },
                    { data : 'action', name : 'action', orderable : false }
                ],
                ajax                : {
                    url : '{!! route('locations.datatable') !!}',
                    data : function (d) {

                        d.province_id     = '{{ $provinceId }}';

                    }
                },
                drawCallback        : function() {
                    drawCallbackHandler(this.api());
                },
                initComplete        : function() {
                    initDataTableComplete(this.api());
                }
            });
        });
    </script>
@endpush