<div class="row">
    <div class="col-sm-5">
        <div class="form-group">
            {{ html()->label('Nombre', 'name')->class('col-sm-2 control-label') }}
            {{ html()->div()->class('col-sm-8')->open() }}
            {{ html()->text('name')->class('form-control')->autofocus()->required()->attribute('aria-describedby', 'helpName') }}
            {{ html()->span()->id('helpName')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-5">
        <div class="form-group">
            {{ html()->label('C. Postal', 'zip')->class('col-sm-2 control-label') }}
            {{ html()->div()->class('col-sm-8')->open() }}
            {{ html()->text('zip')->class('form-control')->required()->attribute('size', 5)->attribute('aria-describedby', 'helpZip') }}
            {{ html()->span()->id('helpZip')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-5">
        <div class="form-group">
            {{ html()->label('Provincia', 'province_id')->class('col-sm-2 control-label') }}
            {{ html()->div()->class('col-sm-8')->open() }}
            {{ html()->select('province_id', $provinces)->class('form-control')->required()->attribute('aria-describedby', 'helpProvince') }}
            {{ html()->span()->id('helpProvince')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>
