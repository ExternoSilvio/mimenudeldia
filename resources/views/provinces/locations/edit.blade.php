@extends('adminlte::page')

@section('title', 'Localidades - Editar')

@push('css')

@endpush

@section('content_header')
<h1>
    Provincias
    <small>Editar localidad</small>
</h1>
<ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li><a href="{{route('provinces.locations.index',$provinceId)}}"><i class="fa fa-map-signs"></i> Localidades</a></li>
    <li class="active">Editar</li>
</ol>
@stop

@section('content')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="nav nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="list-inline-item">
            <a href="{{route('provinces.edit',$provinceId)}}"><i class="fa fa-map"></i>
                Provincia</a>
        </li>
        <li class="list-inline-item active">
            <a href="{{route('provinces.locations.index',$provinceId)}}"><i class="fa fa-map-signs"></i>
                Localidades</a>
        </li>
    </ul>
    <!-- /.box-header -->
    {{ html()->modelForm($location, 'PUT', route('provinces.locations.update', [$provinceId, $location->id]))->class('form-horizontal')->acceptsFiles()->open() }}
    <div class="box-body">
        <div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#fields" aria-controls="fields" role="tab" data-toggle="tab"><i class="fa fa-edit"></i> Datos básicos</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="fields">
                    <br>
                    @include('provinces.locations.fields')
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer">

        {{ html()->a(route('provinces.locations.index',[$provinceId,$location->id]), '<i class="fa fa-fw fa-chevron-left"></i> Volver')->class('btn btn-primary') }}
        {{ html()->div()->class('btn-group pull-right')->open() }}
        {{ html()->a(route('provinces.locations.delete', [$provinceId, $location->id]), '<i class="fa fa-fw fa-trash"></i> Eliminar')->class('btn btn-danger')->data('remove', '¿Está seguro de que quiere borrar esta localidad?') }}
        {{ html()->submit('<i class="fa fa-fw fa-check"></i> Guardar')->class('btn btn-success') }}
        {{ html()->div()->close() }}
    </div>
    {{ html()->form()->close() }}
    <!-- /.box-body -->
</div>
@stop

@push('scripts')

@endpush