@extends('adminlte::page')

@section('title', 'Provincias')

@section('content_header')
    <h1>
        Provincias
        <small>Listado de provincias</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Provincias</li>
    </ol>
@stop

@section('content')
    <div class="nav nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="list-inline-item active">
                <a><i class="fa fa-map"></i>
                    Provincias</a>
            </li>
        </ul>
        <div class="box-body table-fix-height">
            <div class="box-body table-fix-height">
                <div class="row">
                    <div class="col-sm-12">
                        <a href="{!! route('provinces.new') !!}" class="btn btn-xs btn-success pull-right">Añadir provincia</a>
                    </div>
                </div>
            </div>
            <table class="table table-bordered table-hover table-striped" id="users-table" style="width:100%">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>acción</th>
                </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@stop

@push('scripts')
    <script>
        var drawCallbackHandler = function(api) {
            removeBehavior();
        };

        var initDataTableComplete = function(api) {
            //removeBehavior();
        };

        $(document).ready(function() {
            $('#users-table').DataTable({
                dom                 : 'Bfrtip',
                buttons             : [
                    'copy', 'excel', 'pdf', 'print'
                ],
                processing          : true,
                serverSide          : true,
                fixedHeader         : true,
                scrollX             : true,
                scrollCollapse      : true,
                // rowReorder          : true,
                stateSave           : true,
                stateDuration       : -1,
                language            : {
                    url     : 'https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json',
                    buttons : {
                        'copy'  : 'Copiar',
                        'Excel' : 'Excel',
                        'PDF'   : 'PDF',
                        'print' : 'Imprimir'
                    }
                },
                order               : [[ 0, "asc" ]],
                fixedColumns        : {
                    leftColumns: 1,
                    rightColumns: 1
                },
                columnDefs          : [
                    { targets: 0, width: 'calc(100% - 40px)' },
                    { targets: 1, width: '40px', className: 'dt-center' }
                ],
                columns             : [
                    { data : 'name', name : 'name'  },
                    { data : 'action', name : 'action', orderable : false }
                ],
                ajax                : '{!! route('provinces.datatable') !!}',
                drawCallback        : function() {
                    drawCallbackHandler(this.api());
                },
                initComplete        : function() {
                    initDataTableComplete(this.api());
                }
            });
        });
    </script>
@endpush