<div class="row">
    <div class="col-xs-12 col-sm-5">
        <div class="form-group">
            {{ html()->label('Nombre', 'name')->class('col-sm-4 control-label') }}
            {{ html()->div()->class('col-sm-8')->open() }}
            {{ html()->text('name')->class('form-control')->autofocus()->required()->attribute('aria-describedby', 'helpName') }}
            {{ html()->span()->id('helpName')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-5">
        <div class="form-group">
            {{ html()->label('Categoría (opcional)', 'category_id')->class('col-sm-4 control-label') }}
            {{ html()->div()->class('col-sm-8')->open() }}
            {{ html()->select('category_id', $categories)->class('form-control')->required()->attribute('aria-describedby', 'helpCategory') }}
            {{ html()->span()->id('helpCategory')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>
