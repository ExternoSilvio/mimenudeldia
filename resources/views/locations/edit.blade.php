@extends('adminlte::page')

@section('title', 'Localidades - Editar')

@push('css')

@endpush

@section('content_header')
<h1>
    Localidades
    <small>Editar localidad</small>
</h1>
<ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li><a href="{{route('locations.index')}}"><i class="fa fa-users"></i> Localidades</a></li>
    <li class="active">Editar</li>
</ol>
@stop

@section('content')
<div class="nav nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="list-inline-item active">
            <a><i class="fa fa-map-signs"></i>
                Localidades</a>
        </li>
    </ul>
    <!-- /.box-header -->
    {{ html()->modelForm($location, 'PUT', route('locations.update', $location->id ))->class('form-horizontal')->acceptsFiles()->open() }}
    <div class="box-body">
        <div>
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#fields" aria-controls="fields" role="tab" data-toggle="tab"><i class="fa fa-edit"></i> Datos básicos</a></li>
            </ul>
        </div>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="fields">
                <br>
                @include('locations.fields')
            </div>
        </div>
    </div>
    <div class="box-footer">
        {{ html()->a(route('locations.index'), '<i class="fa fa-fw fa-chevron-left"></i> Volver')->class('btn btn-primary') }}
        {{ html()->div()->class('btn-group pull-right')->open() }}
        {{ html()->a(route('locations.delete', $location->id ), '<i class="fa fa-fw fa-trash"></i> Eliminar')->class('btn btn-danger')->data('remove', '¿Está seguro de que quiere borrar esta localidad?') }}
        {{ html()->submit('<i class="fa fa-fw fa-check"></i> Guardar')->class('btn btn-success') }}
        {{ html()->div()->close() }}
    </div>
    {{ html()->form()->close() }}
    <!-- /.box-body -->
</div>
@stop

@push('scripts')

@endpush