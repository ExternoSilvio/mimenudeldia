@extends('adminlte::page')

@section('title', 'Localidades')

@section('content_header')
    <h1>
        Localidades
        <small>Listado de localidades</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Localidades</li>
    </ol>
    @if(session()->has('message') )
        <div class="alert alert-success">
            {{session('message')}}
        </div>
    @endif
@stop

@section('content')
    <div class="nav nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="list-inline-item active">
                <a><i class="fa fa-map-signs"></i>
                    Localidades</a>
            </li>
        </ul>
        <!-- /.box-header -->
        <div class="box-body table-fix-height">
            <div class="box-body table-fix-height">
                {{ html()->form('GET', route('locations.index'))->class('form-horizontal')->open() }}
                <div class="row">
                    <div class="col-sm-3" data-toggle="tooltip" data-placement="top" title="Filtrar por provincia">
                        {{ html()->select('province', $provinces, request('province'))->class('form-control select2') }}
                    </div>
                    <div class="col-sm-3">
                        <div class="btn-group btn-block">
                            {{ html()->submit('<i class="fa fa-fw fa-filter"></i> Filtrar')->class('btn btn-primary') }}
                            {{ html()->a(url()->current(),'<i class="fa fa-fw fa-trash"></i>')->class('btn btn-primary') }}
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <a href="{!! route('locations.new') !!}" class="btn btn-xs btn-success pull-right">Añadir localidad</a>
                    </div>
                </div>
                {{ html()->form()->close() }}
            </div>
                <table class="table table-bordered table-hover table-striped" id="users-table" style="width:100%">
                    <thead>
                    <tr>
                        <th>Provincia</th>
                        <th>Nombre</th>
                        <th>zip</th>
                        <th>Acción</th>
                    </tr>
                    </thead>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        @stop

        @push('scripts')
            <script>

                $("#province").select2({
                });

                var drawCallbackHandler = function(api) {
                    removeBehavior();
                };

                var initDataTableComplete = function(api) {
                    //removeBehavior();
                };

                $(document).ready(function() {

                    $('#users-table').DataTable({
                        dom                 : 'Bfrtip',
                        buttons             : [
                            'copy', 'excel', 'pdf', 'print'
                        ],
                        processing          : true,
                        serverSide          : true,
                        fixedHeader         : true,
                        scrollX             : true,
                        scrollCollapse      : true,
                        // rowReorder          : true,
                        stateSave           : true,
                        stateDuration       : -1,
                        language            : {
                            url     : 'https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json',
                            buttons : {
                                'copy'  : 'Copiar',
                                'Excel' : 'Excel',
                                'PDF'   : 'PDF',
                                'print' : 'Imprimir'
                            }
                        },
                        order               : [[ 0, "asc" ]],
                        fixedColumns        : {
                            leftColumns: 1,
                            rightColumns: 1
                        },
                        columnDefs          : [
                            { targets: 0, width: 'calc(40%)' },
                            { targets: 1, width: '20%'},
                            { targets: 2, width: 'calc(40% - 40px)'},
                            { targets: 3, width: '40px', className: 'dt-center' }
                        ],
                        columns             : [
                            { data : 'province', name : 'province' },
                            { data : 'name', name : 'name'  },
                            { data : 'zip', name : 'zip'},
                            { data : 'action', name : 'action', orderable : false }
                        ],
                        ajax                : {
                            url : '{!! route('locations.datatable') !!}',
                            data : function (d) {

                                d.province_global     = '{{ request('province', null) }}';

                            }
                        },
                        drawCallback        : function() {
                            drawCallbackHandler(this.api());
                        },
                        initComplete        : function() {
                            initDataTableComplete(this.api());
                        }
                    });
                });
            </script>
    @endpush
