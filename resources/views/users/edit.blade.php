@extends('adminlte::page')

@section('title', 'Usuarios - Editar')

@push('css')

@endpush

@section('content_header')
    <h1>
        Usuarios
        <small>Editar usuario</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="/users"><i class="fa fa-users"></i> Usuarios</a></li>
        <li class="active">Editar</li>
    </ol>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@stop

@section('content')
    <div class="nav nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="list-inline-item active">
                <a><i class="fa fa-cutlery"></i>
                    Menus</a>
            </li>
        </ul>
        <!-- /.box-header -->
        {{ html()->modelForm($user, 'PUT', route('users.update', $user->id))->class('form-horizontal')->acceptsFiles()->open() }}
        <div class="box-body">
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#fields" aria-controls="fields" role="tab" data-toggle="tab"><i class="fa fa-edit"></i> Datos básicos</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="fields">
                        <div class="row">
                            <div class="col-sm-6">
                                <br>
                                @include('users.fields')
                            </div>
                            <div class="col-sm-6">
                                <br>
                                @if ($user->id !== 1)
                                    <h5>Roles</h5>
                                    <div class="list-group">
                                        @foreach($roles as $role)
                                            <a href="javascript:;" class="list-group-item @if($user->hasRole($role->name)) list-group-item-info @endif" data-swap="[type='checkbox']|checked|list-group-item-info">
                                                @if($user->hasRole($role->name))
                                                    {{ html()->checkbox()->name('roles[]')->value($role->name)->checked(true) }}
                                                    <i class="fa fa-fw fa-lock"></i>
                                                @else
                                                    {{ html()->checkbox()->name('roles[]')->value($role->name)->checked(false) }}
                                                    <i class="fa fa-fw fa-unlock"></i>
                                                @endif
                                                {{ $role->name }}
                                            </a>
                                        @endforeach
                                    </div>
                                    <h5>Permisos</h5>
                                    <div class="list-group">
                                        @foreach($permissions as $permission)
                                            <a href="javascript:;" class="list-group-item @if($user->hasPermissionTo($permission->name)) list-group-item-info @endif" data-swap="[type='checkbox']|checked|list-group-item-info">
                                                @if($user->hasPermissionTo($permission->name))
                                                    {{ html()->checkbox()->name('permissions[]')->value($permission->name)->checked(true) }}
                                                    <i class="fa fa-fw fa-lock"></i>
                                                @else
                                                    {{ html()->checkbox()->name('permissions[]')->value($permission->name)->checked(false) }}
                                                    <i class="fa fa-fw fa-unlock"></i>
                                                @endif
                                                {{ $permission->name }}
                                            </a>
                                        @endforeach
                                    </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
                {{--<div class="col-sm-6">
                   <div class="panel panel-default">
                       <div class="panel-heading"><i class="fa fa-fw fa-user"></i> Avatar</div>
                       <div class="panel-body">
                           <div class="row">
                               <div class="col-sm-3">
                                   @if ($user->avatar !== null)
                                       <img src="{{ $user->avatar }}" alt="{{ $user->name }}" class="img-responsive img-circle" width="120">
                                   @else
                                       <img src="{{ asset('img/user_profile.png') }}" alt="{{ $user->name }}" class="img-responsive" width="120">
                                   @endif
                               </div>
                           </div>
                       </div>
                       <div class="panel-footer">
                           <div class="row">
                               {{ html()->label('Avatar', 'avatar')->class('col-sm-4') }}
                               {{ html()->div()->class('col-sm-8')->open() }}
                               {{ html()->input('file', 'avatar') }}
                               {{ html()->div()->close() }}
                           </div>
                       </div>
                   </div>--}}
        <div class="box-footer">
            @if ($user->id !== Auth::id())
                {{ html()->a(route('users.index'), '<i class="fa fa-fw fa-chevron-left"></i> Volver')->class('btn btn-primary') }}
            @endif
            {{ html()->div()->class('btn-group pull-right')->open() }}
            @if ($user->id !== null && $user->id !== Auth::id())
                {{ html()->a(route('users.delete', $user->id), '<i class="fa fa-fw fa-trash"></i> Eliminar')->class('btn btn-danger')->data('remove', '¿Está seguro de que quiere borrar este usuario?') }}
            @endif
            {{ html()->submit('<i class="fa fa-fw fa-check"></i> Guardar')->class('btn btn-success') }}
            {{ html()->div()->close() }}
        </div>
    {{ html()->form()->close() }}
    <!-- /.box-body -->
    </div>
@stop

@push('scripts')

@endpush