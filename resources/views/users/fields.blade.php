<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {{ html()->label('Nombre', 'name')->class('col-sm-2 control-label') }}
            {{ html()->div()->class('col-sm-8')->open() }}
            {{ html()->text('name')->class('form-control')->autofocus()->required()->attribute('aria-describedby', 'helpName') }}
            {{ html()->span()->id('helpName')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {{ html()->label('Email', 'email')->class('col-sm-2 control-label') }}
            {{ html()->div()->class('col-sm-8')->open() }}
            {{ html()->text('email')->class('form-control')->required()->attribute('aria-describedby', 'helpEmail') }}
            {{ html()->span()->id('helpEmail')->class('help-block') }}
            {{ html()->div()->close() }}
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><i class="fa fa-fw fa-key"></i> Contraseña</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    {{ html()->label('Contraseña', 'password')->class('col-sm-2 control-label') }}
                    {{ html()->div()->class('col-sm-8')->open() }}
                    {{ html()->password('password')->class('form-control')->attribute('aria-describedby', 'helpPassword') }}
                    {{ html()->span()->id('helpPassword')->class('help-block') }}
                    {{ html()->div()->close() }}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    {{ html()->label('Confirmar', 'password_confirmation')->class('col-sm-2 control-label') }}
                    {{ html()->div()->class('col-sm-8')->open() }}
                    {{ html()->password('password_confirmation')->class('form-control')->attribute('aria-describedby', 'help') }}
                    {{ html()->span()->id('help')->class('help-block') }}
                    {{ html()->div()->close() }}
                </div>
            </div>
        </div>
    </div>
</div>