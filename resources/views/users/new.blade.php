@extends('adminlte::page')

@section('title', 'Usuarios - Nuevo')

@push('css')

@endpush

@section('content_header')
    <h1>
        Usuarios
        <small>Nuevo usuario</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="/users"><i class="fa fa-users"></i> Usuarios</a></li>
        <li class="active">Nuevo</li>
    </ol>
@stop

@section('content')
    <div class="nav nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="list-inline-item active">
                <a><i class="fa fa-users"></i>
                    Usuarios</a>
            </li>
        </ul>
        <!-- /.box-header -->
        {{ html()->modelForm($user, 'POST', route('users.create'))->class('form-horizontal')->open() }}
        <div class="box-body">
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#fields" aria-controls="fields" role="tab" data-toggle="tab"><i class="fa fa-edit"></i> Datos básicos</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="fields">
                        <br>
                        @include('users.fields')
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            {{ html()->a(route('users.index'), '<i class="fa fa-fw fa-chevron-left"></i> Volver')->class('btn btn-primary') }}
            {{ html()->div()->class('btn-group pull-right')->open() }}
            {{ html()->submit('Crear')->class('btn btn-success') }}
            {{ html()->div()->close() }}
        </div>
    {{ html()->form()->close() }}
    <!-- /.box-body -->
    </div>
@stop

@push('scripts')

@endpush