@extends('adminlte::page')

@section('title', 'Platos - Editar')

@push('css')

@endpush

@section('content_header')
<h1>
    Platos
    <small>Editar Platos</small>
</h1>
<ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li><a href="/dishes"><i class="fa fa-circle-o"></i> Platos</a></li>
    <li class="active">Editar</li>
</ol>
@stop

@section('content')
<div class="nav nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="list-inline-item active">
            <a><i class="fa fa-circle-o"></i>
                Platos</a>
        </li>
    </ul>
    <!-- /.box-header -->
    {{ html()->modelForm($dish, 'PUT', route('dishes.update', $dish->id))->class('form-horizontal')->acceptsFiles()->open() }}
    <div class="box-body">
        <div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#fields" aria-controls="fields" role="tab" data-toggle="tab"><i class="fa fa-edit"></i> Datos básicos</a></li>
                <li role="presentation"><a href="#features" aria-controls="features" role="tab" data-toggle="tab"><i class="fa fa-fw fa-tags"></i>Características</a></li>
                <li role="presentation"><a href="#offers" aria-controls="offers" role="tab" data-toggle="tab"><i class="fa fa-fw fa-area-chart"></i>Ofertas</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="fields">
                    <br>
                    @include('dishes.fields')
                </div>
                <div role="tabpanel" class="tab-pane" id="features">
                    <div class="list-group">
                        <br>
                        <div class="row">
                            @foreach ($features as $feature)
                                <div class="col-sm-3">
                                    <a href="javascript:void(0)" class="list-group-item  @if ($current_features->contains('feature_id', $feature->id)) list-group-item-info @endif">
                                        <input type="checkbox" name="features[]" value="{{ $feature->id }}" @if ($current_features->contains('feature_id', $feature->id)) checked="checked" @endif> &nbsp;{{ $feature->name }}
                                    </a>
                                </div>
                                @if ($loop->iteration % 4 == 0)
                        </div>
                        <br>
                        <div class="row">
                            @endif
                            @endforeach
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane offers" id="offers">
                    <br>
                    <!--edit-offers-block-->
                    @include('offers.edit')
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer">

        {{ html()->a(route('dishes.index'), '<i class="fa fa-fw fa-chevron-left"></i> Volver')->class('btn btn-primary') }}

        {{ html()->div()->class('btn-group pull-right')->open() }}

        {{ html()->a(route('dishes.delete', $dish->id), '<i class="fa fa-fw fa-trash"></i> Eliminar')->class('btn btn-danger')->data('remove', '¿Está seguro de que quiere borrar este plato?') }}

        {{ html()->submit('<i class="fa fa-fw fa-check"></i> Guardar')->class('btn btn-success') }}
        {{ html()->div()->close() }}
    </div>
    {{ html()->form()->close() }}
    <!-- /.box-body -->
</div>
@stop

@push('scripts')

@endpush