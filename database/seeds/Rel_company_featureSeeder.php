<?php

use Illuminate\Database\Seeder;

class Rel_company_featureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (App\Rel_company_feature::all()->isEmpty()) {
            factory(App\Rel_company_feature::class, 100)->create()->each(function ($f) {
                // Do something with $f fake element, f.ex:
                // $f->posts()->save(factory(App\Post::class)->make());
            });
        }
    }
}
