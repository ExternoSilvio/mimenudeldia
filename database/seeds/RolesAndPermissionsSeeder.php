<?php

use Illuminate\Database\Seeder;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = Role::all();
        $permissions = Permission::all();

        if ($roles->count() === 0 && $permissions->count() === 0) {
            // Reset cached roles and permissions
            app()['cache']->forget('spatie.permission.cache');

            // create permissions

            Permission::create(['name' => 'manage-roles']);
            Permission::create(['name' => 'manage-permissions']);
            Permission::create(['name' => 'change-roles']);
            Permission::create(['name' => 'change-permissions']);
            Permission::create(['name' => 'manage-users']);
            Permission::create(['name' => 'can-login']);


            // create roles and assign created permissions
            $role = Role::create(['name' => 'superadmin']);
            $role->givePermissionTo(Permission::all());

            $role = Role::create(['name' => 'admin']);
            $role->givePermissionTo(['manage-users']);

            $role = Role::create(['name' => 'user']);
            $role->givePermissionTo('can-login');
        }

        // Assign Superadmin Role to first user if exists
        $user = User::all()->first();
        if ($user !== null && $user->getRoleNames()->count() === 0) {
            $user->assignRole('superadmin');
        }

    }
}
