<?php

use Illuminate\Database\Seeder;

class DishSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (App\Dish::all()->isEmpty()) {
            factory(App\Dish::class, 200)->create()->each(function ($f) {
                // Do something with $f fake element, f.ex:
                // $f->posts()->save(factory(App\Post::class)->make());
            });
        }
    }
}
