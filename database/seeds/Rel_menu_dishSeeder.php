<?php

use Illuminate\Database\Seeder;

class Rel_menu_dishSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (App\Rel_menu_dish::all()->isEmpty()) {
            factory(App\Rel_menu_dish::class, 50)->create()->each(function ($f) {
                // Do something with $f fake element, f.ex:
                // $f->posts()->save(factory(App\Post::class)->make());
            });
        }

    }
}
