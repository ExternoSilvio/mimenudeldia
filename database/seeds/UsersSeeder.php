<?php

use Illuminate\Database\Seeder;
use \Illuminate\Http\Request;

use App\User;
use App\Services\UsersService;
use App\Repositories\UsersRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\MenuRepository;
use App\Repositories\DishRepository;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        if ($users->count() === 0) {
            $userService = new UsersService(new UsersRepository(),new CompanyRepository(),new MenuRepository(),new DishRepository());

            /*$user = [
                'name' => env('ADMIN_NAME', 'Admin'),
                'email' => env('ADMIN_EMAIL', 'admin@digitalapp.es'),
                'password' => env('ADMIN_PASSWORD', 'Mobile2018'),

            ];*/

            $request = new Request();

            $request->setMethod('POST');

            $request->request->add(['name' => 'Admin']);
            $request->request->add(['email' => 'admin@digitalapp.es']);
            $request->request->add(['password' => 'Mobile2018']);

            $userService->create($request);
        }

        if (App\User::all()->count() == 1 ) {
            factory(App\User::class, 30)->create()->each(function ($f) {
                // Do something with $f fake element, f.ex:
                // $f->posts()->save(factory(App\Post::class)->make());
            });
        }

    }
}
