<?php

use Illuminate\Database\Seeder;

class ScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (App\Schedule::all()->isEmpty()) {
            factory(App\Schedule::class, 250)->create()->each(function ($f) {
                // Do something with $f fake element, f.ex:
                // $f->posts()->save(factory(App\Post::class)->make());
            });
        }
    }
}
