<?php

use Illuminate\Database\Seeder;

class Rel_menu_featureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (App\Rel_menu_feature::all()->isEmpty()) {
            factory(App\Rel_menu_feature::class, 60)->create()->each(function ($f) {
                // Do something with $f fake element, f.ex:
                // $f->posts()->save(factory(App\Post::class)->make());
            });
        }
    }
}
