<?php

use Illuminate\Database\Seeder;

class Rel_dish_featureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (App\Rel_dish_feature::all()->isEmpty()) {
            factory(App\Rel_dish_feature::class, 60)->create()->each(function ($f) {
                // Do something with $f fake element, f.ex:
                // $f->posts()->save(factory(App\Post::class)->make());
            });
        }
    }
}
