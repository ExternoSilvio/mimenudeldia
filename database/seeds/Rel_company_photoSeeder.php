<?php

use Illuminate\Database\Seeder;

class Rel_company_photoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (App\Rel_company_photo::all()->isEmpty()) {
            factory(App\Rel_company_photo::class, 40)->create()->each(function ($f) {
                // Do something with $f fake element, f.ex:
                // $f->posts()->save(factory(App\Post::class)->make());
            });
        }
    }
}
