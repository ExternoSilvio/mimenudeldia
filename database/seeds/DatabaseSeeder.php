<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersSeeder::class);
        $this->call(RolesAndPermissionsSeeder::class);
        $this->call(ProvinceSeeder::class);
        $this->call(LocationSeeder::class);
        $this->call(PhotoSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(FeatureSeeder::class);
        $this->call(CompanySeeder::class);
        $this->call(Rel_company_photoSeeder::class);
        $this->call(SubsidiarySeeder::class);
        $this->call(ScheduleSeeder::class);
        $this->call(OpinionSeeder::class);
        $this->call(FavoriteSeeder::class);
        $this->call(MenuSeeder::class);
        $this->call(DishSeeder::class);
        $this->call(Rel_menu_dishSeeder::class);
        $this->call(Rel_menu_featureSeeder::class);
        $this->call(OfferSeeder::class);
        $this->call(ScoreSeeder::class);
        $this->call(Rel_dish_featureSeeder::class);
        $this->call(Rel_company_featureSeeder::class);
    }
}
