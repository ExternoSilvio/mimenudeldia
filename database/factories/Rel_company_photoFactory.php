<?php

use Faker\Generator as Faker;

$factory->define(App\Rel_company_photo::class, function (Faker $faker) {

    $company_id = $faker->randomElement(App\Company::all()->pluck('id')->all());
    $photo_id = $faker->randomElement(App\Photo::all()->pluck('id')->all());

    $rel_company_photo = App\Rel_company_photo::where('company_id','=',$company_id)
                                                ->where('photo_id','=',$photo_id)
                                                ->get();

    if(!count($rel_company_photo))
    {
        return [
            'company_id' => $company_id,
            'photo_id'   => $photo_id
        ];
    }
    else
    {
        return [];
    }

});
