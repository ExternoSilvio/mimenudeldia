<?php

use Faker\Generator as Faker;

$factory->define(App\Schedule::class, function (Faker $faker) {

    $subsidiary_id = $faker->randomElement(App\Subsidiary::all()->pluck('id')->all());

    $day = $faker->randomElement([0,1,2,3,4,5,6]);
    $finish_at = $faker->time('H:i:s');
    $start_at = $faker->time('H:i:s',$finish_at);

    $schedule = App\Schedule::where('subsidiary_id','=',$subsidiary_id)
                            ->where('day','=',$day)
                            ->whereBetween('start_at',[$start_at,$finish_at])
                            ->orWhereBetween('finish_at',[$start_at,$finish_at])
                            ->get();

    if(!count($schedule))
    {
        return [
            'day'           => $day,
            'start_at'      => $start_at,
            'finish_at'     => $finish_at,
            'subsidiary_id' => $subsidiary_id
        ];
    }
    else
    {
        return [];
    }
});
