<?php

use Faker\Generator as Faker;

$factory->define(App\Company::class, function (Faker $faker) {

    $photos = App\Photo::all()->pluck('id')->all();

    return [

        'name'           => $faker->company,
        'identification' => $faker->randomNumber($nbDigits = NULL, $strict = false),
        'web'            => $faker->domainName,
        'information'    => $faker->text($maxNbChars = 100),
        'logo_id'        => $faker->randomElement($photos)
    ];
});
