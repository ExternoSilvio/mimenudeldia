<?php

use Faker\Generator as Faker;

$factory->define(App\Subsidiary::class, function (Faker $faker) {

    $company_id = $faker->randomElement(App\Company::all()->pluck('id')->all());
    $location_id = $faker->randomElement(App\Location::all()->pluck('id')->all());

    //todo include primary

    return [

        'address'     => $faker->address,
        'coordinates' => $faker->latitude(35,44).','.$faker->longitude(-10,4),
        'phone'       => $faker->phoneNumber,
        'company_id'  => $company_id,
        'email'       => $faker->email,
        'location_id' => $location_id
    ];
});
