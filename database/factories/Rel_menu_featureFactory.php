<?php

use Faker\Generator as Faker;

$factory->define(App\Rel_menu_feature::class, function (Faker $faker) {

    $feature_id = $faker->randomElement(App\Feature::all()->pluck('id')->all());
    $menu_id = $faker->randomElement((App\Menu::all()->pluck('id')->all()));

    $rel_menu_feature = App\Rel_menu_feature::where('menu_id','=', $menu_id)
        ->where('feature_id','=',$feature_id)
        ->get();

    //todo meter null por defecto

    //dd($feature_id);

    if(!count($rel_menu_feature))
    {
        return [

            'feature_id' => $feature_id,
            'menu_id' => $menu_id
        ];
    }
    else
    {
        return [];
    }
});
