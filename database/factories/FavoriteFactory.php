<?php

use Faker\Generator as Faker;

$factory->define(App\Favorite::class, function (Faker $faker) {

    $company_id = $faker->randomElement(App\Company::all()->pluck('id')->all());
    $user_id = $faker->randomElement(App\User::all()->pluck('id')->all());

    $favorite = App\Favorite::where('company_id','=',$company_id)
        ->where('user_id','=',$user_id)
        ->get();

    if(!count($favorite))
    {
        return [
            'company_id' => $company_id,
            'user_id'    => $user_id
        ];
    }
    else
    {
        return [];
    }

});
