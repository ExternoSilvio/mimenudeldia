<?php

use Faker\Generator as Faker;

$factory->define(App\Dish::class, function (Faker $faker) {

    $companies = App\Company::all()->pluck('id')->all();
    $categories = App\Category::all()->pluck('id')->all();
    $photos = App\Photo::all()->pluck('id')->all();

    return [

        'name' => $faker->text($maxNbChars = 15),
        'price' => $faker->randomFloat(2,0,20),
        'category_id' => $faker->randomElement($categories),
        'photo_id' => $faker->randomElement($photos),
        'company_id' => $faker->randomElement($companies)
    ];
});
