<?php

use Faker\Generator as Faker;

$factory->define(App\Feature::class, function (Faker $faker) {

    $features = App\Feature::all()->pluck('id')->all();

    return [

        'name'       => $faker->text($maxNbChars = 15),
        'feature_id' => $faker->randomElement($features)
    ];
});
