<?php

use Faker\Generator as Faker;

$factory->define(App\Opinion::class, function (Faker $faker) {

    $subsidiary_id = $faker->randomElement(App\Subsidiary::all()->pluck('id')->all());
    $user_id = $faker->randomElement(App\User::all()->pluck('id')->all());

    $opinion = App\Opinion::where('subsidiary_id','=',$subsidiary_id)
        ->where('user_id','=',$user_id)
        ->get();

    if(!count($opinion))
    {
        return [
            'subsidiary_id' => $subsidiary_id,
            'user_id'       => $user_id,
            'comment'       => $faker->text(1000),
            'status'        => 0
        ];
    }
    else
    {
        return [];
    }
});
