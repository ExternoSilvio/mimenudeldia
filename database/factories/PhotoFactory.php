<?php

use Faker\Generator as Faker;

//todo search images with relation

$factory->define(App\Photo::class, function (Faker $faker) {
    return [
        'url' => $faker->imageUrl($width = 640, $height = 480)
    ];
});
