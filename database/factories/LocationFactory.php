<?php

use Faker\Generator as Faker;

$factory->define(App\Location::class, function (Faker $faker) {

    $provinces = App\Province::all()->pluck('id')->all();

    $location_name = $faker->city;

    $location_exists = App\Location::where('name','=',$location_name)->get();

    $zip = $faker->postcode;

    $zip_exists = App\Location::where('zip','=',$zip)->get();

    return [

        'name'        => (!count($location_exists)) ? $location_name : null ,
        'zip'         => (!count($zip_exists)) ? $zip : null ,
        'province_id' => $faker->randomElement($provinces)

    ];

});
