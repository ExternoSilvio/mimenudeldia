<?php

use Faker\Generator as Faker;

$factory->define(App\Category::class, function (Faker $faker) {

    $categories = App\Category::all()->pluck('id')->all();

    return [

        'name'        => $faker->text($maxNbChars = 15),
        'category_id' => $faker->randomElement($categories)
    ];
});
