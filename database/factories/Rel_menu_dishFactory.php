<?php

use Faker\Generator as Faker;

$factory->define(App\Rel_menu_dish::class, function (Faker $faker) {

    $menu_id = $faker->randomElement(App\Menu::all()->pluck('id')->all());
    $dish_id = $faker->randomElement((App\Dish::all()->pluck('id')->all()));

    $rel_dish_menu = App\Rel_menu_dish::where('dish_id','=', $dish_id)
        ->where('menu_id','=',$menu_id)
        ->get();
    if(!count($rel_dish_menu))
    {
        return [

            'menu_id' => $menu_id,
            'dish_id' => $dish_id
        ];
    }
    else
    {
        return [];
    }

});
