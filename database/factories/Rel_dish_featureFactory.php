<?php

use Faker\Generator as Faker;

$factory->define(App\Rel_dish_feature::class, function (Faker $faker) {

    $feature_id = $faker->randomElement(App\Feature::all()->pluck('id')->all());
    $dish_id = $faker->randomElement((App\Dish::all()->pluck('id')->all()));

    $rel_dish_feature = App\Rel_dish_feature::where('dish_id','=', $dish_id)
                                              ->where('feature_id','=',$feature_id)
                                              ->get();

    //todo meter null por defecto

    //dd($feature_id);

    if(!count($rel_dish_feature))
    {
        return [

            'feature_id' => $feature_id,
            'dish_id' => $dish_id
        ];
    }
    else
    {
        return [];
    }
});
