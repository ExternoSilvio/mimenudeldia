<?php

use Faker\Generator as Faker;



$factory->define(App\Menu::class, function (Faker $faker) {

    $photos = App\Menu::all()->pluck('id')->all();

    $companies = App\Company::all()->pluck('id')->all();

    return [

        'name'     => $faker->text($maxNbChars = 10),
        'price'    => $faker->randomFloat($nbMaxDecimals = 2, $min = 4, $max = 50),
        'photo_id' => $faker->randomElement($photos),
        'observations' => $faker->text($maxNbChars = 100),
        'company_id' => $faker->randomElement($companies)
    ];
});
