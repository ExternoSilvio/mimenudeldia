<?php

use Faker\Generator as Faker;

$factory->define(App\Province::class, function (Faker $faker) {


    $province_name = $faker->state;

    $province = App\Province::where('name',"=",$province_name)->get();

    if(!count($province))
    {
        return [
            'name' => $faker->state
        ];
    }
    else
    {
        return [];
    }

});
