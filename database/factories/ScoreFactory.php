<?php

use Faker\Generator as Faker;

$factory->define(\App\Score::class, function (Faker $faker) {

    $users = App\User::all()->pluck('id')->all();
    $dish_id = null;
    $menu_id = null;
    $company_id = null;

    $rand1 = rand(0,11);

    if( $rand1 >= 0  && $rand1 < 4)
    {
        $dish_id = $faker->randomElement((App\Dish::all()->pluck('id')->all()));
    }
    else
        if($rand1 >=4 && $rand1 < 8 )
        {
            $menu_id = $faker->randomElement(App\Menu::all()->pluck('id')->all());
        }
        else
        {
            $company_id = $faker->randomElement(App\Company::all()->pluck('id')->all());
        }

    return [

        'menu_id'    => $menu_id,
        'dish_id'    => $dish_id,
        'company_id' => $company_id,
        'score'      => $faker->randomFloat(2,0,5),
        'user_id'    => $faker->randomElement($users)
    ];

});
