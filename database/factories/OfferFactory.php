<?php

use Faker\Generator as Faker;

$factory->define(\App\Offer::class, function (Faker $faker) {

    $dish_id = null;
    $menu_id = null;
    $price = null;
    $discount = null;

    $rand1 = rand(0,10);
    $rand2 = rand(0,10);


    if( $rand1 >= 5  && $rand1 < 10)
    {
        $dish_id = $faker->randomElement((App\Dish::all()->pluck('id')->all()));
    }
    else
    {
        $menu_id = $faker->randomElement(App\Menu::all()->pluck('id')->all());
    }

    if( $rand2 >= 5  && $rand2 < 10)
    {
        $element = ($menu_id !== null ) ? App\Menu::find($menu_id) : App\Dish::find($dish_id);

        $price = $faker->randomFloat(2,0, $element->price);

    }
    else
    {
        $discount = $faker->numberBetween($min = 0, $max = 100);
    }

    $finish_at = $faker->date($format = 'Y-m-d');
    $start_at = $faker->date($format = 'Y-m-d', $max = $finish_at);

    return [

        'menu_id'   => $menu_id,
        'dish_id'   => $dish_id,
        'start_at'  => $start_at,
        'finish_at' => $finish_at,
        'price'     => $price,
        'discount'  => $discount
    ];

});
