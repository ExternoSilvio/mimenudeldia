<?php

use Faker\Generator as Faker;

$factory->define(App\Rel_company_feature::class, function (Faker $faker) {

    $company_id = $faker->randomElement(App\Company::all()->pluck('id')->all());
    $feature_id = $faker->randomElement(App\Feature::all()->pluck('id')->all());

    $company_feature = App\Rel_company_feature::where('company_id','=',$company_id)
                                                 ->where('feature_id',"=", $feature_id)
                                                 ->get();
    //dd($company_id);

    if(!count($company_feature))
    {
        return [

            'company_id' => $company_id,
            'feature_id' => $feature_id,
            'value' => $faker->text(200)
        ];
    }
    else
    {
        return [];
    }

});
