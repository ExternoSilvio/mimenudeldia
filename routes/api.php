<?php

use Illuminate\Http\Request;

use App\Http\Controllers\UsersController;
use App\Http\Controllers\SubsidiaryController;
use App\Http\Controllers\PhotoController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\DishController;
use App\Http\Controllers\ProvinceController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\FeatureController;
use App\Http\Controllers\ScheduleController;
use App\Http\Controllers\CategoryController;

use App\Services\OfferService;
use App\Services\ScoreService;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

UsersController::routes();

SubsidiaryController::routes();
SubsidiaryController::routesOpinions();

PhotoController::routes();
PhotoController::routesPhotoCompanyLogo();
PhotoController::routesDishPhoto();
PhotoController::routesMenuPhoto();
PhotoController::routesRelCompanyPhotos();

ProvinceController::routes();

LocationController::routes();

MenuController::routes();
MenuController::routesDishes();
MenuController::routesScore();
MenuController::routesOffer();

CompanyController::routes();
CompanyController::routesFeatures();
CompanyController::routesScore();

FeatureController::routes();

ScheduleController::routes();

CategoryController::routes();

DishController::routes();
DishController::routesFeatures();
DishController::routesScore();
DishController::routesOffer();

/*Route::get('/dishes','DishController@findAll');
Route::post('/companies/{company_id}/dishes','DishController@create');
Route::get('/companies/{company_id}/dishes/{dish_id}','DishController@find');
Route::get('/companies/{company_id}/dishes','DishController@findByCompany');
Route::get('/companies/{company_id}/categories/{category_id}/dishes','DishController@findByCompany');
Route::put('/companies/{company_id}/dishes/{dish_id}','DishController@update');
Route::delete('/companies/{company_id}/dishes/{dish_id}','DishController@delete');*/
