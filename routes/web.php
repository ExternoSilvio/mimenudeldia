<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use \App\Http\Controllers\HomeController;
use \App\Http\Controllers\Web\UserController;
use \App\Http\Controllers\Web\ProvinceController;
use \App\Http\Controllers\Web\LocationController;
use \App\Http\Controllers\Web\SubsidiaryController;
use \App\Http\Controllers\Web\CategoryController;
use \App\Http\Controllers\Web\FeatureController;
use \App\Http\Controllers\Web\CompanyController;
use \App\Http\Controllers\Web\DishController;
use \App\Http\Controllers\Web\MenuController;

use \App\Http\Controllers\WebSite\IndexController;
use \App\Http\Controllers\WebSite\RestauranteController;
use \App\Http\Controllers\WebSite\NegocioController;
use \App\Http\Controllers\WebSite\ContactoController;
use \App\Http\Controllers\WebSite\AppMovilController;
use \App\Http\Controllers\WebSite\GeoLocationController;
use \App\Http\Controllers\WebSite\SendMailController;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();




Route::group([
    'middleware' => 'auth',
    'prefix' => 'panel',
], function() {
    HomeController::routes();
    UserController::routes();
    ProvinceController::routes();
    LocationController::routes();
    SubsidiaryController::routes();
    CategoryController::routes();
    FeatureController::routes();
    CompanyController::routes();
    DishController::routes();
    MenuController::routes();

});


//prueba de vistas web

IndexController::routes();
RestauranteController::routes();
NegocioController::routes();
ContactoController::routes();
AppMovilController::routes();
GeoLocationController::routes();
SendMailController::routes();
