/**
 * Create a form with DELETE method to remove element and append to end of body tag
 * @param url
 * @param csrfToken
 */
var createDeleteForm = function(url, csrfToken) {
    var $deleteForm = $(
        '<form action="' + url + '" method="post">' +
        '   <input type="hidden" name="_method" value="DELETE">' +
        '   <input type="hidden" name="_token" value="' + csrfToken + '">' +
        '</form>'
    );
    $deleteForm.appendTo('body');
    return $deleteForm;
};

/**
 * Add confirmation window to links with remove data attribute, and changes the link to DELETE form
 */
var removeBehavior = function() {
    $('[data-remove]').each(function(i, elem) {
        var $elem = $(elem);
        $elem.off('click').on('click', function(event) {

            event.preventDefault();
            event.stopPropagation();
            if (window.confirm($elem.data('remove'))) {
                createDeleteForm($elem.attr('href'), $('meta[name="csrf-token"]').attr('content')).submit();
            }
        });
    });
};

/**
 * Swap
 */
var swapBehavior = function() {
    $('[data-swap]').each(function(i, elem) {
        var $elem = $(elem);
        var data = $elem.data('swap').split('|');
        if (data.length === 3 && data[0] !== undefined && data[1] !== undefined && data[2] !== undefined) {
            var $checkbox = $elem.find(data[0]);
            $elem.on('click', function (event) {
                event.stopPropagation();
                $checkbox.trigger('click');
            });
            $checkbox.on('click', function (event) {
                event.stopPropagation();
                if ($checkbox.prop(data[1])) {
                    $elem.addClass(data[2]);
                } else {
                    $elem.removeClass(data[2]);
                }
            });
        }
    });
};

$(document).ready(function() {
    $('#flash-overlay-modal').modal();
    $('div.alert').not('.alert-important').delay(5000).fadeOut(350);
    removeBehavior();
    swapBehavior();
});

$(document).ready(function(){

    // active class to the list

    $('.list-group a' ).each(function() {

        //for no conflict with other script

        if($(this).attr('data-swap'))
        {
            return;
        }
        //event to li element
            $(this).click(function(){

                changeActiveClass($(this).find("input"));

                $(this).toggleClass('list-group-item-info');

            });

        //event to check box element

        $(this).find('input').click(function(){

            changeActiveClass($(this));

        });

            //event to
        });

    function changeActiveClass(element){

        $element = $(element);

        if($element.prop('checked'))
            $element.prop("checked",false);
        else
            $element.prop("checked",true);

    }

    //active class to the header and body()

    var url = window.location.href;

    var index = url.charAt(url.indexOf('#') + 1) ;

    $('.box-header li').each(function(){

        if(!isNaN(index - 1))
        {
            if($(this).index() == index)
            {
                $(this).addClass('active');
            }
            else
            {
                $('.box-body').eq($(this).index()).hide();
                $('.box-body + div').eq($(this).index()).hide();
            }
        }
            $(this).click(function(){

                $('.box-header li').each(function () {

                    if($(this).hasClass('active'))
                    {
                        $('.box-body').eq($(this).index()).hide();
                        $('.box-body + div').eq($(this).index()).hide();
                        $(this).removeClass('active');
                    }
                });
                $(this).toggleClass('active');
                $('.box-body').eq($(this).index()).show();
                $('.box-body + div').eq($(this).index()).show();
            });

        });

    //if no '#' exists

    if(isNaN(index-1)) {
        $('.box-header li').eq(0).addClass('active');

        if ($('.box-header li').length) {
            for (var i = 1; i <= $('.box-header li').length; i++)
                $('.box-body').eq(i).hide();
        }
    }


    //set not null all the index 0 select values

    $('form').each(function(){

        $(this).submit(function(){

            $select = $(this).find('select');

            if($select !== null)
            {
                if($select.val() === '0')
                {
                    $select.attr('name',"");
                }
            }
        })

    });

});

