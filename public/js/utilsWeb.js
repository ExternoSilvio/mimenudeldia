
$(document).ready(function(){

    createBehavior();

});

/**
 * Create a form with POST method to add company to favorite
 * @param url
 * @param csrfToken
 */
var createPostForm = function(url, csrfToken) {
    var $postForm = $(
        '<form action="' + url + '" method="post">' +
        '   <input type="hidden" name="_method" value="POST">' +
        '   <input type="hidden" name="_token" value="' + csrfToken + '">' +
        '</form>'
    );
    $postForm.appendTo('body');
    return $postForm;
};

/**
 * Add confirmation window to links with add data attribute, and changes the link to add form
 */

var createBehavior = function() {

    $('[data-create]').each(function(i, elem) {
        var $elem = $(elem);
        $elem.off('click').on('click', function(event) {
            event.preventDefault();
            event.stopPropagation();
            if (window.confirm($elem.data('create'))) {
                createPostForm($elem.attr('href'), $('meta[name="csrf-token"]').attr('content')).submit();
            }
        });
    });
};

//get location agent

$(document).ready(function () {

    var geocoder;

    //todo not for all pages, fix froshing

    $("#owl-companies").hide().removeClass('hide');

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(geoLocation,showError);

    } else {

         console.log("Geolocation is not supported by this browser.");
    }

    function geoLocation(coords)
    {
        //console.log(coords);//https://stackoverflow.com/questions/6797569/get-city-name-using-geolocation/6798005 todo get zip code

        codeLatLng(coords.coords.latitude, coords.coords.longitude);

        $.ajax({

            url: '/geolocation',
            type: 'get',
            data: {
                param: {
                    latitude: coords.coords.latitude,
                    longitude: coords.coords.longitude
                }
            },
            success: function(result) {



                $.each(result['subsidiaries'], function(i, subsidiary) {

                    var distance = (subsidiary.distance < 1) ? parseFloat(subsidiary.distance).toFixed(3) * 1000 + ' m' : parseFloat(subsidiary.distance).toFixed(3).replace(/\./g, ",") + ' km';
                    $('<div class="item" style="padding-left: 0.3em;min-height: 400px">'+
                        '<figure>'+
                        '<div class="icon-overlay icn-link">' +
                        '<a href="/restaurante/'+[subsidiary.id]+'"><img src='+subsidiary.company_logo+' alt=""></a></div>'+
                        '<figcaption class="bordered no-top-border">'+
                        '<div class="info">'+
                        '<h4><a href="/restaurante/'+[subsidiary.id]+'">'+subsidiary.address+'</a></h4>'+
                        '<p>'+ distance +
                        '<br><a href="https://www.google.com/maps/search/?api=1&query='+subsidiary.coordinates+'" target="_blank">ver mapa</a>'+
                        '</p>'+
                        '</div>' +
                        '</figcaption>' +
                        '</figure>' +
                    '</div>').appendTo("#owl-companies");
                });


                $("#owl-companies").owlCarousel({
                    autoPlay: 5000,
                    stopOnHover: true,
                    navigation: true,
                    pagination: true,
                    rewindNav: true,
                    items: 5,
                    navigationText: ["<i class='icon-left-open-mini'></i>", "<i class='icon-right-open-mini'></i>"]
                });

                $("#owl-companies-loader").fadeOut("fast", function() {

                });
            }
        });
    }

    function showError(error) {

        alert('Necesitas activar la localización para mejorar nuestros servicios');

    }

    function initialize() {

        geocoder = new google.maps.Geocoder();

    }

    function codeLatLng(lat, lng) {

        var latlng = new google.maps.LatLng(lat, lng);
        geocoder.geocode({'latLng': latlng}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {

                if (results[1]) {
                    //formatted address
                    //console.log('address',results[0])
                    var zip = results[0].address_components[6].long_name;

                    $('#zip').val(zip);
                    $('#lat').val(lat);
                    $('#lng').val(lng);
                    //find country name
                    // for (var i=0; i<results[0].address_components.length; i++) {
                    //     for (var b=0;b<results[0].address_components[i].types.length;b++) {
                    //
                    //         //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
                    //         if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                    //             //this is the object you are looking for
                    //             city= results[0].address_components[i];
                    //             break;
                    //         }
                    //     }
                    // }
                    // //city data
                    // console.log(city.short_name + " " + city.long_name)


                } else {
                    console.info("No results found");
                }
            } else {
                console.error("Geocoder failed due to: " + status);
            }
        });
    }


    initialize();

    //give me menus filter

    $("#menus_filter").submit(function($element){
        $('.loading').hide().removeClass('hide').fadeIn();
       $element.preventDefault();

       // $("ul.filter li").each(function(i,e){
       //     $(e).remove();
       // });
       /*$("ul.items").each(function(i,e){
           $(e).empty()
       });*/

        $("ul.filter").empty();
        //$("ul.items").attr('style', 'opacity: 1;').empty();

        $container.isotope( 'remove', $('ul.items .item') );


       $.ajax({

           url: '/search',
           type: 'get',
           data: {
               param: $("#zip").val(),
               pos:
                   {
                       lng: $("#lng").val(),
                       lat:$("#lat").val()
                   }
           },

           success:function(result){
               $('.loading').fadeOut();
               //todo pulir busqueda repetitiba

               if(result['features_dishes'] !== null)
               {

                   for(var k in result['features_dishes'])
                   {
                       if (result['features_dishes'].hasOwnProperty(k)) {
                           var value = result['features_dishes'][k];

                           $('<li><a href="#" data-filter=".feature-' + k + '">' + value + '</a></li>').appendTo("ul.filter");
                       }
                   }
                   $('<li><a href="#" data-filter="*" class="active">Todas</a></li>').prependTo("ul.filter");
                   $(result['li']).appendTo("ul.items");
               }
               else
               {
                   if(result['features_dishes'] == null)
                   {
                       $('<li>No hay resultados</li>').appendTo("ul.filter");
                   }
               }


              //todo isotope reload
               portfolioHandler();

               //$('.results').isotope('reloadItems').isotope();

           }
       });

    });

});

