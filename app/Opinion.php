<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Opinion extends Model
{
    //

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $table = 'opinion';

    public function user(){

        return $this->belongsTo('App\User');

    }

    public function subsidiary(){

        return $this->belongsTo('App\Subsidiary');

    }
}
