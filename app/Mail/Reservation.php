<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Reservation extends Mailable
{
    use Queueable, SerializesModels;

    protected $fields;

    /**
     * Create a new message instance.
     * @var request
     * @return void
     */
    public function __construct(Request $fields)
    {
        $this->fields = $fields;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@digitalapp.es')
                    ->subject('mimenudeldia reserva')
                    ->view('mails.reservation')->with([

                        'name'   => $this->fields->name,
                        'email'  => $this->fields->email,
                        'phone'  => $this->fields->phone,
                        'date'   => $this->fields->date,
                        'diners' => $this->fields->diners,
                        'time'   => $this->fields->time,
                        'body'   => $this->fields->body

                    ]);
    }
}
