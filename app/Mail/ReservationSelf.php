<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReservationSelf extends Mailable
{
    use Queueable, SerializesModels;

    protected $fields;

    /**
     * Create a new message instance.
     * @var request
     * @return void
     */
    public function __construct(Request $fields)
    {
        $this->fields = $fields;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@digitalapp.es')
            ->subject('mimenudeldia: nueva reserva')
            ->view('mails.reservationSelf')->with([

                'name'   => $this->fields->name,
                'email'  => $this->fields->email,
                'phone'  => $this->fields->phone,
                'day'    => $this->fields->day,
                'time'   => $this->fields->time,
                'body'   => $this->fields->body

            ]);
    }
}
