<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SelfContact extends Mailable
{
    use Queueable, SerializesModels;

    protected $fields;

    /**
     * Create a new message instance.
     * @var $request
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->fields = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->from('noreply@digitalapp.es')
                    ->subject('mensaje de mimenudeldia: '.$subject)
                    ->view('mails.contactSelf')->with([

                        'name'    =>   $name,
                        'email'   =>   $email,
                        'subject' =>   $subject,
                        'body'    =>   $body
                    ]);
    }
}
