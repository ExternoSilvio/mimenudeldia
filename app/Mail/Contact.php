<?php

namespace App\Mail;

use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Contact extends Mailable
{
    use Queueable, SerializesModels;

    protected $fields;

    /**
     * Create a new message instance.
     * @var $message
     * @return void
     */
    public function __construct(Request $fields)
    {
        $this->fields = $fields;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

            return $this->from('noreply@digitalapp.es')
                ->subject('Mimenudeldía')
                ->view('mails.contact')->with([
                    'name' => $this->fields->name,
                    'subject' => $this->fields->subject,
                    'email' => $this->fields->email,
                    'body' => $this->fields->body
            ]);
    }
}
