<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rel_company_feature extends Model
{
    //
    protected $table = 'rel_company_feature';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'feature_id'
    ];

    public function company(){

        return $this->belongsTo('App\Company');

    }

    public function feature(){

        return $this->belongsTo('App\Feature');

    }
}
