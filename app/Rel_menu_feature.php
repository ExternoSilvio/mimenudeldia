<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rel_menu_feature extends Model
{
    protected $table = 'rel_menu_features';

    public function menu(){

        $this->belongsTo('App\Menu');

    }

    public function feature(){

        $this->belongsTo('App\Feature');

    }

}
