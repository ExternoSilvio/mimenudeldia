<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Score extends Model
{
    //
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $table = 'score';

    public function menu(){

        return $this->belongsTo('App\Menu');

    }

    public function user(){

        return $this->belongsTo('App\User');

    }

    public function company(){

        return $this->belongsTo('App\Company');

    }

    public function dish(){

        return $this->belongsTo('App\dish');

    }
}
