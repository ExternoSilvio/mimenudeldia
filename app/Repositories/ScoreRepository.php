<?php
/**
 * Created by PhpStorm.
 * User: tobiasbachmann
 * Date: 10/4/18
 * Time: 10:14
 */

namespace App\Repositories;


use App\Dish;
use App\Score;

class ScoreRepository
{

    public function findAll(){

        $scores = Score::all();

        return $scores;

    }

    public function findDishesScore($dishId){

        $scores = Score::where('dish_id','=',$dishId)->get();

        return $scores;

    }

    public function findDishScore($dishId,$scoreId){

        $score = Score::where('id','=',$scoreId)
                        ->where('dish_id','=',$dishId)->get()->first();

        return $score;
    }

    public function findMenusScore($menuId){

        $scores = Score::where('menu_id','=',$menuId)->get();

        return $scores;

    }

    public function findMenuScore($menuId,$scoreId){

        $score = Score::where('id','=',$scoreId)
            ->where('menu_id','=',$menuId)->get()->first();

        return $score;
    }

    public function findCompaniesScore($companyId){

        $scores = Score::where('company_id','=',$companyId)->get();

        return $scores;

    }

    public function findCompanyScore($companyId,$scoreId){

        $score = Score::where('id','=',$scoreId)
            ->where('company','=',$companyId)->get()->first();

        return $score;
    }

}