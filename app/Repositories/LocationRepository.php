<?php
/**
 * Created by PhpStorm.
 * User: tobiasbachmann
 * Date: 20/3/18
 * Time: 11:36
 */

namespace App\Repositories;

use App\Http\Controllers\Web\LocationController;
use App\Location;

class LocationRepository
{
    public function find(int $locationId) {

            return  Location::find($locationId);
    }

    public function findAll(){

        return Location::all();

    }

    public function findByProvince(int $provinceId, int $locationId){

        return Location::where('id','=',$locationId)->get()
                         ->where('province_id','=',$provinceId);


    }

    public function findAllByProvince(int $provinceId) {
        return Location::where('province_id','=',$provinceId)->get();
    }

    public function findByZip(int $zip){

        return Location::where('zip','=',$zip)->get()->first();

    }


}