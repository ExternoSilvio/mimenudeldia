<?php
/**
 * Created by PhpStorm.
 * User: paveldelpozo
 * Date: 16/3/18
 * Time: 16:48
 */

namespace App\Repositories;

use App\Province;

class ProvinceRepository
{

    public function find(int $province_id) {
        return Province::find($province_id);
    }

    public function findAll() {
        return Province::all();
    }



}