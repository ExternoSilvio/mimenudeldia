<?php
/**
 * Created by PhpStorm.
 * User: tobiasbachmann
 * Date: 9/4/18
 * Time: 15:45
 */

namespace App\Repositories;

use App\Offer;


class OfferRepository
{


    public function findAll(){

        $offers = Offer::find()->get();

        return $offers;
    }

    public function find($offerId){

        return Offer::find($offerId);

    }

    public function findDish(int $dishId, int $offerId){

    $offer = Offer::where('id','=',$offerId)
        ->where('dish_id','=',$dishId)
        ->get()->first();

    return $offer;
}

    public function findDishes(int $dishId){

        $offer = Offer::where('dish_id','=',$dishId)
                        ->get();

        return $offer;
    }

    public function findMenu(int $menuId, int $offerId){

        $offer = Offer::where('id','=',$offerId)
            ->where('menu_id','=',$menuId)
            ->get()->first();

        return $offer;
    }

    public function findMenus(int $menuId){

        $offer = Offer::where('menu_id','=',$menuId)->get();

        return $offer;
    }
}