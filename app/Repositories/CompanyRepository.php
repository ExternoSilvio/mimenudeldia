<?php
/**
 * Created by PhpStorm.
 * User: tobiasbachmann
 * Date: 20/3/18
 * Time: 9:56
 */

namespace App\Repositories;

use App\Company;
use App\Rel_company_feature;

class CompanyRepository
{

    public function find(int $companyId) {
        return Company::find($companyId);
    }

    public function findAll() {
        return Company::all();
    }

    public function findFeatures(int $companyId) {

        $company = $this->find($companyId);

        if($company)
        {
            return $company->features();
        }
        else
        {
            return null;
        }
        /*return Rel_company_feature::where('company_id','=',$companyId)
            ->get();*/


    }

    public function findFeature(int $companyId,int $featureId) {
        return Rel_company_feature::where('company_id','=',$companyId)
            ->where('feature_id','=',$featureId)
            ->get()->first();


    }

    public function findLast(int $limit){

        return Company::orderby('created_at','DESC')->take($limit)->get();

    }


}