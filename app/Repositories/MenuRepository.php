<?php
/**
 * Created by PhpStorm.
 * User: tobiasbachmann
 * Date: 21/3/18
 * Time: 10:36
 */

namespace App\Repositories;

use App\Menu;
use App\Rel_menu_dish;
use App\Rel_menu_feature;
use Illuminate\Support\Facades\DB;

class MenuRepository
{

    public function find(int $menuId){

        return Menu::find($menuId);
    }

    public function findByCompany(int $companyId, int $menuId) {
        return Menu::where('company_id','=',$companyId)
            ->where('id','=',$menuId)
            ->get()->first();


    }

    public function findAllByCompany(int $companyId) {
        return Menu::where('company_id','=',$companyId)
            ->get();
    }

    public function findAll(){

        return Menu::all();
    }

    public function findDishes(int $menuId) {
        return Rel_menu_dish::where('menu_id','=',$menuId)
                                ->get();
    }

    public function findFeatures(int $menuId){

        return Rel_menu_feature::where('menu_id','=',$menuId)
                                ->get();

    }

    public function findDish(int $menuId, int $dishId) {
        return Rel_menu_dish::where('menu_id','=',$menuId)->where('dish_id','=',$dishId)
            ->get()->first();
    }

    public function giveMeMenusByZip($zip){


        $menus = DB::table('menu')
            ->join('company', 'company.id', '=', 'menu.company_id')
            ->join('subsidiary', 'company.id', '=', 'subsidiary.company_id')
            ->join('location', 'location.id', '=', 'subsidiary.location_id')
            ->where('location.zip', '=', $zip)
            ->select('menu.id', 'menu.name')
            ->get();

        return $menus;

//        return  DB::select( DB::raw("SELECT menu.id
//                                            FROM location
//                                            JOIN subsidiary ON location.id = subsidiary.location_id AND location.zip = :zip
//                                            JOIN company ON company.id = subsidiary.company_id
//                                            JOIN menu ON menu.company_id = company.id"),array($zip) );

    }

}