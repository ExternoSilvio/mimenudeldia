<?php
/**
 * Created by PhpStorm.
 * User: tobiasbachmann
 * Date: 21/3/18
 * Time: 11:43
 */

namespace App\Repositories;

use App\Feature;
use App\Rel_dish_feature;

class FeatureRepository
{

    public function find(int $feature_id) {
        return Feature::find($feature_id);
    }

    public function findAll() {
        return Feature::all();
    }

    public function findDishes(int $featureId) {
        return Rel_dish_feature::where('feature_id','=',$featureId)
            ->get();


    }


}