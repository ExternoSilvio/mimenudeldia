<?php
/**
 * Created by PhpStorm.
 * User: tobiasbachmann
 * Date: 22/3/18
 * Time: 9:42
 */

namespace App\Repositories;

use App\User;
use App\Favorite;
use App\Score;

class UsersRepository
{

    public function find(int $users_id) {
        return User::find($users_id);
    }

    public function findAll() {
        return User::all();
    }

    public function exist($email){

        return User::where('email','=',$email)
                    ->where('deleted_at','<>',null)
                    ->withTrashed()->get()->first();

    }

    public function findFavorite(int $companyId, int $usersId){

        return Favorite::where('company_id','=', $companyId)
                        ->where('user_id','=', $usersId)->get()->first();

    }
    public function findFavorites(int $usersId){

        return Favorite::where('user_id','=', $usersId)->get();


    }

    public function findScoreMenus(int $userId){

        return Score::where('user_id','=',$userId)
                     ->where('menu_id','<>', null)->get();

    }

    public function findScoreMenu(int $userId, int $menuId){

        return Score::where('user_id','=',$userId)
            ->where('menu_id','=', $menuId)->get()->first();

    }

    public function findScoreDishes(int $userId){

        return Score::where('user_id','=',$userId)
            ->where('dish_id','<>', null)->get();

    }
}