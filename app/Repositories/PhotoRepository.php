<?php
/**
 * Created by PhpStorm.
 * User: tobiasbachmann
 * Date: 23/3/18
 * Time: 11:20
 */

namespace App\Repositories;

use App\Company;
use App\Dish;
use App\Photo;
use App\Menu;
use App\Rel_company_photo;

class PhotoRepository
{

    public function find($photoId){


        $photo = Photo::find($photoId);

        return $photo;

    }

    public function findByUrl($photoUrl){


        $photo = Photo::where('url','=',$photoUrl)->get()->first();

        return $photo;

    }

    public function findAll(){

        return $photos = Photo::all();

    }

    public function findCompanyLogo($companyId,$logoId){

        $company = Company::where('id','=',$companyId)
                            ->where('logo_id','=',$logoId)->get()->first();

        return $company;
    }

    public function findDishPhotoByCompany($companyId, $dishId, $photoId){

        $dish = Dish::where('id','=',$dishId)
                    ->where('company_id','=',$companyId)
                    ->where('photo_id','=',$photoId)->get()->first();

        return $dish;
    }

    public function findDishPhoto($dishId,$photoId){

        $dish = Dish::where('id','=',$dishId)
                    ->where('photo_id','=',$photoId)->get()->firs();

        return $dish;
    }

    public function findMenuPhoto($menuId, $photoId){

        $menu = Menu::where('id','=',$menuId)
            ->where('photo_id','=',$photoId)->get()->first();

        return $menu;
    }

    public function findMenuPhotoByCompany($companyId,$menuId, $photoId){

        $menu = Menu::where('id','=',$menuId)
                    ->where('company_id','=',$companyId)
                    ->where('photo_id','=',$photoId)->get()->first();

        return $menu;
    }

    public function findCompanyPhotos(int $companyId){

        return $rel_company_photos = Rel_company_photo::where('company_id','=',$companyId)->get();

    }

    public function findCompanyPhoto(int $companyId, int $photoId){

        return $rel_company_photos = Rel_company_photo::where('company_id','=',$companyId)
                                                        ->where('photo_id','=',$photoId)
                                                        ->get()->first();

    }





}