<?php
/**
 * Created by PhpStorm.
 * User: tobiasbachmann
 * Date: 20/3/18
 * Time: 13:29
 */

namespace App\Repositories;


use App\Opinion;
use App\Services\SubsidiaryService;
use App\Subsidiary;

class SubsidiaryRepository
{


    public function find(int $subsidiaryId)
    {
        return Subsidiary::find($subsidiaryId);
    }

    public function findByCompany(int $companyId, int $subsidiaryId) {
        return Subsidiary::where('company_id','=',$companyId)
                            ->where('id','=',$subsidiaryId)
                            ->get()->first();


    }

    public function findAllByLocation(int $locationId){

        return Subsidiary::where('location_id','=',$locationId)->get();

    }

    public function findAll(){

        return Subsidiary::all();

    }

    public function findLast(int $limit){

        return Subsidiary::orderBy('created_at','DESC')->take($limit)->get();

    }

    public static function findLastStatic(int $limit){

        return Subsidiary::orderBy('created_at','DESC')->take($limit)->get();

    }

    public function findPrimary(int $companyId){

        return Subsidiary::where('company_id','=',$companyId)->get()->first();

    }

    public function findAllByCompany(int $companyId) {
        return Subsidiary::where('company_id','=',$companyId)
                            ->get();
    }

    public function findOpinions(int $subsidiary_id){

        return Opinion::where('subsidiary_id','=',$subsidiary_id)
            ->get();

    }

    public function findOpinion(int $opinionId){

        return Opinion::find($opinionId);


    }
}