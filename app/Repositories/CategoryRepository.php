<?php
/**
 * Created by PhpStorm.
 * User: tobiasbachmann
 * Date: 20/3/18
 * Time: 17:41
 */

namespace App\Repositories;

use App\Category;


class CategoryRepository
{

    public function find(int $category_id) {
        return Category::find($category_id);
    }

    public function findAll() {
        return Category::all();
    }

}