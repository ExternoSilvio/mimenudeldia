<?php
/**
 * Created by PhpStorm.
 * User: tobiasbachmann
 * Date: 20/3/18
 * Time: 16:26
 */

namespace App\Repositories;

use App\Schedule;

class ScheduleRepository
{

    public function find(int $subsidiaryId, int $scheduleId) {
        return Schedule::where('id','=',$scheduleId)
            ->where('subsidiary_id','=',$subsidiaryId)
            ->get()->first();


    }

    public function findAll(int $subsidiaryId) {
        return Schedule::where('subsidiary_id','=',$subsidiaryId)
                        ->get();
    }
}