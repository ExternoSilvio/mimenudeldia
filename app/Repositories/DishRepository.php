<?php
/**
 * Created by PhpStorm.
 * User: tobiasbachmann
 * Date: 20/3/18
 * Time: 18:28
 */

namespace App\Repositories;


use App\Dish;
use App\Location;
use App\Rel_dish_feature;
use Illuminate\Support\Facades\DB;


class DishRepository
{

    public function find(int $dishId){

        return Dish::find($dishId);

    }

    public function findByCompany(int $companyId, int $dishId) {
        return Dish::where('company_id','=',$companyId)
            ->where('id','=',$dishId)
            ->get()->first();


    }

    public function findAll(){

        return Dish::all();

    }

    public function findAllByCompany(int $companyId) {
        return Dish::where('company_id','=',$companyId)
            ->get();
    }

    public function giveMeDishesByZip(int $zip)
    {
        $dishes = DB::table('dish')
            ->join('company', 'company.id', '=', 'dish.company_id')
            ->join('subsidiary', 'company.id', '=', 'subsidiary.company_id')
            ->join('location', 'location.id', '=', 'subsidiary.location_id')
            ->where('location.zip', '=', $zip)
            ->select('dish.id', 'dish.name')
            ->get();

        return $dishes;
//            DB::select( DB::raw("SELECT dish.id,dish.name
//                                            FROM location
//                                            JOIN subsidiary ON location.id = subsidiary.location_id AND location.zip = :zip
//                                            JOIN company ON company.id = subsidiary.company_id
//                                            JOIN dish ON dish.company_id = company.id"),array($zip) );


    }

    public function giveMeFeaturesByZip(int $zip){


        $features = DB::table('feature')
            ->join('rel_dish_feature', 'feature.id', '=', 'rel_dish_feature.feature_id')
            ->join('dish', 'dish.id', '=', 'rel_dish_feature.dish_id')
            ->join('company', 'company.id', '=', 'dish.company_id')
            ->join('subsidiary', 'company.id', '=', 'subsidiary.company_id')
            ->join('location', 'location.id', '=', 'subsidiary.location_id')
            ->where('location.zip', '=', $zip)
            ->select('feature.id', 'feature.name')
            ->get();

        return $features;

//        return  DB::select( DB::raw( "SELECT DISTINCT(feature.id),feature.name
//                                    FROM location
//                                    JOIN subsidiary ON location.id = subsidiary.location_id AND location.zip = :zip
//                                    JOIN company ON company.id = subsidiary.company_id
//                                    JOIN dish ON dish.company_id = company.id
//                                    JOIN rel_dish_feature ON rel_dish_feature.dish_id = dish.id
//                                    JOIN feature ON feature.id = rel_dish_feature.feature_id"),array($zip) );

    }

    public function findFeatures(int $dishId) {
        return Rel_dish_feature::where('dish_id','=',$dishId)
                                ->get();


    }

    public function findFeature(int $dishId,int $featureId) {
        return Rel_dish_feature::where('dish_id','=',$dishId)
                                ->where('feature_id','=',$featureId)
                                ->get()->first();


    }
}