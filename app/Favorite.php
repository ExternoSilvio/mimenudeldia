<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Favorite extends Model
{
    //
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $table = 'favorite';

    public function user(){

        return $this->belongsTo('App\User');
    }

    public function company (){

        return $this->belongsTo('App\Company');

    }
}
