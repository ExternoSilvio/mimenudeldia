<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Model
{
    //

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $table = 'menu';

    public function photo(){

        return $this->belongsTo('App\Photo');

    }

    public function company(){

        return $this->belongsTo('App\Company');

    }

    public function scores(){

        return $this->hasMany('App\Score');

    }

    public function dishes(){

        return $this->hasMany('App\Rel_menu_dish');

    }

    public function features(){

        return $this->hasMany('App\Rel_menu_feature');

    }

    public function offers(){

        return $this->hasMany('App\Offer');

    }


}
