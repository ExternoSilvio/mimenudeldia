<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    //
    protected $table = 'feature';

    public function feature(){

       return $this->belongsTo('App\Feature');

    }

    public  function features(){

        return $this->hasMany('App\Feature');

    }

    public function relDishesFeatures(){


        return $this->hasMany('App\Rel_dish_feature');

    }

    public function relCompaniesFeatures(){


        return $this->hasMany('App\Rel_company_feature');

    }

    public function menus(){


        return $this->hasMany('App\Rel_menu_feature');

    }
}
