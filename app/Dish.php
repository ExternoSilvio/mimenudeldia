<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dish extends Model
{
    //
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $table = 'dish';

    public function offers(){

        return $this->hasMany('App\Offer');

    }

    public function menus(){

        return $this->hasMany('App\Rel_menu_dish');

    }

    public function scores(){

        return $this->hasMany('App\Score');

    }

    public function features(){

        return $this->hasMany('App\Rel_dish_feature');

    }

    public function category(){

        return $this->belongsTo('App\Category');

    }

    public function company(){

        return $this->belongsTo('App\Company');

    }

    public  function photo(){

        return $this->belongsTo('App\Photo');

    }
}
