<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    //

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $table = 'company';

    public function subsidiaries(){

        return $this->hasMany('App\Subsidiary');

    }

    public function photos(){

        return $this->hasMany('App\Rel_company_photo');

    }

    public function logo(){

        return $this->belongsTo('App\Photo');

    }

    public function user(){

        return $this->belongsTo('App\User');
    }

    public function features(){

        return $this->hasMany('App\Rel_company_feature');

    }

    public function scores(){

        return $this->hasMany('App\Score');

    }

    public function favorites(){


        return $this->hasMany('App\Favorite');

    }

    public function menus(){

        return $this->hasMany('App\Menu');

    }

    public function dishes(){

        return $this->hasMany('App\Dish');
    }

}
