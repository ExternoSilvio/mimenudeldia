<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rel_dish_feature extends Model
{
    //
    protected $table = 'rel_dish_feature';

    public function feature(){

        return $this->belongsTo('App\Feature');

    }

    public function dish(){

        return $this->belongsTo('App\Dish');

    }
}
