<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subsidiary extends Model
{
    //
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $table = 'subsidiary';

    public function company(){

        return $this->belongsTo('App\Company');
    }

    public function location(){

        return $this->belongsTo('App\Location');
    }

    public function opinions(){

        return $this->hasMany('App\Opinion');

    }

    public function schedules(){

        return $this->hasMany('App\Schedule');

    }
}
