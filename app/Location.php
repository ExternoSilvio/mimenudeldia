<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    //
    protected $table = 'location';


    public function province(){

       return $this->belongsTo('App\Province');
    }

    public function subsidiaries(){

        return $this->hasMany('App\Subsidiary');

    }
}
