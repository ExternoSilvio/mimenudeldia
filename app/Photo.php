<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    //
    protected $table = 'photo';

    public function dishes(){

        return $this->hasMany('App\Dish');

    }

    public function menus(){

        return $this->hasMany('App\Menu');

    }

    public function companies(){

        return $this->hasMany('App\Company','logo_id');

    }

    public function relCompaniesPhotos(){

        return $this->hasMany('App\Rel_company_photo');

    }
}
