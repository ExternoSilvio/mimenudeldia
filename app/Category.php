<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table = 'category';

    public function dishes(){

        return $this->hasMany('App\Dish');

    }

    public function category(){

        return $this->belongsTo('App\Category');

    }

    public function categories(){

        return $this->hasMany('App\Category');

    }
}
