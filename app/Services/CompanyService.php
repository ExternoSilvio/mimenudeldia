<?php
/**
 * Created by PhpStorm.
 * User: tobiasbachmann
 * Date: 20/3/18
 * Time: 9:57
 */

namespace App\Services;

use Illuminate\Http\Request;
use App\Repositories\CompanyRepository;
use App\Repositories\FeatureRepository;
use App\Company;
use App\Rel_company_feature;
use App\Repositories\DishRepository;
use App\Repositories\SubsidiaryRepository;

class CompanyService
{

    private $companyRepository;
    private $featureRepository;
    private $dishRepository;
    private $subsidiaryRepository;

    /**
     * CompanyService constructor.
     * @param $companyRepository
     * @param $featureRepository
     * @param $dishRepository
     * @param $subsidiaryRepository
     */
    public function __construct(CompanyRepository $companyRepository, FeatureRepository $featureRepository, DishRepository $dishRepository, SubsidiaryRepository $subsidiaryRepository)
    {
        $this->companyRepository = $companyRepository;
        $this->featureRepository = $featureRepository;
        $this->dishRepository = $dishRepository;
        $this->subsidiaryRepository = $subsidiaryRepository;
    }

    public function find(int $companyId) {

        $companyId = $this->companyRepository->find($companyId);

        return $companyId;
    }

    public function findAll() {
        $companies = $this->companyRepository->findAll();

        if($companies->count())
        {
            return $companies;
        }
        else
        {
            return null;
        }

    }

    public function findLast(int $limit){

        $companies = $this->companyRepository->findLast($limit);

        if(count($companies))
        {
            return $companies;
        }

        return null;
    }


    public function giveMeFeaturesFromCompanies(){



    }

    public function findFeatures(int $companyId) {



        $company = $this->find($companyId);

        if($company)
        {
            $features = $this->companyRepository->findFeatures($companyId);

            if($features->count())
            {

                return $this->companyRepository->findFeatures($companyId);
            }

        }

        return null;
    }

    public function hasFeatureCompany(int $companyId, array $features){

        $featuresAll = $this->findFeatures($companyId);

        if($featuresAll !== null)
        {
            $all = $featuresAll->get(['feature_id'])->pluck('feature_id')->all();

            $result = array_intersect($features,$all);

            return (count($result) >= count($all) ? true : false);
        }

        return false;

    }

    public function create(Request $request) {

        try
        {
            $company = new Company();
            $company->name = ($request->has('name') ? $request->name : null);
            $company->identification = ($request->has('identification') ? $request->identification : null);
            $company->web = ($request->has('web') ? $request->web : null);
            $company->information = ($request->has('information') ? $request->information : null);
            $company->save();

            return $company;

        } catch (\Exception $e){

            return null;

        }

    }

    function setFeature(int $companyId, int $featureId ){

        $company = $this->find($companyId);

        if($company)
        {
            $feature = $this->featureRepository->find($featureId);

            if($feature)
            {
                $rel_company_feature = new Rel_company_feature();

                $rel_company_feature->company_id = $company->id;

                $rel_company_feature->feature_id = $feature->id;

                $rel_company_feature->save();

                return $rel_company_feature;
            }
        }

        return null;
    }

    public function update(int $companyId, Request $request) {

        $company = $this->find($companyId);

        if($company)
        {
            $company->name = ($request->has('name') ? $request->name : $company->name);
            $company->identification = ($request->has('identification') ? $request->identification : $company->identification);
            $company->web = ($request->has('web') ? $request->web : $company->web);
            $company->information = ($request->has('information') ? $request->information : $company->information);
            $company->save();

            return $company;
        }

        return null;

    }

    public function delete(int $companyId) {

        $company = $this->find($companyId);

        if($company)
        {
            $featuresRel = $company->features()->get();
            $dishes = $company->dishes()->get();
            $menus = $company->menus()->get();
            $photosRel = $company->photos()->get();
            $favorites = $company->favorites()->get();
            $scores = $company->scores()->get();
            $subsidiaries = $company->subsidiaries()->get();

            foreach ($featuresRel as $featureRel)
            {
                $featureRel->delete();
            }
            foreach ($dishes as $dish)
            {

                $featuresDish = $dish->relDishesFeatures()->get();
                $offers = $dish->offers()->get();
                $menuDishes = $dish->relMenusDishes()->get();
                $scoreDishes = $dish->scores()->get();

                foreach ($featuresDish as $featureDish)
                {
                    $featureDish->delete();
                }
                foreach ($offers as $offer)
                {
                    $offer->delete();
                }
                foreach ($menuDishes as $menuDish)
                {
                    $menuDish->delete();
                }
                foreach ($scoreDishes as $scoreDish)
                {
                    $scoreDish->delete();
                }

                $dish->delete();
            }
            foreach ($menus as $menu)
            {

                $offerMenus = $menu->offers()->get();
                $relMenuDishes = $menu->relMenusDishes()->get();
                $scoreMenus = $menu->scores()->get();

                foreach ($offerMenus as $offerMenu)
                {
                    $offerMenu->delete();
                }
                foreach ($relMenuDishes as $relMenuDish)
                {
                    $relMenuDish->delete();
                }
                foreach ($scoreMenus as $scoreMenu)
                {
                    $scoreMenu->delete();
                }
                $menu->delete();
            }
            foreach ($photosRel as $photoRel)
            {
                $photoRel->delete();
            }
            foreach ($favorites as $favorite)
            {
                $favorite->delete();
            }
            foreach ($scores as $score)
            {
                $score->delete();
            }
            foreach ($subsidiaries as $subsidiary)
            {
                $opinions = $subsidiary->opinions()->get();
                $schedules = $subsidiary->schedules()->get();

                foreach ($opinions as $opinion)
                {
                    $opinion->delete();
                }
                foreach ($schedules as $schedule)
                {
                    $schedule->delete();
                }

                $subsidiary->delete();
            }


            return $company->delete();

        }
        else
        {
            return null;
        }
    }

    public function deleteFeature(int $companyId, int $featureId) {

        $company = $this->find($companyId);

        if($company)
        {
            $feature = $this->companyRepository->findFeature($companyId,$featureId);

            if($feature)
            {
                return $feature->delete();
            }
        }

        return null;
    }

}