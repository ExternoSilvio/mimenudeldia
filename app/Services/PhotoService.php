<?php
/**
 * Created by PhpStorm.
 * User: tobiasbachmann
 * Date: 23/3/18
 * Time: 11:04
 */

namespace App\Services;

use App\Company;
use App\Dish;
use App\Photo;
use App\Menu;
use App\Rel_company_feature;
use App\Rel_company_photo;
use Illuminate\Http\Request;
use App\Repositories\CompanyRepository;
use App\Repositories\PhotoRepository;
use App\Repositories\DishRepository;
use App\Repositories\MenuRepository;
use Illuminate\Support\Facades\Storage;
use Validator;

class PhotoService
{
    /**
     * PhotoService constructor.
     * @param $companyRepository
     */

    private $photoRepository;
    private $companyRepository;
    private $dishRepository;
    private $menuRepository;

    public function __construct(PhotoRepository $photoRepository, CompanyRepository $companyRepository, DishRepository $dishRepository, MenuRepository $menuRepository)
    {
        $this->photoRepository = $photoRepository;
        $this->companyRepository = $companyRepository;
        $this->dishRepository = $dishRepository;
        $this->menuRepository = $menuRepository;
    }

    public function find($photoId){


        $photo = $this->photoRepository->find($photoId);

        if(count($photo))
        {
            return $photo;
        }

        return null;
    }

    public function findByUrl($photoUrl){


        return $this->photoRepository->findByUrl($photoUrl);


    }

    public function findAll(){

        $photos = $this->photoRepository->findAll();

        if($photos->count())
        {
            return $photos;
        }
        else
        {
            return null;
        }
    }

    public function create(Request $request, string $folder = 'public/photos'){

        try
        {
            $photo = new Photo();

            $photo->url = ($request->has('photos') ? $request->photos->store($folder) : null);

            $photo->save();

            return $photo;
        }catch (\Exception $e){

            return null;
        }



    }

    public function delete($photoId){

        $photo = $this->find($photoId);

        if($photo)
        {

            $dishes = $photo->dishes()->get();
            $menus = $photo->menus()->get();
            $companies = $photo->companies()->get();
            $relCompaniesPhotos = $photo->relCompaniesPhotos()->get();



            foreach ($dishes as $dish )
            {
                $dish->photo_id = null;
                $dish->save();
            }
            foreach ($menus as $menu)
            {
                $menu->photo_id = null;
                $menu->save();
            }
            foreach($companies as $company)
            {

                $company->logo_id = null;
                $company->save();
            }
            foreach ($relCompaniesPhotos as $relCompanyPhoto)
            {
                $relCompanyPhoto->delete();
                $relCompanyPhoto->save();
            }

            return $photo->delete();
        }

        return null;
    }

    public function  update($photoId, Request $request) {


    }

    public function findCompanyLogo(int $companyId, int $logoId){

        $company = $this->companyRepository->find($companyId);

        if($company)
        {
            $logo = $this->find($logoId);

            if($logo)
            {
                $logoCompany = $this->photoRepository->findCompanyLogo($companyId, $logoId);

                if($logoCompany)
                {
                    return $logo;
                }

            }
        }

        return null;
    }

    public function setCompanyLogo(int $companyId, Request $request){

        $company = $this->companyRepository->find($companyId);

        if($company)
        {
            $logo = $this->create($request, 'public/photos/logo');

            $company->logo_id = $logo->id;

            $company->save();

            return $logo;

        }

        return null;
    }

    public function deleteCompanyLogo(int $companyId, int $logoId){

        $company = $this->companyRepository->find($companyId);

        if($company)
        {
            $logo = $this->find($logoId);

            if($logo)
            {
                $logoCompany = $this->findCompanyLogo($company->id, $logo->id);

                if($logoCompany)
                {
                    try{

                        Storage::delete($logo->url);
                        $company->logo_id = null;
                        $company->save();
                        return $logo->delete();


                    }catch(\Exception $e){

                        dd($e);
                        return null;

                    }
                }
            }
        }


        return null;


    }

    public function updateCompanyLogo(int $companyId, int $logoId, Request $request){


        $company = $this->companyRepository->find($companyId);

        if( $company )
        {

            $photo = $this->find($logoId);

            if($photo)
            {

                $companyPhoto = $this->findCompanyLogo($company->id, $photo->id);

                if($companyPhoto)
                {

                    try{

                        Storage::delete($photo->url);
                        $photo->url = ($request->has('photos') ? $request->photos->store('public/photos/photoMenu') : null );

                        $photo->save();

                        return $photo;

                    }catch(\Exception $e){

                        return null;

                    }
                }

            }
        }

        return null;
    }

    public function updateLogo(Company $company,Request $request){

        $photo = $this->find($company->logo_id);

        if($photo)
        {
            $companyLogo = $this->findCompanyLogo($company->id, $company->logo_id);

            if($companyLogo)
            {
                Storage::delete($photo->url);
                $this->delete($company->logo_id);

            }
        }

        $logo = new Photo() ;
        $extension = $request->logo->extension();

        $logo->url = $request->file('logo')->storeAs('public/photos/logo', str_slug($company->name).'.'.$extension);

        $logo->save();

        $company->logo_id = $logo->id;

        $company->save();

        return $logo;

    }

    public function findDishPhotoByCompany(int $companyId, int $dishId, int $photoId){

        $company = $this->companyRepository->find($companyId);

        $dish = $this->dishRepository->findByCompany($company->id,$dishId);

        if($company && $dish)
        {

            $photo = $this->find($photoId);

            if($photo)
            {
                $photoDish = $this->photoRepository->findDishPhotoByCompany($companyId, $dishId, $photoId);

                if($photoDish)
                {
                    return $photo;
                }

            }
        }

        return null;
    }

    public function findDishPhoto(int $dishId,int $photoId){

        $dish = $this->photoRepository->findDishPhoto($dishId,$photoId);

        if(count($dish))
        {
            return $dish;
        }
        else
        {
            return null;
        }

    }

    public function setDishPhoto(int $companyId, int $dishId, Request $request){

        $dish = $this->dishRepository->find($companyId,$dishId);

        if($dish)
        {
            $photo = $this->create($request, 'public/photos/photoDish');

            $dish->photo_id = $photo->id;

            $dish->save();

            return $dish;

        }

        return null;
    }

    public function deleteDishPhotoByCompany(int $companyId, int $dishId, int $photoId){

        $company = $this->companyRepository->find($companyId);

        $dish = $this->dishRepository->find($companyId,$dishId);

        if($dish && $company)
        {
            $photo = $this->find($photoId);

            if($photo)
            {
                $dishPhoto = $this->findDishPhoto($company->id, $dishId, $photo->id);

                if($dishPhoto)
                {
                    try{

                        Storage::delete($photo->url);
                        $dishPhoto->logo_id = null;

                        return $photo->delete();

                    }catch(\Exception $e){

                        return null;

                    }
                }
            }
        }


        return null;


    }

    public function updateDishPhotoByCompany(int $companyId, int $dishId, int $photoId, Request $request){

        $company = $this->companyRepository->find($companyId);

        $dish = $this->dishRepository->find($companyId,$dishId);

        if($company && $dish )
        {

            $photo = $this->find($photoId);

            if($photo)
            {

                $menuPhoto = $this->findMenuPhoto($company->id, $dishId, $photo->id);

                if($menuPhoto)
                {

                    try{

                        Storage::delete($photo->url);
                        $photo->url = ($request->has('url') ? $request->url->store('public/photos/photoDish') : null );
                        $photo->save();

                        return $photo;

                    }catch(\Exception $e){

                        return null;

                    }
                }

            }
        }

        return null;
    }

    public function updateDishPhoto(Dish $dish, Request $request){

        $photo = $this->find($dish->photo_id);

        if($photo)
        {
            $dish = $this->findDishphoto($dish->id, $dish->photo_id);

            if($dish)
            {
                Storage::delete($photo->url);
                $this->delete($dish->logo_id);

            }
        }

        $photo = new Photo() ;
        $extension = $request->photo->extension();

        $photo->url = $request->file('photo')->storeAs('public/photos/dish', str_slug($dish->name).'.'.$extension);

        $photo->save();

        $dish->photo_id = $photo->id;

        $dish->save();

        return $photo;
    }

    public function findMenuPhoto(int $menuId, int $photoId){

        $menu = $this->menuRepository->find($menuId);

        if($menu)
        {
            $photo = $this->find($photoId);

            if($photo)
            {
                $photoMenu = $this->photoRepository->findMenuPhoto($menuId, $photoId);

                if($photoMenu)
                {
                    return $photo;
                }

            }
        }

        return null;
    }

    public function findMenuPhotoByCompany(int $companyId, int $menuId, int $photoId){

        $company = $this->companyRepository->find($companyId);

        $menu = $this->menuRepository->find($company->id,$menuId);

        if($company && $menu)
        {
            $photo = $this->find($photoId);

            if($photo)
            {

                $photoMenu = $this->photoRepository->findMenuPhoto($companyId, $menuId, $photoId);

                if($photoMenu)
                {
                    return $photo;
                }

            }
        }

        return null;
    }

    public function setMenuPhoto(int $companyId, int $menuId, Request $request){

        $menu = $this->menuRepository->find($companyId,$menuId);

        $company = $this->companyRepository->find($companyId);

        if($menu && $company)
        {

            $photo = $this->create($request, 'public/photos/photoMenu');

            $menu->photo_id = $photo->id;

            $menu->save();

            return $menu;

        }

        return null;
    }

    public function deleteMenuPhoto(int $companyId, int $menuId, int $photoId){

        $company = $this->companyRepository->find($companyId);

        $menu = $this->menuRepository->find($companyId,$menuId);


        if($menu && $company)
        {
            $photo = $this->find($photoId);

            if($photo)
            {
                $menuPhoto = $this->findMenuPhoto($company->id, $menuId, $photo->id);

                if($menuPhoto)
                {
                    try{

                        Storage::delete($photo->url);
                        $menuPhoto->photo_id = null;

                        return $photo->delete();

                    }catch(\Exception $e){

                        return null;

                    }
                }
            }
        }


        return null;

    }

    public function updateMenuPhoto(Menu $menu, Request $request){

        $photo = $this->find($menu->photo_id);

        if($photo)
        {

            $photoMenu = $this->findMenuPhoto($menu->id, $menu->photo_id);

            if($photoMenu)
            {
                Storage::delete($photo->url);
                $this->delete($menu->photo_id);

            }
        }

        $photo = new Photo() ;
        $extension = $request->photo->extension();

        $photo->url = $request->file('photo')->storeAs('/public/photos/menu', str_slug($menu->name).'.'.$extension);

        $photo->save();

        $menu->photo_id = $photo->id;

        $menu->save();

        return $photo;
    }

    public function updateMenuPhotoByCompany(int $companyId, int $menuId, int $photoId, Request $request){

        $company = $this->companyRepository->find($companyId);

        $menu = $this->menuRepository->find($companyId,$menuId);

        if($company && $menu )
        {

            $photo = $this->find($photoId);

            if($photo)
            {

                $menuPhoto = $this->findMenuPhoto($company->id, $menuId, $photo->id);

                if($menuPhoto)
                {

                    try{

                        Storage::delete($photo->url);
                        $photo->url = ($request->has('url') ? $request->url->store('public/photos/photoMenu') : null );
                        $photo->save();

                        return $photo;

                    }catch(\Exception $e){

                        return null;

                        //return var_dump($e->getMessage());

                    }
                }

            }
        }

        return null;

    }

    public function findCompanyPhotos(int $companyId){

        $company = $this->companyRepository->find($companyId);

        if($company)
        {
            $photos = $this->photoRepository->findCompanyPhotos($company->id);

            if($photos->count())
            {
                return $photos;
            }
        }

        return null;
    }

    public function setCompanyPhoto(int $companyId, int $photoId){

        $company = $this->companyRepository->find($companyId);

        $photo = $this->photoRepository->find($photoId);

        if($company && $photo)
        {

            $rel_company_photo = new Rel_company_photo();

            $rel_company_photo->company_id = $company->id;

            $rel_company_photo->photo_id = $photo->id;

            $rel_company_photo->save();

            return $rel_company_photo;

        }

        return null;

    }

    public function findCompanyPhoto(int $companyId, int $photoId){

        $company = $this->companyRepository->find($companyId);

        $photo = $this->photoRepository->find($photoId);

        if($company && $photo)
        {
            return $this->photoRepository->findCompanyPhoto($company->id,$photo->id);
        }

        return null;
    }

    public function deleteCompanyPhoto(int $companyId, int $photoId){

        $company = $this->companyRepository->find($companyId);

        $photo = $this->find($photoId);

        if($company && $photo)
        {
            $rel_company_photo = $this->findCompanyPhoto($company->id, $photo->id);

            if($rel_company_photo)
            {

                return $rel_company_photo->delete();
            }
        }

        return null;
    }

}