<?php
/**
 * Created by PhpStorm.
 * User: tobiasbachmann
 * Date: 20/3/18
 * Time: 18:08
 */

namespace App\Services;

use App\Repositories\FeatureRepository;
use Illuminate\Http\Request;
use App\Repositories\CategoryRepository;
use App\Repositories\DishRepository;
use App\Repositories\CompanyRepository;
use App\Dish;
use App\Rel_dish_feature;

class DishService
{

    private $dishRepository;
    private $companyRepository;
    private $categoryRepository;
    private $featureRepository;

    /**
     * DishService constructor.
     * @param CompanyRepository $companyRepository
     * @param DishRepository $dishRepository
     * @param CategoryRepository $categoryRepository
     * @param FeatureRepository $featureRepository
     */

    public function __construct(CompanyRepository $companyRepository, DishRepository $dishRepository, CategoryRepository $categoryRepository, FeatureRepository $featureRepository)
    {

        $this->dishRepository = $dishRepository;
        $this->companyRepository = $companyRepository;
        $this->categoryRepository = $categoryRepository;
        $this->featureRepository = $featureRepository;
    }

    public function findByCompany(int $companyId, int $dishId) {

        $dish = $this->dishRepository->findByCompany($companyId,$dishId);

        return $dish;

    }

    public function find(int $dishId){

        $dish = $this->dishRepository->find($dishId);

        return $dish;

    }


    public function findAll(){

        $dishes = $this->dishRepository->findAll();

        if($dishes->count())
        {
            return $dishes;
        }
        else
        {
            return null;
        }
    }

    public function giveMeDishesByZip(int $zip){

        $dishes = $this->dishRepository->giveMeDishesByZip($zip);

        if(count($dishes))
        {
            return $dishes;
        }
        else
        {
            return null;
        }
    }

    public function giveMeFeaturesByZip(int $zip){

           $features = $this->dishRepository->giveMeFeaturesByZip($zip);

           if(count($features))
           {
               return $features;
           }
           else
           {
                return null;
           }
    }

    public function hasFeatureDish(int $dishId, array $features){

        $featuresAll = $this->findFeatures($dishId);

        if($featuresAll->count())
        {
            $all = array();

            $all[] = $featuresAll->get(0)->feature_id;

            $result = array_intersect($features,$all);


            return (count($result) >= count($all) ? true : false);
        }

        return false;

    }


    public function findAllByCompany(int $companyId) {

        $dishes = $this->dishRepository->findAllByCompany($companyId);

        if($dishes->count())
        {
            return $dishes;
        }
        else
        {
            return null;
        }

    }

    public function findFeatures(int $dishId) {

        $dish = $this->dishRepository->find($dishId);

        if($dish)
        {
            return $this->dishRepository->findFeatures($dishId);
        }

        return null;
    }

    public function findFeaturesByCompany(int $companyId, int $dishId) {

        $dish = $this->dishRepository->findFeature($companyId,$dishId);


        if($dish !== null)
        {
            return $this->dishRepository->findFeatures($dishId);
        }

        return null;
    }

    public function create(Request $request) {



            $company = $this->companyRepository->find($request->company_id);

            if($company)
            {

                $category = ($request->has('cateogry') && $request->categoryId !== null) ? $this->categoryRepository->find($request->categoryId) : null;

                $dish = new Dish();
                $dish->name = ($request->has('name') ? $request->name : null);
                $dish->price = ($request->has('price') ? $request->price: null);
                $dish->photo_id = ($request->has('photoId') ? $request->photoId: null);
                $dish->category_id = ($category !== null) ? $category->id : null ;
                $dish->company_id = $company->id;
                $dish->save();

                return $dish;
            }



        return null;
    }

    public function createByCompany(int $companyId, Request $request) {

        $company = $this->companyRepository->find($companyId);

            if($company)
            {
                $category = $this->categoryRepository->find($request->categoryId);

                if($category)
                {
                    $dish = new Dish();
                    $dish->name = ($request->has('name') ? $request->name : null);
                    $dish->price = ($request->has('price') ? $request->price: null);
                    $dish->photo_id = ($request->has('photoId') ? $request->photoId: null);
                    $dish->category_id = $category->id;
                    $dish->company_id = $company->id;
                    $dish->save();

                    return $dish;
                }

        }

        return null;
    }

    function setFeature(int $dishId, int $featureId ){

        $dish = $this->find($dishId);
        if($dish)
        {
            $feature = $this->featureRepository->find($featureId);

            if($feature)
            {
                $rel_dish_feature = new Rel_dish_feature();

                $rel_dish_feature->dish_id = $dish->id;

                $rel_dish_feature->feature_id = $feature->id;

                $rel_dish_feature->save();

                return $rel_dish_feature;
            }
        }

        return null;
    }

    function setFeatureByCompany(int $companyId, int $dishId, int $featureId ){

        $dish = $this->find($dishId);
        if($dish)
        {
            $feature = $this->featureRepository->find($featureId);

            if($feature)
            {
                $rel_dish_feature = new Rel_dish_feature();

                $rel_dish_feature->dish_id = $dish->id;

                $rel_dish_feature->feature_id = $feature->id;

                $rel_dish_feature->save();

                return $rel_dish_feature;
            }
        }

        return null;
    }

    public function update(int $dishId, Request $request){

            if($request->company_id !== null)
            {
                $company = $this->companyRepository->find($request->company_id);

                if($company)
                {
                    $dish = $this->dishRepository->find($dishId);
                    $category = ($request->has('category_id') && $request->category_id !== null) ? $this->categoryRepository->find($request->category_id) : null ;

                    if($dish)
                    {
                        $dish->name = ($request->has('name') ? $request->name : null);
                        $dish->price = ($request->has('price') ? $request->price: null);
                        //$dish->photo_id = ($request->has('photo_id') ? $request->photo_id: null);
                        $dish->category_id = ($category !== null ) ? $category->id : null;
                        $dish->company_id = $company->id;
                        $dish->save();
                        return $dish;
                    }

                }
            }


            return null;


    }

    public function updateByCompany(int $companyId, int $dishId, Request $request) {

        $company = $this->companyRepository->find($companyId);

        if($company)
        {
            $category = $this->categoryRepository->find($request->categoryId);

            if($category)
            {
                $dish = $this->dishRepository->find($companyId,$dishId);

                if($dish)
                {
                    $dish->name = ($request->has('name') ? $request->name : null);
                    $dish->price = ($request->has('price') ? $request->price: null);
                    $dish->photo_id = ($request->has('photoId') ? $request->photo_id: null);
                    $dish->category_id = $category->id;
                    $dish->company_id = $company->id;
                    $dish->save();
                    return $dish;
                }

            }

        }

        return null;
    }

    public function delete(int $dishId) {
        $dish = $this->find($dishId);

        if($dish)
        {

            $featuresDish = $dish->features()->get();
            $offers = $dish->offers()->get();
            $menuDishes = $dish->menus()->get();
            $scoreDishes = $dish->scores()->get();

            foreach ($featuresDish as $featureDish)
            {
                $featureDish->delete();
            }
            foreach ($offers as $offer)
            {
                $offer->delete();
            }
            foreach ($menuDishes as $menuDish)
            {
                $menuDish->delete();
            }
            foreach ($scoreDishes as $scoreDish)
            {
                $scoreDish->delete();
            }


            return $dish->delete();
        }
        else
        {
            return null;
        }


    }

    public function deleteByCompany(int $companyId, int $dishId) {
        $dish = $this->findByCompany($companyId, $dishId);

        if($dish)
        {

                $featuresDish = $dish->relDishesFeatures()->get();
                $offers = $dish->offers()->get();
                $menuDishes = $dish->menusDishes()->get();
                $scoreDishes = $dish->scores()->get();

                foreach ($featuresDish as $featureDish)
                {
                    $featureDish->delete();
                }
                foreach ($offers as $offer)
                {
                    $offer->delete();
                }
                foreach ($menuDishes as $menuDish)
                {
                    $menuDish->delete();
                }
                foreach ($scoreDishes as $scoreDish)
                {
                    $scoreDish->delete();
                }


            return $dish->delete();
        }
        else
        {
            return null;
        }


    }

    public function deleteFeature(int $companyId, int $dishId, int $featureId) {

        $dish = $this->findByCompany($companyId,$dishId);

        if($dish)
        {
            $feature = $this->dishRepository->findFeature($dishId,$featureId);

            if($feature)
            {
                return $feature->delete();
            }


        }

        return null;
    }
}