<?php
/**
 * Created by PhpStorm.
 * User: tobiasbachmann
 * Date: 20/3/18
 * Time: 12:55
 */

namespace App\Services;


use App\Opinion;
use App\Repositories\UsersRepository;
use Faker\Provider\hy_AM\Company;
use Illuminate\Http\Request;

use App\Repositories\LocationRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\SubsidiaryRepository;

use App\Subsidiary;
use Illuminate\Support\Facades\Auth;

class SubsidiaryService
{

    private $subsidiaryRepository;
    private $locationRepository;
    private $companyRepository;
    private $usersRepository;

    /**
     * LocationService constructor.
     * @param SubsidiaryRepository $subsidiaryRepository
     * @param LocationRepository $locationRepository
     * @param CompanyRepository $companyRepository
     * @param UsersRepository $usersRepository
     */

    public function __construct(CompanyRepository $companyRepository,LocationRepository $locationRepository, SubsidiaryRepository $subsidiaryRepository, UsersRepository $usersRepository)
    {
        $this->locationRepository = $locationRepository;
        $this->subsidiaryRepository = $subsidiaryRepository;
        $this->companyRepository = $companyRepository;
        $this->usersRepository = $usersRepository;
    }



    public function find($subsidiaryId)
    {
        $subsidiary = $this->subsidiaryRepository->find($subsidiaryId);

        if($subsidiary)
        {
            return $subsidiary;
        }
        else
        {
            return null;
        }
    }

    public function findAll(){


        $subsidiaries = $this->subsidiaryRepository->findAll();

        if($subsidiaries->count())
        {
            return $subsidiaries;
        }
        else
        {
            return null;
        }

    }

    public function findPrimary(int $companyId){

        $subsidiary = $this->subsidiaryRepository->findPrimary($companyId);

        // dd($subsidiary);

        if($subsidiary)
        {
            return $subsidiary;
        }

        return null;
    }

    public function findLast(int $limit ){

        $subsidiaries = $this->subsidiaryRepository->findLast($limit);

        if(count($subsidiaries))
        {
            return $subsidiaries;
        }

        return null;
    }

    public static function findLastStatic(int $limit){

        return SubsidiaryRepository::findLastStatic($limit);

    }

    public function findAllByCompany(int $companyId) {

        $company = $this->companyRepository->find($companyId);

        if($company)
        {
            $companies = $this->subsidiaryRepository->findAllByCompany($companyId);

            if($companies->count())
            {
                return $companies;
            }
        }

        return null;

    }

    public function findAllByLocation(int $locationId){

        $subsidiaries = $this->subsidiaryRepository->findAllByLocation($locationId);

        if(count($subsidiaries))
        {
            return $subsidiaries;
        }

        return null;
    }

    public function findByCompany(int $companyId, int $subsidiaryId) {

        $company = $this->companyRepository->find($companyId);

        if($company)
        {
            $subsidiary = $this->subsidiaryRepository->findByCompany($companyId,$subsidiaryId);

            if($subsidiary)
            {
                return $subsidiary;
            }
        }

        return null;

    }


    public function findOpinions(int $company_id, int $subsidiary_id){

        $subsidiary = $this->find($subsidiary_id);

        if($subsidiary)
        {
            $opinions = $this->subsidiaryRepository->findOpinions($subsidiary->id);

            if($opinions->count())
            {
                return $opinions;
            }
        }

        return null;


    }

    /*public function findOpinionBySubsidiary(int $companyId, int $subsidiaryId, int $opinionId){

        $subsidiary = $this->subsidiaryRepository->findOpinion($companyId,$subsidiaryId);

        if($subsidiary)
        {
            return $this->subsidiaryRepository->findOpinion($opinionId);
        }

        return null;

    }*/

    public function findOpinion(int $opinionId){

        $opinion = $this->subsidiaryRepository->findOpinion($opinionId);

        if($opinion)
        {
            return $opinion;
        }

        return null;

    }

    public function create(Request $request) {

        if($request->has('company_id') && $this->companyRepository->find($request->company_id) !== null
            && $request->has('location_id') && $this->locationRepository->find($request->location_id) !== null)
        {

            $subsidiary = new Subsidiary();
            $subsidiary->address = ($request->has('address') ? $request->address : null);
            $subsidiary->coordinates = ($request->has('coordinates') ? $request->coordinates: null);
            $subsidiary->phone = ($request->has('phone') ? $request->phone: null);
            $subsidiary->email = ($request->has('email') ? $request->email: null);
            $subsidiary->company_id = $request->company_id;
            $subsidiary->location_id = $request->location_id;

            $subsidiary->save();

            return $subsidiary;
        }

        return null;
    }

    public function createByCompany(int $companyId, Request $request) {

        $company = $this->companyRepository->find($companyId);

        if($company)
        {

            $location = $this->locationRepository->find($request->location_id);


            if($location)
            {

                $subsidiary = new Subsidiary();
                $subsidiary->address = ($request->has('address') ? $request->address : null);
                $subsidiary->coordinates = ($request->has('coordinates') ? $request->coordinates: null);
                $subsidiary->phone = ($request->has('phone') ? $request->phone: null);
                $subsidiary->email = ($request->has('email') ? $request->email: null);
                $subsidiary->company_id = $company->id;
                $subsidiary->location_id = $location->id;
                $subsidiary->primary = (($request->has('primary') && $request->primary == true) || $request->has('primary_first') ? 1 : 0);
                $subsidiary->save();

                return $subsidiary;
            }

        }

        return null;
    }



    function setOpinion(int $companyId, int $subsidiaryId, Request $request ){

        $subsidiary = $this->find($subsidiaryId);

        if($subsidiary)
        {

            $user = Auth::user();

            if($user)
            {
                $opinion = new Opinion();

                $opinion->subsidiary_id = $subsidiary->id;

                $opinion->user_id = Auth::id();

                $opinion->comment = $request->has('comment') ? $request->comment : null ;

                $opinion->save();

                return $opinion;
            }
        }

        return null;
    }

    public function updateByCompany(int $companyId, int $subsidiaryId, Request $request) {

        $subsidiary = $this->find($subsidiaryId);
        if($subsidiary)
        {

            if($request->has('primary') && $request->primary == true )
            {

                $primary = $this->subsidiaryRepository->findPrimary($companyId);

                if($primary !== null)
                {
                    $primary->primary = 0;
                    $primary->save();
                }
            }

            $subsidiary->address = ($request->has('address') ? $request->address : $subsidiary->address);
            $subsidiary->coordinates = ($request->has('coordinates') ? $request->coordinates : $subsidiary->coordinates);
            $subsidiary->phone = ($request->has('phone') ? $request->phone : $subsidiary->phone);
            $subsidiary->email = ($request->has('email') ? $request->email : $subsidiary->email);
            $subsidiary->location_id = ($request->has('location_id') ? $request->location_id : $subsidiary->location_id);
            $subsidiary->primary = ($request->has('primary') && $request->primary == true  ? 1 : 0);
            $subsidiary->save();

            return $subsidiary;
        }

        return null;

    }

    public function update($subsidiaryId, Request $request){

        $subsidiary = $this->subsidiaryRepository->find($subsidiaryId);

        if($subsidiary)
        {

            $subsidiary->address = ($request->has('address') ? $request->address : $subsidiary->address);
            $subsidiary->coordinates = ($request->has('coordinates') ? $request->coordinates : $subsidiary->coordinates);
            $subsidiary->phone = ($request->has('phone') ? $request->phone : $subsidiary->phone);
            $subsidiary->email = ($request->has('email') ? $request->email : $subsidiary->email);
            $subsidiary->location_id = ($request->has('location_id') ? $request->location_id : $subsidiary->location_id);
            $subsidiary->save();

            return $subsidiary;
        }

        return null;

    }


    public  function updateOpinion(int $companyId, int $subsidiaryId, int $opinionId, Request $request){

        $company = $this->companyRepository->find($companyId);

        if($company)
        {
            $subsidiary = $this->find($companyId,$subsidiaryId);

            if($subsidiary)
            {
                $opinion = $this->findOpinion($companyId,$subsidiaryId,$opinionId);

                if($opinion && $opinion->user_id == Auth::id())
                {
                    $opinion->comment = ($request->has('comment') ? $request->comment : null);
                    $opinion->save();

                    return $opinion;
                }

            }

        }

        return null;
    }

    public function delete(int $subsidiaryId)
    {
        $subsidiary = $this->subsidiaryRepository->find($subsidiaryId);

        if($subsidiary)
        {
            return $subsidiary->delete();
        }
        else
        {
            return null;
        }
    }
    public function deleteByCompany(int $companyId, int $subsidiaryId) {
        $subsidiary = $this->findByCompany($companyId, $subsidiaryId);

        if($subsidiary)
        {
            $schedules = $subsidiary->schedules()->get();

            foreach ($schedules as $schedule)
            {
                $schedule->delete();
            }
            $opinions = $subsidiary->opinions()->get();

            foreach ($opinions as $opinion)
            {
                $opinion->delete();
            }
            return $subsidiary->delete();
        }


        return null;
    }

    public function deleteOpinionBySubsidiary(int $companyId, int $subsidiaryId,int $opinionId) {

        $subsidiary = $this->find($subsidiaryId);

        if($subsidiary)
        {
            $opinion = $this->findOpinion($opinionId);

            if($opinion)
            {
                return $opinion->delete();
            }
        }


        return null;
    }

    public function deleteOpinion(int $opinionId) {


            $opinion = $this->findOpinion($opinionId);

            if($opinion)
            {
                return $opinion->delete();
            }

        return null;
    }

    public function denyOpinion(int $opinionId){

        $opinion = $this->findOpinion($opinionId);

        if($opinion)
        {
            $opinion->status = false;
            $opinion->save();
            return $opinion;

        }
        else
        {
            return null;
        }
    }

    public function acceptOpinion(int $opinionId){

        $opinion = $this->findOpinion($opinionId);

        if($opinion)
        {
            $opinion->status = true;
            $opinion->save();

            return $opinion;

        }
        else
        {
            return null;
        }
    }

    function distance($lat1, $lon1, $lat2, $lon2, $unit = 'K') {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

}