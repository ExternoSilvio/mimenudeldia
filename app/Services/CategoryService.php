<?php
/**
 * Created by PhpStorm.
 * User: tobiasbachmann
 * Date: 20/3/18
 * Time: 17:28
 */

namespace App\Services;

use Illuminate\Http\Request;
use App\Category;
use App\Repositories\CategoryRepository;

class CategoryService
{

    private $categoryRepository;

    /**
     * CategoryService constructor.
     * @param $categoryRepository
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function find(int $categoryId) {

        $category = $this->categoryRepository->find($categoryId);

        if($category)
        {
            return $category;
        }
        else
        {
            return null;
        }
    }

    public function findAll() {

        $categories = $this->categoryRepository->findAll();

        if($categories->count())
        {
            return $categories;
        }

        return null;
    }

    public function create(Request $request) {

        if($request->has('name') && $request->name !== null )
        {

            $category = new Category();
            $category->name = $request->name;
            $category->category_id = ($request->has('category_id') && $this->categoryRepository->find($request->category_id) ? $request->category_id : null);

            return $category->save();

        }

        return null;

    }


    public function update(int $categoryId, Request $request) {

            $category = $this->find($categoryId);

            if($category)
            {
                $category->name = ($request->has('name') ? $request->name : $category->name);
                $category->category_id = ($request->has('category_id') && $this->categoryRepository->find($request->category_id) ? $request->category_id : null);

                $category->save();

                return $category;
            }
            else
            {
                return null;
            }

    }

    public function delete(int $categoryId) {

        $category = $this->find($categoryId);

        if($category)
        {

            $categories = $category->categories()->get();
            $dishes = $category->dishes()->get();

            foreach ($categories as $categoryFk)
            {
                $categoryFk->category_id = null;
                $categoryFk->save();
            }
            foreach ($dishes as $dish)
            {

                $dish->category_id = null;
                $dish->save();
            }
            return $category->delete();

        }
        else
        {
            return null;
        }

    }

}