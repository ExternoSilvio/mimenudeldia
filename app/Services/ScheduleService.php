<?php
/**
 * Created by PhpStorm.
 * User: tobiasbachmann
 * Date: 20/3/18
 * Time: 15:59
 */

namespace App\Services;


use Illuminate\Http\Request;
use App\Repositories\ScheduleRepository;
use App\Repositories\SubsidiaryRepository;
use App\Repositories\CompanyRepository;



use App\Schedule;


class ScheduleService
{

    private $scheduleRepository;
    private $subsidiaryRepository;
    private $companyRepository;


    /**
     * ScheduleService constructor.
     * @param ScheduleRepository $scheduleRepository
     * @param SubsidiaryRepository $subsidiaryRepository
     * @param CompanyRepository $companyRepository
     *
     */
    public function __construct(ScheduleRepository $scheduleRepository, SubsidiaryRepository $subsidiaryRepository, CompanyRepository $companyRepository)
    {
        $this->scheduleRepository = $scheduleRepository;
        $this->subsidiaryRepository = $subsidiaryRepository;
        $this->companyRepository = $companyRepository;

    }

    public function findByCompany(int $companyId, int $subsidiaryId, int $scheduleId = null) {

        $subsidiary = $this->subsidiaryRepository->findByCompany($companyId,$subsidiaryId);

        if($subsidiary)
        {
            $schedule = $this->scheduleRepository->find($subsidiaryId, $scheduleId);

            if($schedule)
            {
                return $schedule;
            }
        }

        return null;
    }

    public function findAll(int $companyId, int $subsidiaryId) {

        $subsidiary = $this->subsidiaryRepository->findByCompany($companyId,$subsidiaryId);

        if($subsidiary)
        {
            $schedule = $this->scheduleRepository->findAll($subsidiaryId);

            if($schedule->count())
            {
               return $schedule;
            }
        }

        return null;
    }

    public function find(int $subsidiaryId, int $scheduleId = null) {

        $subsidiary = $this->subsidiaryRepository->find($subsidiaryId);

        if($subsidiary)
        {
            $schedule = $this->scheduleRepository->find($subsidiaryId, $scheduleId);

            if($schedule)
            {
                return $schedule;
            }
        }

        return null;
    }

    public function create(int $subsidiaryId,Request $request) {

        $subsidiary = $this->subsidiaryRepository->find($subsidiaryId);

        if($subsidiary)
        {

            $schedule = new Schedule();
            $schedule->day = ($request->has('day') ? $request->day : null);
            $schedule->start_at = ($request->has('start_at') ? $request->start_at : null);
            $schedule->finish_at = ($request->has('finish_at') ? $request->finish_at : null);
            $schedule->subsidiary_id = $subsidiary->id;
            $schedule->save();

            return $schedule;
        }

        return null;
    }

    public function createByCompany(int $companyId, int $subsidiaryId,Request $request) {

        $subsidiary = $this->subsidiaryRepository->findByCompany($companyId,$subsidiaryId);

        if($subsidiary)
        {

            $schedule = new Schedule();
            $schedule->day = ($request->has('day') ? $request->day : null);
            $schedule->start_at = ($request->has('start_at') ? $request->start_at : null);
            $schedule->finish_at = ($request->has('finish_at') ? $request->finish_at : null);
            $schedule->subsidiary_id = $subsidiary->id;
            $schedule->save();

            return $schedule;
        }

        return null;
    }

    public function update(int $companyId, int $subsidiaryId, int $scheduleId, Request $request) {

        $schedule = $this->find($companyId,$subsidiaryId, $scheduleId);

        if($schedule)
        {
            $schedule->day = ($request->has('day') ? $request->day : $schedule->day);
            $schedule->start_at = ($request->has('start_at') ? $request->start_at : $schedule->start_at);
            $schedule->finish_at = ($request->has('finish_at') ? $request->finish_at : $schedule->finish_at);
            $schedule->save();

            return $schedule;
        }

        return null;

    }

    public function deleteByCompany(int $companyId, int $subsidiaryId,int $scheduleId) {

        $subsidiary = $this->subsidiaryRepository->findByCompany($companyId,$subsidiaryId);

        if($subsidiary)
        {
            $schedule = $this->find($companyId,$subsidiaryId, $scheduleId);

            if($schedule)
            {
                return $schedule->delete();
            }
        }

        return null;
    }

    public function delete(int $subsidiaryId,int $scheduleId) {

        $subsidiary = $this->subsidiaryRepository->find($subsidiaryId);

        if($subsidiary)
        {
            $schedule = $this->find($subsidiaryId, $scheduleId);

            if($schedule)
            {
                return $schedule->delete();
            }
        }

        return null;
    }
}