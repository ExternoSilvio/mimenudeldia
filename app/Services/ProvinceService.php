<?php
/**
 * Created by PhpStorm.
 * User: paveldelpozo
 * Date: 16/3/18
 * Time: 16:49
 */

namespace App\Services;

use App\Repositories\ProvinceRepository;
use App\Province;
use Illuminate\Http\Request;

class ProvinceService
{

    private $provinceRepository;

    /**
     * ProvinceService constructor.
     * @param $provinceRepository
     */
    public function __construct(ProvinceRepository $provinceRepository)
    {
        $this->provinceRepository = $provinceRepository;
    }

    public function find(int $provinceId) {

        $province = $this->provinceRepository->find($provinceId);

        if($province)
        {
            return $province;
        }
        else
        {
            return null;
        }

    }

    public function findAll() {
        $provinces = $this->provinceRepository->findAll();

        if($provinces->count())
        {
            return $provinces;
        }

        return $provinces;
    }

    public function create(Request $request) {

        if($request->has('name') && $request->name !== null)

            $province = new Province();
            $province->name = $request->name;
            $province->save();

            return $province;



            return null;




    }

    public function update(int $provinceId, Request $request) {

        $province = $this->find($provinceId);


        if($province)
        {

            $province->name = ($request->has('name') ? $request->name : $province->name);
            $province->save();

            return $province;
        }
        else
        {
            return null;
        }
    }

    public function delete(int $provinceId) {

        $province = $this->find($provinceId);

        if($province)
        {
            $locations = $province->locations()->get();

            foreach ($locations as $location)
            {
                $subsidiaries = $location->subsidiaries()->get();

                foreach ($subsidiaries as $subsidiary)
                {
                    $opinions = $subsidiary->opinions()->get();
                    $schedules = $subsidiary->schedules()->get();

                    foreach ($opinions as $opinion)
                    {
                        $opinion->delete();
                    }
                    foreach ($schedules as $schedule)
                    {
                        $schedule->delete();
                    }
                }

                $location->delete();
            }
            $province->delete();

            return 'deleted';
        }
        else
        {
            return null;
        }
    }

}