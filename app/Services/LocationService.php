<?php
/**
 * Created by PhpStorm.
 * User: tobiasbachmann
 * Date: 20/3/18
 * Time: 10:50
 */

namespace App\Services;

use Illuminate\Http\Request;
use App\Repositories\LocationRepository;
use App\Repositories\ProvinceRepository;
use App\Location;

class LocationService
{

    private $locationRepository;
    private $provinceRepository;

    /**
     * LocationService constructor.
     * @param ProvinceRepository $provinceRepository
     * @param LocationRepository $locationRepository
     */
    public function __construct(ProvinceRepository $provinceRepository,LocationRepository $locationRepository)
    {
        $this->locationRepository = $locationRepository;
        $this->provinceRepository = $provinceRepository;
    }

    public function find(int $locationId) {

        $location = $this->locationRepository->find($locationId);

        if($location)
        {
            return $location;
        }
        else
        {
            return null;
        }
    }


    public function findByProvince(int $locationId, int $provinceId = null) {

        $location = $this->locationRepository->findByProvince($locationId, $provinceId);

        return $location;
    }

    public function findAllByProvince($provinceId) {

        $locations = $this->locationRepository->findAllByProvince($provinceId);

        if(count($locations))
        {
            return $locations;
        }
        else
        {
            return null;
        }
    }

    public function findByZip(int $zip){

        $location = $this->locationRepository->findByZip($zip);

        if($location)
        {
            return $location;
        }
        else
        {
            return null;
        }
    }

    public function findAll(){

        $locations = $this->locationRepository->findAll();

        if($locations->count())
        {
            return $locations;
        }
        else
        {
            return null;
        }
    }

    public function create(int $provinceId,Request $request) {

        $province = $this->provinceRepository->find($provinceId);

        if($province)
        {
            $location = new Location();
            $location->name = ($request->has('name') ? $request->name : null);
            $location->zip = ($request->has('zip') ? $request->zip : null);
            $location->province_id = $province->id;
            $location->save();

            return $location;
        }

        return null;
    }

    public function update(int $provinceId,int $locationId, Request $request) {

        $location = $this->locationRepository->find($locationId);

        if($location)
        {
            $location->name = ($request->has('name') ? $request->name : $location->name);
            $location->zip = ($request->has('zip') ? $request->zip : $location->zip);
            $location->province_id = ($request->has('province_id') ? $request->province_id : $location->province_id);
            $location->save();

            return $location;
        }

        return null;

    }

    public function delete(int $locationId) {

        $location = $this->find($locationId);



        if($location)
        {
            $subsidiaries = $location->subsidiaries()->get();

            foreach ($subsidiaries as $subsidiary)
            {

                $opinions = $subsidiary->opinions()->get();
                $schedules = $subsidiary->schedules()->get();

                foreach ($opinions as $opinion)
                {
                    $opinion->delete();
                }
                foreach ($schedules as $schedule)
                {
                    $schedule->delete();
                }

                $subsidiary->delete();
            }

            return $location->delete();
        }
        else
        {
            return null;
        }

    }



}