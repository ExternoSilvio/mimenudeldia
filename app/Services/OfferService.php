<?php
/**
 * Created by PhpStorm.
 * User: tobiasbachmann
 * Date: 9/4/18
 * Time: 13:43
 */

namespace App\Services;

use Carbon\Carbon;
use App\Repositories\OfferRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\DishRepository;
use App\Repositories\MenuRepository;
use App\Offer;
use Illuminate\Http\Request;
use Route;
use DataTimeZone;

class OfferService
{

    private $offerRepository;
    private $companyRepository;
    private $dishRepository;
    private $menuRepository;

    /**
     * OfferService constructor.
     * @param OfferRepository $offerRepository
     * @param CompanyRepository $companyRepository
     * @param DishRepository $dishRepository
     * @param MenuRepository $menuRepository
     */
    public function __construct(OfferRepository $offerRepository, CompanyRepository $companyRepository, DishRepository $dishRepository, MenuRepository $menuRepository)
    {

        $this->offerRepository = $offerRepository;
        $this->companyRepository = $companyRepository;
        $this->dishRepository = $dishRepository;
        $this->menuRepository = $menuRepository;

    }

    public function findAll(){

        $offers = $this->offerRepository->findAll();

        $offers_arr = array();

        foreach($offers as $offer)
        {
            $start = new Carbon($offer->start_at);
            $finish = new Carbon($offer->finish_at);
            $now = new Carbon('Europe/Madrid');

            if ($now->between($start, $finish)) {
                $offers_arr[] = $offer;
            }
        }

        if($offers_arr)
        {
            return $offers_arr;
        }
        else
        {
            return null;
        }

    }

    public function find($offerId){

        $offer = $this->offerRepository->find($offerId);

        if($offer !== null)
        {
            return $offer;
        }
        else
        {
            return null;
        }

    }

    public function findDish(int $companyId, int $dishId, int $offerId){

        $company = $this->companyRepository->find($companyId);

        $dish = $this->dishRepository->find($company->id,$dishId);

        if($company && $dish)
        {

            $offer = $this->offerRepository->findDish($dish->id, $offerId);

            $start = new Carbon($offer->start_at);
            $finish = new Carbon($offer->finish_at);
            $now = Carbon::now('Europe/Madrid');

            if($now->between($start,$finish))
            {
                return $offer;
            }

        }

        return null;

    }

    public function setDish(int $dishId, Request $request){

        $dish = $this->dishRepository->find($dishId);

        if($dish)
        {

            $offer = new Offer();
            $offer->dish_id = $dish->id;
            $offer->start_at = ($request->has('start_at')? $request->input('start_at') : null );
            $offer->finish_at = ($request->has('finish_at')? $request->input('finish_at') : null );
            $offer->price = ($request->has('type_value') && $request->type == 'price'? $request->input('type_value') : null );
            $offer->discount = ($request->has('type_value') && $request->type == 'discount' ? $request->input('type_value') : null );

            $offer->save();

            return $offer;

        }

        return null;
    }

    public function setDishByCompany(int $companyId, int $dishId, Request $request){

        $company = $this->companyRepository->find($companyId);

        $dish = $this->dishRepository->find($company->id,$dishId);

        if($company && $dish)
        {

            $offer = new Offer();
            $offer->dish_id = $dish->id;
            $offer->start_at = ($request->has('start_at')? $request->input('start_at') : null );
            $offer->finish_at = ($request->has('finish_at')? $request->input('finish_at') : null );
            $offer->price = ($request->has('price')? $request->input('price') : null );
            $offer->discount = ($request->has('discount')? $request->input('discount') : null );

            $offer->save();

            return $offer;

        }

        return null;
    }

    public function findDishes(int $companyId, int $dishId){

        $company = $this->companyRepository->find($companyId);

        $dish = $this->dishRepository->find($company->id,$dishId);

        if($company && $dish)
        {

            $offers= $this->offerRepository->findDishes($dish->id);

            $offers_arr = array();

            foreach($offers as $offer)
            {
                $start = new Carbon($offer->start_at);
                $finish = new Carbon($offer->finish_at);
                $now = Carbon::now('Europe/Madrid');

                if ($now->between($start, $finish)) {
                    $offers_arr[] = $offer;
                }
            }

            if($offers_arr)
            {
                return $offers_arr;
            }

        }

        return null;

    }

    public function putDish(int $companyId, int $dishId, int $offerId, Request $request){

        $company = $this->companyRepository->find($companyId);

        $dish = $this->dishRepository->find($company->id,$dishId);

        if($company && $dish)
        {
            $offer = $this->offerRepository->findDish($dish->id, $offerId);

            if($offer)
            {

                $offer->start_at = ($request->has('start_at')? $request->input('start_at') : null );
                $offer->finish_at = ($request->has('finish_at')? $request->input('finish_at') : null );
                $offer->price = ($request->has('price')? $request->input('price') : null );
                $offer->discount = ($request->has('discount')? $request->input('discount') : null );
                $offer->save();

                return $offer;

            }
        }

        return null;

    }

    public function deleteDish(int $companyId, int $dishId, int $offerId){

        $company = $this->companyRepository->find($companyId);

        $dish = $this->dishRepository->find($company->id,$dishId);

        if($company && $dish)
        {
            $offer = $this->offerRepository->findDish($dish->id, $offerId);

            if($offer)
            {
                return $offer->delete();

            }
        }

        return null;

    }





    public function findMenu(int $companyId, int $menuId, int $offerId){

        $company = $this->companyRepository->find($companyId);

        $menu = $this->menuRepository->find($company->id,$menuId);

        if($company && $menu)
        {
            $offer = $this->offerRepository->findMenu($menu->id, $offerId);

            if($offer)
            {
                $start = new Carbon($offer->start_at);
                $finish = new Carbon($offer->finish_at);
                $now = new Carbon('Europe/Madrid');

                if($now->between($start,$finish))
                {
                    return $offer;
                }
            }

        }

        return null;

    }

    public function setMenuByCompany(int $companyId, int $menuId, Request $request){

        $company = $this->companyRepository->find($companyId);

        $menu = $this->menuRepository->find($menuId);

        if($company && $menu)
        {

            $offer = new Offer();
            $offer->menu_id = $menu->id;
            $offer->start_at = ($request->has('start_at')? $request->input('start_at') : null );
            $offer->finish_at = ($request->has('finish_at')? $request->input('finish_at') : null );
            $offer->price = ($request->has('price')? $request->input('price') : null );
            $offer->discount = ($request->has('discount')? $request->input('discount') : null );

            $offer->save();

            return $offer;

        }

        return null;
    }

    public function setMenu(int $menuId, Request $request){

        $menu = $this->menuRepository->find($menuId);

        if($menu)
        {

            $offer = new Offer();
            $offer->menu_id = $menu->id;
            $offer->start_at = ($request->has('start_at')? $request->input('start_at') : null );
            $offer->finish_at = ($request->has('finish_at')? $request->input('finish_at') : null );
            $offer->price = ($request->has('type_value') && $request->type == 'price'? $request->input('type_value') : null );
            $offer->discount = ($request->has('type_value') && $request->type == 'discount' ? $request->input('type_value') : null );

            $offer->save();

            return $offer;

        }

        return null;
    }

    public function findMenus(int $companyId, int $menuId){

        $company = $this->companyRepository->find($companyId);

        $menu = $this->menuRepository->find($menuId);

        if($company && $menu)
        {

            $offers= $this->offerRepository->findMenus($menu->id);

            $offers_arr = array();

            foreach($offers as $offer)
            {
                $start = new Carbon($offer->start_at);
                $finish = new Carbon($offer->finish_at);
                $now = Carbon::now('Europe/Madrid');

                if ($now->between($start, $finish)) {
                    $offers_arr[] = $offer;
                }
            }

            if($offers_arr)
            {
                return $offers_arr;
            }

        }

        return null;

    }

    public function putMenu(int $companyId, int $menuId, int $offerId, Request $request){

        $company = $this->companyRepository->find($companyId);

        $menu = $this->menuRepository->find($company->id,$menuId);

        if($company && $menu)
        {
            $offer = $this->offerRepository->findMenu($menu->id, $offerId);

            if($offer)
            {

                $offer->start_at = ($request->has('start_at')? $request->input('start_at') : null );
                $offer->finish_at = ($request->has('finish_at')? $request->input('finish_at') : null );
                $offer->price = ($request->has('price')? $request->input('price') : null );
                $offer->discount = ($request->has('discount')? $request->input('discount') : null );
                $offer->save();

                return $offer;

            }
        }

        return null;

    }

    public function deleteMenu(int $companyId, int $menuId, int $offerId){

        $company = $this->companyRepository->find($companyId);

        $menu = $this->menuRepository->find($company->id,$menuId);

        if($company && $menu)
        {
            $offer = $this->offerRepository->findMenu($menu->id, $offerId);

            if($offer)
            {
                $offer->delete();

                return null;
            }
        }

        return null;

    }

    public function delete($offerId){

        $offer = $this->offerRepository->find($offerId);



        if($offer !== null)
        {

            return $offer->delete();
        }

        else
        {
            return null;
        }
    }

}