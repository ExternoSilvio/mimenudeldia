<?php
/**
 * Created by PhpStorm.
 * User: tobiasbachmann
 * Date: 21/3/18
 * Time: 11:33
 */

namespace App\Services;

use App\Repositories\FeatureRepository;
use App\Repositories\DishRepository;
use Illuminate\Http\Request;
use App\Feature;

class FeatureService
{

    private $featureRepository;
    private $dishRepository;

    /**
     * FeatureService constructor.
     * @param $featureRepository
     * @param $dishRepository
     */
    public function __construct(FeatureRepository $featureRepository,DishRepository $dishRepository)
    {
        $this->featureRepository = $featureRepository;
        $this->dishRepository = $dishRepository;
    }

    public function find(int $featureId) {
        $feature = $this->featureRepository->find($featureId);

        return $feature;
    }

    public function findAll() {

        $features = $this->featureRepository->findAll();

        if($features->count())
        {
            return $features;
        }
        else
        {
            return null;
        }

    }



    public function create(Request $request) {

        if($request->has('name') && $request->name !== null)
        {
            $feature = new Feature();
            $feature->name = $request->name;
            $feature->feature_id = ($request->has('feature_id') && $this->featureRepository->find($request->feature_id) ? $request->feature_id : null);
            $feature->save();

            return $feature;
        }

        return null;


    }


    public function update(int $featureId, Request $request) {

        $feature = $this->find($featureId);

        if($feature)
        {
            $feature->name = ($request->has('name') ? $request->name : $feature->name);
            $feature->feature_id = ($request->has('feature_id') && $this->featureRepository->find($request->feature_id) ? $request->feature_id : $feature->feature_id);
            $feature->save();

            return $feature;
        }
        else
        {
            return null;
        }

    }

    public function delete(int $featureId) {

        $feature = $this->find($featureId);
        if($feature)
        {
            $relDishesFeatures = $feature->relDishesFeatures()->get();
            foreach($relDishesFeatures as $relFeature) {
                $relFeature->delete();
            }
            $relCompaniesFeatures = $feature->relCompaniesFeatures()->get();
            foreach($relCompaniesFeatures as $relFeature) {
                $relFeature->delete();
            }
            $featuresSelf = $feature->features()->get();

            foreach ($featuresSelf as $featureSelf)
            {

                $featureSelf->feature_id = null;
                $featureSelf->save();
            }
            return $feature->delete();


        }
        else
        {
            return null;
        }

    }
}