<?php
/**
 * Created by PhpStorm.
 * User: tobiasbachmann
 * Date: 21/3/18
 * Time: 10:25
 */

namespace App\Services;

use App\Rel_menu_dish;
use App\Rel_menu_feature;
use Illuminate\Http\Request;
use App\Repositories\CompanyRepository;
use App\Repositories\MenuRepository;
use App\Repositories\DishRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\FeatureRepository;
use App\Menu;


class MenuService
{

    private $menuRepository;
    private $companyRepository;
    private $dishRepository;
    private $categoryRepository;
    private $featureRepository;

    /**
     * MenuService constructor.
     * @param CompanyRepository $companyRepository
     * @param MenuRepository $menuRepository
     * @param DishRepository $dishRepository
     * @param CategoryRepository $categoryRepository
     * @param FeatureRepository $featureRepository
     */

    public function __construct(CompanyRepository $companyRepository, MenuRepository $menuRepository, DishRepository $dishRepository, CategoryRepository $categoryRepository, FeatureRepository $featureRepository)
    {

        $this->menuRepository = $menuRepository;
        $this->companyRepository = $companyRepository;
        $this->dishRepository = $dishRepository;
        $this->categoryRepository = $categoryRepository;
        $this->featureRepository = $featureRepository;

    }

    public function find(int $menuId) {

        return $this->menuRepository->find($menuId);
    }

    public function findByCompany(int $companyId, int $menuId) {

        return $this->menuRepository->findByCompany($companyId,$menuId);
    }

    public function findAll() {

        $menus = $this->menuRepository->findAll();

        if($menus->count())
        {

            return $menus;
        }
        else
        {

            return null;
        }
    }

    public function findAllByCompany(int $companyId) {

         $menus = $this->menuRepository->findAll($companyId);

         if($menus->count())
         {

             return $menus;
         }
         else
         {

             return null;
         }
    }

    public function giveMeMenusByZip($zip){

        $menus = $this->menuRepository->giveMeMenusByZip($zip);

        if(count($menus))
        {
            return $menus;
        }
        else
        {
            return null;
        }
    }

    public function findDishes(int $companyId, int $menuId) {

        $menu = $this->find($companyId,$menuId);

        if($menu)
        {
            $dishes = $this->menuRepository->findDishes($menuId);

            if($dishes->count())
            {
                return $dishes;
            }
        }

        return null;
    }

    public function findFeatures($menuId){

        $features = $this->menuRepository->findFeatures($menuId);

        if(count($features))
        {
            return $features;
        }
        else
        {
            return null;
        }
    }

    public function hasFeatureMenu(int $menuId, array $features){

        $featuresAll = $this->findFeatures($menuId);

        if(count($featuresAll))
        {

            $all = $featuresAll->get(0)->pluck('feature_id')->all();

            $result = array_intersect($features,$all);

            return (count($result) >= count($all) ? true : false);
        }

        return false;

    }

    public function create(Request $request) {

        if($request->company_id !== null && $request->name !== null)
        {
            $company = $this->companyRepository->find($request->company_id);

            if($company)
            {
                $menu = new Menu();
                $menu->name = $request->name;
                $menu->observations = ($request->has('observations') ? $request->observations : null);
                $menu->price = ($request->has('price') ? $request->price: null);
                $menu->company_id = $company->id;
                //todo añadir columna category_id
                //$menu->category_id = ($category !== null ) ? $category->id : null ;
                $menu->save();

                return $menu;
            }

        }
        return null;

    }

    public function createByCompany(int $companyId, Request $request) {

        $company = $this->companyRepository->find($companyId);

        if($company)
        {
                $menu = new Menu();
                $menu->name = ($request->has('name') ? $request->name : null);
                $menu->observations = ($request->has('observations') ? $request->observations : null);
                $menu->price = ($request->has('price') ? $request->price: null);
                $menu->photo_id = ($request->has('photoId') ? $request->photoId: null);
                $menu->company_id = $company->id;
                $menu->save();
                return $menu;

        }

        return null;
    }

    public function setFeature($menuId, $featureId){

        if($featureId !== null && $menuId !== null)
        {
            $menu = $this->find($menuId);
            $feature = $this->featureRepository->find($featureId);

            if($menu && $feature)
            {

                $rel_menu_feature = new Rel_menu_feature();

                $rel_menu_feature->menu_id = $menu->id;

                $rel_menu_feature->feature_id = $feature->id;

                $rel_menu_feature->save();

                return $rel_menu_feature;
            }
        }

        return null;
    }

    function setDish(int $menuId, int $dishId){

        $menu = $this->find($menuId);

        if($menu)
        {
            $dish = $this->dishRepository->find($dishId);

            if($dish)
            {
                $rel_menu_dish = new Rel_menu_dish();

                $rel_menu_dish->dish_id = $dish->id;

                $rel_menu_dish->menu_id = $menu->id;

                $rel_menu_dish->save();

                return $rel_menu_dish;
            }
        }

        return null;
    }

    function setDishByCompany(int $companyId, int $menuId, int $dishId){

        $menu = $this->find($companyId,$menuId);

        if($menu)
        {
            $dish = $this->dishRepository->find($companyId, $dishId);

            if($dish)
            {
                $rel_menu_dish = new Rel_menu_dish();

                $rel_menu_dish->dish_id = $dish->id;

                $rel_menu_dish->menu_id = $menu->id;

                $rel_menu_dish->save();

                return $rel_menu_dish;
            }
        }

        return null;
    }

    public function update(int $menuId, Request $request) {



        if($request->company_id !== null )
        {
            $company = $this->companyRepository->find($request->company_id);

            if($company)
            {
                $menu = $this->menuRepository->find($menuId);

                if($menu)
                {
                    $menu->name = ($request->has('name') ? $request->name : null);
                    $menu->observations = ($request->has('observations') ? $request->observations : null);
                    $menu->price = ($request->has('price') ? $request->price: null);
                    $menu->company_id = $company->id;
                    $menu->save();
                    return $menu;
                }
            }
        }

        return null;

    }

    public function updateByCompany(int $companyId, int $menuId, Request $request) {

        $company = $this->companyRepository->find($companyId);

        if($company)
        {
                $menu = $this->menuRepository->find($companyId,$menuId);

                if($menu)
                {
                    $menu->name = ($request->has('name') ? $request->name : null);
                    $menu->observations = ($request->has('observations') ? $request->observations : null);
                    $menu->price = ($request->has('price') ? $request->price: null);
                    $menu->photo_id = ($request->has('photoId') ? $request->photo_id: null);
                    $menu->company_id = $company->id;
                    $menu->save();
                    return $menu;
                }

            return null;
        }
    }

    public function delete(int $menuId) {

        $menu = $this->find($menuId);

        if($menu)
        {

            $offers = $menu->offers()->get();
            $relMenuDishes = $menu->dishes()->get();
            $scores = $menu->scores()->get();

            foreach ($offers as $offer)
            {
                $offer->delete();
            }
            foreach ($relMenuDishes as $relMenuDish)
            {
                $relMenuDish->delete();
            }
            foreach ($scores as $score)
            {
                $score->delete();
            }

            return $menu->delete();
        }
        else
        {
            return null;
        }

    }

    public function deleteByCompany(int $companyId, int $menuId) {

        $menu = $this->find($companyId, $menuId);

        if($menu)
        {

            $offers = $menu->offers()->get();
            $relMenuDishes = $menu->relMenusDishes()->get();
            $scores = $menu->relMenudish()->get();

            foreach ($offers as $offer)
            {
                $offer->delete();
            }
            foreach ($relMenuDishes as $relMenuDish)
            {
                $relMenuDish->delete();
            }
            foreach ($scores as $score)
            {
                $score->delete();
            }

            return $menu->delete();
        }
        else
        {
            return null;
        }

    }

    public function deleteDish(int $companyId, int $menuId, int $dishId) {

        $menu = $this->find($companyId,$menuId);

        if($menu)
        {
            $dish = $this->menuRepository->findDish($menuId,$dishId);

            if($dish)
            {
                return $dish->delete();
            }
        }

        return null;
    }
}