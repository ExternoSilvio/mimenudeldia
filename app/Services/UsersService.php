<?php
/**
 * Created by PhpStorm.
 * User: tobiasbachmann
 * Date: 21/3/18
 * Time: 18:17
 */

namespace App\Services;

use App\Favorite;
use App\Repositories\DishRepository;
use App\Score;
use Illuminate\Http\Request;

use App\Repositories\UsersRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\MenuRepository;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersService
{

    private $usersRepository;
    private $companyRepository;
    private $menuRepository;
    private $dishRepository;

    /**
     * UsersService constructor.
     * @param $usersRepository
     * @param $companyRepository
     * @param $menuRepository
     * @param $dishRepository
     */
    public function __construct(UsersRepository $usersRepository, CompanyRepository $companyRepository, MenuRepository $menuRepository, DishRepository $dishRepository)
    {
        $this->usersRepository = $usersRepository;
        $this->companyRepository = $companyRepository;
        $this->menuRepository = $menuRepository;
        $this->dishRepository = $dishRepository;
    }

    public function find(int $usersId) {
        return $this->usersRepository->find($usersId);
    }

    public function findAll() {

        $users = $this->usersRepository->findAll();

        if($users->count())
        {
            return $users;
        }

        return null;
    }

    /*public  function  exist($email){

        $user = $this->usersRepository->exist($email);

        if($user->count())
        {
            return $user;
        }

        return null;
    }*/

    public function create(Request $data)
    {

            $user = $this->usersRepository->exist($data->email);

            if($user != null)
            {
                $user->name = $data->name;
                $user->deleted_at = null;
                $user->password = Hash::make($data->password);
                $user->save();

                return $user;

            }
            else
            {
                $user = new User();
                $user->name = $data->name;
                $user->email = $data->email;
                $user->password = Hash::make($data->password);
                $user->save();

                return $user;
            }

    }

    public function update(int $usersId, Request $request) {
        $users= $this->find($usersId);
        $users->name = ($request->has('name') ? $request->name : null);
        $users->email = ($request->has('email') ? $request->email : null);
        $users->password = ($request->has('password') ? Hash::make($request->password) : null);
        $users->save();

        return $users;
    }

    public function delete(int $usersId) {

        $user = $this->find($usersId);

        if($user)
        {
            $scores = $user->scores()->get();
            $favorites = $user->favorites()->get();
            $opinions = $user->opinions()->get();

            foreach ($scores as $score)
            {
                $score->delete();
            }
            foreach ($favorites as $favorite)
            {
                $favorite->delete();
            }
            foreach ($opinions as $opinion)
            {
                $opinion->delete();
            }

            $user->delete();
        }
        return null;
    }

    public function findFavorites(){

        $user = $this->usersRepository->find(Auth::id());

        if($user)
        {
            $favorites = $this->usersRepository->findFavorites($user->id);

            if($favorites->count())
            {
                return $favorites;
            }
        }

        return null;

    }

    public function findFavorite(int $companyId){

        $user = $this->usersRepository->find(Auth::id());

        if($user)
        {
            $company = $this->companyRepository->find($companyId);

            if($company)
            {
                $favorite = $this->usersRepository->findFavorite($company->id,$user->id);

                if($favorite)
                {
                    return $favorite;
                }
            }

        }

        return null;

    }

    public function findScoreMenus(int $userId){

        $user = $this->find($userId);

        if($user)
        {
            $scores = $this->usersRepository->findScoreMenus($userId);

            if($scores->count())
            {
                return $scores;
            }
        }

        return null;

    }

    public function findScoreMenu(int $userId, int $menuId){

        $user = $this->find($userId);

        if($user)
        {
            $score = $this->usersRepository->findScoreMenu($userId, $menuId);

            if($score)
            {
                return $score;
            }
        }

        return null;
    }

    public function findScoreDishes(int $userId, int $companyId){

        $user = $this->find($userId);

        if($user)
        {
            $scores = $this->usersRepository->findScoreDishes($user->id);

            if($scores->count())
            {
                return $scores;
            }
        }

        return null;
    }

    public function setFavorite(int $companyId){

        $user = $this->usersRepository->find(Auth::id());

        if($user)
        {
            $company = $this->companyRepository->find($companyId);

            if($company)
            {
                $favorite = $this->usersRepository->findFavorite($user->id, $company->id);

                if(!$favorite)
                {

                    $favoriteNew = new Favorite();

                    $favoriteNew->user_id = $user->id;

                    $favoriteNew->company_id = $company->id;

                    $favoriteNew->save();

                    return $favoriteNew;
                }

            }

        }

        return null;


    }

    public function setScoreMenu(int $userId, int $menuId, Request $request){

        $user = $this->usersRepository->find($userId);

        if($user)
        {
            $score = new Score();

            $score->user_id = $user->id;

            $score->menu_id = $menuId;

            $score->score = $request->has('score')? $request->score : null ;

            $score->save();

            return $score;
        }

        return null;
    }

    public function setScoreDish(int $userId, int $companyId, int $dishId, Request $request){

        $user = $this->usersRepository->find($userId);

        if($user)
        {
            $company = $this->companyRepository->find($companyId);

            if($company)
            {
                $dish = $this->dishRepository->find($company->id, $dishId);

                if($dish)
                {
                    $score = new Score();

                    $score->user_id = $user->id;

                    $score->dish_id = $dish->id;

                    $score->score = $request->has('score')? $request->score : null ;

                    $score->save();

                    return $score;
                }
            }
        }

        return null;
    }


    public function updateScoreMenu(int $userId, int $menuId, Request $request){

        $user = $this->usersRepository->find($userId);

        if($user)
        {
            $score = $this->findScoreMenu($user->id, $menuId);

            $score->user_id = $user->id;

            $score->menu_id = $menuId;

            $score->score = $request->has('score')? $request->score : null ;

            $score->save();

            return $score;
        }

        return null;

    }

    public function deleteFavorite(int $companyId){

        $user = $this->usersRepository->find(Auth::id());

        if($user)
        {
            $company = $this->companyRepository->find($companyId);

            if($company)
            {
                $favorite = $this->findFavorite($user->id, $company->id);

                if($favorite)
                {

                    $favorite->user_id = $user->id;

                    $favorite->company_id = $company->id;

                    $favorite->delete();

                    return $favorite;
                }

            }
        }

        return null;

    }

    public function deleteScoreMenu(int $usersId, int $menuId){

        $user = $this->usersRepository->find($usersId);

        if($user)
        {
                $score = $this->findScoreMenu($user->id, $menuId);

                if($score)
                {

                    $score->delete();

                    return $score;
                }
        }

        return null;

    }


    /*public function create(Request $request) {
        $users = new User();
        $users->name = ($request->has('name') ? $request->name : null);
        $users->email = ($request->has('email') ? $request->email : null);
        $users->password = ($request->has('password') ? $request->password : null);
        $users->save();
        return $users;
    }

    public function update(int $usersId, Request $request) {
        $users= $this->find($usersId);
        $users->name = ($request->has('name') ? $request->name : null);
        $users->email = ($request->has('email') ? $request->email : null);
        $users->password = ($request->has('password') ? $request->password : null);
        $users->save();
        return $users;
    }

    */
}