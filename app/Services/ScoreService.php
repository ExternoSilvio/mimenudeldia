<?php
/**
 * Created by PhpStorm.
 * User: tobiasbachmann
 * Date: 10/4/18
 * Time: 10:03
 */

namespace App\Services;

use App\Score;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Route;

use App\Repositories\CompanyRepository;
use App\Repositories\DishRepository;
use App\Repositories\MenuRepository;
use App\Repositories\UsersRepository;
use App\Repositories\ScoreRepository;

class ScoreService
{

    private $companyRepository;
    private $dishRepository;
    private $menuRepository;
    private $usersRepository;
    private $scoreRepository;

    /**
     * OfferService constructor.
     *
     * @param CompanyRepository $companyRepository
     * @param DishRepository $dishRepository
     * @param MenuRepository $menuRepository
     * @param UsersRepository $usersRepository
     * @param ScoreRepository $scoreRepository
     */

    public function __construct(CompanyRepository $companyRepository, DishRepository $dishRepository, MenuRepository $menuRepository, UsersRepository $usersRepository, ScoreRepository $scoreRepository)
    {
        $this->companyRepository = $companyRepository;
        $this->dishRepository = $dishRepository;
        $this->menuRepository = $menuRepository;
        $this->usersRepository = $usersRepository;
        $this->scoreRepository = $scoreRepository;
    }

    public function findAll(){

        $scores = $this->scoreRepository->findAll();

        if($scores->count())
        {
            return $scores;
        }

        return null;


    }

    public function findDishesScore(int $companyId, int $dishId){

        $company = $this->companyRepository->find($companyId);

        $dish = $this->dishRepository->find($company->id,$dishId);

        if($company && $dish)
        {
            $scores = $this->scoreRepository->findDishesScore($dishId);

            if($scores->count())
            {
                return $scores;
            }

        }

        return null;

    }

    public function setDishScore(int $companyId, int $dishId,Request $request){

        $company = $this->companyRepository->find($companyId);

        $dish = $this->dishRepository->find($companyId,$dishId);

        if($company && $dish)
        {
            $score = new Score();

            $score->user_id = Auth::id();
            $score->dish_id = $dish->id;
            $score->score = ($request->has('score') ? $request->input('score') : null );

            $score->save();

            return $score;

        }

        return null;
    }

    public function findDishScore(int $companyId, int $dishId, int $scoreId){

        $company = $this->companyRepository->find($companyId);

        $dish = $this->dishRepository->find($companyId,$dishId);

        if($company && $dish)
        {
            $score = $this->scoreRepository->findDishScore($dish->id,$scoreId);

            if($score)
            {
                return $score;
            }
        }

        return null;

    }

    public function putDishScore(int $companyId, int $dishId, int $scoreId, Request $request){

        $company = $this->companyRepository->find($companyId);

        $dish = $this->dishRepository->find($companyId,$dishId);

        $score = $this->scoreRepository->findDishScore($dish->id,$scoreId);

        if($company && $dish && $score)
        {

            $score->user_id = Auth::id();
            $score->dish_id = $dish->id;
            $score->score = ($request->has('score') ? $request->input('score') : null );

            $score->save();

            return $score;

        }

        return null;

    }

    public function deleteDishScore(int $companyId, int $dishId, int $scoreId){

        $company = $this->companyRepository->find($companyId);

        $dish = $this->dishRepository->find($companyId,$dishId);

        $score = $this->scoreRepository->findDishScore($dish->id,$scoreId);

        if($company && $dish && $score)
        {
            return $score->delete();

        }

        return null;

    }

    public function findMenusScore(int $companyId, int $menuId){

        $company = $this->companyRepository->find($companyId);

        $menu = $this->menuRepository->find($company->id,$menuId);

        if($company && $menu)
        {
            $scores = $this->scoreRepository->findMenusScore($menuId);

            if($scores->count())
            {
                return $scores;
            }
        }

        return null;

    }

    public function setMenuScore(int $companyId, int $menuId,Request $request){

        $company = $this->companyRepository->find($companyId);

        $menu = $this->menuRepository->find($companyId,$menuId);

        if($company && $menu)
        {
            $score = new Score();

            $score->user_id = Auth::id();
            $score->menu_id = $menu->id;
            $score->score = ($request->has('score') ? $request->input('score') : null );

            $score->save();

            return $score;

        }

        return null;
    }

    public function findMenuScore(int $companyId, int $menuId, int $scoreId){

        $company = $this->companyRepository->find($companyId);

        $menu = $this->menuRepository->find($companyId,$menuId);

        if($company && $menu)
        {
            $score = $this->scoreRepository->findMenuScore($menu->id,$scoreId);

            if($score)
            {
                return $score;
            }
        }

        return null;

    }

    public function putMenuScore(int $companyId, int $menuId, int $scoreId, Request $request){

        $company = $this->companyRepository->find($companyId);

        $menu = $this->menuRepository->find($companyId,$menuId);

        $score = $this->scoreRepository->findMenuScore($menu->id,$scoreId);

        if($company && $menu && $score)
        {

            $score->user_id = Auth::id();
            $score->menu_id = $menu->id;
            $score->score = ($request->has('score') ? $request->input('score') : null );

            $score->save();

            return $score;

        }

        return null;

    }

    public function deleteMenuScore(int $companyId, int $menuId, int $scoreId){

        $company = $this->companyRepository->find($companyId);

        $menu = $this->menuRepository->find($companyId,$menuId);

        $score = $this->scoreRepository->findMenuScore($menu->id,$scoreId);

        if($company && $menu && $score)
        {
            return $score->delete();
        }

        return null;

    }

    public function findCompaniesScore(int $companyId){

        $company = $this->companyRepository->find($companyId);

        if($company)
        {
            $scores = $this->scoreRepository->findCompaniesScore($companyId);

            if($scores->count())
            {
                return $scores;
            }
        }

        return null;

    }

    public function setCompanyScore(int $companyId, Request $request){

        $company = $this->companyRepository->find($companyId);

        if($company)
        {
            $score = new Score();

            $score->user_id = Auth::id();
            $score->company_id = $company->id;
            $score->score = ($request->has('score') ? $request->input('score') : null );

            $score->save();

            return $score;

        }

        return null;
    }

    public function findCompanyScore(int $companyId, int $scoreId){

        $company = $this->companyRepository->find($companyId);


        if($company)
        {
            $score = $this->scoreRepository->findCompanyScore($company->id,$scoreId);

            if($score)
            {
                return $score;
            }
        }

        return null;

    }

    public function putCompanyScore(int $companyId, int $scoreId, Request $request){

        $company = $this->companyRepository->find($companyId);

        $score = $this->scoreRepository->findCompanyScore($company->id,$scoreId);

        if($company && $score)
        {

            $score->user_id = Auth::id();
            $score->company_id = $company->id;
            $score->score = ($request->has('score') ? $request->input('score') : null );

            $score->save();

            return $score;

        }

        return null;

    }

    public function deleteCompanyScore(int $companyId, int $scoreId){

        $company = $this->companyRepository->find($companyId);

        $score = $this->scoreRepository->findCompanyScore($company->id,$scoreId);

        if($company && $score)
        {
            return $score->delete();

        }

        return null;

    }

    //Auth::id()

}