<?php

namespace App\Http\Controllers\Web;

use App\Company;
use App\Dish;
use App\Menu;
use App\Offer;
use App\Photo;
use App\Services\FeatureService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Services\CompanyService;
use App\Services\PhotoService;
use App\Services\DishService;
use App\Services\MenuService;
use App\Services\CategoryService;
use App\Services\OfferService;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Validator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Route;
use Yajra\DataTables\Facades\DataTables;

class MenuController extends Controller
{

    private $companyService;
    private $photoService;
    private $featuresService;
    private $dishService;
    private $categoryService;
    private $menuService;
    private $offerService;

    /**
     * DishController constructor.
     * @param CompanyService $companyService
     * @param PhotoService $photoService
     * @param FeatureService $featureService
     * @param DishService $dishService
     * @param CategoryService $categoryService
     * @param MenuService $menuService
     * @param OfferService $offerService
     *
     */
    public function __construct(CompanyService $companyService, PhotoService $photoService, FeatureService $featureService, DishService $dishService, CategoryService $categoryService, MenuService $menuService, OfferService $offerService)
    {
        $this->companyService = $companyService;
        $this->photoService = $photoService;
        $this->featuresService = $featureService;
        $this->dishService = $dishService;
        $this->categoryService = $categoryService;
        $this->menuService = $menuService;
        $this->offerService = $offerService;
    }

    /**
     * Displays datatables front end view
     * @return \Illuminate\View\View
     */
    public function index(){

        $features = $this->featuresService->findAll();
        $companies = $this->companyService->findAll();
        $categories = $this->categoryService->findAll();

        if($features !== null)
        {
            $features= $features->pluck('name','id');//->prepend('Todas las características', 0);
        }
        else
        {
            $features = [0 => 'No hay características'];
        }

        if($companies !== null)
        {
            $companies = $companies->pluck('name','id')->prepend('Todas las compañías', 0);
        }
        else
        {
            $companies = [0 => 'No hay compañías'];
        }

        if($categories !== null)
        {
            $categories = $categories->pluck('name','id')->prepend('Elige una categoría', 0);
        }
        else
        {
            $categories = [0 => 'No hay categorías'];
        }

        return view('menus.indexB')->with([
            'features' => $features,
            'companies' => $companies,
            'categories' => $categories
        ]);
    }

    /**
     * Displays datatables front end view
     * @return \Illuminate\View\View
     */
    public function indexFilteredByCompany($companyId){

        $features = $this->featuresService->findAll();
        $companies = $this->companyService->findAll();
        $categories = $this->categoryService->findAll();

        if($features !== null)
        {
            $features= $features->pluck('name','id')->prepend('Elige una característica', 0);
        }
        else
        {
            $features = [0 => 'No hay características'];
        }

        if($companies !== null)
        {
            $companies = $companies->pluck('name','id')->prepend('Filtrar por compañía', 0);
        }
        else
        {
            $companies = [0 => 'No hay compañías'];
        }

        if($categories !== null)
        {
            $categories = $categories->pluck('name','id')->prepend('Elige una categoría', 0);
        }
        else
        {
            $categories = [0 => 'No hay categorías'];
        }

        return view('companies.menus.index')->with([
            'features' => $features,
            'companies' => $companies,
            'categories' => $categories,
            'companyId' => $companyId
        ]);
    }

    /**
     * Display form for create new menu
     * @return $this
     */
    public function new() {
        $dishes = $this->dishService->findAll();
        $companies = $this->companyService->findAll();
        $features = $this->featuresService->findAll();
        $categories = $this->categoryService->findAll();

        if($companies !== null)
        {
            $companies = $companies->pluck('name','id')->prepend('Elige una compañía',0);
        }
        else
        {
            $companies = [0 => 'No hay compañías'];
        }

        return view('menus.new')->with([
            'companies' => $companies,
            'dishes' => $dishes,
            'features' => $features,
            'categories' => $categories,
            'photo' => $photo = null,
            'menu' => new Menu()
        ]);
    }

    /**
     * Display form for create new menu
     * @return $this
     */
    public function newByCompany(int $companyId) {
        $dishes = $this->dishService->findAll();
        $companies = $this->companyService->findAll();

        if($companies !== null)
        {
            $companies = $companies->pluck('name','id')->prepend('Elige una compañía',0);
        }
        else
        {
            $companies = [0 => 'No hay compañías'];
        }

        return view('companies.menus.new')->with([
            'dishes' => $dishes,
            'photo' => $photo = null,
            'menu' => new Menu(),
            'companyId' => $companyId,

        ]);
    }

    /**
     * Display form for menu edition
     * @param int $menuId
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit(int $menuId) {

        $menu = $this->menuService->find($menuId);
        $dishes = $this->dishService->findAll();
        $current_dishes = $menu->dishes()->get();
        $companies = $this->companyService->findAll();
        $features = $this->featuresService->findAll();
        $current_features = $menu->features()->get();
        $offers = $menu->offers()->get();


        if($companies !== null)
        {
            $companies = $companies->pluck('name','id')->prepend('Elige una compañía',0);
        }
        else
        {
            $companies = [0 => 'No hay compañías'];
        }

        if ($menu !== null) {

            $photo = $this->photoService->find($menu->photo_id);

            return view('menus.edit')->with([
                'carbon' => new Carbon(),
                'offers' => $offers,
                'object' => new Offer(),
                'route' => 'menus',
                'folder' => 'offers.fields',
                'entity_id' => $menu->id,
                'menu' => $menu,
                'dishes' => $dishes,
                'current_dishes' => $current_dishes,
                'features' => $features,
                'current_features' => $current_features,
                'companies' => $companies,
                'photo' => $photo
            ]);
        } else {
            Session::flash('message_error','El menu especificado no ha sido encontrado o no existe');
            return redirect(route('menu.index'));
        }
    }


    /**
     * Display form for menu edition
     * @param int $companyId
     * @param int $menuId
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function editByCompany(int $companyId, int $menuId) {

        $menu = $this->menuService->find($menuId);
        $dishes = $this->dishService->findAll();
        $current_dishes = $menu->dishes()->get();
        $companies = $this->companyService->findAll();
        $features = $this->featuresService->findAll();
        $current_features = $menu->features()->get();
        $offers = $menu->offers()->get();


        if($companies !== null)
        {
            $companies = $companies->pluck('name','id')->prepend('Elige una compañía',0);
        }
        else
        {
            $companies = [0 => 'No hay compañías'];
        }

        if ($menu !== null) {

            $photo = $this->photoService->find($menu->photo_id);

            return view('companies.menus.edit')->with([
                'carbon' => new Carbon(),
                'offers' => $offers,
                'object' => new Offer(),
                'route' => 'menus',
                'folder' => 'offers.fields',
                'entity_id' => $menu->id,
                'menu' => $menu,
                'companyId' => $companyId,
                'dishes' => $dishes,
                'current_dishes' => $current_dishes,
                'features' => $features,
                'current_features' => $current_features,
                'companies' => $companies,
                'photo' => $photo
            ]);
        } else {
            Session::flash('message_error','El menu especificado no ha sido encontrado o no existe');
            return redirect(route('companies.menu.index',$companyId));
        }
    }

    /**
     * Create a new menu with input requested
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(Request $request) {
        $input = $request;
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'required',
            'company_id' => 'required'
        ]);
        if ($validator->fails()) {
            Session::flash('message_error','Se produjo un error al crear el menu');
            return redirect()->to(route('menus.create'))
                ->withInput($request->input())
                ->withErrors($validator->errors());
        } else {

            $menu = $this->menuService->create($input);

            if(count($request->dishes))
            {
                foreach ($request->dishes as $dish) {

                    $this->menuService->setDish($menu->id,$dish);
                }
            }
            if(count($request->features))
            {
                foreach ($input->features as $feature)
                {
                    $this->menuService->setFeature($menu->id,$feature);
                }
            }

            if($request->has('photo') && $request->input('photo') !== null)
            {

                $photo = new Photo() ;

                $photo->url = $request->file('photo')->store('public/photosMenu');

                $photo->save();

                $menu->photo_id = $photo->id;

                $menu->save();
            }
            Session::flash('message','Menu creado correctamente');
            return redirect(route('menus.index'));
        }
    }

    /**
     * Create a new menu with input requested
     * @param int $companyId
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createByCompany(int $companyId, Request $request) {
        $input = $request;
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'required',
        ]);
        if ($validator->fails()) {
            Session::flash('message_error','Se produjo un error al crear el menu');
            return redirect()->to(route('companies.menus.create',$companyId))
                ->withInput($request->input())
                ->withErrors($validator->errors());
        } else {

            $menu = $this->menuService->createByCompany($companyId,$input);

            if(count($request->dishes))
            {
                foreach ($request->dishes as $dish) {

                    $this->menuService->setDish($menu->id,$dish);
                }
            }

            if($request->has('photo') && $request->file('photo') !== null)
            {

                $photo = new Photo() ;

                $extension = $request->photo->extension();

                $photo->url = $request->file('photo')->storeAs('/public/photos/menu', str_slug($menu->name).'.'.$extension);

                $photo->save();

                $menu->photo_id = $photo->id;

                $menu->save();
            }
            Session::flash('message','Menu creado correctamente');
            return redirect(route('companies.menus.index',$companyId));
        }
    }

    /**
     * Create a new offer with input requested
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createOffer(Request $request) {
        $input = $request;
        $validator = Validator::make($request->all(), [

            //'finish_at' => 'min:'.$request->start_at.'| min:'.Carbon::now().'|nullable'

        ]);
        if ($validator->fails()) {
            dd($validator->errors());
            Session::flash('message_error','Se produjo un error al crear la oferta');
            return redirect()->to(route('menus.index'))
                ->withInput($request->input())
                ->withErrors($validator->errors());
        } else {

            $offersToRemove = ($input->has('offers') ? $input->offers : [] );

            foreach ($offersToRemove as $offer)
            {
                $this->offerService->delete($offer);
            }
            if($input->start_at !== null)
            {
                $offer = $this->offerService->setMenu($input->hidden_id,$input);
            }


            Session::flash('message','Oferta creada correctamente');
            return redirect(route('menus.index'));
        }
    }

    /**
     * Create a new offer with input requested
     * @param $companyId
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createOfferByCompany(int $companyId, Request $request) {

        $input = $request;
        $validator = Validator::make($request->all(), [

            'type' => 'filled',
            'type_value' => 'filled',
            //todo validar las ofertas
            //'start_at' =>'min:'.Carbon::now(),

        ]);
        if ($validator->fails()) {


            Session::flash('message_error','Se produjo un error al crear la oferta');
            return redirect('/companies/'.$companyId.'/menus/'.$input->hidden_id.'/edit#1')
                ->withInput($request->input())
                ->withErrors($validator->errors());

        } else {



            $offersToRemove = ($input->has('offers') ? $input->offers : [] );

            foreach ($offersToRemove as $offer)
            {

                $this->offerService->delete($offer);
            }

            $offer = "";

            if($input->start_at !== null)
            {
               $offer = $this->offerService->setMenu($input->hidden_id,$input);
            }


            Session::flash('message','Oferta creada correctamente');
            return redirect('/companies/'.$companyId.'/menus/'.$input->hidden_id.'/#offers');



        }
    }

    /**
     * Update the specified menu with input requested
     * @param int $menuId
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(int $menuId, Request $request) {

        $menu = $this->menuService->find($menuId);

        if ($menu !== null) {
            $input = $request;

            $validator = Validator::make($request->all(), [
                'name' => 'filled',
                'price' => 'required',
                'start_at' => 'max:'.$input->finish_at,
                'type_value' => 'nullable|numeric|min:0'
            ]);
            if ($validator->fails()) {
                Session::flash('message_error','Se produjo un error al guardar el menu');
                return redirect()->to(route('menus.edit', $menu->id))
                    ->withInput($request->input())
                    ->withErrors($validator->errors());
            } else {

                $this->menuService->update($menuId, $input);

                $dishesToRemove = $menu->dishes()->get()->all();

                foreach ($dishesToRemove as $dish)
                {
                    $dish->delete();
                }


                if(count($request->dishes))
                {
                    foreach ($request->dishes as $dish) {

                        $this->menuService->setDish($menu->id,$dish);
                    }
                }


                $featuresToRemove = $menu->features()->get()->all();

                foreach ($featuresToRemove as $feature)
                {
                    $feature->delete();
                }

                if(count($request->features))
                {
                    foreach ($request->features as $feature) {

                        $this->menuService->setFeature($menu->id,$feature);
                    }
                }


                $offersToRemove = ($input->has('offers') ? $input->offers : [] );

                foreach ($offersToRemove as $offer)
                {

                    $this->offerService->delete($offer);
                }

                if($input->start_at !== null && $input->type_value !== null)
                {

                    $this->offerService->setMenu($input->hidden_id,$input);
                }

                if($request->has('photo') && $request->file('photo') !== null)
                {
                    $this->photoService->updateMenuPhoto($menu,$request);
                }

                Session::flash('message','Menú guardado correctamente');

                return redirect(route('menus.index'));

            }
        } else {
            Session::flash('message_error','El menu  especificado no ha sido encontrado o no existe');
            return redirect(route('menus.index'));
        }
    }

    /**
     * Update the specified menu with input requested
     * @param int $companyId
     * @param int $menuId
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateByCompany(int $companyId, int $menuId, Request $request) {


        $menu = $this->menuService->find($menuId);

        if ($menu !== null) {
            $input = $request;

            //dd($input->start_at < $input->finish_at ? true : false);

            $validator = Validator::make($request->all(), [
                'name' => 'filled',
                'price' => 'required',
                'start_at' => 'max:'.$input->finish_at,
                'type_value' => 'nullable|numeric|min:0'
            ]);
            if ($validator->fails()) {
                Session::flash('message_error','Se produjo un error al guardar el menu');
                return redirect()->to(route('companies.menus.edit', [$companyId, $menu->id]))
                    ->withInput($request->input())
                    ->withErrors($validator->errors());
            } else {

                $this->menuService->updateByCompany($companyId,$menuId, $input);

                $dishesToRemove = $menu->dishes()->get()->all();

                foreach ($dishesToRemove as $dish)
                {
                    $dish->delete();
                }

                if(count($request->dishes))
                {
                    foreach ($request->dishes as $dish) {

                        $this->menuService->setDish($menu->id,$dish);
                    }
                }

                $offersToRemove = ($input->has('offers') ? $input->offers : [] );

                foreach ($offersToRemove as $offer)
                {

                    $this->offerService->delete($offer);
                }

                if($input->start_at !== null && $input->type_value !== null)
                {

                    $this->offerService->setMenu($input->hidden_id,$input);
                }

                if($request->has('photo') && $request->file('photo') !== null)
                {
                    $this->photoService->updateMenuPhoto($menu,$request);
                }

                Session::flash('message','Menú guardado correctamente');

                return redirect(route('companies.menus.index',$companyId));

            }
        } else {
            Session::flash('message_error','El menu  especificado no ha sido encontrado o no existe');
            return redirect(route('companies.menus.index',$companyId));
        }
    }

    /**
     * Delete the specified menu
     * @param int $menuId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete(int $menuId) {


        $menu = $this->menuService->find($menuId);

        if ($menu !== null) {
            $this->menuService->delete($menuId);
            Session::flash('message','Menu borrado correctamente');
            return redirect(route('menus.index'));
        } else {
            Session::flash('message_error','El menu especificado no ha sido encontrado o no existe');
            return redirect(route('menus.index'));
        }
    }

    /**
     * Delete the specified menu
     * @param $companyId
     * @param int $menuId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteByCompany(int $companyId, int $menuId) {


        $menu = $this->menuService->find($menuId);

        if ($menu !== null) {
            $this->menuService->delete($menuId);
            Session::flash('message','Menu borrado correctamente');
            return redirect(route('companies.menus.index',$companyId));
        } else {
            Session::flash('message_error','El menu especificado no ha sido encontrado o no existe');
            return redirect(route('companies.menus.index',$companyId));
        }
    }

    /**
     * Process datatables ajax request.
     * @return \Illuminate\Http\JsonResponse
     */
    public function datatable(Request $request)
    {

        if($request->has('company'))
        {
            $menus = $this->menuService->findAllByCompany($request->company);

        }
        else
            if($request->has('company_global'))
            {
                    $menus = $this->menuService->findAll();
            }
            else
            {
                $menus = collect();
            }



        return DataTables::collection($menus)
            ->filter(function ($instance) use ($request) {

                if($request->has('company'))
                {
                    $company = $request->company;
                }
                else
                    if($request->has('company_global'))
                    {
                        $company = $request->company_global;
                    }
                    else
                    {
                        $company = null;
                    }

                $search = $request->has('search') ? Str::lower($request->get('search')['value']) : null;
                $features      = $request->has('features') ? explode('|', $request->get('features')) : null;
                $category      = $request->has('category') ? explode('|', $request->get('category')) : null;

                if ($search !== null && $search !== "") {
                    $instance->collection = $instance->collection->filter(function ($row) use ($search) {
                        $str_name = Str::lower($row['name']);
                        return Str::contains($str_name, $search) ? true : false;
                    });
                }

                if ($features !== null && count($features) > 0 && $features[0] !== "" && $features[0] != 0) {

                    $instance->collection = $instance->collection->filter(function ($menu) use ($features) {
//                        $tattoos = Tattoo::withAllTags($tags)->where('id', '=', $tattoo['id'])->get();
//                        return ($tattoos->count() > 0);

                        return  $this->menuService->hasFeatureMenu($menu['id'], $features);

                    });
                }

                if ($company !== null && count($company) > 0 && $company[0] !== "" && $company[0] != 0) {

                    $instance->collection = $instance->collection->filter(function ($menu) use ($company) {
//                        $tattoos = Tattoo::withAllTags($tags)->where('id', '=', $tattoo['id'])->get();
//                        return ($tattoos->count() > 0);
                        return  ($this->menuService->findByCompany($company , $menu['id'] ) !== null) ? true : false ;

                    });
                }
                if ($category !== null && count($category) > 0 && $category[0] !== "" && $category[0] != 0) {

                    $instance->collection = $instance->collection->filter(function ($menu) use ($category) {
//                        $tattoos = Tattoo::withAllTags($tags)->where('id', '=', $tattoo['id'])->get();
//                        return ($tattoos->count() > 0);
                        return  true; //($this->menuService->findByCategory($category[0] , $menu['id'] ) !== null) ? true : false ;

                    });
                }
            })
            ->addColumn('company_id', function($menu) {

                $company = $menu->company()->get()->first()->name;

                return ($company !== null) ? $company : [ 0 =>'No existe compañía' ];


            })
            ->addColumn('category_id', function($menu) {

                //todo añadir columna category

                //$category = $menu->category()->get()->first();

                return ['-'];//($category !== null) ? [$category->name] : [ 0 =>'No existe categoría' ];


            })
            ->addColumn('score', function($menu) {

                /*if(count($dish))
                {
                    $scores = $dish->scores()->get();

                    if(count($scores))
                    {
                        $scores = $scores->pluck('score')->all();

                        $svgScore = array_sum($scores)/count($scores);

                        return [$svgScore.'/10 ('.count($scores).' votes)'];
                    }
                }*/

                if(count($menu))
                {
                    $scores = $menu->scores()->get();

                    if(count($scores))
                    {
                        $scores = $scores->pluck('score')->all();

                        $svgScore = array_sum($scores)/count($scores);

                        $stars = substr($svgScore,0,1);

                        $rest = substr($svgScore,2,1);

                        $print = array();

                        for($i = 0 ; $i < $stars; $i++)
                        {
                            $print[]="<i class='fa fa-star' style='color:gold;'></i>";
                        }

                        if($rest > 5 )
                        {
                            $print[]="<i class='fa fa-star' style='color:gold;'></i>";
                        }
                        else
                            if($rest <=5 && $rest)
                            {
                                $print[]="<i class='fa fa-star-half-o' style='color:gold;'></i>";
                            }

                        $c = count($print);

                        for($i = 0 ; $i < (5 - $c); $i++ )
                        {
                            $print[] = "<i class='fa fa-star-o' style='color:gold;'></i>";
                        }

                        return implode(" ",$print);
                    }


                }

                return [0 => 'No hay valoraciones'];


            })
            ->addColumn('photo_id', function($menu) {

                $photoId = $menu->photo()->get([0 => 'id']);

                if(count($photoId))
                {
                    $photoId = $menu->photo()->get([0 => 'id'])[0];
                }
                $photo = $this->photoService->find($photoId);

                if($photo !== null)
                {
                    $url = asset('storage'.str_after($photo[0]->url, 'public'));

                    if($url !== null)
                    {
                        return '<div><img class="img-thumbnail" style="width:40px;" src="'.$url.'"></div>';
                    }

                }

                $url = asset('storage/photos/default/no-photo.png');

                return '<div><img class="img-thumbnail" style="width:40px" src="'.$url.'"></div>';

            })
            ->addColumn('action', function ($menu) use ($request){

                $user = Auth::user();

                if($user->hasRole('superadmin') && $request->has('company_global'))
                {
                    return '<div class="btn-group">
                                <a href="'.route('menus.edit', $menu->id ).'" class="btn btn-xs btn-primary"><i class="fa fa-fw fa-edit"></i></a>
                                <a href="'.route('menus.delete', $menu->id ).'" class="btn btn-xs btn-danger" data-remove="¿Está seguro de que quiere borrar este menú?"><i class="fa fa-fw fa-trash"></i></a>
                            </div>';
                }
                $company = $menu->company()->get([0 => 'id'])[0];

                return '<div class="btn-group">
                                <a href="'.route('companies.menus.edit', [$company->id,$menu->id]).'" class="btn btn-xs btn-primary"><i class="fa fa-fw fa-edit"></i></a>
                                <a href="'.route('companies.menus.delete', [$company->id, $menu->id]).'" class="btn btn-xs btn-danger" data-remove="¿Está seguro de que quiere borrar este menú?"><i class="fa fa-fw fa-trash"></i></a>
                            </div>';

            })
            ->rawColumns(['action','score','photo_id'])
            ->setRowId('{{$id}}')
            ->make(true);
    }

    public static function routes() {
        Route::get(     '/menus',                                        'Web\MenuController@index'                     )->name('menus.index'                     )->middleware('auth');
        Route::get(     '/companies/{companyId}/menus',                  'Web\MenuController@indexFilteredByCompany'    )->name('companies.menus.index'           )->middleware('auth');
        Route::get(     '/menus/new',                                    'Web\MenuController@new'                       )->name('menus.new'                       )->middleware('auth');
        Route::get(     '/companies/{companyId}/menus/new',              'Web\MenuController@newByCompany'              )->name('companies.menus.new'             )->middleware('auth');
        Route::post(    '/menus/new',                                    'Web\MenuController@create'                    )->name('menus.create'                    )->middleware('auth');
        Route::post(    '/companies/{company}/menus/new',                'Web\MenuController@createByCompany'           )->name('companies.menus.create'          )->middleware('auth');
        Route::post(    '/menus/newOffer',                               'Web\MenuController@createOffer'               )->name('menus.createOffer'               )->middleware('auth');
        Route::post(    '/companies/{companyId}/menus/newOffer',         'Web\MenuController@createOfferByCompany'      )->name('companies.menus.createOffer'     )->middleware('auth');
        Route::get(     '/menus/{menuId}/edit',                          'Web\MenuController@edit'                      )->name('menus.edit'                      )->middleware('auth');
        Route::get(     '/companies/{companyId}/menus/{menuId}/edit',    'Web\MenuController@editByCompany'             )->name('companies.menus.edit'            )->middleware('auth');
        Route::put(     '/menus/{menuId}/edit',                          'Web\MenuController@update'                    )->name('menus.update'                    )->middleware('auth');
        Route::put(     '/companies/{companyId}/menus/{menuId}/edit',    'Web\MenuController@updateByCompany'           )->name('companies.menus.update'          )->middleware('auth');
        Route::delete(  '/menus/{menuId}/delete',                        'Web\MenuController@delete'                    )->name('menus.delete'                    )->middleware('auth');
        Route::delete(  '/companies/{companyId}/menus/{menuId}/delete',  'Web\MenuController@deleteByCompany'           )->name('companies.menus.delete'          )->middleware('auth');
        Route::get(     '/menus/datatable',                              'Web\MenuController@datatable'                 )->name('menus.datatable'                 )->middleware('auth');
    }
}
