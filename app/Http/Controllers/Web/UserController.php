<?php
/**
 * Created by PhpStorm.
 * User: tobiasbachmann
 * Date: 16/4/18
 * Time: 11:22
 */


namespace App\Http\Controllers\Web;


use App\User;
use Illuminate\Support\Facades\Session;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Validator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Route;
use App\Services\UsersService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;


class UserController extends Controller
{

    private $usersService;

    /**
     * UserController constructor.
     * @param UsersService $usersService
     *
     */
    public function __construct(UsersService $usersService)
    {
        $this->usersService = $usersService;
    }

    /**
     * Displays datatables front end view
     * @return \Illuminate\View\View
     */
    public function index() {
        return view('users.index');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function profile() {
        return redirect(route('users.edit', Auth::id()));
    }

    /**
     * Display form for create new user
     * @return $this
     */
    public function new() {
        return view('users.new')->with([
            'user' => new User()
        ]);
    }

    /**
     * Create a new user with input requested
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(Request $request) {
        $input = $request;
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:5'
        ]);
        if ($validator->fails()) {
            Session::flash('message_error','Se produjo un error al crear el usuario');
            return redirect()->to(route('users.create'))
                ->withInput($request->input())
                ->withErrors($validator->errors());
        } else {
            $user = $this->usersService->create($input);

            Session::flash('message','Usuario creado correctamente');
            return redirect(route('users.edit', $user->id));
        }
    }

    /**
     * Display form for user edition
     * @param int $userId
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit(int $userId) {
        $roles = Role::all();
        $permissions = Permission::all();
        $user = $this->usersService->find($userId);
        $user->password = null;
        if ($user !== null) {
            return view('users.edit')->with([
                'user' => $user,
                'roles' => $roles,
                'permissions' => $permissions
            ]);
        } else {
            Session::flash('message_error','El usuario especificada no ha sido encontrada o no existe');
            return redirect(route('users.index'));
        }
    }

    /**
     * Update the specified user with input requested
     * @param int $userId
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(int $userId, Request $request) {
        $user = $this->usersService->find($userId);
        if ($user !== null) {
            $input = $request;
            if ($request->has('avatar')) {
                $input['avatar'] = $this->usersService->uploadUserAvatar($user, $request->file('avatar'), 200);
            }

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email',
                'password' => 'required|min:5'
            ]);
            if ($validator->fails()) {
                Session::flash('message_error','Se produjo un error al guardar el usuario');
                return redirect()->to(route('users.edit', $user->id))
                    ->withInput($request->input())
                    ->withErrors($validator->errors());
            } else {
                $this->usersService->update($userId, $input);
                if ($request->has('roles') && $userId !== 1) {
                    $user->syncRoles($request->input('roles'));
                } else {
                    //$user->syncRoles([]);
                }
                if ($request->has('permissions') && $userId !== 1) {
                    $user->syncPermissions($request->input('permissions'));
                } else {
                    //$user->syncPermissions([]);
                }
                if ($userId === Auth::id()) {
                    Session::flash('message','Ha actualizado correctamente su perfil');
                    return redirect(route('users.edit', Auth::id()));
                } else {
                    Session::flash('message','Usuario guardado correctamente');
                    return redirect(route('users.index'));
                }
            }
        } else {
            Session::flash('message_error','El usuario especificado no ha sido encontrada o no existe');
            return redirect(route('users.index'));
        }
    }

    /**
     * Delete the specified user
     * @param int $userId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete(int $userId) {

        $user = $this->usersService->find($userId);

        if ($user !== null) {
            $this->usersService->delete($userId);
            Session::flash('message','Usuario borrada correctamente');
            return redirect(route('users.index'));
        } else {
            Session::flash('message_error','El usuario especificado no ha sido encontrada o no existe');
            return redirect(route('users.index'));
        }
    }
    public function datatable(Request $request)
    {

        $users = $this->usersService->findAll();
        return DataTables::collection($users)
            ->filter(function ($instance) use ($request) {
                $search = $request->has('search') ? Str::lower($request->get('search')['value']) : null;
                if ($search !== null && $search !== "") {
                    $instance->collection = $instance->collection->filter(function ($row) use ($search) {
                        $str_name = Str::lower($row['name']);
                        return Str::contains($str_name, $search) ? true : false;
                    });
                }
            })
            ->editColumn('email', function($user) {
                return '<a href="mailto:'.$user->email.'"><i class="fa fa-fw fa-envelope-o"></i> '.$user->email.'</a>';
            })
            ->editColumn('created_at', function($user) {
                $date = Carbon::createFromTimeString($user->created_at);
                return $date->format('d/m/Y H:i:s');
            })
            ->addColumn('action', function ($user) {
                if ($user->id !== null && $user->id !== Auth::id()) {
                    return '<div class="btn-group">
                                <a href="'.route('users.edit', $user->id).'" class="btn btn-xs btn-primary"><i class="fa fa-fw fa-edit"></i></a>
                                <a href="'.route('users.delete', $user->id).'" class="btn btn-xs btn-danger" data-remove="¿Está seguro de que quiere borrar este usuario?"><i class="fa fa-fw fa-trash"></i></a>
                            </div>';
                } else {
                    return '<div class="btn-group">
                                <a href="'.route('users.edit', $user->id).'" class="btn btn-xs btn-primary"><i class="fa fa-fw fa-edit"></i></a>
                            </div>';
                }
            })
            ->rawColumns(['email', 'verified', 'action'])
            ->setRowId('{{$id}}')
            ->make(true);
    }

    public static function routes() {
        Route::get(     '/users',                   'Web\UserController@index'       )->name('users.index'    )->middleware('auth');
        Route::get(     '/users/profile',           'Web\UserController@profile'     )->name('users.profile'  )->middleware('auth');
        Route::get(     '/users/new',               'Web\UserController@new'         )->name('users.new'      )->middleware('auth');
        Route::post(    '/users/new',               'Web\UserController@create'      )->name('users.create'   )->middleware('auth');
        Route::get(     '/users/{userId}/edit',     'Web\UserController@edit'        )->name('users.edit'     )->middleware('auth');
        Route::put(     '/users/{userId}/edit',     'Web\UserController@update'      )->name('users.update'   )->middleware('auth');
        Route::delete(  '/users/{userId}/delete',   'Web\UserController@delete'      )->name('users.delete'   )->middleware('auth');
        Route::get(     '/users/datatable',         'Web\UserController@datatable'   )->name('users.datatable')->middleware('auth');
    }

}