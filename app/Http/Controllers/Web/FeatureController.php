<?php

namespace App\Http\Controllers\Web;

use App\Feature;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Services\FeatureService;
use Illuminate\Support\Facades\Session;
use Validator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Route;
use Yajra\DataTables\Facades\DataTables;

class FeatureController extends Controller
{
    //
    private $featureService;

    /**
     * CategoryController constructor.
     * @param FeatureService $featureService
     *
     */
    public function __construct(FeatureService $featureService)
    {
        $this->featureService = $featureService;
    }

    /**
     * Displays datatables front end view
     * @return \Illuminate\View\View
     */
    public function index(){


        $features = $this->featureService->findAll();


        if($features !== null)
        {
            $features = $features->pluck('name','id')->prepend('Todas las características',0);
        }
        else
        {
            $features = [0 => 'No hay características'];
        }

        return view('features.index')->with([
            'features' => $features
        ]);

    }

    /**
     * Display form for create new feature
     * @return $this
     */
    public function new() {

        $features = $this->featureService->findAll();


        if($features !== null)
        {
            $features = $features->pluck('name','id')->prepend('Elige una característica',0);
        }
        else
        {
            $features = [0 => 'No hay características'];
        }

        return view('features.new')->with([
            'features' => $features,
            'feature' => new Feature()
        ]);
    }

    /**
     * Display form for feature edition
     * @param int $featureId
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit(int $featureId) {

        $features = $this->featureService->findAll();


        if($features !== null)
        {
            $features = $features->pluck('name','id')->prepend('Elige una característica',0);
            unset($features[$featureId]);
        }
        else
        {
            $features = [0 => 'No hay características'];
        }

        $feature = $this->featureService->find($featureId);

        if ($feature !== null) {
            return view('features.edit')->with([
                'feature' => $feature,
                'features' => $features,
            ]);
        } else {
            Session::flash('message_error','La caracteística especificada no ha sido encontrada o no existe');
            return redirect(route('features.index'));
        }
    }

    /**
     * Create a new feature with input requested
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(Request $request) {

        $input = $request;

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:feature',

        ]);
        if ($validator->fails()) {
            Session::flash('message_error','Se produjo un error al crear la característica');
            return redirect()->to(route('features.create'))
                ->withInput($request->input())
                ->withErrors($validator->errors());
        } else {

            $feature = $this->featureService->create($input);

            Session::flash('message','Característica creada correctamente');
            return redirect(route('features.index'));
        }
    }

    /**
     * Update the specified feature with input requested
     * @param int $featureId
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(int $featureId, Request $request) {
        $feature = $this->featureService->find($featureId);

        if ($feature !== null) {
            $input = $request;

            $validator = Validator::make($request->all(), [
                'name' => 'filled|unique:feature,id',
            ]);
            if ($validator->fails()) {
                Session::flash('message_error','Se produjo un error al guardar la características');
                return redirect()->to(route('features.edit', $feature->id))
                    ->withInput($request->input())
                    ->withErrors($validator->errors());
            } else {

                $this->featureService->update($featureId, $input);

                Session::flash('message','Característica guardada correctamente');

                return redirect(route('features.index'));

            }
        } else {
            Session::flash('message_error','La característica especificada no ha sido encontrada o no existe');
            return redirect(route('features.index'));
        }
    }

    /**
     * Delete the specified feature
     * @param int $featureId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete(int $featureId) {


        $feature = $this->featureService->find($featureId);

        if ($feature !== null) {
            $this->featureService->delete($featureId);
            Session::flash('message','Característica borrada correctamente');
            return redirect(route('features.index'));
        } else {
            Session::flash('message_error','La característica especificada no ha sido encontrada o no existe');
            return redirect(route('features.index'));
        }
    }

    /**
     * Process datatables ajax request.
     * @return \Illuminate\Http\JsonResponse
     */
    public function datatable(Request $request)
    {

        $features = $this->featureService->findAll();

        $features = ($features !== null) ? $features : collect();

        return DataTables::collection($features)
            ->filter(function ($instance) use ($request) {

                $search = $request->has('search') ? Str::lower($request->get('search')['value']) : null;
                $feature_id  = $request->has('feature_id') ? intval($request->get('feature_id')) : null;

                if ($search !== null && $search !== "") {
                    $instance->collection = $instance->collection->filter(function ($row) use ($search) {
                        $str_name = Str::lower($row['name']);
                        return Str::contains($str_name, $search) ? true : false;
                    });
                }

                if ($feature_id !== null && $feature_id > 0) {
                    $instance->collection = $instance->collection->filter(function ($features) use ($feature_id) {
                        $feature = intval($features['feature_id']);
                        return ($feature === $feature_id);
                    });
                }
            })
            ->addColumn('feature', function($feature) {
                $feature = $feature->feature()->get();
                return ($feature->count() > 0) ? $feature->first()->name : 'Principal';

//                try
//                {
//                    return  $feature = $feature->feature()->get()->first()->name;
//
//                }catch (\Exception $e){
//
//                    return 'Característica eliminada';
//                }

            })
            ->addColumn('action', function ($feature) {
                return '<div class="btn-group">
                                <a href="'.route('features.edit', $feature->id).'" class="btn btn-xs btn-primary"><i class="fa fa-fw fa-edit"></i></a>
                                <a href="'.route('features.delete', $feature->id).'" class="btn btn-xs btn-danger" data-remove="¿Está seguro de que quiere borrar esta catacterísticas?"><i class="fa fa-fw fa-trash"></i></a>
                            </div>';

            })
            ->rawColumns(['action'])
            ->setRowId('{{$id}}')
            ->make(true);
    }

    public static function routes() {
        Route::get(     '/features',                       'Web\FeatureController@index'       )->name('features.index'    )->middleware('auth');
        Route::get(     '/features/new',                   'Web\FeatureController@new'         )->name('features.new'      )->middleware('auth');
        Route::post(    '/features/new',                   'Web\FeatureController@create'      )->name('features.create'   )->middleware('auth');
        Route::get(     '/features/{featuresId}/edit',     'Web\FeatureController@edit'        )->name('features.edit'     )->middleware('auth');
        Route::put(     '/features/{featuresId}/edit',     'Web\FeatureController@update'      )->name('features.update'   )->middleware('auth');
        Route::delete(  '/features/{featuresId}/delete',   'Web\FeatureController@delete'      )->name('features.delete'   )->middleware('auth');
        Route::get(     '/features/datatable',             'Web\FeatureController@datatable'   )->name('features.datatable')->middleware('auth');
    }
}
