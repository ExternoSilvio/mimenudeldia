<?php

namespace App\Http\Controllers\Web;

use App\Company;
use App\Dish;
use App\Offer;
use App\Photo;
use App\Services\CategoryService;
use App\Services\FeatureService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Services\CompanyService;
use App\Services\PhotoService;
use App\Services\DishService;
use App\Services\OfferService;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Validator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Route;
use Yajra\DataTables\Facades\DataTables;

class DishController extends Controller
{
    private $companyService;
    private $photoService;
    private $featuresService;
    private $dishService;
    private $offerService;
    private $categoryService;

    /**
     * DishController constructor.
     * @param CompanyService $companyService
     * @param PhotoService $photoService
     * @param FeatureService $featureService
     * @param DishService $dishService
     * @param OfferService $offerService
     * @param CategoryService $categoryService
     */
    public function __construct(CompanyService $companyService, PhotoService $photoService, FeatureService $featureService, DishService $dishService, OfferService $offerService, CategoryService $categoryService)
    {
        $this->companyService = $companyService;
        $this->photoService = $photoService;
        $this->featuresService = $featureService;
        $this->dishService = $dishService;
        $this->offerService = $offerService;
        $this->categoryService = $categoryService;
    }

    /**
     * Displays datatables front end view
     * @return \Illuminate\View\View
     */
    public function index(){

        $features = $this->featuresService->findAll();
        $companies = $this->companyService->findAll();
        $categories = $this->categoryService->findAll();

        if($features !== null)
        {
            $features = $features->pluck('name','id');//->prepend('Elige una característica', 0);
        }
        else
        {
            $features = [0 => 'No hay características'];
        }

        if($companies !== null)
        {
            $companies = $companies->pluck('name','id')->prepend('Todas las compañías', 0);
        }
        else
        {
            $features = [0 => 'No hay compañías'];
        }
        if($categories !== null)
        {
            $categories = $categories->pluck('name','id')->prepend('Elige una categoría', 0);
        }
        else
        {
            $features = [0 => 'No hay categorías'];
        }

        return view('dishes.index')->with([
            'features' => $features,
            'companies' => $companies,
            'categories' => $categories
        ]);
    }

    /**
     * Displays datatables front end view
     * @return \Illuminate\View\View
     */
    public function indexFilteredByCompany(int $companyId){

        $features = $this->featuresService->findAll();
        $companies = $this->companyService->findAll();

        if($features !== null)
        {
            $features= $features->pluck('name','id');//->prepend('Elige una característica', 0);
        }
        else
        {
            $features = [0 => 'No hay características'];
        }

        if($companies !== null)
        {
            $companies = $companies->pluck('name','id')->prepend('Elige una compañía', 0);
        }
        else
        {
            $features = [0 => 'No hay compañías'];
        }

        return view('companies.dishes.index')->with([
            'features' => $features,
            'companies' => $companies,
            'companyId' => $companyId
        ]);
    }

    /**
     * Display form for create new dish
     * @return $this
     */
    public function new() {

        $companies = $this->companyService->findAll();
        $features = $this->featuresService->findAll();
        $categories = $this->categoryService->findAll();

        if($companies !== null)
        {
            $companies = $companies->pluck('name','id')->prepend('Elige una compañía',0);
        }
        else
        {
            $companies = [0 => 'No hay compañías'];
        }

        if($categories !== null)
        {
            $categories = $categories->pluck('name','id')->prepend('Elige una categoría',0);
        }
        else
        {
            $categories = [0 => 'No hay compañías'];
        }

        return view('dishes.new')->with([
            'companies' => $companies,
            'features' => $features,
            'categories' => $categories,
            'photo' => $photo = null,
            'dish' => new Dish()
        ]);
    }

    /**
     * Display form for create new dish
     * @param $companyId
     * @return $this
     */
    public function newByCompany(int $companyId) {

        $features = $this->featuresService->findAll();
        $categories = $this->categoryService->findAll();

        if($categories !== null)
        {
            $categories = $categories->pluck('name','id')->prepend('Elige una categoría',0);
        }
        else
        {
            $categories = [0 => 'No hay categorías'];
        }

        return view('companies.dishes.new')->with([

            'companyId' => $companyId,
            'features' => $features,
            'categories' => $categories,
            'photo' => $photo = null,
            'dish' => new Dish()
        ]);
    }

    /**
     * Display form for dish edition
     * @param int $dishId
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit(int $dishId) {

        $dish = $this->dishService->find($dishId);
        $companies = $this->companyService->findAll();
        $categories = $this->categoryService->findAll();
        $features = $this->featuresService->findAll();
        $current_features = $dish->features()->get();
        $offers = $dish->offers()->get();

        if($companies !== null)
        {
            $companies = $companies->pluck('name','id')->prepend('Elige una compañía',0);
        }
        else
        {
            $companies = [0 => 'No hay compañías'];
        }

        if($categories !== null)
        {
            $categories = $categories->pluck('name','id')->prepend('Elige una categoria',0);
        }
        else
        {
            $companies = [0 => 'No hay compañías'];
        }

        if ($dish !== null) {

            $photo = $this->photoService->find($dish->photo_id);

            return view('dishes.edit')->with([
                'carbon' => new Carbon(),
                'offers' => $offers,
                'object' => new Offer(),
                'route' => 'dishes',
                'folder' => 'offers.fields',
                'entity_id' => $dish->id,
                'dish' => $dish,
                'features' => $features,
                'categories' => $categories,
                'current_features' => $current_features,
                'companies' => $companies,
                'photo' => $photo
            ]);
        } else {
            Session::flash('message_error','El plato especificadaono ha sido encontrado o no existe');
            return redirect(route('dishes.index'));
        }
    }

    /**
     * Display form for dish edition
     * @param int $companyId
     * @param int $dishId
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function editByCompany(int $companyId, int $dishId) {

        $dish = $this->dishService->find($dishId);
        $categories = $this->categoryService->findAll();
        $features = $this->featuresService->findAll();
        $current_features = $dish->features()->get();
        $offers = $dish->offers()->get();

        if($categories !== null)
        {
            $categories = $categories->pluck('name','id')->prepend('Elige una categoria',0);
        }
        else
        {
            $categories = [0 => 'No hay categorías'];
        }



        if ($dish !== null) {

            $photo = $this->photoService->find($dish->photo_id);

            return view('companies.dishes.edit')->with([
                'carbon' => new Carbon(),
                'offers' => $offers,
                'object' => new Offer(),
                'route' => 'dishes',
                'folder' => 'offers.fields',
                'entity_id' => $dish->id,
                'dish' => $dish,
                'companyId' => $companyId,
                'features' => $features,
                'categories' => $categories,
                'current_features' => $current_features,
                'photo' => $photo
            ]);
        } else {
            Session::flash('message_error','El plato especificadaono ha sido encontrado o no existe');
            return redirect(route('companies.dishes.index',$companyId));
        }
    }

    /**
     * Create a new dish with input requested
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(Request $request) {

        $input = $request;
        $validator = Validator::make($request->all(), [
            'name' => 'filled',
            'price' => 'filled|numeric',
            'company_id' => 'filled|exists:mysql.company,id',
            //todo arreglar el filtro exists
            'category_id' => 'exists:mysql.category,id',
            'photo' => 'image|dimensions:max_width=500,max_height=500',
            'features' => 'exists:mysql.feature,id'
        ]);
        if ($validator->fails()) {
            Session::flash('message_error','Se produjo un error al crear el plato');
            return redirect()->to(route('dishes.create'))
                ->withInput($request->input())
                ->withErrors($validator->errors());
        } else {

            $dish = $this->dishService->create($input);

            if(count($input->features))
            {
                foreach ($input->features as $feature)
                {
                    $this->dishService->setFeature($dish->id,$feature);
                }
            }


            if($request->has('photo') && $request->input('photo') !== null)
            {

                $photo = new Photo() ;

                $photo->url = $request->file('photo')->store('public/photosDish');

                $photo->save();

                $dish->logo_id = $dish->id;

                $dish->save();
            }
            Session::flash('message','Plato creado correctamente');
            return redirect(route('dishes.index'));
        }
    }

    /**
     * Create a new dish with input requested
     * @param $companyId
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createByCompany(int $companyId, Request $request) {

        $request = ( $request->category_id ) ? $request : $request->except('category_id');

        $input = $request;
        $validator = Validator::make($request->all(), [
            'name' => 'filled',
            'price' => 'filled|numeric',
            'company_id' => 'filled|exists:mysql.company,id',
            //todo arreglar el filtro exists
            'category_id' => 'nullable|exists:mysql.category,id',
            'photo' => 'image|dimensions:max_width=500,max_height=500',
            'features' => 'exists:mysql.feature,id'
        ]);
        if ($validator->fails()) {
            Session::flash('message_error','Se produjo un error al crear el plato');
            return redirect()->to(route('companies.dishes.new',$companyId))
                ->withInput($request->input())
                ->withErrors($validator->errors());
        } else {

            $dish = $this->dishService->create($input);

            if(count($input->features))
            {
                foreach ($input->features as $feature)
                {
                    $this->dishService->setFeature($dish->id,$feature);
                }
            }

            if($request->has('photo') && $request->input('photo') !== null)
            {

                $photo = new Photo() ;

                $photo->url = $request->file('photo')->store('public/photosDish');

                $photo->save();

                $dish->logo_id = $dish->id;

                $dish->save();
            }
            Session::flash('message','Plato creado correctamente');
            return redirect(route('companies.dishes.index',$companyId));
        }
    }
    /**
     * Create a new offer with input requested
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createOffer(Request $request) {
        $input = $request;
        $validator = Validator::make($request->all(), [

            'finish_at' => 'min:'.$request->start_at.'| min:'.Carbon::now().'|nullable'

        ]);
        if ($validator->fails()) {

            Session::flash('message_error','Se produjo un error al crear la oferta');
            return redirect()->to(route('dishes.edit',$request->hidden_id))
                ->withInput($request->input())
                ->withErrors($validator->errors());
        } else {

            $offersToRemove = ($input->has('offers') ? $input->offers : [] );

            foreach ($offersToRemove as $offer)
            {
                $this->offerService->delete($offer);
            }
            if($input->start_at !== null)
            {
                $offer = $this->offerService->setDish($input->hidden_id,$input);
            }


            Session::flash('message','Oferta creada correctamente');
            return redirect(route('dishes.index'));
        }
    }

    /**
     * Create a new offer with input requested
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createOfferByCompany(int $companyId, Request $request) {
        $input = $request;
        $validator = Validator::make($request->all(), [

            //'finish_at' => 'min:'.$request->start_at.'| min:'.Carbon::now().'|nullable'

        ]);
        if ($validator->fails()) {

            Session::flash('message_error','Se produjo un error al crear la oferta');
            return redirect()->to(route('companies.dishes.edit',[$companyId,$input->hidden_id]))
                ->withInput($request->input())
                ->withErrors($validator->errors());
        } else {

            $offersToRemove = ($input->has('offers') ? $input->offers : [] );

            foreach ($offersToRemove as $offer)
            {
                $this->offerService->delete($offer);
            }
            if($input->start_at !== null)
            {
                $offer = $this->offerService->setDish($input->hidden_id,$input);
            }


            Session::flash('message','Oferta creada correctamente');
            return redirect(route('companies.dishes.index',[$companyId,$input->hidden_id]));
        }
    }

    /**
     * Update the specified dish with input requested
     * @param int $dishId
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(int $dishId, Request $request) {

        $dish = $this->dishService->find($dishId);

        if ($dish !== null) {
            $input = $request;

            $validator = Validator::make($request->all(), [
                'name' => 'filled',
                'price' => 'required',
                'start_at' => 'max:'.$input->finish_at,
                'type_value' => 'nullable|numeric|min:0',
                'company_id' => 'required'
            ]);
            if ($validator->fails()) {
                Session::flash('message_error','Se produjo un error al guardar el plato');
                return redirect()->to(route('dishes.edit', $dish->id))
                    ->withInput($request->input())
                    ->withErrors($validator->errors());
            } else {

                $this->dishService->update($dishId, $input);

                $featuresToRemove = $dish->features()->get()->all();

                foreach ($featuresToRemove as $feature)
                {
                    $feature->delete();
                }

                if(count($request->features))
                {
                    foreach ($request->features as $feature) {

                        $this->dishService->setFeature($dish->id,$feature);
                    }
                }

                $offersToRemove = ($input->has('offers') ? $input->offers : [] );

                foreach ($offersToRemove as $offer)
                {

                    $this->offerService->delete($offer);
                }

                if($input->start_at !== null && $input->type_value !== null)
                {

                    $this->offerService->setDish($input->hidden_id,$input);
                }

                if($request->has('photo') && $request->file('photo') !== null)
                {
                    $this->photoService->updateDishPhoto($dish,$request);
                }

                Session::flash('message','Plato guardado correctamente');

                return redirect(route('dishes.index'));

            }
        } else {
            Session::flash('message_error','El plato  especificado no ha sido encontrado o no existe');
            return redirect(route('dishes.index'));
        }
    }

    /**
     * Update the specified dish with input requested
     * @param int $companyId
     * @param int $dishId
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateByCompany(int $companyId, int $dishId, Request $request) {

        $dish = $this->dishService->find($dishId);

        if ($dish !== null) {
            $input = $request;

            $validator = Validator::make($request->all(), [
                'name' => 'filled',
                'price' => 'required',
                'start_at' => 'max:'.$input->finish_at,
                'type_value' => 'nullable|numeric|min:0'
            ]);
            if ($validator->fails()) {
                Session::flash('message_error','Se produjo un error al guardar el plato');
                return redirect()->to(route('companies.dishes.edit', [$companyId, $dish->id]))
                    ->withInput($request->input())
                    ->withErrors($validator->errors());
            } else {

                $this->dishService->update($dishId, $input);


                $featuresToRemove = $dish->features()->get()->all();

                foreach ($featuresToRemove as $feature)
                {
                    $feature->delete();
                }

                if(count($request->features))
                {
                    foreach ($request->features as $feature) {

                        $this->dishService->setFeature($dish->id,$feature);
                    }
                }

                $offersToRemove = ($input->has('offers') ? $input->offers : [] );

                foreach ($offersToRemove as $offer)
                {

                    $this->offerService->delete($offer);
                }

                if($input->start_at !== null && $input->type_value !== null)
                {

                    $this->offerService->setDish($input->hidden_id,$input);
                }

                if($request->has('photo') && $request->file('photo') !== null)
                {
                    $this->photoService->updateDishPhoto($dish,$request);
                }

                Session::flash('message','Plato guardado correctamente');

                return redirect(route('companies.dishes.index',$companyId));

            }
        } else {
            Session::flash('message_error','El plato  especificado no ha sido encontrado o no existe');
            return redirect(route('companies.dishes.updated',[$companyId,$dishId]));
        }
    }

    /**
     * Delete the specified company
     * @param int $dishId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete(int $dishId) {


        $dish = $this->dishService->find($dishId);

        if ($dish !== null) {
            $this->dishService->delete($dishId);
            Session::flash('message','Plato borrado correctamente');
            return redirect(route('dishes.index'));
        } else {
            Session::flash('message_error','El plato especificado no ha sido encontrado o no existe');
            return redirect(route('dishes.index'));
        }
    }

    /**
     * Delete the specified company
     * @param int $companyId
     * @param int $dishId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteByCompany(int $companyId, int $dishId) {
        $dish = $this->dishService->find($dishId);

        if ($dish !== null) {
            $this->dishService->delete($dishId);
            Session::flash('message','Plato borrado correctamente');
            return redirect(route('companies.dishes.index',$companyId));
        } else {
            Session::flash('message_error','El plato especificado no ha sido encontrado o no existe');
            return redirect(route('companies.dishes.index',$companyId));
        }
    }

    /**
     * Process datatables ajax request.
     * @return \Illuminate\Http\JsonResponse
     */
    public function datatable(Request $request)
    {

        if($request->has('company'))
        {
            $dishes = $this->dishService->findAllByCompany($request->company);
        }
        else
            if($request->has('company_global'))
            {
                $dishes = $this->dishService->findAll();
            }

         $dishes = ( $dishes !== null ) ? $dishes : collect();

        return DataTables::collection($dishes)
            ->filter(function ($instance) use ($request) {

                $search = $request->has('search') ? Str::lower($request->get('search')['value']) : null;
                $features      = $request->has('features') ? explode('|', $request->get('features')) : null;
                $company      = $request->has('company_global') ? $request->get('company_global') : null;
                $categories      = $request->has('category') ? $request->get('category') : null;

                if ($search !== null && $search !== "") {
                    $instance->collection = $instance->collection->filter(function ($row) use ($search) {
                        $str_name = Str::lower($row['name']);
                        return Str::contains($str_name, $search) ? true : false;
                    });
                }

                if ($features !== null && count($features) > 0 && $features[0] !== "" && $features[0] != 0) {

                    $instance->collection = $instance->collection->filter(function ($dish) use ($features) {
                        return  $this->dishService->hasFeatureDish($dish['id'], $features);

                    });
                }

                if ($company !== null && count($company) > 0 && $company !== "" && $company != 0) {

                    $instance->collection = $instance->collection->filter(function ($dish) use ($company) {
                        return  ($this->dishService->findByCompany($company , $dish['id'] ) !== null) ? true : false ;

                    });
                }
            })
            ->addColumn('company_id', function($dish) {

                $company = $dish->company()->get()->first()->name;
                return ($company !== null) ? $company : [ 0 =>'No existe compañía' ];


            })
            ->addColumn('category_id', function($dish) {

                $category = $dish->category()->get()->first();
                return ($category !== null) ? [$category->name] : [ 0 =>'No existe categoría' ];

            })
            ->addColumn('score', function($dish) {

                /*if(count($dish))
                {
                    $scores = $dish->scores()->get();

                    if(count($scores))
                    {
                        $scores = $scores->pluck('score')->all();

                        $svgScore = array_sum($scores)/count($scores);

                        return [$svgScore.'/10 ('.count($scores).' votes)'];
                    }
                }*/

                if(count($dish))
                {
                    $scores = $dish->scores()->get();

                    if(count($scores))
                    {
                        $scores = $scores->pluck('score')->all();

                        $svgScore = array_sum($scores)/count($scores);

                        $stars = substr($svgScore,0,1);

                        $rest = substr($svgScore,2,1);

                        $print = array();

                        for($i = 0 ; $i < $stars; $i++)
                        {
                            $print[]="<i class='fa fa-star' style='color:gold;'></i>";
                        }

                        if($rest > 5 )
                        {
                            $print[]="<i class='fa fa-star' style='color:gold;'></i>";
                        }
                        else
                            if($rest <=5 && $rest)
                            {
                                $print[]="<i class='fa fa-star-half-o' style='color:gold;'></i>";
                            }

                        $c = count($print);

                        for($i = 0 ; $i < (5 - $c); $i++ )
                        {
                            $print[] = "<i class='fa fa-star-o' style='color:gold;'></i>";
                        }

                        return implode(" ",$print);
                    }


                }

                return [0 => 'No hay valoraciones'];


            })
            ->addColumn('photo_id', function($dish) {

                $photoId = $dish->photo()->get([0 => 'id']);

                if(count($photoId))
                {
                    $photoId = $dish->photo()->get([0 => 'id'])[0];
                }
                $photo = $this->photoService->find($photoId);

                if($photo !== null)
                {

                    $url = (str_contains($photo[0]->url, 'http')) ? $photo[0]->url : asset('storage'.str_after($photo[0]->url, 'public'));

                    return '<div><img class="img-thumbnail" style="width:40px" src="'.$url.'"></div>';
                }

                $url = asset('storage/photos/default/no-photo.png');

                return '<div><img class="img-thumbnail" style="width:40px" src="'.$url.'"></div>';

            })
            ->addColumn('action', function ($dish) use($request) {

                $user = Auth::user();

                if($user->hasRole('superadmin') && $request->has('company_global'))
                {
                    return '<div class="btn-group">
                                <a href="'.route('dishes.edit', $dish->id ).'" class="btn btn-xs btn-primary"><i class="fa fa-fw fa-edit"></i></a>
                                <a href="'.route('dishes.delete', $dish->id ).'" class="btn btn-xs btn-danger" data-remove="¿Está seguro de que quiere borrar este plato?"><i class="fa fa-fw fa-trash"></i></a>
                            </div>';
                }
                $companyId = $dish->company()->get([0 => 'id'])[0];

                return '<div class="btn-group">
                                <a href="'.route('companies.dishes.edit', [$companyId, $dish->id]).'" class="btn btn-xs btn-primary"><i class="fa fa-fw fa-edit"></i></a>
                                <a href="'.route('companies.dishes.delete', [$companyId, $dish->id]).'" class="btn btn-xs btn-danger" data-remove="¿Está seguro de que quiere borrar este plato?"><i class="fa fa-fw fa-trash"></i></a>
                            </div>';

            })
            ->rawColumns(['action','score','photo_id'])
            ->setRowId('{{$id}}')
            ->make(true);
    }

    public static function routes() {
        Route::get(     '/dishes',                                           'Web\DishController@index'                        )->name('dishes.index'                     )->middleware('auth');
        Route::get(     '/dishes/new',                                       'Web\DishController@new'                          )->name('dishes.new'                       )->middleware('auth');
        Route::post(    '/dishes/new',                                       'Web\DishController@create'                       )->name('dishes.create'                    )->middleware('auth');
        Route::post(    '/dishes/newOffer',                                  'Web\DishController@createOffer'                  )->name('dishes.createOffer'               )->middleware('auth');
        Route::get(     '/dishes/{dishId}/edit',                             'Web\DishController@edit'                         )->name('dishes.edit'                      )->middleware('auth');
        Route::put(     '/dishes/{dishId}/edit',                             'Web\DishController@update'                       )->name('dishes.update'                    )->middleware('auth');
        Route::delete(  '/dishes/{dishId}/delete',                           'Web\DishController@delete'                       )->name('dishes.delete'                    )->middleware('auth');
        Route::get(     '/dishes/datatable',                                 'Web\DishController@datatable'                    )->name('dishes.datatable'                 )->middleware('auth');

        Route::post(    '/companies/{companyId}/dishes/newOfferByCompany',   'Web\DishController@createOfferByCompany'         )->name('companies.dishes.createOffer'      )->middleware('auth');
        Route::get(     '/companies/{companyId}/dishes/{dishId}/edit',       'Web\DishController@editByCompany'                )->name('companies.dishes.edit'             )->middleware('auth');

        Route::get(     '/companies/{companyId}/dishes',                     'Web\DishController@indexFilteredByCompany'       )->name('companies.dishes.index'             )->middleware('auth');
        Route::get(     '/companies/{companyId}/dishes/new',                 'Web\DishController@newByCompany'                 )->name('companies.dishes.new'              )->middleware('auth');
        Route::post(    '/companies/{companyId}/dishes/new',                 'Web\DishController@createByCompany'              )->name('companies.dishes.create'           )->middleware('auth');
        Route::put(     '/companies/{companyId}/dishes/{dishId}/edit',       'Web\DishController@updateByCompany'              )->name('companies.dishes.update'           )->middleware('auth');
        Route::delete(  '/companies/{companyId}/dishes/{dishId}/delete',     'Web\DishController@deleteByCompany'              )->name('companies.dishes.delete'           )->middleware('auth');
    }
}
