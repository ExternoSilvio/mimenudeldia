<?php

namespace App\Http\Controllers\Web;

use App\Province;
use App\Services\ProvinceService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Route;
use Yajra\DataTables\Facades\DataTables;

class ProvinceController extends Controller
{
    //

    private $provinceService;
    /**
     * ProvincesController constructor.
     * @param ProvinceService $provinceService
     *
     */
    public function __construct(ProvinceService $provinceService)
    {
        $this->provinceService = $provinceService;
    }

    /**
     * Displays datatables front end view
     * @return \Illuminate\View\View
     */
    public function index(){

        return view('provinces.index');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function profile() {
        return redirect(route('provinces.edit', Auth::id()));
    }

    /**
     * Display form for create new user
     * @return $this
     */
    public function new() {
        return view('provinces.new')->with([
            'province' => new Province()
        ]);
    }

    /**
     * Display form for province edition
     * @param int $provinceId
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit(int $provinceId) {

        $province = $this->provinceService->find($provinceId);

        if ($province !== null) {
            return view('provinces.edit')->with([
                'province' => $province,

            ]);
        } else {
            Session::flash('message_error','La provincia especificada no ha sido encontrada o no existe');
            return redirect(route('provinces.index'));
        }
    }

    /**
     * Create a new user with input requested
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(Request $request) {
        $input = $request;
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:province',
        ]);
        if ($validator->fails()) {
            Session::flash('message_error','Se produjo un error al crear la provincia');
            return redirect()->to(route('provinces.create'))
                ->withInput($request->input())
                ->withErrors($validator->errors());
        } else {
            $province = $this->provinceService->create($input);

            Session::flash('message','Provincia creada correctamente');
            return redirect(route('provinces.index', $province->id));
        }
    }

    /**
     * Update the specified user with input requested
     * @param int $provinceId
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(int $provinceId, Request $request) {
        $province = $this->provinceService->find($provinceId);
        if ($province !== null) {
            $input = $request;

            $validator = Validator::make($request->all(), [
                'name' => 'required|unique:province',
            ]);
            if ($validator->fails()) {
                Session::flash('message_error','Se produjo un error al guardar la provincia');
                return redirect()->to(route('provinces.edit', $province->id))
                    ->withInput($request->input())
                    ->withErrors($validator->errors());
            } else {

                 $this->provinceService->update($provinceId, $input);

                 Session::flash('message','Provincia guardada correctamente');

                 return redirect(route('provinces.index'));

            }
        } else {
            Session::flash('message_error','La provincia especificada no ha sido encontrada o no existe');
            return redirect(route('provinces.index'));
        }
    }

    /**
     * Delete the specified user
     * @param int $provinceId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete(int $provinceId) {

        $province = $this->provinceService->find($provinceId);

        if ($province !== null) {
            $this->provinceService->delete($provinceId);
            Session::flash('message','Provincia borrada correctamente');
            return redirect(route('provinces.index'));
        } else {
            Session::flash('message_error','La provincia especificada no ha sido encontrada o no existe');
            return redirect(route('provinces.index'));
        }
    }

    /**
     * Process datatables ajax request.
     * @return \Illuminate\Http\JsonResponse
     */
    public function datatable(Request $request)
    {
        $provinces = $this->provinceService->findAll();
        return DataTables::collection($provinces)
            ->filter(function ($instance) use ($request) {
                $search = $request->has('search') ? Str::lower($request->get('search')['value']) : null;
                if ($search !== null && $search !== "") {
                    $instance->collection = $instance->collection->filter(function ($row) use ($search) {
                        $str_name = Str::lower($row['name']);
                        return Str::contains($str_name, $search) ? true : false;
                    });
                }
            })
            ->addColumn('action', function ($province) {
                    return '<div class="btn-group">
                                <a href="'.route('provinces.edit', $province->id).'" class="btn btn-xs btn-primary"><i class="fa fa-fw fa-edit"></i></a>
                                <a href="'.route('provinces.delete', $province->id).'" class="btn btn-xs btn-danger" data-remove="¿Está seguro de que quiere borrar este usuario?"><i class="fa fa-fw fa-trash"></i></a>
                            </div>';

            })
            ->rawColumns(['action'])
            ->setRowId('{{$id}}')
            ->make(true);
    }

    public static function routes() {
        Route::get(     '/provinces',                       'Web\ProvinceController@index'       )->name('provinces.index'    )->middleware('auth');
        Route::get(     '/provinces/profile',               'Web\ProvinceController@profile'     )->name('provinces.profile'  )->middleware('auth');
        Route::get(     '/provinces/new',                   'Web\ProvinceController@new'         )->name('provinces.new'      )->middleware('auth');
        Route::post(    '/provinces/new',                   'Web\ProvinceController@create'      )->name('provinces.create'   )->middleware('auth');
        Route::get(     '/provinces/{provinceId}/edit',     'Web\ProvinceController@edit'        )->name('provinces.edit'     )->middleware('auth');
        Route::put(     '/provinces/{provinceId}/edit',     'Web\ProvinceController@update'      )->name('provinces.update'   )->middleware('auth');
        Route::delete(  '/provinces/{provinceId}/delete',   'Web\ProvinceController@delete'      )->name('provinces.delete'   )->middleware('auth');
        Route::get(     '/provinces/datatable',             'Web\ProvinceController@datatable'   )->name('provinces.datatable')->middleware('auth');
    }
}
