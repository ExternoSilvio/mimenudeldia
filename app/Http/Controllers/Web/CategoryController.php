<?php

namespace App\Http\Controllers\Web;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Services\CategoryService;
use Illuminate\Support\Facades\Session;
use Validator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Route;
use Yajra\DataTables\Facades\DataTables;

class CategoryController extends Controller
{
    //

    private $categoryService;

    /**
     * CategoryController constructor.
     * @param categoryService $categoryService
     *
     */
    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * Displays datatables front end view
     * @return \Illuminate\View\View
     */
    public function index(){

        $categories = $this->categoryService->findAll();

        if($categories !== null)
        {
            $categories = $categories->pluck('name','id')->prepend('Elige una categoría', 0);
        }
        else
        {
            $categories = [0 => 'No hay categorías'];
        }

        return view('categories.index')->with([
            'categories' => $categories
        ]);
    }

    /**
     * Display form for create new category
     * @return $this
     */
    public function new() {

        $categories = $this->categoryService->findAll();

        if($categories !== null)
        {
            $categories = $categories->pluck('name','id')->prepend('Todas las categorías', 0);
        }
        else
        {
            $categories = 'No hay categorías';
        }

        return view('categories.new')->with([
            'categories' => $categories,
            'category' => new Category()
        ]);
    }

    /**
     * Display form for category edition
     * @param int $categoryId
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit(int $categoryId) {

        $categories = $this->categoryService->findAll();

        if($categories !== null)
        {
            $categories = $categories->pluck('name','id')->prepend('Elige una categoría', 0);
        }
        else
        {
            $categories = 'No hay categorías';
        }

        $category = $this->categoryService->find($categoryId);

        if ($category !== null) {
            return view('categories.edit')->with([
                'category' => $category,
                'categories' => $categories,
            ]);
        } else {
            Session::flash('message_error','La categoría especificada no ha sido encontrada o no existe');
            return redirect(route('categories.index'));
        }
    }

    /**
     * Create a new category with input requested
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(Request $request) {
        $input = $request;
        $validator = Validator::make($request->all(), [
            'name' => 'filled',
            'categories' => 'filled|exists:mysql.category,id'

        ]);
        if ($validator->fails()) {
            Session::flash('message_error','Se produjo un error al crear la categoría');
            return redirect()->to(route('categories.create'))
                ->withInput($request->input())
                ->withErrors($validator->errors());
        } else {

            $category = $this->categoryService->create($input);

            Session::flash('message','Categoría creada correctamente');
            return redirect(route('categories.index'));
        }
    }

    /**
     * Update the specified category with input requested
     * @param int $categoryId
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(int $categoryId, Request $request) {

        $category = $this->categoryService->find($categoryId);

        $same = ($request->has('category_id') && $request->category_id !== $categoryId) ? true : false;

        if ($category !== null && $same) {
            $input = $request;

            $validator = Validator::make($request->all(), [
                'name' => 'filled',
                'categories' => 'filled|exists:mysql.category,id'
            ]);
            if ($validator->fails()) {
                Session::flash('message_error','Se produjo un error al guardar la categoría');
                return redirect()->to(route('categories.edit', $category->id))
                    ->withInput($request->input())
                    ->withErrors($validator->errors());
            } else {

                $this->categoryService->update($categoryId, $input);

                Session::flash('message','Categoría guardada correctamente');

                return redirect(route('categories.index'));

            }
        } else {
            Session::flash('message_error','La categoría especificada no ha sido encontrada o no existe');
            return redirect(route('categories.index'));
        }
    }

    /**
     * Delete the specified category
     * @param int $categoryId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete(int $categoryId) {


        $location = $this->categoryService->find($categoryId);

        if ($location !== null) {
            $this->categoryService->delete($categoryId);
            Session::flash('message','Categoría borrada correctamente');
            return redirect(route('categories.index'));
        } else {
            Session::flash('message_error','La categoría especificada no ha sido encontrada o no existe');
            return redirect(route('categories.index'));
        }
    }

    /**
     * Process datatables ajax request.
     * @return \Illuminate\Http\JsonResponse
     */
    public function datatable(Request $request)
    {

        $categories = $this->categoryService->findAll();

        $categories = ($categories !== null) ? $categories : collect();

        return DataTables::collection($categories)
            ->filter(function ($instance) use ($request) {

                $search = $request->has('search') ? Str::lower($request->get('search')['value']) : null;
                $category_id  = $request->has('category_id') ? intval($request->get('category_id')) : null;

                if ($search !== null && $search !== "") {
                    $instance->collection = $instance->collection->filter(function ($row) use ($search) {
                        $str_name = Str::lower($row['name']);
                        return Str::contains($str_name, $search) ? true : false;
                    });
                }

                if ($category_id !== null && $category_id > 0) {
                    $instance->collection = $instance->collection->filter(function ($categories) use ($category_id) {
                        $category = intval($categories['category_id']);
                        return ($category === $category_id);
                    });
                }
            })
            ->addColumn('category', function($category) {
                $category = $category->category()->get();
                return ($category->count() > 0) ? $category->first()->name : 'Principal';
//                try
//                {
//                   return  $category = $category->category()->get()->first()->name;
//
//                }catch (\Exception $e){
//
//                    return 'Categoría eliminada';
//                }

            })
            ->addColumn('action', function ($category) {
                return '<div class="btn-group">
                                <a href="'.route('categories.edit', $category->id).'" class="btn btn-xs btn-primary"><i class="fa fa-fw fa-edit"></i></a>
                                <a href="'.route('categories.delete', $category->id).'" class="btn btn-xs btn-danger" data-remove="¿Está seguro de que quiere borrar esta categoría?"><i class="fa fa-fw fa-trash"></i></a>
                            </div>';

            })
            ->rawColumns(['action'])
            ->setRowId('{{$id}}')
            ->make(true);
    }

    public static function routes() {
        Route::get(     '/categories',                   'Web\CategoryController@index'       )->name('categories.index'    )->middleware('auth');
        Route::get(     '/categories/new',               'Web\CategoryController@new'         )->name('categories.new'      )->middleware('auth');
        Route::post(    '/categories/new',               'Web\CategoryController@create'      )->name('categories.create'   )->middleware('auth');
        Route::get(     '/categories/{categoryId}/edit',     'Web\CategoryController@edit'    )->name('categories.edit'     )->middleware('auth');
        Route::put(     '/categories/{categoryId}/edit',     'Web\CategoryController@update'  )->name('categories.update'   )->middleware('auth');
        Route::delete(  '/categories/{categoryId}/delete',   'Web\CategoryController@delete'  )->name('categories.delete'   )->middleware('auth');
        Route::get(     '/categories/datatable',         'Web\CategoryController@datatable'   )->name('categories.datatable')->middleware('auth');
    }
}
