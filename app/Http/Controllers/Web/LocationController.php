<?php

namespace App\Http\Controllers\Web;


    use App\Location;
    use App\Province;
    use App\Services\LocationService;
    use App\Services\ProvinceService;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\Session;
    use Validator;
    use Illuminate\Support\Carbon;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Str;
    use Route;
    use Yajra\DataTables\Facades\DataTables;

class LocationController extends Controller
{
    //

    private $provinceService;
    private $locationService;

    /**
     * ProvincesController constructor.
     * @param LocationService $locationService
     * @param ProvinceService $provinceService
     *
     */
    public function __construct(LocationService $locationService, ProvinceService $provinceService)
    {
        $this->locationService = $locationService;
        $this->provinceService = $provinceService;
    }

    /**
     * Displays datatables front end view
     * @return \Illuminate\View\View
     */
    public function index(){

        $provinces = $this->provinceService->findAll()->pluck('name', 'id')->prepend('Todas las provincias', 0);

        return view('locations.index')->with([
            'provinces' => $provinces
        ]);
    }

    /**
     * Displays datatables front end view
     * @return \Illuminate\View\View
     */
    public function indexFilteredByProvince(int $provinceId){

        $provinces = $this->provinceService->findAll()->pluck('name', 'id')->prepend('Filtrar por provincia',0);

        return view('provinces.locations.index')->with([
            'provinces' => $provinces,
            'provinceId' => $provinceId
        ]);
    }
    /**
     * Display form for create new user
     * @return $this
     */
    public function new() {
        $provinces = $this->provinceService->findAll()->pluck('name', 'id');;
        return view('locations.new')->with([
            'provinces' => $provinces,
            'location' => new Location()
        ]);
    }

    /**
     * Display form for create new user
     * @param $provinceId
     * @return $this
     */
    public function newByProvince(int $provinceId) {

        $provinces = $this->provinceService->findAll()->pluck('name', 'id');
        $province = $this->provinceService->find($provinceId);

        return view('provinces.locations.new')->with([
            'provinces' => $provinces,
            'location' => new Location(),
            'province' => $province
        ]);
    }

    /**
     * Display form for user edition
     * @param int $locationId
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit(int $locationId) {
        $provinces = $this->provinceService->findAll()->pluck('name', 'id');;
        $location = $this->locationService->find($locationId);

        if ($location !== null) {
            return view('locations.edit')->with([
                'location' => $location,
                'provinces' => $provinces,
            ]);
        } else {
            Session::flash('message_error','La localidad especificada no ha sido encontrada o no existe');
            return redirect(route('locations.index'));
        }
    }

    /**
     * Display form for location edition
     * @param int $provinceId
     * @param int $locationId
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function editByProvince(int $provinceId, int $locationId) {
        $provinces = $this->provinceService->findAll()->pluck('name', 'id');;
        $location = $this->locationService->find($locationId);

        if ($location !== null) {
            return view('provinces.locations.edit')->with([
                'location' => $location,
                'provinces' => $provinces,
                'provinceId' => $provinceId
            ]);
        } else {
            Session::flash('message_error','La localidad especificada no ha sido encontrada o no existe');
            return redirect(route('provinces.locations.index',$provinceId));
        }
    }

    /**
     * Create a new user with input requested
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(Request $request) {
        $input = $request;
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:location',
            'zip' => 'required|string|min:5|max:5',
            'province_id' => 'required|exists:province,id'
        ]);
        if ($validator->fails()) {
            Session::flash('message_error','Se produjo un error al crear la localidad');
            return redirect()->to(route('locations.create'))
                ->withInput($request->input())
                ->withErrors($validator->errors());
        } else {
            $location = $this->locationService->create($input['province_id'], $input);

            Session::flash('message','Localidad creada correctamente');
            return redirect(route('locations.index'));
        }
    }

    /**
     * Create a new user with input requested
     * @param int $provinceId
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createByProvince(int $provinceId, Request $request) {
        $input = $request;
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:location',
            'zip' => 'required|string|min:5|max:5',
            'province_id' => 'required|exists:province,id'
        ]);
        if ($validator->fails()) {
            Session::flash('message_error','Se produjo un error al crear la localidad');
            return redirect()->to(route('provinces.locations.create',$provinceId))
                ->withInput($request->input())
                ->withErrors($validator->errors());
        } else {
            $location = $this->locationService->create($input['province_id'], $input);

            Session::flash('message','Localidad creada correctamente');
            return redirect(route('provinces.locations.index',$provinceId));
        }
    }

    /**
     * Update the specified user with input requested
     * @param int $locationId
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(int $locationId, Request $request) {
        $location = $this->locationService->find($locationId);

        if ($location !== null) {
            $input = $request;

            $validator = Validator::make($request->all(), [
                'name' => 'filled|unique:province',
                'zip' => 'filled|string|min:5|max:5',
                'province_id' => 'filled|exists:province,id'
            ]);
            if ($validator->fails()) {
                Session::flash('message_error','Se produjo un error al guardar la localidad');
                return redirect()->to(route('locations.edit', $location->id))
                    ->withInput($request->input())
                    ->withErrors($validator->errors());
            } else {

                $this->locationService->update($input->province_id, $locationId, $input);

                Session::flash('message','Localidad guardada correctamente');

                return redirect(route('locations.index'));

            }
        } else {
            Session::flash('message_error','La localidad especificada no ha sido encontrada o no existe');
            return redirect(route('locations.index'));
        }
    }

    /**
     * Update the specified user with input requested
     * @param int $companyId
     * @param int $locationId
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateByProvince(int $provinceId, int $locationId, Request $request) {
        $location = $this->locationService->find($locationId);

        if ($location !== null) {
            $input = $request;

            $validator = Validator::make($request->all(), [
                'name' => 'filled|unique:province',
                'zip' => 'filled|string|min:5|max:5',
                'province_id' => 'filled|exists:province,id'
            ]);
            if ($validator->fails()) {
                Session::flash('message_error','Se produjo un error al guardar la localidad');
                return redirect()->to(route('provinces.locations.edit', [$provinceId, $location->id]))
                    ->withInput($request->input())
                    ->withErrors($validator->errors());
            } else {

                $this->locationService->update($input->province_id, $locationId, $input);

                Session::flash('message','Localidad guardada correctamente');

                return redirect(route('provinces.locations.index',[$provinceId]));

            }
        } else {
            Session::flash('message_error','La localidad especificada no ha sido encontrada o no existe');
            return redirect(route('provinces.locations.index'));
        }
    }
    /**
     * Delete the specified user
     * @param int $locationId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete(int $locationId) {


        $location = $this->locationService->find($locationId);

        if ($location !== null) {
            $this->locationService->delete($locationId);
            Session::flash('message','Localidad borrada correctamente');
            return redirect(route('locations.index'));
        } else {
            Session::flash('message_error','La localidad especificada no ha sido encontrada o no existe');
            return redirect(route('locations.index'));
        }
    }

    /**
     * Delete the specified user
     * @param int $provinceId
     * @param int $locationId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteByProvince(int $provinceId, int $locationId) {

        $location = $this->locationService->find($locationId);

        if ($location !== null) {
            $this->locationService->delete($locationId);
            Session::flash('message','Localidad borrada correctamente');
            return redirect(route('provinces.ocations.index',$provinceId));
        } else {
            Session::flash('message_error','La localidad especificada no ha sido encontrada o no existe');
            return redirect(route('provinces.locations.index',$provinceId));
        }
    }

    /**
     * Process datatables ajax request.
     * @return \Illuminate\Http\JsonResponse
     */
    public function datatable(Request $request)
    {


        if($request->has('province_global'))
        {
            $locations = $this->locationService->findAll();
        }
        else
            if($request->has('province_id'))
            {
                $locations = $this->locationService->findAllByProvince($request->province_id);

            }

        $locations = ($locations !== null) ? $locations : collect() ;


        return DataTables::collection($locations)
            ->filter(function ($instance) use ($request) {

                $search = $request->has('search') ? Str::lower($request->get('search')['value']) : null;
                $province_id  = $request->has('province_global') ? intval($request->get('province_global')) : null;

                if ($search !== null && $search !== "") {
                    $instance->collection = $instance->collection->filter(function ($row) use ($search) {
                        $str_name = Str::lower($row['name']);
                        return Str::contains($str_name, $search) ? true : false;
                    });
                }

                if ($province_id !== null && $province_id > 0) {
                    $instance->collection = $instance->collection->filter(function ($locations) use ($province_id) {
                        $province = intval($locations['province_id']);
                        return ($province === $province_id);
                    });
                }
            })
            ->addColumn('province', function($location) {
                return ($location->province() !== null) ? $location->province()->get()->first()->name : 'Provincia eliminada';
            },1)
            ->addColumn('action', function ($location) use ($request) {

                $user = Auth::user();

                $provinceId = $location->province()->get([0 => 'id'])[0];

                if($user->hasRole('superadmin') && $request->has('province_global'))
                {
                    return '<div class="btn-group">
                               <a href="'.route('locations.edit', $location->id).'" class="btn btn-xs btn-primary"><i class="fa fa-fw fa-edit"></i></a>
                               <a href="'.route('locations.delete', $location->id).'" class="btn btn-xs btn-danger" data-remove="¿Está seguro de que quiere borrar esta localidad?"><i class="fa fa-fw fa-trash"></i></a>
                        </div>';
                }
                return '<div class="btn-group">
                               <a href="'.route('provinces.locations.edit', [$provinceId,$location->id]).'" class="btn btn-xs btn-primary"><i class="fa fa-fw fa-edit"></i></a>
                               <a href="'.route('provinces.locations.delete', [$provinceId, $location->id]).'" class="btn btn-xs btn-danger" data-remove="¿Está seguro de que quiere borrar esta localidad?"><i class="fa fa-fw fa-trash"></i></a>
                        </div>';



            })
            ->rawColumns(['action'])
            ->setRowId('{{$id}}')
            ->make(true);
    }

    public static function routes() {
        Route::get(     '/locations',                                               'Web\LocationController@index'                       )->name('locations.index'                      )->middleware('auth');
        Route::get(     '/provinces/{provinceId}/locations',                        'Web\LocationController@indexFilteredByProvince'     )->name('provinces.locations.index'            )->middleware('auth');
        Route::get(     '/locations/new',                                           'Web\LocationController@new'                         )->name('locations.new'                        )->middleware('auth');
        Route::get(     '/provinces/{provinceId}/locations/new',                    'Web\LocationController@newByProvince'               )->name('provinces.locations.new'              )->middleware('auth');
        Route::post(    '/locations/new',                                           'Web\LocationController@create'                      )->name('locations.create'                     )->middleware('auth');
        Route::post(    '/provinces/{provinceId}/locations/new',                    'Web\LocationController@createByProvince'            )->name('provinces.locations.create'           )->middleware('auth');
        Route::get(     '/locations/{locationId}/edit',                             'Web\LocationController@edit'                        )->name('locations.edit'                       )->middleware('auth');
        Route::get(     '/province/{provinceId}/locations/{locationId}/edit',       'Web\LocationController@editByProvince'              )->name('provinces.locations.edit'             )->middleware('auth');
        Route::put(     '/locations/{locationId}/edit',                             'Web\LocationController@update'                      )->name('locations.update'                     )->middleware('auth');
        Route::put(     '/provinces/{provinceId}/locations/{locationId}/edit',      'Web\LocationController@updateByProvince'            )->name('provinces.locations.update'           )->middleware('auth');
        Route::delete(  '/locations/{locationId}/delete',                           'Web\LocationController@delete'                      )->name('locations.delete'                     )->middleware('auth');
        Route::delete(  '/provinces/{provinceId}/locations/{locationId}/delete',    'Web\LocationController@deleteByProvince'            )->name('provinces.locations.delete'           )->middleware('auth');
        Route::get(     '/locations/datatable',                                     'Web\LocationController@datatable'                   )->name('locations.datatable'                  )->middleware('auth');
    }
}

