<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


    use App\Location;
    use App\Subsidiary;
    use App\Company;
    use App\Services\SubsidiaryService;
    use App\Services\LocationService;
    use App\Services\CompanyService;
    use App\Services\ScheduleService;
use Illuminate\Support\Facades\Session;
use Validator;
    use Illuminate\Support\Carbon;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Str;
    use Route;
    use Yajra\DataTables\Facades\DataTables;

class SubsidiaryController extends Controller
{
    //

    private $subsidiaryService;
    private $companyService;
    private $locationService;
    private $scheduleService;

    /**
     * SubsidiaryController constructor.
     * @param LocationService $locationService
     * @param CompanyService $companyService
     * @param SubsidiaryService $subsidiaryService
     * @param ScheduleService $scheduleService
     *
     */
    public function __construct(LocationService $locationService, CompanyService $companyService, SubsidiaryService $subsidiaryService, ScheduleService $scheduleService)
    {
        $this->locationService = $locationService;
        $this->companyService = $companyService;
        $this->subsidiaryService = $subsidiaryService;
        $this->scheduleService = $scheduleService;
    }

    /**
     * Displays datatables front end view
     * @return \Illuminate\View\View
     */
    public function index(){
        $locations = $this->locationService->findAll();
        $companies = $this->companyService->findAll();

        if($locations !== null)
        {
            $locations = $locations->pluck('name', 'id')->prepend('Todas las localizaciones',0);
        }
        else
        {
            $locations = [0 => 'No hay sedes'];
        }
        if($companies !== null)
        {
            $companies = $companies->pluck('name','id')->prepend('Todas las compañias',0);
        }
        else
        {
            $companies = [0 => 'No hay compañias'];
        }
        return view('subsidiaries.index')->with([
            'locations' => $locations,
            'companies' => $companies
        ]);
    }

    /**
     * Displays datatables front end view
     * @param $companyId
     * @return \Illuminate\View\View
     */
    public function indexFilteredByCompany($companyId){
        $locations = $this->locationService->findAll();

        if($locations !== null)
        {
            $locations = $locations->pluck('name', 'id')->prepend('Todas las localizaciones',0);
        }
        else
        {
            $locations = [0 => 'No hay sedes'];
        }
        return view('companies.subsidiaries.index')->with([
            'locations' => $locations,
            'companyId' => $companyId
        ]);
    }

    /**
     * Display form for create new user
     * @return $this
     */
    public function new() {
        $locations = $this->locationService->findAll();
        $companies = $this->companyService->findAll();

        if($locations !== null)
        {
            $locations = $locations->pluck('name', 'id')->prepend('Elige una localización',0);
        }
        else
        {
            $locations = [0 => 'No hay sedes'];
        }
        if($companies !== null)
        {
            $companies = $companies->pluck('name','id')->prepend('Elige una compañia',0);
        }
        else
        {
            $companies = [0 => 'No hay compañias'];
        }

        return view('subsidiaries.new')->with([
            'locations' => $locations,
            'companies' => $companies,
            'subsidiary' => new Subsidiary()
        ]);
    }

    /**
     * Display form for create new user
     * @param $companyId
     * @return $this
     */
    public function newByCompany(int $companyId) {
        $locations = $this->locationService->findAll();
        $companies = $this->companyService->findAll();

        if($locations !== null)
        {
            $locations = $locations->pluck('name', 'id')->prepend('Elige una localización',0);
        }
        else
        {
            $locations = [0 => 'No hay sedes'];
        }
        if($companies !== null)
        {
            $companies = $companies->pluck('name','id')->prepend('Elige una compañia',0);
        }
        else
        {
            $companies = [0 => 'No hay compañias'];
        }

        return view('companies.subsidiaries.new')->with([
            'locations' => $locations,
            'companies' => $companies,
            'subsidiary' => new Subsidiary(),
            'companyId' => $companyId
        ]);
    }

    /**
     * Display form for user edition
     * @param int $subsidiaryId
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit(int $subsidiaryId) {
        $subsidiary = $this->subsidiaryService->find($subsidiaryId);
        $locations = $this->locationService->findAll();
        $companies = $this->companyService->findAll();
        $schedules = $subsidiary->schedules()->get();



        if($locations !== null)
        {
            $locations = $locations->pluck('name', 'id')->prepend('Elige una localización',0);
        }
        else
        {
            $locations = [0 => 'No hay sedes'];
        }
        if($companies !== null)
        {
            $companies = $companies->pluck('name','id')->prepend('Elige una compañia',0);
        }
        else
        {
            $companies = [0 => 'No hay compañias'];
        }



        if ($subsidiaryId !== null) {
            return view('subsidiaries.edit')->with([
                'schedules' => $schedules,
                'object' => new Subsidiary(),
                'route' => 'subsidiaries',
                'folder' => 'schedules.fields',
                'weekdays' => [0 => 'Domingo', 1 => 'Lunes', 2 => 'Martes', 3 => 'Miercoles', 4 => 'Jueves', 5 => 'Viernes', 6 => 'Sábado'],
                'locations' => $locations,
                'companies' => $companies,
                'entity_id' => $subsidiary->id,
                'subsidiary' => $subsidiary
            ]);
        } else {
            Session::flash('message_error','El local especificado no ha sido encontrado o no existe');
            return redirect(route('subsidiaries.index'));
        }
    }

    /**
     * Display form for user edition
     * @param int $subsidiaryId
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function editByCompany(int $companyId, int $subsidiaryId) {

        $subsidiary = $this->subsidiaryService->find($subsidiaryId);
        $locations = $this->locationService->findAll();
        $companies = $this->companyService->findAll();
        $schedules = $subsidiary->schedules()->get();



        if($locations !== null)
        {
            $locations = $locations->pluck('name', 'id')->prepend('Elige una localización',0);
        }
        else
        {
            $locations = [0 => 'No hay sedes'];
        }
        if($companies !== null)
        {
            $companies = $companies->pluck('name','id')->prepend('Elige una compañia',0);
        }
        else
        {
            $companies = [0 => 'No hay compañias'];
        }



        if ($subsidiaryId !== null) {
            return view('companies.subsidiaries.edit')->with([
                'schedules' => $schedules,
                'object' => new Subsidiary(),
                'route' => 'subsidiaries',
                'folder' => 'schedules.fields',
                'weekdays' => [0 => 'Domingo', 1 => 'Lunes', 2 => 'Martes', 3 => 'Miercoles', 4 => 'Jueves', 5 => 'Viernes', 6 => 'Sábado'],
                'locations' => $locations,
                'companies' => $companies,
                'entity_id' => $subsidiary->id,
                'subsidiary' => $subsidiary,
                'companyId' => $subsidiary->company()->get([0 => 'id'])[0]
            ]);
        } else {
            Session::flash('message_error','El local especificado no ha sido encontrado o no existe');
            return redirect(route('companies.subsidiaries.index'));
        }
    }

    /**
     * Create a new subsidiary with input requested
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(Request $request) {

        $input = $request;
        $validator = Validator::make($request->all(), [
            'address' => 'filled',
            'coordenates' => 'filled',
            'company_id' => 'filled|exists:company,id',
            'location_id' => 'filled|exists:location,id',
            'email' => 'required'
        ]);
        if ($validator->fails()) {
            Session::flash('message_error','Se produjo un error al crear el local');
            return redirect()->to(route('subsidiaries.create'))
                ->withInput($request->input())
                ->withErrors($validator->errors());
        } else {

            $subsidiary = $this->subsidiaryService->create($input);

            Session::flash('message','Local creado correctamente');

            return redirect(route('subsidiaries.index'));
        }

    }

    /**
     * Create a new subsidiary with input requested
     * @param int $companyId
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createByCompany(int $companyId, Request $request) {

        $input = $request;
        $validator = Validator::make($request->all(), [
            'address' => 'filled',
            'coordenates' => 'filled',
            'company_id' => 'filled|exists:company,id',
            'location_id' => 'filled|exists:location,id',
            'email' => 'required'
        ]);
        if ($validator->fails()) {
            Session::flash('message_error','Se produjo un error al crear el local');
            return redirect()->to(route('companies.subsidiaries.create',$companyId))
                ->withInput($request->input())
                ->withErrors($validator->errors());
        } else {

            $subsidiary = $this->subsidiaryService->createByCompany($companyId,$input);

            Session::flash('message','Local creado correctamente');
            return redirect(route('companies.subsidiaries.index',$companyId));
        }

    }
    /**
     * Create a new schedule with input requested
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createSchedule(Request $request){

        $input = $request;

        $validator = Validator::make($request->all(), [
            'hidden_id' => 'required|exists:subsidiary,id',

        ]);
        if ($validator->fails()) {

            Session::flash('message_error','Se produjo un error al crear el horario');
            return redirect('/subsidiaries/'.$request->hidden_id.'/edit#1')
                ->withInput($request->input())
                ->withErrors($validator->errors());
        } else {

            if($request->start_at !== null)
            {
                $schedule = $this->scheduleService->create($input->hidden_id,$input);
            }

            Session::flash('message','Horario creado correctamente');

            return redirect('/subsidiaries/'.$request->hidden_id.'/edit#1');

        }

    }

    public function createScheduleByCompany(int $companyId,Request $request){

        $input = $request;

        $validator = Validator::make($request->all(), [
            'hidden_id' => 'required|exists:subsidiary,id',

        ]);
        if ($validator->fails()) {

            Session::flash('message_error','Se produjo un error al crear el horario');
            return redirect('companies/'.$companyId.'subsidiaries/'.$request->hidden_id.'/edit')
                ->withInput($request->input())
                ->withErrors($validator->errors());
        } else {

            if($request->start_at !== null)
            {
                $schedule = $this->scheduleService->create($input->hidden_id,$input);
            }

            Session::flash('message','Horario creado correctamente');

            return redirect('companies/'.$companyId.'/subsidiaries/'.$request->hidden_id.'/edit#1');

        }

    }

    /**
     * Create a new opinion with input requested
     * @param int $subsidiaryId
     * @param int $opinionId
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function denyOpinions(int $subsidiaryId, int $opinionId){

            $opinion = $this->subsidiaryService->findOpinion($opinionId);

            if($opinion !== null)
            {
                $opinion = $this->subsidiaryService->denyOpinion($opinion->id);

                if($opinion !== null)
                {

                    Session::flash('message','Opinion denegada correctamente');

                    return redirect('panel/subsidiaries/'.$subsidiaryId.'/edit#opinions');
                }


            }
            else
            {

                Session::flash('message_error','Petición no procesada correctamente');

                return redirect('panel/subsidiaries/'.$subsidiaryId.'/edit#opinions');
            }


    }

    /**
     * Create a new opinion with input requested
     * @param int $companyId
     * @param int $subsidiaryId
     * @param int $opinionId
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function denyOpinionsByCompany(int $companyId, int $subsidiaryId, int $opinionId){

        $opinion = $this->subsidiaryService->findOpinion($opinionId);

        $urlRedi = 'panel/companies/'.$companyId.'/subsidiaries/'.$subsidiaryId.'/edit#opinions';

        if($opinion !== null)
        {
            $opinion = $this->subsidiaryService->denyOpinion($opinion->id);

            if($opinion !== null)
            {

                Session::flash('message','Opinion denegada correctamente');

                return redirect($urlRedi);
            }


        }
        else
        {

            Session::flash('message_error','Petición no procesada correctamente');

            return redirect($urlRedi);
        }


    }

    /**
     * Delete opinion
     * @param int $subsidiaryId
     * @param int $opinionId
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteOpinions(int $subsidiaryId, int $opinionId){

        $opinion = $this->subsidiaryService->findOpinion($opinionId);

        if($opinion !== null)
        {
            $opinion = $this->subsidiaryService->deleteOpinion($opinion->id);

            if($opinion !== null)
            {

                Session::flash('message','Opinion borrada correctamente');

                return redirect('panel/subsidiaries/'.$subsidiaryId.'/edit#opinions');
            }


        }
        else
        {

            Session::flash('message_error','Petición no procesada correctamente');

            return redirect('panel/subsidiaries/'.$subsidiaryId.'/edit#opinions');
        }


    }

    /**
     * Delete opinion
     * @param int $companyId
     * @param int $subsidiaryId
     * @param int $opinionId
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteOpinionsByCompany(int $companyId, int $subsidiaryId, int $opinionId){

        $opinion = $this->subsidiaryService->findOpinion($opinionId);

        $urlRedi = 'panel/companies/'.$companyId.'/subsidiaries/'.$subsidiaryId.'/edit#opinions';

        if($opinion !== null)
        {
            $opinion = $this->subsidiaryService->deleteOpinion($opinion->id);

            if($opinion !== null)
            {

                Session::flash('message','Opinion borrada correctamente');

                return redirect($urlRedi);
            }


        }
        else
        {

            Session::flash('message_error','Petición no procesada correctamente');

            return redirect($urlRedi);
        }


    }

    /**
     * Update a opinion
     * @param int $subsidiaryId
     * @param int $opinionId
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function acceptOpinions(int $subsidiaryId, int $opinionId){

        $opinion = $this->subsidiaryService->findOpinion($opinionId);

        if($opinion !== null)
        {
            $opinion = $this->subsidiaryService->acceptOpinion($opinion->id);

            if($opinion !== null)
            {
                Session::flash('message','Opinion aceptada correctamente');

                return redirect('panel/subsidiaries/'.$subsidiaryId.'/edit#opinions');
            }


        }

        Session::flash('message_error','Petición no procesada correctamente');

        return redirect('/panel/subsidiaries/'.$subsidiaryId.'/edit#opinions');



    }

    /**
     * Update a opinion
     * @param int $companyId
     * @param int $subsidiaryId
     * @param int $opinionId
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function acceptOpinionsByCompany(int $companyId, int $subsidiaryId, int $opinionId){

        $opinion = $this->subsidiaryService->findOpinion($opinionId);

        $urlRedi = 'panel/companies/'.$companyId.'/subsidiaries/'.$subsidiaryId.'/edit#opinions';

        if($opinion !== null)
        {
            $opinion = $this->subsidiaryService->acceptOpinion($opinion->id);

            if($opinion !== null)
            {
                Session::flash('message','Opinion aceptada correctamente');

                return redirect($urlRedi);
            }

        }

        Session::flash('message_error','Petición no procesada correctamente');

        return redirect($urlRedi);



    }

    /**
     * Update the specified user with input requested
     * @param int $subsidiaryId
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(int $subsidiaryId, Request $request) {
        $subsidiary = $this->subsidiaryService->find($subsidiaryId);

        if ($subsidiary !== null) {
            $input = $request;

            $validator = Validator::make($request->all(), [
                'address' => 'filled',
                'coordenates' => 'filled',
                'company_id' => 'filled|exists:company,id',
                'location_id' => 'filled|exists:location,id',
                'email' => 'required'
            ]);
            if ($validator->fails()) {
                Session::flash('message_error','Se produjo un error al guardar la sede');
                return redirect()->to(route('subsidiaries.edit', $subsidiary->id))
                    ->withInput($request->input())
                    ->withErrors($validator->errors());
            } else {

                $subsidiary = $this->subsidiaryService->update($subsidiaryId, $input);

                if($input->start_at !== null  && $input->finish_at !== null)
                {
                    $schedule = $this->scheduleService->create($subsidiaryId,$request);
                }

                if($input->schedules !== null)
                {
                    foreach ($input->schedules as $schedule)
                    {

                        $s = $this->scheduleService->find($subsidiaryId,$schedule);

                        if($s !== null)
                        {
                            $this->scheduleService->delete($subsidiaryId,$schedule);
                        }

                    }

                }

                Session::flash('message','Sede guardada correctamente');

                return redirect(route('subsidiaries.edit' ,$subsidiary->id));

            }
        } else {
            Session::flash('message_error','La sede especificada no ha sido encontrada o no existe');
            return redirect(route('subsidiaries.edit' ,$subsidiary->id));
        }
    }

    public function updateByCompany(int $companyId, int $subsidiaryId, Request $request) {

        $subsidiary = $this->subsidiaryService->find($subsidiaryId);

        if ($subsidiary !== null) {
            $input = $request;

            $validator = Validator::make($request->all(), [
                'address' => 'filled',
                'coordenates' => 'filled',
                'company_id' => 'filled|exists:company,id',
                'location_id' => 'filled|exists:location,id',
                'email' => 'required',
                'start_at' => 'nullable|before:finish_at',
                'finish' => 'nullable|after:start_at'
            ]);
            if ($validator->fails()) {
                Session::flash('message_error','Se produjo un error al guardar la sede');
                return redirect()->to(route('companies.subsidiaries.edit', [$companyId,$subsidiary->id]))
                    ->withInput($request->input())
                    ->withErrors($validator->errors());
            } else {



                $subsidiary = $this->subsidiaryService->updateByCompany($companyId,$subsidiaryId, $input);

                if($input->start_at !== null  && $input->finish_at !== null)
                {
                    $schedule = $this->scheduleService->createByCompany($companyId,$subsidiaryId,$request);
                }

                if($input->schedules !== null)
                {
                    foreach ($input->schedules as $schedule)
                    {

                        $s = $this->scheduleService->findByCompany($companyId,$subsidiaryId,$schedule);

                        if($s !== null)
                        {
                            $this->scheduleService->deleteByCompany($companyId,$subsidiaryId,$schedule);
                        }

                    }

                }

                Session::flash('message','Sede guardada correctamente');

                return redirect(route('companies.subsidiaries.edit' ,[$companyId,$subsidiary->id]));

            }
        } else {
            Session::flash('message_error','La sede especificada no ha sido encontrada o no existe');
            return redirect(route('companies.subsidiaries.edit' ,[$companyId,$subsidiary->id]));
        }
    }

    /**
     * Delete the specified user
     * @param int $subsidiaryId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete(int $subsidiaryId) {


        $subsidiary = $this->subsidiaryService->find($subsidiaryId);

        if ($subsidiary !== null) {

            $this->subsidiaryService->delete($subsidiaryId);
            Session::flash('message','Localidad borrada correctamente');
            return redirect(route('subsidiaries.index'));
        } else {
            Session::flash('message_error','La localidad especificada no ha sido encontrada o no existe');
            return redirect(route('subsidiaries.index'));
        }
    }

    /**
     * Delete the specified user
     * @param int $companyId
     * @param int $subsidiaryId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteByCompany(int $companyId, int $subsidiaryId) {


        $subsidiary = $this->subsidiaryService->find($subsidiaryId);

        if ($subsidiary !== null) {

            $this->subsidiaryService->delete($subsidiaryId);
            Session::flash('message','Localidad borrada correctamente');
            return redirect(route('companies.subsidiaries.index',$companyId));
        } else {
            Session::flash('message_error','La localidad especificada no ha sido encontrada o no existe');
            return redirect(route('companies.subsidiaries.index',$companyId));
        }
    }

    /**
     * Process datatables ajax request.
     * @return \Illuminate\Http\JsonResponse
     */
    public function datatable(Request $request)
    {

        if($request->has('company_id')) {
            $subsidiaries = $this->subsidiaryService->findAllByCompany($request->company_id);
        } else {
            if($request->has('company_global')) {
                $subsidiaries = $this->subsidiaryService->findAll();
            }
        }

        $subsidiaries = (count($subsidiaries) ? $subsidiaries : collect());

        return DataTables::collection($subsidiaries)
            ->filter(function ($instance) use ($request) {
                $search = $request->has('search') ? Str::lower($request->get('search')['value']) : null;
                $location_id  = $request->has('location_id') ? intval($request->get('location_id')) : null;
                $company_id  = $request->has('company_global') ? intval($request->get('company_global')) : null;

                if ($search !== null && $search !== "") {
                    $instance->collection = $instance->collection->filter(function ($row) use ($search) {
                        $str_name = Str::lower($row['address']);
                        return Str::contains($str_name, $search) ? true : false;
                    });
                }

                if ($location_id !== null && $location_id > 0) {
                    $instance->collection = $instance->collection->filter(function ($subsidiaries) use ($location_id) {
                        $location = intval($subsidiaries['location_id']);
                        return ($location === $location_id);
                    });
                }

                if ($company_id !== null && $company_id > 0) {
                    $instance->collection = $instance->collection->filter(function ($subsidiaries) use ($company_id) {
                        $company = intval($subsidiaries['company_id']);
                        return ($company === $company_id);
                    });
                }
            })
            ->editColumn('email', function($subsidiary) {
                return '<a href="mailto:'.$subsidiary->email.'"><i class="fa fa-fw fa-envelope-o"></i> '.$subsidiary->email.'</a>';
            })
            ->addColumn('location', function($subsidiary) {
                return ($subsidiary->location() !== null) ? $subsidiary->location()->get()->first()->name : 'Localidad  eliminado';
            })
            ->addColumn('company', function($subsidiary) {
                return ($subsidiary->company() !== null) ? $subsidiary->company()->get()->first()->name : 'Compañia  eliminada';
            })
            ->addColumn('action', function ($subsidiary) use ($request){

                $user = Auth::user();

                if($user->hasRole('superadmin') && $request->has('company_global'))
                {
                    return '<div class="btn-group">
                                <a href="'.route('subsidiaries.edit', $subsidiary->id).'" class="btn btn-xs btn-primary"><i class="fa fa-fw fa-edit"></i></a>
                                <a href="'.route('subsidiaries.delete', $subsidiary->id).'" class="btn btn-xs btn-danger" data-remove="¿Está seguro de que quiere borrar esta sede?"><i class="fa fa-fw fa-trash"></i></a>
                            </div>';
                }

                $companyId = $subsidiary->company()->get([0=>'id'])[0];

                return '<div class="btn-group">
                                <a href="'.route('companies.subsidiaries.edit', [$companyId,$subsidiary->id]).'" class="btn btn-xs btn-primary"><i class="fa fa-fw fa-edit"></i></a>
                                <a href="'.route('companies.subsidiaries.delete', [$companyId,$subsidiary->id]).'" class="btn btn-xs btn-danger" data-remove="¿Está seguro de que quiere borrar esta sede?"><i class="fa fa-fw fa-trash"></i></a>
                            </div>';

            })
            ->rawColumns(['action','email'])
            ->setRowId('{{$id}}')
            ->make(true);
    }

    public static function routes() {
        Route::get(     '/subsidiaries',                                                                            'Web\SubsidiaryController@index'                      )->name('subsidiaries.index'                      )->middleware('auth');
        Route::get(     '/companies/{companyId}/subsidiaries',                                                      'Web\SubsidiaryController@indexFilteredByCompany'     )->name('companies.subsidiaries.index'            )->middleware('auth');
        Route::get(     '/subsidiaries/new',                                                                        'Web\SubsidiaryController@new'                        )->name('subsidiaries.new'                        )->middleware('auth');
        Route::get(     '/companies/{companyId}/subsidiaries/new',                                                  'Web\SubsidiaryController@newByCompany'               )->name('companies.subsidiaries.new'              )->middleware('auth');
        Route::post(    '/subsidiaries/new',                                                                        'Web\SubsidiaryController@create'                     )->name('subsidiaries.create'                     )->middleware('auth');
        Route::post(    '/companies/{companyId}/subsidiaries/new',                                                  'Web\SubsidiaryController@createByCompany'            )->name('companies.subsidiaries.create'           )->middleware('auth');
        Route::post(    '/subsidiaries/newSchedule',                                                                'Web\SubsidiaryController@createSchedule'             )->name('subsidiaries.createSchedule'             )->middleware('auth');
        Route::post(    '/companies/{companyId}/subsidiaries/newSchedule',                                          'Web\SubsidiaryController@createScheduleByCompany'    )->name('companies.subsidiaries.createSchedule'   )->middleware('auth');
        Route::get(     '/subsidiaries/{subsidiaryId}/edit',                                                        'Web\SubsidiaryController@edit'                       )->name('subsidiaries.edit'                       )->middleware('auth');
        Route::get(     '/companies/{companyId}/subsidiaries/{subsidiaryId}/edit',                                  'Web\SubsidiaryController@editByCompany'              )->name('companies.subsidiaries.edit'             )->middleware('auth');
        Route::get(     '/subsidiaries/{subsidiaryId}/opinions/{opinionId}/denyOpinions',                           'Web\SubsidiaryController@denyOpinions'               )->name('subsidiaries.denyOpinions'               )->middleware('auth');
        Route::get(     '/companies/{companyId}/subsidiaries/{subsidiaryId}/opinions/{opinionId}/denyOpinions',     'Web\SubsidiaryController@denyOpinionsByCompany'       )->name('companies.subsidiaries.denyOpinions'     )->middleware('auth');
        Route::get(     '/subsidiaries/{subsidiaryId}/opinions/{opinionId}/acceptOpinions',                         'Web\SubsidiaryController@acceptOpinions'             )->name('subsidiaries.acceptOpinions'             )->middleware('auth');
        Route::get(     '/companies/{companyId}/subsidiaries/{subsidiaryId}/opinions/{opinionId}/acceptOpinions',   'Web\SubsidiaryController@acceptOpinionsByCompany'     )->name('companies.subsidiaries.acceptOpinions'   )->middleware('auth');
        Route::put(     '/subsidiaries/{subsidiaryId}/edit',                                                        'Web\SubsidiaryController@update'                     )->name('subsidiaries.update'                     )->middleware('auth');
        Route::put(     '/companies/{companyId}/subsidiaries/{subsidiaryId}/edit',                                  'Web\SubsidiaryController@updateByCompany'            )->name('companies.subsidiaries.update'           )->middleware('auth');
        Route::delete(  '/subsidiaries/{subsidiaryId}/opinions/{opinionId}/deleteOpinions',                         'Web\SubsidiaryController@deleteOpinions'             )->name('subsidiaries.deleteOpinions'             )->middleware('auth');
        Route::delete(  '/companies/{companyId}/subsidiaries/{subsidiaryId}/opinions/{opinionId}/deleteOpinions',   'Web\SubsidiaryController@deleteOpinionsByCompany'    )->name('companies.subsidiaries.deleteOpinions'             )->middleware('auth');
        Route::delete(  '/subsidiaries/{subsidiaryId}/delete',                                                      'Web\SubsidiaryController@delete'                     )->name('subsidiaries.delete'                     )->middleware('auth');
        Route::delete(  '/companies/{companyId}/subsidiaries/{subsidiaryId}/delete',                                'Web\SubsidiaryController@deleteByCompany'            )->name('companies.subsidiaries.delete'           )->middleware('auth');
        Route::get(     '/subsidiaries/datatable',                                                                  'Web\SubsidiaryController@datatable'                  )->name('subsidiaries.datatable'                  )->middleware('auth');
    }

}
