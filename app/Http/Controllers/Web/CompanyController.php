<?php

namespace App\Http\Controllers\Web;

use App\Company;
use App\Photo;
use App\Rel_company_feature;
use App\Services\FeatureService;
use App\Services\LocationService;
use App\Services\SubsidiaryService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Services\CompanyService;
use App\Services\PhotoService;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Validator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Route;
use Yajra\DataTables\Facades\DataTables;

class CompanyController extends Controller
{
    //

    private $companyService;
    private $photoService;
    private $featuresService;
    private $locationService;
    private $subsidiaryService;

    /**
     * CategoryController constructor.
     * @param CompanyService $companyService
     * @param PhotoService $photoService
     * @param FeatureService $featureService
     * @param LocationService $locationService
     * @param SubsidiaryService $subsidiaryService
     */
    public function __construct(CompanyService $companyService, PhotoService $photoService, FeatureService $featureService,LocationService $locationService, SubsidiaryService $subsidiaryService)
    {
        $this->companyService = $companyService;
        $this->photoService = $photoService;
        $this->featuresService = $featureService;
        $this->locationService = $locationService;
        $this->subsidiaryService = $subsidiaryService;
    }

    /**
     * Displays datatables front end view
     * @return \Illuminate\View\View
     */
    public function index(){

        $features = $this->featuresService->findAll();

        if($features !== null)
        {
            $features= $features->pluck('name','id');//->prepend('Todas las características', 0);
        }
        else
        {
            $features = [0 => 'No hay características'];
        }

        return view('companies.index')->with([
            'features' => $features
        ]);
    }

    /**
     * Display form for create new company
     * @return $this
     */
    public function new() {

        $locations = $this->locationService->findAll();

        $features = $this->featuresService->findAll();

        if(count($locations))
        {
            $locations = $locations->pluck('name','id');
        }
        else
        {
            $locations = [0 => 'No hay localizaciones'];
        }

        return view('companies.new')->with([
            'logo' => $logo = null,
            'features' => $features,
            'company' => new Company(),
            'locations' => $locations
        ]);
    }

    /**
     * Display form for company edition
     * @param int $companyId
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit(int $companyId) {

        $company = $this->companyService->find($companyId);

        $features = $this->featuresService->findAll();
        $current_features = $company->features()->get();

        if ($company !== null) {

            $logo = $this->photoService->find($company->logo_id);

            return view('companies.edit')->with([
                'company' => $company,
                'features' => $features,
                'current_features' => $current_features,
                'logo' => $logo
            ]);
        } else {
            Session::flash('message_error','La compañia especificada no ha sido encontrada o no existe');
            return redirect(route('companies.index'));
        }
    }

    /**
     * Create a new company with input requested
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(Request $request) {
        $input = $request;
        $validator = Validator::make($request->all(), [
            'name' => 'filled|unique:mysql.company,name',
            'identification' => 'filled',
            'logo' => 'image|dimensions:max_width=500,max_height=500',
            'feature' => 'exist:mysql.features,id'

        ]);
        if ($validator->fails()) {
            Session::flash('message_error','Se produjo un error al crear la compañía');
            return redirect()->to(route('companies.create'))
                ->withInput($request->input())
                ->withErrors($validator->errors());
        } else {

            $company = $this->companyService->create($input);

            if(count($request->features))
            {
                foreach ($request->features as $feature)
                {
                    $feature = $this->companyService->setFeature($company->id,$feature);
                }
            }

            if($request->has('logo') && $request->input('logo') !== null)
            {
                $logo = new Photo() ;

                $logo->url = $request->file('logo')->store('public/logos');

                $logo->save();

                $company->logo_id = $logo->id;

                $company->save();
            }

            $this->subsidiaryService->createByCompany($company->id, $request);

            Session::flash('message','Campañía creada correctamente');
            return redirect(route('companies.index'));
        }
    }

    /**
     * Update the specified company with input requested
     * @param int $companyId
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(int $companyId, Request $request) {

        $company = $this->companyService->find($companyId);

        if ($company !== null) {
            $input = $request;

            $validator = Validator::make($request->all(), [
                'name' => 'filled',
                'identification' => 'filled',
                'logo' => 'image|dimensions:max_width=500,max_height=500',
                'feature' => 'exist:mysql.features,id'
            ]);
            if ($validator->fails()) {
                Session::flash('message_error','Se produjo un error al guardar la compañía');
                return redirect()->to(route('companies.edit', $company->id))
                    ->withInput($request->input())
                    ->withErrors($validator->errors());
            } else {

                $this->companyService->update($companyId, $input);

                //dd($request->input('features'));


                    $features = $request->input('features');
                    //$company->features()->sync($request->input('features'));
                    // TODO remove all Rel_company_feature de esta company

                    $featuresToRemove = $company->features()->get()->all();

                    foreach ($featuresToRemove as $feature)
                    {
                        $feature->delete();
                    }

                    if($features !== null)
                    {
                        foreach ($features as $feature) {
                            $companyFeature = new Rel_company_feature();
                            $companyFeature->company_id = $company->id;
                            $companyFeature->feature_id = $feature;
                            $companyFeature->save();
                        }
                    }

                if($request->has('logo') && $request->file('logo') !== null)
                {
                    $this->photoService->updateLogo($company,$request);
                }

                Session::flash('message','Compañia guardada correctamente');

                return redirect(route('companies.index'));

            }
        } else {
            Session::flash('message_error','La compañía especificada no ha sido encontrada o no existe');
            return redirect(route('companies.index'));
        }
    }

    /**
     * Delete the specified company
     * @param int $companyId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete(int $companyId) {


        $company = $this->companyService->find($companyId);

        if ($company !== null) {
            $this->companyService->delete($companyId);
            Session::flash('message','Compañia borrada correctamente');
            return redirect(route('companies.index'));
        } else {
            Session::flash('message_error','La compañía especificada no ha sido encontrada o no existe');
            return redirect(route('companies.index'));
        }
    }

    /**
     * Process datatables ajax request.
     * @return \Illuminate\Http\JsonResponse
     */
    public function datatable(Request $request)
    {

        $user = Auth::user();

        if($user->hasRole('superadmin'))
        {
            $companies = $this->companyService->findAll();
        }
        else
        {
            $companies = $user->companies()->get();
        }

        $companies = ($companies !== null) ? $companies : collect();


        return DataTables::collection($companies)
            ->filter(function ($instance) use ($request) {

                $search = $request->has('search') ? Str::lower($request->get('search')['value']) : null;
                $features      = $request->has('features') ? explode('|', $request->get('features')) : null;

                if ($search !== null && $search !== "") {
                    $instance->collection = $instance->collection->filter(function ($row) use ($search) {
                        $str_name = Str::lower($row['name']);
                        return Str::contains($str_name, $search) ? true : false;
                    });
                }

                if ($features !== null && count($features) > 0 && $features[0] !== "" && $features[0] != 0) {

                    $instance->collection = $instance->collection->filter(function ($company) use ($features) {
//                        $tattoos = Tattoo::withAllTags($tags)->where('id', '=', $tattoo['id'])->get();
//                        return ($tattoos->count() > 0);

                        //debug($this->companyService->hasFeatureCompany($company['id'], $features));

                        return  $this->companyService->hasFeatureCompany($company['id'], $features);

                    });
                }
            })
            ->addColumn('score', function($company) {

                if(count($company))
                {
                    $scores = $company->scores()->get();

                    if(count($scores))
                    {
                        $scores = $scores->pluck('score')->all();

                        $svgScore = array_sum($scores)/count($scores);

                        $stars = substr($svgScore,0,1);

                        $rest = substr($svgScore,2,1);

                        $print = array();

                        for($i = 0 ; $i < $stars; $i++)
                        {
                            $print[]="<i class='fa fa-star' style='color:gold;'></i>";
                        }

                        if($rest > 5 )
                        {
                            $print[]="<i class='fa fa-star' style='color:gold;'></i>";
                        }
                        else
                            if($rest <=5 && $rest)
                        {
                            $print[]="<i class='fa fa-star-half-o' style='color:gold;'></i>";
                        }

                        $c = count($print);

                        for($i = 0 ; $i < (5 - $c); $i++ )
                        {
                            $print[] = "<i class='fa fa-star-o' style='color:gold;'></i>";
                        }

                        return implode(" ",$print);
                    }


                }

                return [0 => 'No hay valoraciones'];


            })
            ->addColumn('logo_id', function($company) {

                $photoId = $company->logo()->get([0 => 'id']);

                if(count($photoId))
                {
                    $photoId = $company->logo()->get([0 => 'id'])[0];
                }
                $photo = $this->photoService->find($photoId);

                if($photo !== null)
                {
                    $url = asset('storage'.str_after($photo[0]->url, 'public'));

                    return '<div><img class="img-thumbnail" style="width:40px" src="'.$url.'"></div>';
                }

                $url = asset('storage/photos/default/no-photo.png');

                return '<div><img class="img-thumbnail" style="width:40px" src="'.$url.'"></div>';



            })
            ->addColumn('action', function ($company) {
                return '<div class="btn-group">
                                <a href="'.route('companies.edit', $company->id).'" class="btn btn-xs btn-primary"><i class="fa fa-fw fa-edit"></i></a>
                                <a href="'.route('companies.delete', $company->id).'" class="btn btn-xs btn-danger" data-remove="¿Está seguro de que quiere borrar esta compañia?"><i class="fa fa-fw fa-trash"></i></a>
                            </div>';

            })
            ->rawColumns(['action','score','logo_id'])
            ->setRowId('{{$id}}')
            ->make(true);
    }

    public static function routes() {
        Route::get(     '/companies',                      'Web\CompanyController@index'       )->name('companies.index'    )->middleware('auth');
        Route::get(     '/companies/new',                  'Web\CompanyController@new'         )->name('companies.new'      )->middleware('auth');
        Route::post(    '/companies/new',                  'Web\CompanyController@create'      )->name('companies.create'   )->middleware('auth');
        Route::get(     '/companies/{companyId}/edit',     'Web\CompanyController@edit'        )->name('companies.edit'     )->middleware('auth');
        Route::put(     '/companies/{companyId}/edit',     'Web\CompanyController@update'      )->name('companies.update'   )->middleware('auth');
        Route::delete(  '/companies/{companyId}/delete',   'Web\CompanyController@delete'      )->name('companies.delete'   )->middleware('auth');
        Route::get(     '/companies/datatable',            'Web\CompanyController@datatable'   )->name('companies.datatable')->middleware('auth');
    }
}
