<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;

use App\Services\FeatureService;
use App\Services\CompanyService;
use App\Services\UsersService;
use Illuminate\Support\Facades\Auth;
use Route;

class HomeController extends Controller
{
    private $featureService;
    private $companyService;
    private $usersService;


    /**
     * Create a new controller instance.
     * @param FeatureService $featureService
     * @param CompanyService $companyService
     * @param UsersService $usersService
     * @return void
     */
    public function __construct(FeatureService $featureService,
                                CompanyService $companyService,
                                UsersService $usersService) {
        $this->middleware('auth');

        $this->featureService = $featureService;
        $this->companyService = $companyService;
        $this->usersService = $usersService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $features = $this->featureService->findAll()->count();

        $company = Company::where('user_id','=',Auth::id())->get()->first();

        $menus = ($company != null) ? $company->menus()->get()->count() : 0;
        $dishes = ($company != null) ? $company->dishes()->get()->count() : 0;
        $subsidiaries = ($company != null) ? $company->subsidiaries()->get() : collect();

        $opinions = 0;

        foreach($subsidiaries as $subsidiary)
        {
            $opinions+= $subsidiary->opinions()->get()->count();
        }

        $users = $this->usersService->findAll()->count();
        return view('home')->with([
            'features' => $features,
            'opinions' => $opinions,
            'users' => $users,
            'menus' => $menus,
            'dishes' => $dishes
        ]);
    }

    public static function routes(){

        Route::get('/home', 'HomeController@index')->name('home');

    }
}
