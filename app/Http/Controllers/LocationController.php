<?php

namespace App\Http\Controllers;

use Validator;
use App\Location;
use App\Province;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use function isEmpty;
use App\Services\LocationService;
use Route;

class LocationController extends Controller
{
    //

    private $locationService;


    public function __construct(LocationService $locationService)
    {
        $this->locationService = $locationService;
    }

    public function findAll() {

        $locations = $this->locationService->findAll();

        if($locations != null)
        {
            return response()->json($locations,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }

    }

    public function findAllByProvince(int $provinceId) {

        $locations = $this->locationService->findAllByProvince($provinceId);

        if($locations != null)
        {
            return response()->json($locations,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }

    }

    public function find(int $provinceId,int $locationId) {

        $location = $this->locationService->findByProvince($locationId, $provinceId);

        if($location != null)
        {
            return response()->json($location,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function create(int $locationId,Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:location|max:50',
            'zip' => 'required'

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        } else {

            $location = $this->locationService->create($locationId,$request);

            if($location != null)
            {
                return response()->json($location,201);
            }
            else
            {
                return response()->json('Element not found',404);
            }
        }
    }

    public function delete(int $province_id,int $locationId) {

        $location = $this->locationService->delete($locationId);

        if($location != null)
        {
            return response()->json(null,204);
        }
        else
        {
            return response()->json('Element not found',404);
        }

    }

    public function update(int $provinceId,int $locationId, Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:location|max:50',
            'zip' => 'required|unique:location'

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        else
        {
            $location = $this->locationService->update($provinceId,$locationId, $request);

            if($location != null)
            {
                return response()->json($location,200);
            }
            else
            {
                return response()->json('Element not found',404);
            }
        }

    }

    static public function routes(){

        Route::get(      '/locations','LocationController@findAll'                                      )->middleware('auth:api');
        Route::get(      '/provinces/{province_id}/locations','LocationController@findAllByProvince'    )->middleware('auth:api');
        Route::post(     '/provinces/{province_id}/locations','LocationController@create'               )->middleware('auth:api');
        Route::get(      '/provinces/{province_id}/locations/{location_id}','LocationController@find'   )->middleware('auth:api');
        Route::put(      '/provinces/{province_id}/locations/{location_id}','LocationController@update' )->middleware('auth:api');
        Route::delete(   '/provinces/{province_id}/locations/{location_id}','LocationController@delete' )->middleware('auth:api');

    }

}
