<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Menu;
use App\Company;
use App\Services\MenuService;
use App\Services\OfferService;
use App\Services\ScoreService;use Route;


class MenuController extends Controller
{
    //
    private $menuService;
    private $offerService;
    private $scoreService;

    /**
     * MenuController constructor.
     * @param MenuService $menuService
     * @param OfferService $offerService
     * @param ScoreService $scoreService
     *
     */

    public function __construct(MenuService $menuService, OfferService $offerService, ScoreService $scoreService)
    {
        $this->menuService = $menuService;
        $this->offerService = $offerService;
        $this->scoreService = $scoreService;
    }

    public function findAll(int $companyId) {

        $menus = $this->menuService->findAllByCompany($companyId);

        if($menus != null)
        {

            return response()->json($menus,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }

    }

    public function find(int $companyId,int $menuId) {

        $menu = $this->menuService->findByCompany($companyId,$menuId);

        if($menu != null)
        {
            return response()->json($menu,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function findDishes(int $companyId,int $menuId) {

        $dishes = $this->menuService->findDishes($companyId,$menuId);

        if($dishes != null)
        {
            return response()->json($dishes,200);
        }
       else
       {
           return response()->json('Element not found',404);
       }
    }

    public function findFeatures($menuId){

        $features = $this->menuService->findFeatures($menuId);

        if($features != null)
        {
            return response()->json($features,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }

    }

    public function create(int $companyId,Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:menu|max:50',
            'price' => 'required',
            'observations' => 'required',
            'photoId' =>''
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        } else {

            $menu = $this->menuService->createByCompany($companyId,$request);

            if($menu != null)
            {
                return response()->json($menu,201);
            }
            else
            {
                return response()->json('Element not found',404);
            }
        }
    }

    public function setDish(int $companyId, int $menuId, int $dishId){


        $dish = $this->menuService->setDishByCompany($companyId,$menuId,$dishId);

        if($dish != null)
        {
            return response()->json($dish,201);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function setDishes(int $companyId, int $menuId, Request $request) {

        $dishes = ($request->has('dishes')) ? explode(',', $request->input('dishes')) : [];

        $dishes_arr = array();

        foreach($dishes as $dish) {
            $dishId = intval(trim($dish));
            $nulls = $this->menuService->setDishByCompany($companyId,$menuId,$dishId);

            if($nulls == null)
            {
                $dishes_arr[] = $nulls;
            }
        }
        if(empty($dishes_arr))
        {
            return response()->json(null,204);
        }
        else
        {
            return response()->json('Element not found',404);
        }

    }

    public function delete(int $companyId, int $menuId) {

        $menu = $this->menuService->deleteByCompany($companyId,$menuId);

        if($menu != null)
        {
            return response()->json(null,204);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function deleteDish(int $companyId, int $menuId, int $dishId) {

        $dish = $this->menuService->deleteDish($companyId,$menuId,$dishId);

        if($dish != null)
        {
            return response()->json(null,204);
        }
        else
        {
            return response()->json('Element not found',404);
        }

    }

    public function deleteDishes(int $companyId, int $menuId, Request $request) {

        $dishes = ($request->has('dishes')) ? explode(',', $request->input('dishes')) : [];

        $dishes_arr = array();

        foreach ($dishes as $dish)
        {
            $dishId = intval(trim($dish));

            $nulls = $this->menuService->deleteDish($companyId,$menuId,$dishId);

            if($nulls == null)
            {
                $dishes_arr[] = $nulls;
            }
        }
        if(empty($dishes_arr))
        {
            return response()->json(null,204);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function update(int $companyId, int $menuId, Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:menu|max:50',
            'price' => 'required',
            'observations' => 'required',
            'photoId' =>''

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        else
        {
            $menu = $this->menuService->updateByCompany( $companyId, $menuId, $request);

            if($menu != null)
            {
                return response()->json($menu,200);
            }
            else
            {
                return response()->json('Element not found');
            }
        }

    }

    public function findMenu(int $companyId, int $menuId, int $offerId){

        $offer =  $this->offerService->findMenu($companyId,$menuId,$offerId);

        if($offer != null)
        {
            return response()->json($offer,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function setMenu(int $companyId, int $menuId, Request $request){

        $validator = Validator::make($request->all(), [
            'start_at' => 'required|date_format:Y-m-d H:i:s',
            'finish_at' => 'required|date_format:Y-m-d H:i:s',
            'price' => 'required|numeric',
            'discount' =>'required|numeric'

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        else
        {
            $offer = $this->offerService->setMenuByCompany( $companyId,  $menuId, $request);

            if($offer != null)
            {
                return response()->json($offer,200);
            }
            else
            {
                return response()->json('Element not found');
            }
        }



    }

    public function findMenus(int $companyId, int $menuId){

        $menus = $this->offerService->findMenus($companyId,$menuId);

        if($menus != null)
        {
            return response($menus, 200)->json();
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function putMenu(int $companyId, int $menuId, int $offerId, Request $request){

        $validator = Validator::make($request->all(), [
            'start_at' => 'required|date_format:Y-m-d H:i:s',
            'finish_at' => 'required|date_format:Y-m-d H:i:s',
            'price' => 'required|numeric',
            'discount' =>'required|numeric'

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        else
        {
            $offer = $this->offerService->putMenu( $companyId,  $menuId, $offerId, $request);

            if($offer != null)
            {
                return response()->json($offer,200);
            }
            else
            {
                return response()->json('Element not found',404);
            }
        }



    }

    public function deleteMenu(int $companyId, int $menuId, int $offerId){

        $offer = $this->offerService->deleteMenu($companyId,$menuId,$offerId);

        if($offer != null)
        {
            return response()->json(null,204);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function findMenusScore(int $companyId, int $menuId){

        $score = $this->scoreService->findMenusScore($companyId,$menuId);

        if($score != null)
        {
            return response()->json($score,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }

    }

    public function setMenuScore(int $companyId, int $menuId, Request $request){

        $validator = Validator::make($request->all(), [
            'score' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        else
        {
            $menu = $this->scoreService->setMenuScore( $companyId,  $menuId, $request);

            if($menu != null)
            {
                return response()->json($menu,200);
            }
            else
            {
                return response()->json('Element not found',404);
            }
        }


    }

    public function findMenuScore(int $companyId, int $menuId, int $scoreId){

        $score = $this->scoreService->findMenuScore($companyId,$menuId,$scoreId);

        if($score != null)
        {
            return response()->json($score,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function putMenuScore(int $companyId, int $menuId, int $scoreId, Request $request){

        $validator = Validator::make($request->all(), [
            'score' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        else
        {
            $score = $this->scoreService->putMenuScore( $companyId,  $menuId, $scoreId, $request);

            if($score != null)
            {
                return response()->json($score,200);
            }
            else
            {
                return response()->json('Element not found',404);
            }
        }



    }

    public function deleteMenuScore(int $companyId, int $menuId, int $scoreId){

        $score = $this->scoreService->deleteMenuScore($companyId,$menuId,$scoreId);

        if($score != null)
        {
            return response()->json(null,204);
        }
        else
        {
            return response()->json('Element not found');
        }

    }


    static public function routes(){

        Route::get(    '/companies/{company_id}/menus','MenuController@findAll'             )->middleware('auth:api');
        Route::post(   '/companies/{company_id}/menus','MenuController@create'              )->middleware('auth:api');
        Route::get(    '/companies/{company_id}/menus/{menu_id}','MenuController@find'      )->middleware('auth:api');
        Route::put(    '/companies/{company_id}/menus/{menu_id}','MenuController@update'    )->middleware('auth:api');
        Route::delete( '/companies/{company_id}/menus/{menu_id}','MenuController@delete'    )->middleware('auth:api');

    }

    static public function routesDishes(){

        Route::get(      '/companies/{company_id}/menus/{menu_id}/dishes','MenuController@findDishes'           )->middleware('auth:api'); // Ruta para listar platos de un menú
        Route::post(     '/companies/{company_id}/menus/{menu_id}/dishes/{dish_id}','MenuController@setDish'    )->middleware('auth:api'); // Ruta para añadir un plato a un menú
        Route::post(     '/companies/{company_id}/menus/{menu_id}/dishes','MenuController@setDishes'            )->middleware('auth:api'); // Ruta para añadir varios platos a un menú
        Route::delete(   '/companies/{company_id}/menus/{menu_id}/dishes/{dish_id}','MenuController@deleteDish' )->middleware('auth:api'); // Ruta para quitar un plato de un menú
        Route::delete(   '/companies/{company_id}/menus/{menu_id}/dishes','MenuController@deleteDishes'         )->middleware('auth:api'); // Ruta para quitar varios platos de un menú

    }

    static public function routesScore(){

        Route::get(    '/companies/{company_id}/menus/{menu_id}/scores','MenuController@findMenusScore'               )->middleware('auth:api');
        Route::post(   '/companies/{company_id}/menus/{menu_id}/scores','MenuController@setMenuScore'                 )->middleware('auth:api');
        Route::get(    '/companies/{company_id}/menus/{menu_id}/scores/{score_id}','MenuController@findMenuScore'     )->middleware('auth:api');
        Route::put(    '/companies/{company_id}/menus/{menu_id}/scores/{score_id}','MenuController@putMenuScore'      )->middleware('auth:api');
        Route::delete( '/companies/{company_id}/menus/{menu_id}/scores/{score_id}','MenuController@deleteMenuScore'   )->middleware('auth:api');

    }

    static public function routesOffer(){

        Route::get(    '/companies/{company_id}/menus/{menu_id}/offers/{offer_id}','MenuController@findMenu'     )->middleware('auth:api');
        Route::post(   '/companies/{company_id}/menus/{menu_id}/offers','MenuController@setMenu'                 )->middleware('auth:api');
        Route::get(    '/companies/{company_id}/menus/{menu_id}/offers','MenuController@findMenus'               )->middleware('auth:api');
        Route::put(    '/companies/{company_id}/menus/{menu_id}/offers/{offer_id}','MenuController@putMenu'      )->middleware('auth:api');
        Route::delete( '/companies/{company_id}/menus/{menu_id}/offers/{offer_id}','MenuController@deleteMenu'   )->middleware('auth:api');

    }

}
