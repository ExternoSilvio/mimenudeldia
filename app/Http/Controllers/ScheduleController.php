<?php

namespace App\Http\Controllers;

use http\Env\Response;
use Illuminate\Http\Request;
use Validator;


use App\Services\ScheduleService;
use App\Services\SubsidiaryService;
use Route;

class ScheduleController extends Controller
{
    //

    private $scheduleService;
    private $subsidiaryService;

    /**
     * ScheduleController constructor.
     * @param $scheduleService
     * @param $subsidiaryService
     */
    public function __construct(ScheduleService $scheduleService, SubsidiaryService $subsidiaryService)
    {
        $this->scheduleService = $scheduleService;
        $this->subsidiaryService = $subsidiaryService;
    }

    public function findAll(int $companyId, int $subsidiaryId) {

        $schedules = $this->scheduleService->findAll($companyId, $subsidiaryId);

        if($schedules!= null)
        {
            return response()->json($schedules,200);
        }
        else
        {
            return response()->json('Element not found',404);

        }

    }

    public function find(int $companyId, int $subsidiaryId,int $scheduleId) {

        $schedule = $this->scheduleService->findByCompany($companyId, $subsidiaryId, $scheduleId);

        if($schedule != null)
        {
            return response()->json($schedule,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function create(int $companyId, int $subsidiaryId, Request $request) {
        $validator = Validator::make($request->all(), [
            'day' => 'required',
            'start_at' => 'required',
            'finish_at' => 'required',

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        } else {

            $schedule = $this->scheduleService->createByCompany($companyId,$subsidiaryId,$request);

            if($schedule != null)
            {
                return response()->json($schedule,201);
            }
            else
            {
                return response()->json('Element not found',404);
            }
        }
    }

    public function delete(int $companyId, int $subsidiaryId,int $scheduleId) {
        $schedule = $this->scheduleService->deleteByCompany($companyId, $subsidiaryId, $scheduleId);

        if($schedule != null)
        {
            return response()->json(null,204);
        }
        else
        {
            return response()->json('Element not found',404);
        }

    }

    public function update(int $companyId, int $subsidiaryId,int $scheduleId, Request $request) {
        $validator = Validator::make($request->all(), [
            'day' => 'required',
            'start_at' => 'required',
            'finish_at' => 'required'

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        else
        {
            $schedule = $this->scheduleService->update($companyId,$subsidiaryId,$scheduleId, $request);

            if($schedule != null)
            {
                return response()->json($schedule,200);
            }
            else
            {
                return response()->json('Element not found',404);
            }
        }

    }

    static public function routes(){

        Route::get(     '/companies/{company_id}/subsidiaries/{subsidiary_id}/schedules','ScheduleController@findAll'               )->middleware('auth:api');
        Route::post(    '/companies/{company_id}/subsidiaries/{subsidiary_id}/schedules','ScheduleController@create'                )->middleware('auth:api');
        Route::get(     '/companies/{company_id}/subsidiaries/{subsidiary_id}/schedules/{schedule_id}','ScheduleController@find'    )->middleware('auth:api');
        Route::put(     '/companies/{company_id}/subsidiaries/{subsidiary_id}/schedules/{schedule_id}','ScheduleController@update'  )->middleware('auth:api');
        Route::delete(  '/companies/{company_id}/subsidiaries/{subsidiary_id}/schedules/{schedule_id}','ScheduleController@delete'  )->middleware('auth:api');

    }

}
