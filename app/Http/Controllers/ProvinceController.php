<?php

namespace App\Http\Controllers;


use App\Services\ProvinceService;
use Illuminate\Support\Facades\Auth;
use Validator;
use Route;
use Illuminate\Http\Request;

class ProvinceController extends Controller
{
    private $provinceService;

    /**
     * ProvinceController constructor.
     * @param $provinceService
     */
    public function __construct(ProvinceService $provinceService)
    {
        $this->provinceService = $provinceService;
    }

    public function findAll() {
        // Auth::user();
        // Auth::id();


        return response()->json($this->provinceService->findAll(),200);
    }

    public function find(int $province_id) {

        $province = $this->provinceService->find($province_id);

        if($province != null)
        {
            return response()->json($province,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function create(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:province|max:50'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        } else {
            return response()->json($this->provinceService->create($request),201);
        }
    }

    public function delete(int $province_id) {

       $province = $this->provinceService->delete($province_id);

       if($province != null)
       {
           return response()->json(null,204);
       }
       else
       {
           return response()->json('Element not found',404);
       }
    }

    public function update(int $province_id, Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:province|max:50'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        else
        {
            $province = $this->provinceService->update($province_id, $request);

            if($province != null)
            {
                return response()->json($province,200);
            }
            else
            {
                return response()->json('Element not found',404);
            }
        }


    }

    static public function routes(){

        Route::get(    '/provinces','ProvinceController@findAll'                )->middleware('auth:api');
        Route::post(   '/provinces','ProvinceController@create'                 )->middleware('auth:api');
        Route::get(    '/provinces/{province_id}','ProvinceController@find'     )->middleware('auth:api');
        Route::put(    '/provinces/{province_id}','ProvinceController@update'   )->middleware('auth:api');
        Route::delete(  '/provinces/{province_id}','ProvinceController@delete'  )->middleware('auth:api');

    }
}
