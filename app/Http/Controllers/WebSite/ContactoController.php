<?php

namespace App\Http\Controllers\WebSite;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Route;

class ContactoController extends Controller
{

    public function contacto(){

        return view('mimenudeldiaweb/contacto');

    }

    public static function routes(){

        Route::get(   '/contacto', 'WebSite\ContactoController@contacto'   )->name('website.contacto');

    }
}
