<?php

namespace App\Http\Controllers\WebSite;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Route;
use App\Services\SubsidiaryService;

class GeoLocationController extends Controller
{

    private $subsidiariesService;

    public function __construct(SubsidiaryService $subsidiaryService)
    {
        $this->subsidiariesService = $subsidiaryService;
    }

    public function findNearestSubsidiaries(float $latitude, float $longitude) {
        $subsidiaries = $this->subsidiariesService->findAll();

        $subsidiaries->each(function ($subsidiary, $key) use ($latitude, $longitude) {
            if($subsidiary->coordinates !== null)
            {
                $coordinates = explode(',',$subsidiary->coordinates);
                $distance = $this->subsidiariesService->distance($latitude, $longitude, $coordinates[0], $coordinates[1],'K');
                $subsidiary->distance = $distance;
                $url = $subsidiary->company()->get()->first()->logo()->get();
                $subsidiary->company_logo = (isset($url[0])) ? $url[0]->url : 'images/no-image.svg';
            }
        });

        return $subsidiaries;
    }

    public function findNearestSubsidiariesRequest(Request $position){

        $subsidiaries = $this->findNearestSubsidiaries($position->param['latitude'],$position->param['longitude']);

//        $subsidiaries = $this->subsidiariesService->findAll();
//
//        $subsidiaries->each(function ($subsidiary, $key) use ($position) {
//            if($subsidiary->coordinates !== null)
//            {
//                $coordinates = explode(',',$subsidiary->coordinates);
//
//                $distance = $this->subsidiariesService->distance($position->param['latitude'],$position->param['longitude'],$coordinates[0],$coordinates[1],'K');
//                $subsidiary->distance = $distance;
//                $url = $subsidiary->company()->get()[0]->logo()->get();
//                $subsidiary->company_logo = (isset($url[0])) ? $url[0]->url : 'images/no-image.svg';
//            }
//        });



        $data = [
            'subsidiaries' => $subsidiaries->sortBy("distance")->take(10)->values()
        ];

        return response()->json($data)->setStatusCode(200);

    }


    public static function routes(){

        Route::get('/geolocation', 'WebSite\GeoLocationController@findNearestSubsidiariesRequest')->name('website.findNearestSubsidiaries');

    }
}
