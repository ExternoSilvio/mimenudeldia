<?php

namespace App\Http\Controllers\WebSite;


use function GuzzleHttp\Psr7\build_query;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Route;

use App\Services\CompanyService;
use App\Services\FeatureService;
use App\Services\SubsidiaryService;
use App\Services\UsersService;

class RestauranteController extends Controller
{

    private $companyService;
    private $featureService;
    private $subsidiaryService;
    private $usersService;

    public function __construct(CompanyService $companyService, FeatureService $featureService, SubsidiaryService $subsidiaryService, UsersService $usersService)
    {
       $this->companyService = $companyService;
       $this->featureService = $featureService;
       $this->subsidiaryService = $subsidiaryService;
       $this->usersService = $usersService;
    }

    public function restaurantes(){

        $subsidiaries = $this->subsidiaryService->findLast(10);

        return view('mimenudeldiaweb/restaurantes')->with([

            'subsidiaries' => $subsidiaries

        ]);

    }

    public function restaurante(int $subsidiaryId){

        $subsidiary = $this->subsidiaryService->find($subsidiaryId);
        $company_features = $subsidiary->company()->get()[0]->features()->get();
        $features = $this->featureService->findAll();

        foreach($subsidiary->location()->get() as $location)
            foreach($location->subsidiaries()->get() as $subsidiary)
            {
                if($img = $subsidiary->company()->get())
                {
                    if($img = $img[0]->logo()->get())
                    {
                        if(isset($img[0]))
                            debug($img[0]->url);
                    }

                }

            }





        //todo builder error, sometimes works, sometimes not(blade template)

        //dd($subsidiary->location()->get());

        return view('mimenudeldiaweb/restaurante')->with([

            'subsidiary' => $subsidiary,
            'company_features' => $company_features,
            'features' => $features,
            'weekdays' => array('Lunes','Martes','Miércoles','Jueves', 'Viernes', 'Sábado','Domingo')
        ]);

    }

    public function setFavorite(int $companyId){

        $company = $this->companyService->find($companyId);

        $user = Auth::user();

        $this->usersService->setFavorite($company->id);

        return redirect()->back();

    }

    public static function findLast(int $limit = 4){

        return SubsidiaryService::findLastStatic($limit);

    }


    public static function routes(){

        Route::get('/restaurantes'                            ,'WebSite\RestauranteController@restaurantes'    )->name('website.restaurantes');
        Route::get('/restaurante/{restaurenteId}'             ,'WebSite\RestauranteController@restaurante'     )->name('website.restaurante');
        Route::post('/restaurante/{restaurenteId}/favorites'  ,'WebSite\RestauranteController@setFavorite'     )->name('website.setFavorite');
    }

}
