<?php

namespace App\Http\Controllers\WebSite;

use App\Mail\Contact;
use App\Mail\Reservation;
use App\Mail\ReservationSelf;
use App\Mail\SelfContact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Validator;
use Route;

class SendMailController extends Controller
{

    public function contact(Request $request){

        $validator = Validator::make($request->all(), [
            'name'    => 'required',
            'email'   => 'required|email',
            'subject' => 'required',
            'body'    => 'required'
        ]);

        if($validator->fails())
        {
            return response()->json($validator->errors());
        }
        else
        {
            Mail::to($request->email)->send(new Contact($request));
            Mail::to(env('MAIL_INFO','noreply@digitalapp.es'))->send(new SelfContact($request));

            return response()->json("<i class='fa fa-check-circle' style='color:lightgreen'></i>Formulario enviado");
        }

    }

    public function reservation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'    => 'required',
            'email'   => 'required|email',
            'phone'   => 'required',
            'diners'  => 'required',
            'date'    => 'required',
            'time'    => 'required'

        ]);


        if($validator->fails())
        {
            return response()->json($validator->errors());
        }
        else
        {
            Mail::to($request->email)->send(new Reservation($request));
            Mail::to(env('MAIL_INFO','noreply@digitalapp.es'))->send(new ReservationSelf($request));

            return response()->json("<i class='fa fa-check-circle' style='color:lightgreen'></i>Reserva enviada. Recibirá un correo de confirmación");
        }
    }

    public function emailtemplate(){

        return view('mails.reservation');
    }


    public static function routes(){

        Route::post('/contact-mail',      'WebSite\SendMailController@contact'         )->name('website.contactmail');
        Route::post('/reservation-mail',  'WebSite\SendMailController@reservation'     )->name('website.reservation');
        Route::get('/emailtemplate',      'WebSite\SendMailController@emailtemplate'   )->name('website.emailtemplate');
    }

}
