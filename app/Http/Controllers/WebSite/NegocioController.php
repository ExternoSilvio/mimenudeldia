<?php

namespace App\Http\Controllers\WebSite;

use App\Services\SubsidiaryService;
use App\Services\CompanyService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Route;

class NegocioController extends Controller
{

    private $subsidiaryService;
    private $companyService;

    public function __construct(SubsidiaryService $subsidiaryService, CompanyService $companyService)
    {
        $this->subsidiaryService = $subsidiaryService;
        $this->companyService = $companyService;
    }


    public function negocios(){

        $companies = $this->companyService->findLast(10);

        /*$companies = array();

        for($i = count($companies_prov);$i > count($companies_prov) - 4; $i--)
        {
            $companies[] = $companies_prov[$i-1];
        }


        $subsidiaries_prov = $this->subsidiaryService->findAll();

        $subsidiaries = array();

        for($i = count( $subsidiaries_prov);$i > count( $subsidiaries_prov) - 4; $i--)
        {
            $subsidiaries[] = $subsidiaries_prov[$i-1];
        }*/

        return view('mimenudeldiaweb/negocios')->with([

            'companies' => $companies

        ]);

    }

    public function negocio(int $companyId){

        //todo hay companies sin subsidiadies

        $company = $this->companyService->find($companyId);

        $subsidiary = $this->subsidiaryService->findPrimary($companyId);

        return view('mimenudeldiaweb/negocio')->with([

            'subsidiary' => $subsidiary,

        ]);

    }

    public static function routes(){

        Route::get('/negocios',                  'WebSite\NegocioController@negocios'  )->name('website.negocios');
        Route::get('/negocio/{companyId}',       'WebSite\NegocioController@negocio'   )->name('website.negocio');
    }

}
