<?php

namespace App\Http\Controllers\WebSite;

use App\Dish;
use App\Http\Controllers\LocationController;
use App\Menu;
use App\Services\DishService;
use App\Services\FeatureService;
use App\Services\MenuService;
use App\Http\Controllers\WebSite\GeoLocationController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Route;

use App\Services\SubsidiaryService;
use App\Services\LocationService;
use App\Services\ProvinceService;


class indexController extends Controller
{

    private $subsidiaryService;
    private $locationService;
    private $provinceService;
    private $dishService;
    private $menuService;
    private $featureService;
    private $geolocationController;

    public function __construct(SubsidiaryService $subsidiaryService, LocationService $locationService, ProvinceService $provinceService,
                                DishService $dishService, MenuService $menuService, FeatureService $featureService, GeoLocationController $geoLocationController)
    {
        $this->subsidiaryService = $subsidiaryService;
        $this->locationService = $locationService;
        $this->provinceService = $provinceService;
        $this->dishService = $dishService;
        $this->menuService = $menuService;
        $this->featureService = $featureService;
        $this->geolocationController = $geoLocationController;
    }

    public function index(Request $request){

            $locations = collect();
            $provinces = $this->provinceService->findAll()->pluck('name','id')->prepend('provincia',0);
            $subsidiaries = collect();
            $rel_features_company = collect();
            $rel_features_dishes = collect();
            $features_companies  = collect();
            $features_dishes  = collect();
            $companies = collect();
            $dishes = collect();

        $request->location = ($request->location !== '0') ? $request->location : null ;

        $request->province = ($request->province !== '0') ? $request->province : null ;

        if($request->province !== null && $request->location === null)
        {
            $locations = $this->locationService->findAllByProvince($request->province);
           if($locations !== null)
           {
               foreach ($locations as $location) {
                   foreach ($location->subsidiaries()->get() as $subsidiary) {
                       $subsidiaries[] = $subsidiary;
                   }
               }

               $locations = $locations->pluck('name','id')->prepend('location',0);

           }

        }
        else
        {
            if($request->location !== null && $request->province !== null)
            {
                $result= $this->subsidiaryService->findAllByLocation($request->location);

                if($result !== null)
                {
                    $subsidiaries = $result;
                }

                $locations = $this->locationService->findAllByProvince($request->province)->pluck('name','id')->prepend('provincia',0);
            }
        }

        //todo do query instead of foreach

        $location = null;

        if($request->zip !== null)
        {
            $location = $this->locationService->findByZip($request->zip);
        }

        if($location !== null)
        {
            foreach ($location->subsidiaries()->get() as $subsidiary) {
                $subsidiaries[] = $subsidiary;
            }
        }

        foreach ($subsidiaries->unique('company_id') as $subsidiary)
        {
            $companies[] = $subsidiary->company()->get()[0];
            $dishes[] = $subsidiary->company()->get()[0]->dishes()->get()->all();
        }

        $provisional = $dishes;

        $dishes = collect();

        foreach ($provisional as $arr)
        {
            foreach ($arr as $dish)
            {
                $dishes[] = $dish;
                if(count($dish->features()->get()))
                {
                    $rel_features_dishes[] = $dish->features()->get();
                    foreach($rel_features_dishes as $rel_features_dish)
                        $features_dishes[] = $rel_features_dish[0]->feature()->get()[0];
                }

            }
        }

        foreach ($companies as $company)
        {
                $rel_features_company[] = $company->features()->get();
        }

        $rel_features_company = $rel_features_company->unique('feature_id')->all();

        if(count($rel_features_company))
            $rel_features_company = $rel_features_company[0];

        foreach ($rel_features_company as $feature_company)
        {
            $features_companies[] = $feature_company->feature()->get()[0];
        }


        if(!count($locations))
        {
            $locations = [0 => 'No hay localidades'];
        }

        if(!$request->province)
        {
            $locations = [0 => 'localidad'];
        }

        $size = count($dishes);
        $paginate = 200;
        $dishes = $dishes->paginate($paginate);

        return view('mimenudeldiaweb/index')->with([

            'subsidiaries' => $subsidiaries,
            'features_companies' => $features_companies,
            'dishes' => $dishes,
            'features_dishes' => $features_dishes,
            'locations' => $locations,
            'provinces' => $provinces,
            'size' => $size,
            'paginate' => $paginate
        ]);

    }

    public function giveMeCompanies(int $locationsId){

        $subsidiaries = $this->subsidiaryService->findAllByLocation($locationsId);

        $companies = array();

        foreach ($subsidiaries->company()->get() as $company)
        {
            $companies [] = $company;
        }

    }

    public function search(Request $zip){


            //todo no me sale unico (features y )

            $dishes = $this->dishService->giveMeDishesByZip($zip['param']);
            $menus = $this->menuService->giveMeMenusByZip($zip['param']);
            $features = $this->dishService->giveMeFeaturesByZip($zip['param']);

            $dishes = ($dishes !== null ) ? $dishes->pluck('name', 'id') : collect();
            $menus = ($menus !== null ) ? $menus->pluck('name', 'id') : collect();
            $features = ($features !== null ) ? $features->pluck('name', 'id') : collect();

            debug($dishes,$menus,$features);

            if($dishes->count() == 0)
            {

                $subsidiaries = $this->geolocationController->findNearestSubsidiaries($zip['pos']['lat'], $zip['pos']['lng'])->sortBy('distance')->take(10)->values();

                foreach ($subsidiaries as $subsidiary)
                {
                    $z = $subsidiary->location()->get()->first()->zip;

                    $zipDishes = collect($this->dishService->giveMeDishesByZip($z));
                    $zipMenus = collect($this->menuService->giveMeMenusByZip($z));
                    $zipFeatures = collect($this->dishService->giveMeFeaturesByZip($z));

                    if ($zipDishes !== null) {
                        $dishes = $dishes->union($zipDishes->pluck('name', 'id'));
                    }
                    if($zipMenus !== null)
                    {
                        $menus = $menus->union($zipMenus->pluck('name', 'id'));
                    }
                    if($zipFeatures !== null)
                    {
                        $features = $features->union($zipFeatures->pluck('name', 'id'));
                    }
                }

            }

           foreach ($dishes_arr as $d)
           {
               if(is_array($d))
               {
                   foreach ($d as $i)
                   {
                       $dish = $this->dishService->find($i->id);

                       if($dish !== null )
                       {
                           $dishes[] = $dish;
                       }
                   }
               }
               else
               {
                   $dish = $this->dishService->find($d->id);

                   if($dish !== null )
                   {
                       $dishes[] = $dish;
                   }
               }
           }

            foreach ($menus_arr as $m)
            {

                if(is_array($m))
                {
                    foreach ($m as $i)
                    {
                        if($m !== null)
                        {
                            $menu = $this->menuService->find($i->id);

                            if($menu !== null)
                            {
                                $menus[] = $menu;
                            }
                        }
                    }
                }
                else
                {
                    if($m !== null)
                    {
                        $menu = $this->menuService->find($m->id);

                        if($menu !== null)
                        {
                            $menus[] = $menu;
                        }
                    }
                }

            }

            foreach ($features_arr as $f)
            {
                if(is_array($f))
                {
                    foreach ($f as $i)
                    {
                        if($f !== null)
                        {
                            $feature = $this->featureService->find($i->id);
                            $dishes_features[] = $feature;
                        }
                    }
                }
                else
                {
                    if($f !== null)
                    {
                        $feature = $this->featureService->find($f->id);
                        $dishes_features[] = $feature;
                    }
                }


            }

            $dishes_features = (!count($dishes_features)) ? null : $dishes_features->unique('');

            $li = "";
            foreach ($dishes as $key => $value)
            {

                $dish = $this->dishService->find($key);

                $features_dish = "";
                $f = $dish->features()->get();
                $f = $dish->features()->get();

                if(count($f))
                {
                    $features_dish = " feature-".implode($f->pluck('feature_id')->all(), " feature-");
                }

                $photo = $dish->photo()->get();
                $company = $dish->company()->get();
                $url = (count($photo)) ? $photo[0]->url : asset('images/no-image.svg') ;
                $company_id = (count($company)) ? $company[0]->id : null ;
                $company_name = (count($company)) ? $company[0]->name : null ;
                $price = $dish->price;

                $li .= '<li class="item thumb'.$features_dish.'">'.
                                '<figure class="member">'.
                                    '<div class="member-image">'.
                                        '<div class="text-overlay">'.
                                            '<div class="info">'.
                                                '<h4><a href=""></a></h4>'.
                                                '<p>Sin carta de productos</p>'.
                                                '<p>No envía a domicilio</p>'.
                                            '</div><!-- /.info -->'.
                                        '</div><!-- /.text-overlay -->'.
                                        //'<img src="'.$url.'">'.
                                    '</div>'.
                                    '<figcaption class="bordered no-top-border">'.
                                        '<div class="info">'.
                                            '<h4><a href="'.route('website.restaurante',$company_id).'"></a>'.
                                            '</h4>'.
                                            '<p class="red">'.$company_name.'</p>'.
                                        '</div><!-- /.info -->'.
                                    '</figcaption>'.
                                    '<h4 class="pvp red">'.
                                        '<p><span class="tint-bg">'.$price.'&euro;</span>'.
                                        '</p>'.
                                    '</h4>'.
                                    '<a href="restaurante.html" >'.
                                       '<i class="icon-heart-1 favourite blue">'.
                                        '</i>'.
                                    '</a>'.
                               '</figure>'.
                            '</li>';
            }

            debug($features);

            return[ 'li'=> $li, 'features_dishes'=> $features, 'menus'=>$menus ];

    }

    public static function routes(){

        Route::get(  '/inde',     'WebSite\indexController@index'               )->name('website.index');
        Route::post( '/inde',     'WebSite\indexController@indexFilter'         )->name('website.index');
        Route::get(  '/search',   'WebSite\indexController@search'              )->name('website.search');

    }

}
