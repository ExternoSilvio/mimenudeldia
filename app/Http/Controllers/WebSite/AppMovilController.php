<?php

namespace App\Http\Controllers\WebSite;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Route;

class AppMovilController extends Controller
{

    public function appMovil(){

        return view('mimenudeldiaweb/appmovil');

    }

    public static function routes(){

        Route::get(   '/appMovil','WebSite\AppMovilController@appMovil'   )->name('website.appmovil');

    }
}
