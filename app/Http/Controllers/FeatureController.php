<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\FeatureService;
use Validator;use Route;


class FeatureController extends Controller
{
    //

    private $featureService;

    /**
     * FeatureController constructor.
     * @param $featureService
     */
    public function __construct(FeatureService $featureService)
    {
        $this->featureService = $featureService;
    }

    public function findAll() {

        $features = $this->featureService->findAll();

        if($features !=  null)
        {
            return response()->json($features,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function find(int $featureId) {

        $feature = $this->featureService->find($featureId);

        if($feature != null)
        {
            return response()->json($feature,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function create(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:feature|max:50',
            'feature_id' => '',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        } else {

            $feature = $this->featureService->create($request);

            if($feature != null)
            {
                return response()->json($feature,201);
            }
            else
            {
                return response()->json('Element not found',404);
            }

        }
    }


    public function delete(int $featureId) {

        $feature = $this->featureService->delete($featureId);

        if($feature != null)
        {
            return response()->json(null,204);
        }
       else
       {
           return response()->json('Element not found',404);
       }
    }

    public function update(int $featureId, Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:feature|max:50',
            'feature_id'=>''
        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->errors()], 400);
        }
        else
        {
            $feature = $this->featureService->update($featureId, $request);
            if($feature != null)
            {
                return response()->json($feature,200);
            }
            else
            {
                response()->json('Element not found',404);
            }
        }

    }

    static public function routes(){


        Route::get(     '/features','FeatureController@findAll'               )->middleware('auth:api');
        Route::post(    '/features','FeatureController@create'                )->middleware('auth:api');
        Route::get(     '/features/{feature_id}','FeatureController@find'     )->middleware('auth:api');
        Route::put(     '/features/{feature_id}','FeatureController@update'   )->middleware('auth:api');
        Route::delete(  '/features/{feature_id}','FeatureController@delete'   )->middleware('auth:api');


    }
}
