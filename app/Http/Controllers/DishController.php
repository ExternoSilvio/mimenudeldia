<?php

namespace App\Http\Controllers;

use App\Services\ScoreService;
use Illuminate\Http\Request;
use Validator;
use App\Services\DishService;
use App\Services\OfferService;
use App\Services\Score;
use Route;

class DishController extends Controller
{
    //

    private $dishService;
    private $offerService;
    private $scoreService;

    /**
     * DishController constructor.
     * @param $dishService
     * @param $offerService
     * @param $scoreService
     *
     */

    public function __construct(DishService $dishService, OfferService $offerService, ScoreService $scoreService)
    {
        $this->dishService = $dishService;
        $this->offerService = $offerService;
        $this->scoreService = $scoreService;
    }

    public function findAll(int $companyId) {

        $dishes = $this->dishService->findAllByCompany($companyId);

        if($dishes != null)
        {
            return response()->json($dishes,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }

    }

    public function find(int $companyId,int $dishId) {

        $dish = $this->dishService->findByCompany($companyId,$dishId);

        if($dish != null)
        {
            return response()->json($dish,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function findFeaturesByCompany(int $company, int $dish) {

        $features = $this->dishService->findFeaturesByCompany($company, $dish);

        if($features != null)
        {
            return response()->json($features,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }

    }

    public function createByCompany(int $companyId,Request $request) {

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:dish|max:50',
            'price' => 'required',
            'categoryId' => 'required',
            'photoId' =>''
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        } else {

            $dish = $this->dishService->createByCompany($companyId,$request);

            if($dish != null)
            {
                return response()->json($dish,201);
            }
             else
             {
                 return response()->json('Element not found',404);
             }
        }
    }

    public function setFeatureByCompany(int $companyId,int $dishId, int $featureId ){


        $feature = $this->dishService->setFeatureByCompany($companyId,$dishId,$featureId);

        if($feature != null)
        {
            return response()->json($feature,201);
        }
        else
        {
            return response()->json('Element not found',404);
        }

    }

    public function setFeaturesByCompany(int $companyId, int $dishId, Request $request) {
        $features = ($request->has('features')) ? explode(',', $request->input('features')) : [];

        $features_arr = array();

        foreach($features as $feature) {
            $featureId = intval(trim($feature));
            $nulls = $this->dishService->setFeatureByCompany($companyId,$dishId,$featureId);

            if($nulls == null)
            {
                $features_arr[] = $nulls;
            }
        }

        if(!$features_arr)
        {
            return response()->json(null,204);
        }
        else
        {
            return response()->json('Some elements not found',404);
        }
    }

    public function deleteByCompany(int $companyId, int $dishId) {

        $dish = $this->dishService->deleteByCompany($companyId,$dishId);

        if($dish != null)
        {
            return response()->json(null,204);
        }
        else
        {
            response()->json('Element not found',404);
        }
    }

    public function deleteFeature(int $companyId, int $dishId, int $featureId) {

        $feature = $this->dishService->deleteFeature($companyId,$dishId,$featureId);

        if($feature != null)
        {
            return response()->json(null,204);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function deleteFeatures(int $companyId, int $dishId, Request $request) {

        $features = ($request->has('features')) ? explode(',', $request->input('features')) : [];

        $features_arr = array();

        foreach ($features as $feature)
        {
            $featureId = intval(trim($feature));

            $nulls = $this->dishService->deleteFeature($companyId,$dishId, $featureId);

            if(!$nulls)
            {
                $features_arr[] = $nulls;
            }
        }

        if(!$features_arr)
        {
            return response()->json(null,204);
        }
        else
        {
            return response()->json('Some elements not found',404);
        }
    }

    public function update(int $companyId, int $dishId, Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:category|max:50',
            'price' => 'required',
            'categoryId' => 'required',
            'photoId' =>''

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        else
        {
            $dish = $this->dishService->updateByCompany( $companyId,  $dishId, $request);

            if($dish != null)
            {
                return response()->json($dish,200);
            }
            else
            {
                return response()->json('Element not found',404);
            }
        }

    }

    public function findDish(int $companyId, int $dishId, int $offerId){

        $dish = $this->offerService->findDish($companyId,$dishId,$offerId);

        if($dish != null)
        {
            return response()->json($dish,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }
}

    public function setDish(int $companyId, int $dishId, Request $request){

        $validator = Validator::make($request->all(), [
            'start_at' => 'required|date_format:Y-m-d H:i:s',
            'finish_at' => 'required|date_format:Y-m-d H:i:s',
            'price' => 'required|numeric',
            'discount' =>'required|numeric'

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        else
        {
            $dish = $this->offerService->setDishByCompany( $companyId,  $dishId, $request);

            if($dish != null)
            {
                return response()->json($dish,200);
            }
            else
            {
                return response()->json('Element not found',404);
            }
        }

    }

    public function findDishes(int $companyId, int $dishId){

        $dishes = $this->offerService->findDishes($companyId,$dishId);

        if($dishes != null)
        {
            return response()->json($dishes,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function putDish(int $companyId, int $dishId, int $offerId, Request $request){

        $validator = Validator::make($request->all(), [
            'start_at' => 'required|date_format:Y-m-d H:i:s',
            'finish_at' => 'required|date_format:Y-m-d H:i:s',
            'price' => 'required|numeric',
            'discount' =>'required|numeric'

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        else
        {
            $dish = $this->offerService->putDish( $companyId,  $dishId, $offerId, $request);

            if($dish != null)
            {
                return response()->json($dish,200);
            }
            else
            {
                return response()->json('Element not found',404);
            }
        }

    }

    public function deleteDish(int $companyId, int $dishId, int $offerId){

        $dish = $this->offerService->deleteDish($companyId,$dishId,$offerId);

        if($dish != null)
        {
            return response()->json(null,204);
        }
        else
        {
            return response()->json('Element not found',404);
        }

    }

    public function findDishesScore(int $companyId, int $dishId){

        $dishes = $this->scoreService->findDishesScore($companyId,$dishId);

        if($dishes != null)
        {
            return response()->json($dishes,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function setDishScore(int $companyId, int $dishId, Request $request){

        $validator = Validator::make($request->all(), [
            'score' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        else
        {
            $score = $this->scoreService->setDishScore( $companyId,  $dishId, $request);

            if($score != null )
            {
                return response()->json($score,200);
            }
            else
            {
                return response()->json('Element not found',404);
            }
        }


    }

    public function findDishScore(int $companyId, int $dishId, int $scoreId){

        $score = $this->findDishScore($companyId,$dishId,$scoreId);

        if($score != null)
        {
            return response()->json($score,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }

    }

    public function putDishScore(int $companyId, int $dishId, int $scoreId, Request $request){

        $validator = Validator::make($request->all(), [
            'score' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        else
        {
            $score = $this->scoreService->putDishScore( $companyId,  $dishId, $scoreId, $request);

            if($score != null)
            {
                return response()->json($score,200);
            }
            else
            {
                return response()->json('Element not found',404);
            }
        }


    }

    public function deleteDishScore(int $companyId, int $dishId, int $scoreId){

        $dish = $this->scoreService->deleteDishScore($companyId,$dishId,$scoreId);

        if($dish != null)
        {
            return response()->json(null,204);
        }
        else
        {
            return response()->json('Element not found',404);
        }

    }

    static public function routes(){

        Route::get(    '/companies/{company_id}/dishes','DishController@findAll'              )->middleware('auth:api');
        Route::post(   '/companies/{company_id}/dishes','DishController@create'               )->middleware('auth:api');
        Route::get(    '/companies/{company_id}/dishes/{dishes_id}','DishController@find'     )->middleware('auth:api');
        Route::put(    '/companies/{company_id}/dishes/{dishes_id}','DishController@update'   )->middleware('auth:api');
        Route::delete( '/companies/{company_id}/dishes/{dishes_id}','DishController@delete'   )->middleware('auth:api');

    }

    static public function routesFeatures(){

        Route::get(    '/companies/{company_id}/dishes/{dishes_id}/features','DishController@findFeatures'              )->middleware('auth:api');
        Route::post(   '/companies/{company_id}/dishes/{dishes_id}/features/{feature_id}','DishController@setFeature'   )->middleware('auth:api');
        Route::post(   '/companies/{company_id}/dishes/{dishes_id}/features','DishController@setFeatures'               )->middleware('auth:api');
        Route::delete( '/companies/{company_id}/dishes/{dishes_id}/features/{feature_id}','DishController@deleteFeature')->middleware('auth:api');
        Route::delete( '/companies/{company_id}/dishes/{dishes_id}/features','DishController@deleteFeatures'            )->middleware('auth:api');

    }

    static public function routesScore(){

        Route::get(   '/companies/{company_id}/dishes/{dish_id}/scores','DishController@findDishesScore');
        Route::post(  '/companies/{company_id}/dishes/{dish_id}/scores','DishController@setDishScore');
        Route::get(   '/companies/{company_id}/dishes/{dish_id}/scores{score_id}','DishController@findDishScore');
        Route::put(   '/companies/{company_id}/dishes/{dish_id}/scores/{score_id}','DishController@putDishScore');
        Route::delete('/companies/{company_id}/dishes/{dish_id}/scores/{score_id}','DishController@deleteDishScore');

    }

    static public function routesOffer(){

        Route::get(    '/companies/{company_id}/dishes/{dish_id}/offers/{offer_id}','DishController@findDish');
        Route::post(   '/companies/{company_id}/dishes/{dish_id}/offers','DishController@setDish');
        Route::get(    '/companies/{company_id}/dishes/{dish_id}/offers','DishController@findDishes');
        Route::put(    '/companies/{company_id}/dishes/{dish_id}/offers/{offer_id}','DishController@putDish');
        Route::delete( '/companies/{company_id}/dishes/{dish_id}/offers/{offer_id}','DishController@deleteDish');

    }
}
