<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use Validator;
use App\Services\SubsidiaryService;
use Route;

class SubsidiaryController extends Controller
{
    //

    private $subsidiaryService;

    /**
     * SubsidiaryController constructor.
     * @param $subsidiaryService
     */

    public function __construct(SubsidiaryService $subsidiaryService)
    {
        $this->subsidiaryService = $subsidiaryService;
    }

    public function findAllByCompany(int $companyId) {

        $subsidiaries = $this->subsidiaryService->findAllByCompany($companyId);

        if($subsidiaries != null)
        {
            return response()->json($subsidiaries,200);
        }
        else
        {
            return response()->json('Elements not found');
        }

    }

    public function findByCompany(int $companyId,int $subsidiaryId) {

        $subsidiary = $this->subsidiaryService->findByCompany($companyId,$subsidiaryId);

        if($subsidiaryId != null)
        {
            return response()->json($subsidiary,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public  function findOpinions(int $company_id, int $subsidiary_id){

        $opinions = $this->subsidiaryService->findOpinions($company_id,$subsidiary_id);

        if($opinions != null)
        {
            return response()->json($opinions, 200);
        }
       else
       {
           return response()->json('Element not found',404);
       }
    }

    public  function findOpinion(int $companyId, int $subsidiary_id, int $opinionId){

        $opinion = $this->subsidiaryService->findOpinion($opinionId);

        if($opinion != null)
        {
            return response()->json($opinion,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function create(int $companyId,Request $request) {
        $validator = Validator::make($request->all(), [
            'address' => 'required|unique:subsidiary|max:50',
            'coordinates' => 'required|unique:subsidiary|max:255',
            'phone' => 'required',
            'locationId' => 'required',
            'email' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        } else {

            $subsidiary = $this->subsidiaryService->createByCompany($companyId,$request);

            if($subsidiary != null)
            {
                return response()->json($subsidiary,201);
            }
            else
            {
                return response()->json('Element not found',404);
            }
        }
    }

    public function setOpinion(int $company_id, int $subsidiary_id, Request $request){

        $validator = Validator::make($request->all(), [
            'comment' => 'required',

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }else{

            $opinion = $this->subsidiaryService->setOpinion($company_id, $subsidiary_id, $request);

            if($opinion != null)
            {
                return response()->json($opinion);
            }
            else
            {
                response()->json('Element not found',404);
            }
        }
    }

    public function delete(int $companyId, int $subsidiaryId) {

        $subsidiary = $this->subsidiaryService->deleteByCompany($companyId,$subsidiaryId);

        if($subsidiary != null)
        {
            return response()->json(null,204);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function deleteOpinion(int $companyId, int $subsidiaryId, int $opinionId){

        $opinion = $this->subsidiaryService->deleteOpinion($opinionId);

        if($opinion !=null )
        {
            return response()->json(null,204);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function update(int $companyId, int $subsidiaryId, Request $request) {
        $validator = Validator::make($request->all(), [
            'address' => 'required|unique:subsidiary|max:50',
            'coordinates' => 'required|unique:subsidiary|max:255',
            'phone' => 'required',
            'locationId' => 'required',
            'email' => 'required'

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        else
        {
            $subsidiary = $this->subsidiaryService->updateByCompany( $companyId,  $subsidiaryId, $request);

            if($subsidiary != null)
            {
                return response()->json($subsidiary,200);
            }
            else
            {
                return response()->json('Element not found',404);
            }
        }


    }
    public function updateOpinion(int $companyId, int $subsidiaryId,int $opinionId, Request $request) {
        $validator = Validator::make($request->all(), [
            'comment' => 'required'

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        else
        {
            $opinion = $this->subsidiaryService->updateOpinion( $companyId,  $subsidiaryId, $opinionId,$request);

            if($opinion != null)
            {
                return response()->json($opinion,200);
            }
            else
            {
                return response()->json('Element not found',404);
            }
        }

    }


    public static function routes(){

        Route::get(     '/companies/{company_id}/subsidiaries','SubsidiaryController@findAll'                   )->middleware('auth:api');
        Route::post(    '/companies/{company_id}/subsidiaries','SubsidiaryController@create'                    )->middleware('auth:api');
        Route::get(     '/companies/{company_id}/subsidiaries/{subsidiary_id}','SubsidiaryController@find'      )->middleware('auth:api');
        Route::put(     '/companies/{company_id}/subsidiaries/{subsidiary_id}','SubsidiaryController@update'    )->middleware('auth:api');
        Route::delete(  '/companies/{company_id}/subsidiaries/{subsidiary_id}','SubsidiaryController@delete'    )->middleware('auth:api');

    }

    public static function routesOpinions(){

        Route::get('/companies/{company_id}/subsidiaries/{subsidiary_id}/opinions','SubsidiaryController@findOpinions')->middleware('auth:api');
        Route::get('/companies/{company_id}/subsidiaries/{subsidiary_id}/opinions/{opinion_id}','SubsidiaryController@findOpinion')->middleware('auth:api');
        Route::post('/companies/{company_id}/subsidiaries/{subsidiary_id}/opinions','SubsidiaryController@setOpinion')->middleware('auth:api');
        Route::put('/companies/{company_id}/subsidiaries/{subsidiary_id}/opinions/{opinion_id}','SubsidiaryController@updateOpinion')->middleware('auth:api');
        Route::delete('/companies/{company_id}/subsidiaries/{subsidiary_id}/opinions/{opinion_id}','SubsidiaryController@deleteOpinion')->middleware('auth:api');


    }
}
