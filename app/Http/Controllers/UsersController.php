<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Route;
use Validator;

use App\Services\UsersService;

class UsersController extends Controller
{
    //
    private $usersService;

    /**
     * UsersController constructor.
     * @param $usersService
     */
    public function __construct(UsersService $usersService)
    {
        $this->usersService = $usersService;
    }

    public function findAll() {

        $users = $this->usersService->findAll();

        if($users != null)
        {
            return response()->json($users,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function find(int $users_id) {

        $user = $this->usersService->find($users_id);

        if($user != null)
        {
            return response()->json($user,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function delete(int $users_id) {

        $user = $this->usersService->delete($users_id);

        if($user != null)
        {
            return response()->json(null,204);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public  function findFavorites(){

        $favorites = $this->usersService->findFavorites();

        if($favorites != null)
        {
            return response()->json($favorites,200);
        }
        else
        {
            return response()->json('Elements not found',404);
        }

    }


    public function findFavorite(int $companyId){

        $favorite = $this->usersService->findFavorite($companyId);

        if($favorite != null)
        {
            return response()->json($favorite,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function findScoreMenus(int $userId){

        return response()->json($this->usersService->findScoreMenus($userId));

    }

    public function findScoreDishes(int $userId, int $companyId){

        return response()->json($this->usersService->findScoreDishes($userId, $companyId));

    }

    public function findScoreMenu(int $userId, int $menuId){

        return response()->json($this->usersService->findScoreMenu($userId,$menuId));

    }

    public function setFavorite(int $companyId){

        $favorite = $this->usersService->setFavorite($companyId);

        if($favorite != null)
        {
            return response()->json($favorite,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }


    }

    public function setScoreMenu(int $userId, int $menuId, Request $request){

        $validator = Validator::make($request->all(), [
            'score' => 'required'

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        } else {
            return response()->json($this->usersService->setScoreMenu($userId,$menuId,$request));
        }



    }

    public function setScoreDish(int $userId, int $companyId, int $dishId, Request $request){

        $validator = Validator::make($request->all(), [
            'score' => 'required'

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        } else {
            return response()->json($this->usersService->setScoreDish($userId, $companyId, $dishId, $request));
        }



    }


    public function updateScoreMenu(int $usersId, int $menuId, Request $request){

        return response()->json($this->usersService->updateScoreMenu($usersId, $menuId, $request));

    }

    public function deleteFavorite(int $companyId){

        $favorite = $this->usersService->deleteFavorite($companyId);

        if($favorite != null)
        {
            return response()->json($favorite,201);
        }
        else
        {
            return response()->json('Element not found',404);
        }

    }

    public function deleteScoreMenu(int $usersId, int $menuId){

        return response()->json($this->usersService->deleteScoreMenu($usersId,$menuId));

    }

    /*public function create(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:users|max:50',
            'email' => 'required|unique:users|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        } else {
            return response()->json($this->usersService->create($request),201);
        }
    }


    public function update(int $users_id, Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:users|max:50',
            'email' => 'required|unique:users',
            'password' =>'required|unique:users'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        return response()->json($this->usersService->update($users_id, $request),200);
    }*/

    public static function routes(){

        //rutas de user

        Route::get(     '/users','UsersController@findAll');
        //Route::post('/users','UsersController@create');
        Route::get(     '/users/{users_id}','UsersController@find');
        //Route::put('/users/{users_id}','UsersController@update');
        Route::delete(  '/users/{users_id}','UsersController@delete');

        //rutas de favoritos

        Route::get(     '/favorites','UsersController@findFavorites'                )->middleware('auth:api');
        Route::post(    '/favorites/{company_id}','UsersController@setFavorite'     )->middleware('auth:api');
        Route::get(     '/favorites/{company_id}','UsersController@findFavorite'    )->middleware('auth:api');
        Route::delete(  '/favorites','UsersController@deleteFavorites'              )->middleware('auth:api');
        Route::delete(  '/favorites/{company_id}','UsersController@deleteFavorite'  )->middleware('auth:api');

        //rutas de score/menu

        //Estas rutas en principios son redundantes

        /*Route::get('/users/{user_id}/menus','UsersController@findScoreMenus')->middleware('auth:api');
        Route::post('/users/{user_id}/menus/{menu_id}','UsersController@setScoreMenu')->middleware('auth:api');
        Route::get('/users/{user_id}/menus/{menu_id}','UsersController@findScoreMenu')->middleware('auth:api');
        Route::put('/users/{user_id}/menus/{menu_id}','UsersController@updateScoreMenu')->middleware('auth:api');
        Route::delete('/users/{user_id}/menus/{menu_id}','UsersController@deleteScoreMenu')->middleware('auth:api');

        //rutas de score/dish

        Route::get('/users/{user_id}/companies/{company_id}/dishes','UsersController@findScoreDishes')->middleware('auth:api');
        Route::post('/users/{user_id}/companies/{company_id}/dishes/{dish_id}','UsersController@setScoreDish')->middleware('auth:api');
        Route::get('/users/{user_id}/companies/{company_id}/dishes/{dish_id}','UsersController@findScoreDishes')->middleware('auth:api');
        Route::put('/users/{user_id}/companies/{company_id}/dishes/{dish_id}','UsersController@updateScoreDishes')->middleware('auth:api');
        Route::delete('/users/{user_id}/companies/{company_id}/dishes/{dish_id}','UsersController@deleteScoreDishes')->middleware('auth:api');*/


    }
}
