<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

use App\Services\CategoryService;
use Route;

class CategoryController extends Controller
{
    //
    private $categoryService;

    /**
     * CategoryController constructor.
     * @param $categoryService
     */
    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function findAll() {

        $categories =  $this->categoryService->findAll();

        if($categories != null)
        {
            return response()->json($categories,200);
        }
        else
        {
            return response()->json('Elements not found',404);
        }


    }

    public function find(int $categoryId) {

        $category = $this->categoryService->find($categoryId);

        if($category != null)
        {
            return response()->json($category,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function create(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:category|max:50',
            'category_id'

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        } else {

            $category = $this->categoryService->create($request);

            if($category != null)
            {
                return response()->json($category,201);
            }
            else
            {
                return response()->json('Element not found', 404);
            }

        }
    }

    public function delete(int $categoryId) {

        $category = $this->categoryService->delete($categoryId);

        if($category != null)
        {
            return response()->json(null,204);
        }
        else
        {
            return response()->json('Element not found',404);
        }

    }

    public function update(int $categoryId, Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:category|max:50',

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        else
        {
            $category = $this->categoryService->update($categoryId, $request);

            if($category != null)
            {
                return response()->json($category,200);
            }
            else
            {
                return response()->json('Element not found',404);
            }
        }



    }

    static public function routes(){

        Route::get(    '/categories','CategoryController@findAll'              )->middleware('auth:api');
        Route::post(   '/categories','CategoryController@create'               )->middleware('auth:api');
        Route::get(    '/categories/{category_id}','CategoryController@find'   )->middleware('auth:api');
        Route::put(    '/categories/{category_id}','CategoryController@update' )->middleware('auth:api');
        Route::delete( '/categories/{category_id}','CategoryController@delete' )->middleware('auth:api');

    }
}
