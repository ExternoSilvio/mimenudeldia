<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Services\CompanyService;
use App\Services\ScoreService;use Route;

class CompanyController extends Controller
{
    //

    private $companyService;
    private $scoreService;

    /**
     * CompanyController constructor.
     * @param $companyService
     * @param $scoreService
     */
    public function __construct(CompanyService $companyService, ScoreService $scoreService)
    {
        $this->companyService = $companyService;
        $this->scoreService = $scoreService;
    }

    public function findAll() {

        $companies = $this->companyService->findAll();

        if($companies != null)
        {
            return response()->json($companies,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function find(int $company_id) {

        $company = $this->companyService->find($company_id);

        if($company != null)
        {
            return response()->json($company,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function findFeatures(int $company) {

        $features = $this->companyService->findFeatures($company);

        if($features != null)
        {
            return response()->json($features,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function create(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:province|max:50',
            'identification' => 'required|unique:company|max:50',
            'web' => 'required|max:100',
            'information' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        } else {

            $company = $this->companyService->create($request);

            if($company != null)
            {
                return response()->json($company,201);
            }
            else
            {
                return response()->json('Element not found',404);
            }
        }
    }

    public function setFeature(int $companyId, int $featureId ){


        $feature = $this->companyService->setFeature($companyId,$featureId);

        if($feature != null)
        {
            return response()->json($feature,201);
        }
        else
        {
            return response()->json('Element not found',404);
        }

    }

    public function setFeatures(int $companyId, Request $request) {

        $features = ($request->has('features')) ? explode(',', $request->input('features')) : [];
        $feature_arr = array();

        foreach($features as $feature) {
            $featureId = intval(trim($feature));
            $nulls = $this->companyService->setFeature($companyId,$featureId);

            if($nulls == null)
            {
                $feature_arr[] = $nulls;
            }
        }

        if(empty($feature_arr))
        {
            return response()->json(null,204);
        }
        else
        {
            return response()->json('Some elements not found',404);
        }

    }

    public function delete(int $company_id) {

        $company = $this->companyService->delete($company_id);

        if($company != null)
        {
            return response()->json(null,204);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function deleteFeature(int $companyId, int $featureId) {

        $feature = $this->companyService->deleteFeature($companyId, $featureId);

        if($feature != null)
        {
            return response()->json(null,204);
        }
        else
        {
            response()->json('Element not found',404);
        }
    }

    public function deleteFeatures(int $companyId, Request $request) {

        $features = ($request->has('features')) ? explode(',', $request->input('features')) : [];

        $feature_arr = array();

        foreach ($features as $feature)
        {
            $featureId = intval(trim($feature));

            $nulls = $this->companyService->deleteFeature($companyId, $featureId);

            if($nulls == null)
            {
                $feature_arr[] = $nulls;
            }
        }

        if(empty($feature_arr))
        {
            return response()->json(null,204);
        }
        else
        {
            return response()->json('Some elements not found',404);
        }
    }

    public function update(int $company_id, Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:province|max:50',
            'identification' => 'required|unique:company|max:50',
            'web' => 'required|max:100',
            'information' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        else
        {
            $company = $this->companyService->update($company_id, $request);

            if($company != null)
            {
                return response()->json($company,200);
            }
            else
            {
                return response()->json('Element not found',404);
            }
        }


    }

    public function findCompaniesScore(int $companyId){

        $score = $this->scoreService->findCompaniesScore($companyId);

        if($score != null)
        {
            return response()->json($score,200);
        }
        else
        {
            return response()->json('Element not found');
        }


    }

    public function setCompanyScore(int $companyId, Request $request){

        $validator = Validator::make($request->all(), [
            'score' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        else
        {
            $score = $this->scoreService->setCompanyScore( $companyId, $request);

            if($score != null)
            {
                return response()->json($score,200);
            }
            else
            {
                return response()->json('Element not found',404);
            }
        }



    }

    public function findCompanyScore(int $companyId, int $scoreId){

        $score = $this->findCompanyScore($companyId, $scoreId);

        if($score != null )
        {
            return response()->json($score,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }

    }

    public function putCompanyScore(int $companyId, int $scoreId, Request $request){

        $validator = Validator::make($request->all(), [
            'score' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        else
        {
            $company = $this->scoreService->putCompanyScore( $companyId, $scoreId, $request);

            if($company != null)
            {
                return response()->json($company,200);
            }
            else
            {
                return response()->json('Element not found',404);
            }
        }

    }

    public function deleteCompanyScore(int $companyId, int $scoreId){

        $score = $this->scoreService->deleteCompanyScore($companyId, $scoreId);

        if($score != null)
        {
            return response()->json(null,204);
        }
        else
        {
            return response()->json('Element not found',404);
        }

    }


    static public function routes(){

        Route::get(     '/companies','CompanyController@findAll'             )->middleware('auth:api');
        Route::post(    '/companies','CompanyController@create'              )->middleware('auth:api');
        Route::get(     '/companies/{company_id}','CompanyController@find'   )->middleware('auth:api');
        Route::put(     '/companies/{company_id}','CompanyController@update' )->middleware('auth:api');
        Route::delete(  '/companies/{company_id}','CompanyController@delete' )->middleware('auth:api');


    }

    static public function routesFeatures(){

        Route::get(     '/companies/{company_id}/features','CompanyController@findFeatures'                   )->middleware('auth:api');
        Route::post(    '/companies/{company_id}/features/{feature_id}','CompanyController@setFeature'        )->middleware('auth:api');
        Route::post(    '/companies/{company_id}/features','CompanyController@setFeatures'                    )->middleware('auth:api');
        Route::delete(  '/companies/{company_id}/features/{feature_id}','CompanyController@deleteFeature'     )->middleware('auth:api');
        Route::delete(  '/companies/{company_id}/features','CompanyController@deleteFeatures'                 )->middleware('auth:api');

    }

    static public function routesScore(){

        Route::get(    '/companies/{company_id}/scores','CompanyController@findCompaniesScore');
        Route::post(   '/companies/{company_id}/scores','CompanyController@setCompanyScore');
        Route::get(    '/companies/{company_id}/scores{score_id}','CompanyController@findCompanyScore');
        Route::put(    '/companies/{company_id}/scores/{score_id}','CompanyController@putCompanyScore');
        Route::delete( '/companies/{company_id}/scores/{score_id}','CompanyController@deleteCompanyScore');

    }

}
