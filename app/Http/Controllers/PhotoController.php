<?php

namespace App\Http\Controllers;

use App\Photo;
use App\Rel_company_photo;
use App\Services\PhotoService;
use Illuminate\Http\Request;
use Validator;
use Route;

class PhotoController extends Controller
{
    //
    /**
     * PhotoController constructor.
     * @param $photoService
     */

    private $photoService;


    public function __construct(PhotoService $photoService)
    {
        $this->photoService = $photoService;
    }

    public function findAll(){

        $photos = $this->photoService->findAll();

        if($photos != null)
        {
            return response()->json($photos,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function find(int $photoId){

        $photo = $this->photoService->find($photoId);

        if($photo != null)
        {
            return response()->json($photo,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

//    public function create(Request $request){
//
//        $validator = Validator::make($request->all(), [
//            'url' => 'required|unique:photo|max:500',
//
//        ]);
//
//        if ($validator->fails()) {
//            return response()->json(['error' => $validator->errors()], 400);
//        } else {
//            return response()->json($this->photoService->create($request),200);
//        }
//
//
//    }

    public function delete($photoId){

        $photo = $this->photoService->delete($photoId);

        if($photo != null)
        {
            return response()->json(null,204);
        }
        else
        {
            return response()->json('Element not found',404);
        }

    }


    //en principio no hace falta
    /*public function update($photoId,Request $request){

        $validator = Validator::make($request->all(), [
            'photo' => 'required',

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->errors()], 400);
        }else{
            return response()->json($this->photoService->update($photoId, $request),200);
        }

    }*/

    public function findCompanyLogo(int $companyId, int $logoId){

        $logo = $this->photoService->findCompanyLogo($companyId, $logoId);

        if($logo != null)
        {
            return response()->json($logo,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function setCompanyLogo(int $companyId, Request $request){

        $logo = $this->photoService->setCompanyLogo($companyId, $request);

        if($logo != null)
        {
            return response()->json($logo,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function deleteCompanyLogo(int $companyId, int $logoId){

        $logo = $this->photoService->deleteCompanyLogo($companyId, $logoId);

        if($logo != null)
        {
            return response()->json(null,204);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function updateCompanyLogo(int $companyId, int $logoId, Request $request){


        $logo = $this->photoService->updateCompanyLogo($companyId, $logoId,$request);


        if($logo != null)
        {
            return response()->json($logo,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public  function findDishPhoto(int $companyId, int $dishId, int $photoId){


        $photo = $this->photoService->findDishPhotoByCompany($companyId,$dishId,$photoId);

        if($photo != null)
        {
            return response()->json($photo,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }

    }

    public function setDishPhoto(int $companyId, int $dishId,Request $request){

        $photo = $this->photoService->setDishPhoto($companyId,$dishId,$request);

        if($photo != null)
        {
            return response()->json($photo,201);
        }
        else
        {
            return response()->json('Element not found',404);
        }

    }

    public function deleteDishPhoto(int $companyId, int $dishId, int $photoId){

        $photo = $this->photoService->deleteDishPhotoByCompany($companyId,$dishId,$photoId);

        if($photo != null)
        {
            return response()->json($photo, 204);
        }
        else
        {
            return response()->json('Element not found',404);
        }

    }

    public function updateDishPhoto(int $companyId, int $dishId, int $photoId, Request $request){

        $photo = $this->photoService->updateDishPhotoByCompany($companyId,$dishId,$photoId,$request);

        if($photo != null)
        {
            return response()->json($photo,200);
        }
        else
        {
            return response('Element not found',404);
        }
    }

    public  function findMenuPhoto(int $companyId, int $menuId, int $photoId){

        $photo = $this->photoService->findMenuPhotoByCompany($companyId,$menuId,$photoId);

        if($photo != null)
        {
            return response()->json($photo,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function setMenuPhoto(int $companyId, int $menuId, Request $request){

        $photo = $this->photoService->setMenuPhoto($companyId,$menuId,$request);

        if($photo != null)
        {
            return response()->json($photo,201);
        }
        else
        {
            return response()->json('Element not found', 404);
        }
    }

    public function deleteMenuPhoto(int $companyId, int $menuId, int $photoId){

        $photo = $this->photoService->deleteMenuPhoto($companyId,$menuId,$photoId);

        if($photo != null)
        {
            return response()->json($photo,204);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function updateMenuPhoto(int $companyId, int $menuId, int $photoId, Request $request){

        $menuPhoto = $this->photoService->updateMenuPhotoByCompany($companyId,$menuId,$photoId,$request);

        if($menuPhoto != null)
        {
            return response()->json($menuPhoto,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }
    }

    public function findCompanyPhotos(int $companyId){

        $photo = $this->photoService->findCompanyPhotos($companyId);

        if($photo != null)
        {
            return response()->json($photo,200);
        }
        else
        {
            return response()->json('Element not found',404);
        }

    }
    public function setCompanyPhoto(int $companyId, int $photoId){

        $photo = $this->photoService->setCompanyPhoto($companyId,$photoId);

        if($photo != null)
        {
            return response()->json($photo,201);
        }
        else
        {
            return response()->json('Element not found',404);
        }


    }

    public function setCompanyPhotos(int $companyId, Request $request){

        $photos = ($request->has('photos')) ? explode(',', $request->input('photos')) : [];

        $photos_arr = array();

        foreach($photos as $photo) {
            $photoId = intval(trim($photo));
            $nulls = $this->photoService->setCompanyPhoto($companyId,$photoId);

            if($nulls == null)
            {
                $photos_arr[] = $nulls;
            }
        }

        if(empty($photos_arr))
        {
            return response()->json(null,204);
        }
        else
        {
            return response()->json('Some elements not found',404);
        }

    }

    public function deleteCompanyPhoto(int $companyId, int $photoId){

        $photo = $this->photoService->deleteCompanyPhoto($companyId, $photoId);


        if($photo != null)
        {
            return response()->json(null,204);
        }
        else
        {
            return response()->json('Element not found',404);
        }

    }

    public function deleteCompanyPhotos(int $companyId, Request $request){

        $photos = ($request->has('photos')) ? explode(',', $request->input('photos')) : [];

        $photos_arr = array();

        foreach($photos as $photo) {
            $photoId = intval(trim($photo));
            $nulls = $this->photoService->deleteCompanyPhoto($companyId,$photoId);

            if($nulls == null)
            {
                $photos_arr[] = $nulls;
            }
        }

        if(empty($photos_arr))
        {
            return response()->json(null,204);
        }
        else
        {
            return response()->json('Some elements not found');
        }
    }


    public static function routes(){

        Route::get(   '/photos','PhotoController@findAll'           )->middleware('auth:api');
        Route::post(  '/photos','PhotoController@create'            )->middleware('auth:api');
        Route::get(   '/photos/{photo_id}','PhotoController@find'   )->middleware('auth:api');
        Route::put(   '/photos/{photo_id}','PhotoController@update' )->middleware('auth:api');
        Route::delete('/photos/{photo_id}','PhotoController@delete' )->middleware('auth:api');

    }

    public static function routesPhotoCompanyLogo(){

        Route::get(    '/companies/{company_id}/logos/{logo_id}','PhotoController@findCompanyLogo'    )->middleware('auth:api');
        Route::post(   '/companies/{company_id}/logos','PhotoController@setCompanyLogo'               )->middleware('auth:api');
        Route::put(    '/companies/{company_id}/logos/{logo_id}','PhotoController@updateCompanyLogo'  )->middleware('auth:api');
        Route::delete( '/companies/{company_id}/logos/{logo_id}','PhotoController@deleteCompanyLogo'  )->middleware('auth:api');
    }

    public static function routesDishPhoto(){

        Route::get(    '/companies/{company_id}/dishes/{dish_id}/photos/{photo_id}','PhotoController@findDishPhoto'    )->middleware('auth:api');
        Route::post(   '/companies/{company_id}/dishes/{dish_id}/photos','PhotoController@setDishPhoto'                )->middleware('auth:api');
        Route::put(    '/companies/{company_id}/dishes/{dish_id}/photos/{photo_id}','PhotoController@updateDishPhoto'  )->middleware('auth:api');
        Route::delete( '/companies/{company_id}/dishes/{dish_id}/photos/{photo_id}','PhotoController@deleteDishPhoto'  )->middleware('auth:api');


    }

    public static function routesMenuPhoto(){

        Route::get(    '/companies/{company_id}/menus/{menu_id}/photos/{photo_id}','PhotoController@findMenuPhoto'    )->middleware('auth:api');
        Route::post(   '/companies/{company_id}/menus/{menu_id}/photos','PhotoController@setMenuPhoto'                )->middleware('auth:api');
        Route::put(    '/companies/{company_id}/menus/{menu_id}/photos/{photo_id}','PhotoController@updateMenuPhoto'  )->middleware('auth:api');
        Route::delete( '/companies/{company_id}/menus/{menu_id}/photos/{photo_id}','PhotoController@deleteMenuPhoto'  )->middleware('auth:api');


    }

    public static function routesRelCompanyPhotos(){

        Route::get(     '/companies/{company_id}/photos','PhotoController@findCompanyPhotos'             )->middleware('auth:api');
        Route::post(    '/companies/{company_id}/photos/{photo_id}','PhotoController@setCompanyPhoto'    )->middleware('auth:api');
        Route::post(    '/companies/{company_id}/photos','PhotoController@setCompanyPhotos'              )->middleware('auth:api');
        Route::delete(  '/companies/{company_id}/photos/{photo_id}','PhotoController@deleteCompanyPhoto' )->middleware('auth:api');
        Route::delete(  '/companies/{company_id}/photos','PhotoController@deleteCompanyPhotos'           )->middleware('auth:api');

    }

}
