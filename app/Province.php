<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    //
    protected $table = 'province';

    public function locations(){

        return $this->hasMany('App\Location');

    }
}
