<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rel_company_photo extends Model
{
    //
    protected $table = 'rel_company_photo';

    public function company(){

        return $this->belongsTo('App\Company');

    }

    public function photo(){

        return $this->belongsTo('App\Photo');

    }
}
