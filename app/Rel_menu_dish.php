<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rel_menu_dish extends Model
{
    //
    protected $table = 'rel_menu_dish';

    public function dish(){

        $this->belongsTo('App\Dish');

    }

    public function menu(){

        $this->belongsTo('App\Menu');

    }
}
